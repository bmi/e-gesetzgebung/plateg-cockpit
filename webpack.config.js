// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpackConfig = require('@leanup/stack-react/webpack.config');
const webpackConfPlusHTML = (env, argv) => {
  const conf = webpackConfig(env, argv);
  const path = require('path');
  conf.entry = {
    main: './src/main.ts',
    light: './src/main_light.ts',
  };
  conf.module.rules.push({
    test: /\.html$/i,
    loader: 'html-loader',
  });

  conf.module.rules.push({
    test: /\.(jpe?g|png|gif|svgz|license)$/i,
    use: [
      {
        loader: 'file-loader',
        options: {
          esModule: false,
        },
      },
    ],
  });

  conf.resolve.alias['react'] = path.resolve('./node_modules/react');
  conf.resolve.alias['react-dom'] = path.resolve('./node_modules/react-dom');
  conf.resolve.alias['react-router'] = path.resolve('./node_modules/react-router');
  conf.resolve.alias['react-router-dom'] = path.resolve('./node_modules/react-router-dom');

  conf.module.rules.push({
    test: /\.packed\.js$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          esModule: false,
        },
      },
    ],
  });

  conf.module.rules.unshift({
    test: /runtime.js$/,
    loader: 'string-replace-loader',
    options: {
      search: /[^\w]Function\(/,
      replace: '// Function(',
    },
  });

  conf.output = {
    chunkFilename: (pathData) => {
      return (pathData.chunk.name === 'main' || pathData.chunk.name === 'light') ? '[name].js' : '[contenthash].js';
    },
  };

  conf.plugins.forEach((p, i) => {
    if (p instanceof MiniCssExtractPlugin) {
      conf.plugins.splice(i, 1, new MiniCssExtractPlugin({
        chunkFilename: (pathData) => {
          return (pathData.chunk.name === 'main' || pathData.chunk.name === 'light') ? '[name].css' : '[contenthash].css';
        }
      }));
    }
  });

  return conf;
};

module.exports = webpackConfPlusHTML;
