// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

/* eslint-disable no-unused-vars */
function checkBrowserSupport() {
  let isUnsupportedBrowser = !/chrome|safari|firefox|edge/.test(navigator.userAgent.toLowerCase());
  let alertMessage =
    'Der von Ihnen genutzte Browser wird nicht unterstützt. Bitte nutzen Sie einen der folgenden Browser: \n o Google Chrome \n o Mozilla Firefox \n o Microsoft Edge \n o Apple Safari.';
  if (isUnsupportedBrowser) alert(alertMessage);
}
