// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Configuration, SucheControllerApi, UserControllerApi } from '@plateg/rest-api';
import { configureRestApi, GlobalDI } from '@plateg/theme';

function loadApp(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('userController', () => new UserControllerApi(configRestCalls));
  GlobalDI.getOrRegister('sucheController', () => new SucheControllerApi(configRestCalls));
  import('./react.main.light');
}

configureRestApi(loadApp);
