// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const de_contacts = {
  eGesetzSupport: {
    email: 'support.egesetzgebung@bmi.bund.de',
  },
  footer: {
    cockpit: {
      faq: {
        login: {
          pkpSupportEmail: 'pkp-support@email.com',
          pkpFachadministration: 'https://fachadministration.web.url.de',
        },
      },
    },
    barrierefreiheit: {
      element2b: {
        pkpBarrierefreiheit: 'https://barreierefreiheit.web.url.de',
      },
    },
  },
};
