// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { br } from '../de-br';
import { bt } from '../de-bt';
import { de_contacts } from './contacts';

export const de = {
  cockpit: {
    bt: { ...bt },
    br: { ...br },
    langCode: 'de-DE',
    generalErrorMsg:
      'Es ist ein unerwarteter Fehler aufgetreten. Bitte wenden Sie sich an den Support. Wir entschuldigen uns für entstandene Umstände. {{fehlercode}}',
    eGesetzSupportEmail: de_contacts.eGesetzSupport.email,
    login: {
      title: 'Für diese Anwendung müssen Sie sich anmelden.',
      paragraph1: 'Diese Anwendung steht nur angemeldeten Nutzenden zur Verfügung.',
      loginButton: 'Anmelden',
      registerButton: 'Registrieren',
      contactButton: 'Kontakt und Feedback',
      faqButton: 'Häufige Fragen',
      ueberDieseSeiteButton: 'Über diese Seite',
      ueberDasProjektButton: 'Über das Projekt',
      releaseNotes: 'Release Notes',
      searchEgesetz: 'E-Gesetzgebung durchsuchen',
      newsletterButton: 'Newsletter Abo',
      barrierefreiheitBtn: 'Zugänglichkeit',
      leichteSpracheBtn: 'Leichte Sprache',
      registerInfo:
        'Haben Sie bereits ein Konto beim Planungs- und Kabinettmanagement-Programm (PKP)? Dieses Konto nutzen Sie ebenfalls, um sich bei der E-Gesetzgebung anzumelden.',
      logoutMessage: 'Sie wurden erfolgreich abgemeldet',
    },
    stellvertretung: {
      changeSuccess: 'Sie haben das Konto erfolgreich gewechselt. Nun arbeiten Sie als Stellvertretung {{name}}.',
      changeError: 'Beim Kontowechsel von ihrem Account zum Stellvertreter ist leider ein Fehler aufgetreten.',
      resetSuccess:
        'Sie haben die Arbeit als Stellvertretung für {{name}} beendet. Sie arbeiten nun wieder in Ihrem eigenen Namen.',
      resetError: 'Beim Kontowechsel vom Stellvertreter zu ihrem Account ist leider ein Fehler aufgetreten.',
      editorTitle: 'Editor für Rechtsetzungstexte',
      editorContent:
        'Die Bearbeitung von Regelungsentwürfen als Stellvertretung wird zu einem späteren Zeitpunkt implementiert.',
    },
    contactModal: {
      text: 'Sie haben Anregungen, Verbesserungsvorschläge oder einen Fehler in einer der Anwendungen der <nobr>E-Gesetzgebung</nobr> entdeckt? Wir freuen uns über Ihr Feedback.',
      mail: 'Nachricht an die E-Gesetzgebung',
    },
    newsletterModal: {
      title: 'Anmeldung zum Newsletter',
      text: 'Wenn Sie auch per E-Mail regelmäßig Informationen über das Projekt erhalten möchten, melden Sie sich formlos zu unseren Newsletter an:',
      mail: 'Anmeldung zum Newsletter',
      subject: 'Anmeldung zum Newsletter',
    },
    searchModal: {
      tabLabels: {
        all: 'Alle',
        workingAids: 'Arbeitshilfen',
        faq: 'Häufige Fragen',
        docsAndData: 'Dokumente und Dateien',
      },
      title: 'Suche in der E-Gesetzgebung',
      text1: 'Der Funktionsumfang der Suche wird stetig erweitert.',
      text2: 'Derzeit können Sie die Anwendungen "Arbeitshilfenbibliothek" und Häufige Fragen durchsuchen.',
      searchInput: {
        inputPlaceholder: 'Suchbegriff eingeben',
        resetSearchWord: 'Suchbegriff zurücksetzen',
        submitBtn: 'Suchen',
      },
      searchResults: 'Suchergebnisse',
      results: '{{resNumber}} Ergebnisse',
      selectSort: {
        label: 'Sortieren nach:',
        emptyValue: 'Bitte auswählen',
        alphabetical: 'alphabetisch',
        rank: 'Rang',
      },
      upToDateNote: {
        title: 'Hinweis: Aktualität der Einträge',
        text: 'Eine Arbeitshilfe ist nicht auf aktuellem Stand? Gerne können Sie uns {{emailLink}} informieren.',
      },
      selectBibFilter: {
        label: 'Filtern nach: Anwendung',
        bib: 'BIB',
        evir: 'eVIR',
      },
      noResult: {
        text1: `Es wurden leider keine Ergebnisse für „{{searchWord}}“ gefunden.`,
        text2: 'Vielleicht liefert ein anderer Suchbegriff oder eine andere Schreibweise das passende Ergebnis?',
      },
      searchResultItem: {
        referencesFound: 'weitere Fundstellen in diesem Element',
      },
    },
    userMenu: {
      manageAccount: 'Konto verwalten',
      messages: 'Benachrichtigungen',
      bitteMitzeichnung: 'Bitte um Mitzeichnung',
      inqueries: 'Anfragen',
      settings: 'Einstellungen',
      abstimmungen: 'Abstimmung',
      logout: 'Abmelden',
    },
    staticPages: {
      barrierefreiheit: 'Zugänglichkeit',
      erklaerung: 'Erklärung zur Barrierefreiheit',
      handbuch: 'Nutzungshinweise',
      ueberDasProjekt: 'Über das Projekt',
      legaldoc: 'LegalDocML.de als Standard für einen digitalen Rechtsetzungsprozess',
      vorstellung: 'Über diese Seite',
      impressum: 'Impressum',
      leichteSprache: 'Leichte Sprache',
      navigationshilfe: 'Navigationshilfe',
      werkzeuge: 'Werkzeuge der E-Gesetzgebung',
      datenschutz: 'Datenschutz',
      faq: 'Häufige Fragen',
      vsnfd: 'Hinweis zu VS-NfD auf der Plattform E-Gesetzgebung',
      releaseNotes: 'Release Notes',
      previousReleases: 'Release Notes der vergangenen Releases',
      bundesregierungsplattform: 'Bundesregierungsplattform',
      editor: 'Editor',
      bundestagsplattform: 'Bundestagsplattform',
      bundesratsplattform: 'Bundesratsplattform',
      gesetzgebungsportal: 'Gesetzgebungsportal',
      versionen: 'Versionen',
    },
    settings: {
      breadcrumb: {
        settings: 'Einstellungen',
      },
      nav: {
        allgemein: 'Allgemein',
        startseite: 'Startseite',
        allgemeinRechtevergabe: 'Stellvertretungen',
        allgemeinBarrierefreiheit: 'Zugänglichkeit',
        hra: 'Abstimmung',
        zeit: 'Zeitplanungen',
        egfa: 'Gesetzesfolgenabschätzungen',
        emailList: 'E-Mail-Listen',
      },
      settingsItem: { active: { true: 'Ein', false: 'Aus' } },
      drawerWidth: {
        label: 'Spaltenbreite wählen',
        small: 'Schmal',
        normal: 'Normal',
        big: 'Breit',
      },
      pages: {
        saveBtn: 'Speichern',
        successMsg: 'Einstellungen wurden erfolgreich gespeichert',
        welcome: {
          title: 'Einstellungen',
          content:
            'Sie können anwendungsspezifische Einstellungen vornehmen, indem Sie die Anwendungen auf der linken Seite auswählen.',
        },
        hra: {
          title: 'Abstimmung',
          subtitle: 'E-Mail-Benachrichtigungen',
          meineAbstimmungen: {
            title: 'Meine Abstimmungen',
            content: 'Wir benachrichtigen Sie per E-Mail, wenn eine Person an Ihrer Abstimmung teilgenommen hat.',
          },
          bitteUmMitzeichnung: {
            title: 'Bitte um Mitzeichnung',
            content:
              'Wir benachrichtigen Sie per E-Mail rund um „Bitte um Mitzeichnung“, wie zum Beispiel, wenn Sie zu einer Abstimmung hinzugefügt wurden, aus einer Abstimmung entfernt wurden, Änderungen an dieser Abstimmung vorgenommen wurden oder Ihre Anfrage zur Teilnahme an einer Abstimmung bestätigt/abgelehnt wurde.',
          },
          fristverlaengerungen: {
            title: 'Fristverlängerungen',
            content:
              'Wir benachrichtigen Sie per E-Mail, wenn Sie eine neue Anfrage zur Fristverlängerung erhalten haben.',
          },
          teilnahmeanfragen: {
            title: 'Teilnahmeanfragen',
            content:
              'Wir benachrichtigen Sie per E-Mail, wenn Sie eine neue Anfrage zur Teilnahme an einer Abstimmung erhalten haben.',
          },
        },
        zeit: {
          title: 'Einstellungen zu Zeitplanungen und Zeitplanungsvorlagen',
          subtitle: 'E-Mail-Benachrichtigungen',
          zeitplanungsvorlagen: {
            title: 'Zeitplanungsvorlagen',
            content:
              'Wir benachrichtigen Sie per E-Mail, wenn eine Person Ihnen die Leserechte für eine Zeitplanungsvorlage erteilt hat.',
          },
          zeitplanungen: {
            title: 'Zeitplanungen',
            content:
              'Wir benachrichtigen Sie per E-Mail, wenn eine Person Ihnen die Leserechte/Schreibrechte für eine Zeitplanung erteilt hat.',
          },
        },
        barrierefreiheit: {
          title: 'Zugänglichkeit',
          tabellenAnmerkung: {
            title: 'Geöffnete Anmerkung in Tabellen',
            content: 'In den Tabellen der E-Gesetzgebung sind die Anmerkungen zu allen Einträgen immer aufgeklappt.',
          },
          navigationspunkte: {
            title: 'Mehrzeilige Navigationspunkte',
            content:
              'Navigationspunkte brechen in weitere Zeilen um und werden nicht durch Auslassungspunkte abgekürzt.',
          },
          vereinfachteMehrauswahl: {
            title: 'Vereinfachte Mehrauswahl',
            content:
              'Umfangreiche Auswahlmöglichkeiten werden vollständig angezeigt. Separates Scrollen innerhalb der Inhalte entfällt.',
          },
          infoDrawer: {
            title: 'Breite der rechten Informationsleiste',
            content: 'Die Informationsleiste auf der rechten Seite hat beim Öffnen eine feste Breite.',
          },
        },
        startseite: {
          title: 'Startseite',
          schnellzugriff: {
            title: 'Schnellzugriff auf aktive Regelungsvorhaben',
            content:
              'Im oberen Bereich der Startseite werden alle aktiven Regelungsvorhaben angezeigt, die Sie erstellt haben.',
          },
        },
        egfa: {
          title: 'Einstellungen zu Gesetzesfolgenabschätzungen',
          subtitle: 'E-Mail-Benachrichtigungen',
          schnellzugriff: {
            title: 'Lese- und/oder Schreibrechte für Modul einer GFA',
            content: `Wir benachrichtigen Sie per E-Mail, wenn Sie Leserechte und/oder
              Schreibrechte für ein Modul einer Gesetzesfolgenabschätzung 
              erhalten haben.`,
          },
        },
        rechtevergabe: {
          title: 'Meine Stellvertretungen',
          contentStellvertreterView:
            ' Als Stellvertretung sind Sie nicht berechtigt, Anpassungen an den Einstellungen zur Stellvertretung vornehmen.',
          content: `Hier legen Sie Personen als Ihre Stellvertretung fest. <br> 
          Eine Stellvertretung erhält in Ihrem Namen Zugriff auf Ihre E-Gesetzgebungs-Oberfläche. Sie besitzt die gleichen Berechtigungen wie Sie und kann Ihre Vorgänge in Ihrem Namen bearbeiten. <br />
          Änderungen an der Federführung eines Regelungsvorhabens sowie an den Einstellungen zur Stellvertretung sind nicht möglich.`,
          title2: 'Stellvertretungen hinzufügen',
          title2Drawer: `Hier können Sie Personen als Ihre Stellvertretung auswählen. Sie können die Stellvertretung nur innerhalb Ihrer eigenen Organisation vergeben. <br /> Die hinzugefügten Nutzerinnen und Nutzer erhalten eine Benachrichtigung über die Ernennung zur Stellvertretung. Es können mehrere Personen als Stellvertretung benannt werden.`,
          searchLabel: 'Suche nach Stellvertreterinnen und Stellvertretern',
          searchLabelDrawer:
            'Hier können Sie eine Person suchen und als Stellvertreterin oder Stellvertreter hinzufügen. Sie können mehrere Personen als Ihre Stellvertretung auswählen. Sie können hier auch E-Mail-Listen verwenden, die Sie über die Einstellungen verwalten können. <br /> Sie können nur Personen aus Ihrer eigenen Organisation auswählen, die für die E-Gesetzgebung registriert sind.',
          addressLabel: 'Stellvertretungen',
          addressLabelDrawer:
            'Hier sehen Sie eine Übersicht der von Ihnen ausgewählten Personen für Ihre Stellvertretung.',
          btnSubmit: 'Stellvertretungen jetzt speichern',
          errorParticipant: 'Sie dürfen sich nicht selbst als Stellvertreterin oder Stellvertreter hinzufügen.',
          successMsg:
            'Sie haben die Stellvertretungen erfolgreich geändert. Hinzugefügte Stellvertretungen wurden per E-Mail benachrichtigt.',
          deleteSuccessMsg: 'Sie haben die Stellvertretungen erfolgreich geändert.',
        },
        emailList: {
          title: 'E-Mail-Listen',
          content: `Hier erstellen und verwalten Sie E-Mail-Listen. Diese Listen können Sie dann etwa bei der Abstimmungseinladung oder bei der Vergabe von Berechtigungen verwenden. Diese Listen sind nur für Sie sichtbar.`,
          title2: 'Meine E-Mail-Listen',
          btnAddNewList: 'Neue Liste hinzufügen',
          btnEdit: 'Bearbeiten',
          btnDelete: 'Löschen',
          btnActualize: 'Liste aktualisieren',
          labelActualize: 'Liste aktualisieren:',
          linkActualize: 'Nicht mehr registrierte Einträge entfernen',
          labelOutdated: 'Liste veraltet, bitte aktualisieren',
          labelRessort: 'Ressort und Referat',
          labelBeteiligt: 'Beteiligt',
          userNotActive: '(nicht mehr registriert)',
          modalNewList: {
            mainTitle: 'Neue E-Mail-Liste anlegen',
            mainTitleEdit: 'E-Mail-Liste bearbeiten',
            btnCancel: 'Abbrechen',
            btnNext: 'Nächster Schritt',
            btnPrev: 'Vorheriger Schritt',
            page1: {
              title: 'Schritt 1 von 2: Bezeichnung der E-Mail-Liste festlegen',
              requiredInfo: 'Pflichtfelder sind mit einem * gekennzeichnet.',
              label: 'Titel',
              info: {
                title: 'Erklärung zu: „Titel“',
                text: 'Wählen Sie hier einen aussagekräftigen Titel für Ihre E-Mail-Liste. Dieser Titel muss eindeutig sein.',
              },
              requiredMsg: 'Bitte geben Sie einen Titel an.',
              errorUniqueTitle: 'Der Name der E-Mail-Liste muss eindeutig sein.',
              textLength: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten',
            },
            page2: {
              title: 'Schritt 2 von 2: E-Mail-Liste verwalten',
              requiredInfo: 'Pflichtfelder sind mit einem * gekennzeichnet.',
              searchLabel: 'Suche nach Personen',
              searchHint: '(z. B. Name, Ressort, Verteiler, E-Mail-Adresse, E-Mail-Liste)',
              searchDrawerLabel: 'Erklärung zu: „Suche nach Personen“',
              searchDrawer: `Hier können Sie die Adressdatenbank durchsuchen. Tippen Sie dazu einfach den Namen oder das Ressort ein. Personen, die nicht im IAM registriert sind, können aus Gründen des Datenschutzes nicht zu einer E-Mail-Liste hinzugefügt werden.
                Sie können mehrere Suchbegriffe eintragen. Trennen Sie diese dazu einfach durch Leerzeichen. Die Begriffe werden dann für die Suche kombiniert. Die Treffer enthalten alle angegebenen Suchbegriffe. Es wird eine Und-Verknüpfung der Begriffe verwendet.`,
              addressLabel: 'Hinzugefügte Personen*',
              noSelectedPerson: 'Es wurde noch keine Person hinzugefügt.',
              addressDrawerLabel: 'Erklärung zu: „Hinzugefügte Personen“',
              addressDrawer:
                'Sofern Sie bereits eine oder mehrere Personen ausgewählt haben, sehen Sie hier die ausgewählten Personen.',
              error: 'Die E-Mail-Liste enthält keine Personen und kann nicht gespeichert werden.',
              errorParticipant: 'Sie dürfen sich nicht selbst als Adressatin oder Adressat hinzufügen.',
              btnNext: 'Eingaben prüfen',
            },
            page3: {
              title: 'Eingaben prüfen',
              listTitleLabel: 'Bezeichnung der E-Mail-Liste:',
              usersLabel: 'Hinzugefügte Personen:',
              btnNext: 'E-Mail-Liste jetzt anlegen',
              btnNextEdit: 'E-Mail-Liste jetzt aktualisieren',
            },
            successMsg: 'Sie haben die E-Mail-Liste <b>{{listName}}</b> erfolgreich angelegt.',
            successMsgEdit: 'Die E-Mail-Liste <b>{{listName}}</b> wurde erfolgreich aktualisiert.',
          },
          deleteModal: {
            title: 'Möchten Sie die E-Mail-Liste löschen?',
            content: 'Achtung, wenn Sie die E-Mail-Liste löschen, ist sie nicht mehr verfügbar.',
            okText: 'Löschen',
            cancelText: 'Abbrechen',
            successMsg: 'Die E-Mail-Liste <b>{{listName}}</b> wurde erfolgreich gelöscht.',
          },
        },
      },
    },
    header: {
      ungeleseneNeuigkeiten: 'Ungelesene Neuigkeiten {{ungeleseneNeuigkeiten}}',
      logo: {
        title: 'zur Startseite Plattform E-Gesetzgebung',
        alt: 'zur Startseite Plattform E-Gesetzgebung',
      },
      title: 'E-Gesetzgebung',
      lightTitle: 'im Internet',
      titleAddition: ' | Bundesregierung',
    },
    errorpage: {
      title: 'Seite nicht gefunden (Fehler 404)',
      description: 'Leider kann die von Ihnen gewählte Seite nicht angezeigt werden.',
    },
    footerLight: {
      datenschutz: {
        title: 'Datenschutzerklärung',
        subtitle1: '1. Grundlagen',
        paragraf11:
          'Für das Bundesministerium des Innern und für Heimat (BMI) hat ein verantwortungsbewusster Umgang mit personenbezogenen Daten hohe Priorität. Wir möchten, dass Sie wissen, wann welche Daten durch das BMI erhoben und verwendet werden.',
        paragraf12:
          'Wir haben technische und organisatorische Maßnahmen getroffen, die sicherstellen, dass die Vorschriften über den Datenschutz sowohl von uns als auch von unseren externen Dienstleistern beachtet werden.',
        paragraf13:
          'Die Verarbeitung personenbezogener Daten im BMI erfolgt in Übereinstimmung mit der Europäischen Datenschutz-Grundverordnung (EU-DSGVO) und dem Bundesdatenschutzgesetz (BDSG).',
        subsubtitle11: '1.1	Verantwortlichkeit und Datenschutzbeauftragte',
        paragraf111: 'Verantwortlich für die Verarbeitung von personenbezogenen Daten ist das',
        block111:
          '<strong>Bundesministerium des Innern und für Heimat<br>Alt-Moabit 140<br>10557 Berlin<br>Telefon: +49-(0)30 18 681-0<br>Fax: +49-(0)30 18 681-12926<br>E-Mail: <a id="datenschutz-light-email-link1" href="mailto:poststelle@bmi.bund.de">poststelle@bmi.bund.de</a><br>DE-Mail: <a id="datenschutz-light-email-link3" href="mailto:poststelle@bmi-bund.de-mail.de">poststelle@bmi-bund.de-mail.de</a></strong>',
        paragraf112:
          'Bei konkreten Fragen zum Schutz Ihrer Daten wenden Sie sich bitte an die Datenschutzbeauftragte im BMI:',
        block112:
          '<strong>Beauftragte für den Datenschutz im BMI<br>Bundesministerium des Innern und für Heimat<br>Alt-Moabit 140<br>10557 Berlin<br>Telefon: +49-(0)30 18 681-0<br>E-Mail: <a id="datenschutz-light-email-link3" href="mailto:bds@bmi.bund.de">bds@bmi.bund.de</a></strong>',
        subsubtitle12: '1.2	Personenbezogene Daten',
        paragraf121:
          'Personenbezogene Daten sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person beziehen. Als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt – insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung – identifiziert werden kann.',
        subsubtitle13: '1.3 Minderjährigenschutz',
        paragraf131:
          'Personen unter 16 Jahren sollten ohne Zustimmung der Eltern oder Erziehungsberechtigten keine personenbezogenen Daten an uns übermitteln. Wissentlich sammeln wir keine personenbezogenen Daten von Kindern und Jugendlichen und geben diese auch nicht an Dritte weiter.',
        subtitle2: '2.\tDatenverarbeitung im Zusammenhang mit dem Besuch dieser Internetseite',
        subsubtitle21: '2.1	Datenerfassung',
        paragraf21:
          'Beim Aufrufen unserer Webseite werden durch den auf Ihrem Endgerät zum Einsatz kommenden Browser automatisch Informationen an den Server der Webseite gesendet. Diese Informationen werden temporär in einem sogenannten Logfile gespeichert. Folgende Informationen werden dabei ohne Ihr Zutun erfasst und auf einem Server des Informationstechnikzentrums Bund (ITZBund) bis zur automatisierten Löschung gespeichert:',
        list21:
          '<ul><li>IP-Adresse des anfragenden Rechners,</li><li>Datum, Uhrzeit und Dauer des Zugriffs,</li><li>Name und URL der abgerufenen Datei,</li><li>Webseite, von der der Zugriff erfolgt (Referrer-URL),</li><li>Anzahl der Besuche,</li><li>verwendeter Browser und ggf. das Betriebssystem Ihres Rechners sowie der Name Ihres Access-Providers.</li></ul>',
        subsubtitle22: '2.2	Zweck der Verarbeitung',
        paragraf22: 'Die genannten Daten werden zu folgenden Zwecken verarbeitet:',
        list22:
          '<ul><li>Gewährleistung eines reibungslosen Verbindungsaufbaus der Webseite,</li><li>Gewährleistung einer komfortablen Nutzung unserer Webseite,</li><li>Auswertung der Systemsicherheit und -stabilität sowie</li><li>zu weiteren administrativen Zwecken.</li></ul>',
        paragraf23:
          'Auf der Webseite haben Sie die Möglichkeit, Eingabefelder in den Anwendungen der E-Gesetzgebung zu befüllen. Damit Sie diese Eingaben in Form eines PDF-Dokuments exportieren können und die Eingaben nicht sofort verloren sind, sobald Sie die Anwendung schließen, findet eine temporäre Verarbeitung dieser Eingaben statt. Die Speicherung dieser Eingaben findet nur lokal auf Ihrem Endgerät statt. Dabei werden diese Eingaben darüber hinaus nicht weiterverarbeitet und auch an niemanden übermittelt.',
        paragraf24:
          'Nur wenn Sie aktiv die Kontaktmöglichkeit der E-Gesetzgebung über die Webseite nutzen, werden Ihre dort gemachten Angaben erhoben und gespeichert, um Ihr Anliegen bearbeiten zu können.',
        subsubtitle23: '2.3	Einsatz von Cookies',
        paragraf25:
          'Beim Besuch der Webseite kommen aus technischen Gründen Session-Cookies zum Einsatz, diese enthalten keine personenbezogenen Daten. Der Einsatz erfolgt auf Grundlage von Artikel 6 Absatz 1 Buchstabe e und Absatz 3 Buchstabe b DS-GVO in Verbindung mit E-Government-Gesetz (EGovG) (vgl. auch § 25 Absatz 2 Nummer 2 Telekommunikation-Telemedien-Datenschutz-Gesetz [TTDSG]) im Rahmen der Öffentlichkeitsarbeit zur bedarfsorientierten Bereitstellung von Informationen zu den dem BMJ übertragenen Aufgaben.',
        paragraf26:
          'Session-Cookies sind kleine Informationseinheiten, die ein Anbieter im Arbeitsspeicher des Computers der Besucherin bzw. des Besuchers speichert. In einem Session-Cookie wird eine zufällig erzeugte eindeutige Identifikationsnummer abgelegt, eine sogenannte Session-ID. Die Session-ID basiert insbesondere nicht auf der IP-Adresse oder anderen zurückverfolgbaren Daten. Außerdem enthält ein Cookie die Angabe über seine Herkunft und die Speicherfrist. Diese Cookies können keine anderen Daten speichern. Session-Cookies verfallen nach Ablauf der Sitzung und werden automatisch gelöscht, wenn das Browserfenster geschlossen wird.',
        subtitle3: '3. Verarbeitung personenbezogener Daten im Rahmen der Kontaktaufnahme',
        paragraf31: 'Die Verarbeitung der personenbezogenen Daten geschieht in Abhängigkeit des Kontaktweges.',
        subsubtitle31: '3.1 Kontaktaufnahme per E-Mail oder per Brief',
        paragraf311:
          'Die Kontaktaufnahme mit der E-Gesetzgebung per E-Mail ist über folgende E-Mail-Adresse möglich: <a id="datenschutz-light-email-link6" href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>. Die an diese E-Mail-Adresse gesandten und in der zuständigen Organisationseinheit des BMI gespeicherten personenbezogenen Daten werden nach einem Jahr gelöscht.',
        paragraf312:
          'In den Organisationseinheiten werden die von Ihnen übermittelten Daten (z. B.: Name, Vorname, Anschrift), zumindest jedoch die E-Mail- bzw. die Postadresse sowie die in der E-Mail bzw. dem Brief enthaltenen Informationen (inklusive ggf. von Ihnen übermittelter personenbezogener Daten) zum Zwecke der Kontaktaufnahme und Bearbeitung Ihres Anliegens verarbeitet. Diese Verarbeitung der von Ihnen übermittelten personenbezogenen Daten ist zum Zweck der Bearbeitung Ihres Anliegens erforderlich.',
        subsubtitle32: '3.2 Kontaktaufnahme per Telefon',
        paragraf321:
          'Wenn Sie mit uns per Telefon Kontakt aufnehmen werden, soweit dies erforderlich ist, personenbezogenen Daten von Ihnen zur Bearbeitung Ihres Anliegens verarbeitet.',
        subtitle4: '4. Weitere Rechtsgrundlagen für die Verarbeitung personenbezogener Daten',
        paragraf41:
          'Das BMI verarbeitet bei der Wahrnehmung der ihm obliegenden im öffentlichen Interesse liegenden Aufgaben personenbezogene Daten. Rechtsgrundlage der Verarbeitung ist hier Art. 6 Abs. 1 lit. e der Datenschutz-Grundverordnung der EU (DSGVO) in Verbindung mit § 3 BDSG. Soweit eine Verarbeitung personenbezogener Daten im Einzelfall zur Erfüllung einer rechtlichen Verpflichtung erforderlich sein sollte, dient Art. 6 Abs. 1 lit. c DSGVO in Verbindung mit der entsprechenden Rechtsvorschrift, aus der sich die rechtliche Verpflichtung ergibt, als Rechtsgrundlage.',
        paragraf42:
          'Wir sind auf Grundlage von Artikel 6 Absatz 1 lit. e DSGVO in Verbindung mit § 5 des Gesetzes über das Bundesamt für Sicherheit in der Informationstechnik (BSI-Gesetz) zur Speicherung von Daten zum Schutz vor Angriffen auf die Internetinfrastruktur des BMI und der Kommunikationstechnik des Bundes über den Zeitpunkt Ihres Besuches hinaus verpflichtet. Daten, die beim Zugriff auf das Internetangebot des BMI protokolliert wurden, werden an Dritte nur übermittelt, soweit wir rechtlich dazu verpflichtet sind oder die Weitergabe im Falle von Angriffen auf die Kommunikationstechnik des Bundes zur Rechts- oder Strafverfolgung erforderlich ist. Weitergabe in anderen Fällen erfolgt nicht. Eine Zusammenführung dieser Daten mit anderen Datenquellen, zum Beispiel zum Anlegen von Nutzerprofilen, erfolgt durch das BMI nicht.',
        subtitle5: '5. Speicherdauer',
        paragraf51:
          'Weiterhin verarbeitet das BMI personenbezogene Daten entsprechend den gesetzlichen Vorschriften für die hier jeweils dargestellten Zwecke und nur so lange, wie eine persönliche Identifikation der betroffenen Person für den jeweiligen Zweck erforderlich ist. Anschließend erfolgt eine Löschung oder datenschutzgerechte Neutralisierung bzw. Anonymisierung. Personenbezogene Daten, die gemäß BSI-Gesetz verarbeitet werden, werden für eine Dauer von 90 Tagen gespeichert. Das BMI verarbeitet weiterhin Ihre personenbezogenen Daten gemäß den für die Aufbewahrung von Schriftgut geltenden Fristen der Registraturrichtlinie, die die Gemeinsame Geschäftsordnung der Bundesministerien (GGO) ergänzt.',
        subtitle6: '6. Ihre Rechte',
        paragraf61:
          'Sie haben gegenüber dem Verantwortlichen folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:',
        listTitle61: 'Recht auf Auskunft, Art. 15 DSGVO',
        listContent61:
          'Mit dem Recht auf Auskunft erhält die betroffene Person eine umfassende Einsicht in die ihn angehenden Daten und einige andere wichtige Kriterien, wie beispielsweise die Verarbeitungszwecke oder die Dauer der Speicherung. Es gelten die in § 34 BDSG geregelten Ausnahmen von diesem Recht.',
        listTitle62: 'Recht auf Berichtigung, Art. 16 DSGVO',
        listContent62:
          'Das Recht auf Berichtigung beinhaltet die Möglichkeit für die betroffene Person, unrichtige ihn angehende personenbezogene Daten korrigieren zu lassen.',
        listTitle63: 'Recht auf Löschung, Art. 17 DSGVO',
        listContent63:
          'Das Recht auf Löschung beinhaltet die Möglichkeit für die betroffene Person, Daten beim Verantwortlichen löschen zu lassen. Dies ist allerdings nur dann möglich, wenn die ihn angehenden personenbezogenen Daten nicht mehr notwendig sind, rechtswidrig verarbeitet werden oder eine diesbezügliche Einwilligung widerrufen wurde. Es gelten die in § 35 BDSG geregelten Ausnahmen von diesem Recht.',
        listTitle64: 'Recht auf Einschränkung der Verarbeitung, Art. 18 DSGVO',
        listContent64:
          'Das Recht auf Einschränkung der Verarbeitung beinhaltet die Möglichkeit für die betroffene Person, eine weitere Verarbeitung der ihr angehenden personenbezogenen Daten vorerst zu verhindern. Eine Einschränkung tritt vor allem in der Prüfungsphase anderer Rechtewahrnehmungen durch die betroffene Person ein.',
        listTitle65: 'Recht auf Datenübertragbarkeit, Art. 20 DSGVO',
        listContent65:
          'Das Recht auf Datenübertragbarkeit beinhaltet die Möglichkeit für die betroffene Person, die sie angehenden personenbezogenen Daten in einem gängigen, maschinenlesbaren Format vom Verantwortlichen zu erhalten, um sie ggf. an einen anderen Verantwortlichen weiterleiten zu lassen. Gemäß Art. 20 Absatz 3 Satz 2 DSGVO steht dieses Recht aber dann nicht zur Verfügung, wenn die Datenverarbeitung der Wahrnehmung öffentlicher Aufgaben dient.',
        listTitle66:
          'Recht auf Widerspruch gegen die Erhebung, Verarbeitung und beziehungsweise oder Nutzung, Art. 21 DSGVO',
        listContent66:
          'Das Recht auf Widerspruch beinhaltet die Möglichkeit für Betroffene, in einer besonderen Situation der weiteren Verarbeitung ihrer personenbezogenen Daten zu widersprechen, soweit diese durch die Wahrnehmung öffentlicher Aufgaben oder öffentlicher sowie privater Interessen gerechtfertigt ist. Es gelten die in § 36 BDSG geregelten Ausnahmen von diesem Recht.',
        listTitle67: 'Recht auf Widerruf der Einwilligung, Art. 13 und 14 DSGVO',
        listContent67:
          'Soweit die Verarbeitung der personenbezogenen Daten auf Grundlage einer Einwilligung erfolgt, kann die betroffene Person diese jederzeit für den entsprechenden Zweck widerrufen. Die Rechtmäßigkeit der Verarbeitung aufgrund der getätigten Einwilligung bleibt bis zum Eingang des Widerrufs unberührt.',
        subtitle7: '7. Beschwerdemöglichkeit',
        paragraf62:
          'Ihnen steht zudem gemäß Art. 77 DSGVO ein Beschwerderecht bei der datenschutzrechtlichen Aufsichtsbehörde, dem Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI), Graurheindorfer Str. 153, 53117 Bonn, Telefon: +49 (0)228 997799-0, <a id="datenschutz-light-email-link7" target="_blank" href="https://www.bfdi.bund.de">www.bfdi.bund.de</a>, zu.',
        paragraf63:
          'Sie können sich mit Fragen und Beschwerden auch jederzeit an die oben genannte Datenschutzbeauftragte im BMI wenden.',
        subtitle8: '8. Links zu anderen Webseiten',
        paragraf71:
          'Diese Webseite enthält Links zu anderen Webseiten, deren Datenschutzbestimmungen abweichen können und auf deren Inhalt wir keinen Einfluss haben. Welche Rechte und Einstellungsmöglichkeiten Sie zum Schutz Ihrer Privatsphäre haben, entnehmen Sie bitte den jeweiligen Datenschutzhinweisen der Anbieterinnen und Anbieter.',
        subtitle9: '9. Änderungen der Datenschutzerklärung',
        paragraf81:
          'Im Zuge der (technologischen) Weiterentwicklung unserer Webseite können Änderungen dieser Datenschutzerklärung erforderlich werden. Daher wird empfohlen, sich diese Datenschutzerklärung zeitweilig erneut durchzulesen.',
      },
    },
    footer: {
      dienstekonsolidierung: { alt: 'Logo Dienstekonsolidierung' },
      backToTop: 'Zum Seitenanfang',
      copyright: '© Bundesministerium des Innern und für Heimat',
      datenschutz: {
        title: 'Datenschutzerklärung',
        subtitle1: '1. Grundlagen',
        subtitle11: '1.1. Verantwortlicher und Datenschutzbeauftragter',
        paragraf111: 'Verantwortlich für die Verarbeitung von personenbezogenen Daten ist:',
        block112:
          'Bundesministerium des Innern und für Heimat<strong><br>Alt-Moabit 140<br>10557 Berlin<br>Telefon: +49-(0)30 18 681-0<br>Fax: +49-(0)30 18 681-12926<br>E-Mail: <a id="datenschutz-email-link1" href="mailto:poststelle@bmi.bund.de">poststelle@bmi.bund.de</a><br>DE-Mail: <a id="datenschutz-email-link2" href="mailto:poststelle@bmi-bund.de-mail.de">poststelle@bmi-bund.de-mail.de</a><br></strong>',
        paragraf112:
          'Bei Fragen zum Datenschutz und zu dieser Datenschutzerklärung erreichen Sie die Datenschutzbeauftragte bzw. den Datenschutzbeauftragten des BMI unter:',
        block113:
          'Beauftragte/r für den Datenschutz<strong><br>Alt-Moabit 140<br>10557 Berlin<br>Telefon: +49-(0)30 18 681-0<br>Fax: +49-(0)30 18 681-12926<br>E-Mail: <a id="datenschutz-email-link3" href="mailto:bds@bmi.bund.de">bds@bmi.bund.de</a><br></strong>',
        paragraf114:
          'Das Bundesministerium des Innern und für Heimat wird von dem Auftragsverarbeiter (AV) ITZBund unterstützt. Mit dem AV wurde eine Auftragsverarbeitungsvereinbarung gemäß Art. 28 DSGVO geschlossen.',
        subtitle12: '1.2. Personenbezogene Daten',
        paragraf121:
          'Personenbezogene Daten sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person beziehen. Als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt – insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten und/oder zu einer Online-Kennung – identifiziert werden kann.',
        subtitle2: '2. Verarbeitung personenbezogener Daten',
        paragraf21:
          'Im Rahmen der Registrierung und Nutzung der E-Gesetzgebung werden personenbezogene Daten verarbeitet und gespeichert. Die Betroffenengruppe richtet sich hierbei auf Nutzende der Anwendung sowie an Fachadministrierende.',
        paragraf22: 'Dabei werden im Hinblick auf die Datenkategorien folgende personenbezogene Daten verarbeitet:',
        listTitle23: 'Personen ohne Benutzerkonto:',
        list231: '<ul><li>E-Mail-Adresse (im Falle von Supportanfragen)</li></ul>',
        listTitle24: 'Personen mit Benutzerkonto (Benutzerdaten):',
        list241:
          '<ul><li>ID (GID)</li><li>Anrede</li><li>Titel (optional)</li><li>Vorname</li><li>Nachname</li><li>Dienstl. Telefonnr.</li><li>Ressort</li><li>Organisationseinheit</li><li>Dienststelle</li><li>Funktion</li><li>Dienstl. E-Mail-Adresse</li></ul>',
        subtitle3: '3. Datenempfänger',
        paragraf31:
          'Ihre Daten werden vom BMI im Rahmen seiner Zuständigkeit zweckgebunden verarbeitet. Das BMI setzt das ITZBund als Dienstleister für die Auftragsverarbeitung ein. Mit diesem Dienstleister wurde ein Vertrag gemäß Art. 28 DSGVO geschlossen; datenschutzrechtlich verantwortlich bleibt das BMI.',
        paragraf32:
          'Darüber hinaus kann es im Einzelfall zur Weitergabe von Daten an Dritte kommen, soweit das BMI hierzu rechtlich verpflichtet ist oder dies zur Abwehr einer Gefahr für die öffentliche Sicherheit, zur Verfolgung von Straftaten oder bspw. zur Abwehr von Angriffen auf unsere IT-Infrastruktur erforderlich ist. Eine Weitergabe in anderen Fällen erfolgt nicht. Eine Zusammenführung dieser Daten mit anderen Datenquellen, zum Beispiel zum Anlegen von Nutzerprofilen, erfolgt nicht.',
        subtitle4: '4. Rechtsgrundlagen und Zwecke der Verarbeitung',
        paragraf41: 'Die Verarbeitung der unter Punkt 2 aufgeführten personenbezogenen Daten ist rechtmäßig.',
        paragraf42:
          'Rechtsgrundlage der Verarbeitung ist hier Art. 6 Abs. 1 lit. e DSGVO in Verbindung mit § 3 BDSG. Für Personen, die im Rahmen ihrer dienstlichen Tätigkeit ein Benutzerkonto anlegen, gilt als Rechtsgrundlage für die Verarbeitung ihrer personenbezogenen Daten Art. 88 DSGVO in Verbindung mit § 26 BDSG.',
        paragraf43:
          'Die Nutzung der aufgeführten personenbezogenen Daten ist erforderlich, um die von der IT-Anwendung E-Gesetzgebung unterstützten Aufgaben im Rahmen des Rechtsetzungsprozesses zu erfüllen. Diese Aufgaben liegen in der Zuständigkeit verantwortlicher Personen der öffentlichen Stelle (siehe Punkt 2 betroffene Personen).',
        paragraf44:
          'Sollte das BMI personenbezogene Daten aufgrund einer rechtlichen Verpflichtung weitergeben, ist die rechtliche Grundlage dafür Art. 6 Abs. 1 lit. c DSGVO.',
        subtitle5: '5. Speicherdauer',
        listTitle51: 'Löschfristen:',
        list511:
          '<ul><li>Benutzerdaten werden gelöscht, wenn das Benutzerkonto nicht mehr benötigt wird.</li><li>Inhaltsdaten werden gelöscht, wenn das Benutzerkonto nicht mehr benötigt wird.</li><li>Metadaten werden gelöscht, wenn das Benutzerkonto nicht mehr benötigt wird.</li><li>Protokolldaten werden nach 90 Tagen gem. Rahmendatenschutzkonzept Protokollierung Detektion des BSI gelöscht.</li></ul>',
        paragraf52:
          'Die Speicherung einer Kommunikation zwischen Ihnen und dem BMI richtet sich nach den für die Aufbewahrung von Schriftgut geltenden Fristen der <a id="datenschutz-link1" target="_blank" href="https://www.bmi.bund.de/SharedDocs/downloads/DE/veroeffentlichungen/themen/ministerium/registraturrichtlinie.html">Registraturrichtlinie</a>, die die <a id="datenschutz-link2" target="_blank" href="https://www.bmi.bund.de/DE/themen/moderne-verwaltung/verwaltungsmodernisierung/geschaeftsordnung-bundesministerien/geschaeftsordnung-bundesministerien-node.html">Gemeinsame Geschäftsordnung der Bundesministerien</a> (GGO) ergänzt.',
        subtitle6: '6. Profiling und elektronische Entscheidungsfindung',
        paragraf61:
          'Eine automatisierte Entscheidungsfindung einschließlich Profiling (Art. 13 Abs. 2 DSGVO, Art. 22 Abs. 1, 4 DSGVO) findet nicht statt.',
        subtitle7: '7. Ihre Rechte',
        paragraf71:
          'Sie haben gegenüber der verantwortlichen Person folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:',
        listTitle72: 'Recht auf Auskunft, Art. 15 DSGVO',
        listContent72:
          'Mit dem Recht auf Auskunft erhalten Sie eine umfassende Einsicht in die Sie angehenden Daten und einige andere wichtige Kriterien, wie beispielsweise die Verarbeitungszwecke oder die Dauer der Speicherung. Es gelten die in § 34 BDSG geregelten Ausnahmen von diesem Recht.',
        listTitle73: 'Recht auf Berichtigung, Art. 16 DSGVO',
        listContent73:
          'Das Recht auf Berichtigung beinhaltet die Möglichkeit für Sie, unrichtige oder unvollständige Sie angehende personenbezogene Daten korrigieren zu lassen.',
        listTitle74: 'Recht auf Löschung, Art. 17 DSGVO',
        listContent74:
          'Das Recht auf Löschung beinhaltet die Möglichkeit für Sie, Daten bei der verantwortlichen Person löschen zu lassen. Dies ist allerdings nur dann möglich, wenn die Sie angehenden personenbezogenen Daten nicht mehr notwendig sind, rechtswidrig verarbeitet werden oder eine diesbezügliche Einwilligung widerrufen wurde. Es gelten die in § 35 BDSG geregelten Ausnahmen von diesem Recht.',
        listTitle75: 'Recht auf Einschränkung der Verarbeitung, Art. 18 DSGVO',
        listContent75:
          'Das Recht auf Einschränkung der Verarbeitung beinhaltet die Möglichkeit für Sie, eine weitere Verarbeitung der Sie angehenden personenbezogenen Daten vorerst zu verhindern. Eine Einschränkung tritt vor allem in der Prüfungsphase anderer Rechtewahrnehmungen durch die betroffene Person ein.',
        listTitle76: 'Recht auf Datenübertragbarkeit, Art. 20 DSGVO',
        listContent76:
          'Das Recht auf Datenübertragbarkeit beinhaltet die Möglichkeit für Sie, die Sie angehenden personenbezogenen Daten in einem gängigen, maschinenlesbaren Format von der verantwortlichen Peron zu erhalten, um sie ggf. an eine andere verantwortliche Person weiterleiten zu lassen. Gemäß Art. 20 Abs. 3 S. 2 DSGVO steht dieses Recht aber dann nicht zur Verfügung, wenn die Datenverarbeitung der Wahrnehmung öffentlicher Aufgaben dient.',
        listTitle77: 'Recht auf Widerspruch gegen die Erhebung, Verarbeitung und bzw. oder Nutzung, Art. 21 DSGVO',
        listContent77:
          'Das Recht auf Widerspruch beinhaltet die Möglichkeit für Sie, in einer besonderen Situation der weiteren Verarbeitung Ihrer personenbezogenen Daten zu widersprechen, soweit diese durch die Wahrnehmung öffentlicher Aufgaben oder öffentlicher sowie privater Interessen gerechtfertigt ist. Es gelten die in § 36 BDSG geregelten Ausnahmen von diesem Recht.',
        paragraf78:
          'Zur Ausübung der vorgenannten Rechte können Sie sich schriftlich an die unter Punkt 1.1 genannten Stellen wenden.',
        paragraf79:
          'Ihnen steht zudem gemäß Art. 77 DSGVO ein Beschwerderecht bei der datenschutzrechtlichen Aufsichtsbehörde, dem Bundesbeauftragten für den Datenschutz und die Informationsfreiheit (BfDI), Graurheindorfer Str. 153, 53117 Bonn, Telefon: +49 (0)228 997799-0, <a id="datenschutz-link3" target="_blank" href="https://www.bfdi.bund.de/DE/Home/home_node.html">www.bfdi.bund.de</a> zu.',
        paragraf80:
          'Sie können sich mit Fragen und Beschwerden auch an den unter Punkt 1.1 genannten, mit dem Datenschutz beauftragten Kontakt im BMI wenden.',
      },
      versionen: {
        title: 'Versionen',
        plFE: 'Plattform-Frontend',
        plBE: 'Plattform-Backend',
        edFE: 'Editor-Frontend',
        edBE: 'Editor-Backend',
        rbac: 'RBAC',
      },
      impressum: {
        title: 'Impressum',
        einleitung:
          'Gemäß Verwaltungsvereinbarung über die Umsetzung des Projektes E-Gesetzgebung liegt die Projektleitung beim',
        impressum:
          'Bundesministerium des Innern und für Heimat<br>DG II 6 - Maßnahmen Enterprise Ressource Management und Elektronische Verwaltungsarbeit<br>Alt-Moabit 140<br>10557 Berlin<br>Telefon: +49-(0)30 18 681-0<br>E-Mail: <a href="mailto:support.egesetzgebung@bmi.bund.de">support.egesetzgebung@bmi.bund.de</a>',
        subtitle: 'Rechtliche Hinweise',
        hinweise1:
          'Die Inhalte dieser Internet-Seiten sollen den Zugang der Öffentlichkeit zu Informationen unseres Ministeriums erleichtern und ein zutreffendes Bild von den Tätigkeiten, Planungen und Vorhaben des Bundesministeriums des Innern (nachfolgend BMI) vermitteln. Auf die Richtigkeit, Aktualität, Vollständigkeit, Verständlichkeit und jederzeitige Verfügbarkeit der bereitgestellten Informationen wird sorgfältig geachtet.',
        hinweise2: 'Dennoch sind folgende Einschränkungen zu machen:',
        hinweise3: '1. Inhalte anderweitiger Anbieter',
        hinweise4:
          'Die auf diesen Seiten vorhandenen Links zu Inhalten von Internet-Seiten Dritter („fremden Inhalten“) wurden durch das BMI nach bestem Wissen und unter Beachtung größtmöglicher Sorgfalt erstellt und vermitteln lediglich den Zugang zu „fremden Inhalten“. Dabei wurde auf die Vertrauenswürdigkeit dritter Anbieter und die Fehlerfreiheit sowie Rechtmäßigkeit der „fremden Inhalte“ besonders geachtet.',
        hinweise5:
          'Da jedoch der Inhalt von Internetseiten dynamisch ist und sich jederzeit ändern kann, ist eine stetige Einzelfallprüfung sämtlicher Inhalte, auf die ein Link erstellt wurde, nicht in jedem Fall möglich. Das BMI macht sich deshalb den Inhalt von Internet-Seiten Dritter, die mit der eigenen Internetpräsenz verlinkt sind, insoweit ausdrücklich nicht zu eigen. Für Schäden aus der Nutzung oder Nichtnutzung „fremder Inhalte“ haftet ausschließlich der jeweilige Anbieter der Seite, auf die verwiesen wurde.',
        hinweise6: '2. Eigene Inhalte',
        hinweise7:
          'Soweit die auf diesen Seiten eingestellten Inhalte Rechtsvorschriften, amtliche Hinweise, Empfehlungen oder Auskünfte enthalten, sind sie nach bestem Wissen und unter Beachtung größtmöglicher Sorgfalt erstellt. Bei Unstimmigkeiten gilt jedoch ausschließlich die aktuelle amtliche Fassung, wie sie im dafür vorgesehenen amtlichen Verkündungsorgan veröffentlicht ist. Etwaige rechtliche Hinweise, Empfehlungen und Auskünfte sind unverbindlich; eine Rechtsberatung findet nicht statt.',
        hinweise8:
          'Für das bereitgestellte Informationsangebot gilt folgende Haftungsbeschränkung: Das BMI haftet nicht für Schäden, die durch die Nutzung oder Nichtnutzung angebotener Informationen entstehen. Für etwaige Schäden, die beim Aufrufen oder Herunterladen von Daten durch Computerviren oder der Installation oder Nutzung von Software verursacht werden, wird nicht gehaftet.',
        hinweise9: '3. Fehlermeldungen',
        hinweise10:
          'Sollten auf dieser Internetpräsenz Links enthalten sein, die auf rechtswidrige oder fehlerhafte Inhalte Dritter verweisen, so bittet die Redaktion die Nutzenden dieser Website darum, hierauf ggf. aufmerksam zu machen. Ebenso wird um eine Nachricht unter der angegebenen Kontaktadresse gebeten, wenn eigene Inhalte nicht fehlerfrei, aktuell, vollständig und verständlich sind.',
        hinweise11: '4. Datenschutz',
        hinweise12: "Erfahren Sie mehr hierzu auf der Seite <a href='#/datenschutz'>Datenschutz</a>.",
        hinweise13: '5. Urheberrecht',
        hinweise14: 'Das Copyright für Texte liegt, soweit nicht anders vermerkt, beim BMI.',
        hinweise15:
          'Das Copyright für Bilder liegt, soweit nicht anders vermerkt, beim BMI oder bei der Bundesbildstelle des Presse- und Informationsamtes der Bundesregierung.',
        hinweise16:
          'Auf den BMI-Webseiten zur Verfügung gestellte Texte, Textteile, Grafiken, Tabellen oder Bildmaterialien dürfen ohne vorherige Zustimmung des BMI nicht vervielfältigt, nicht verbreitet und nicht ausgestellt werden.',
        hinweise17: '6. Nutzungsbedingungen für Bilder',
        hinweise18:
          'Das BMI hat alle Nutzungsrechte von den Urheberinnen und Urhebern der digitalen Bilder erworben, soweit nichts Gegenteiliges, z.B. in den begleitenden Bildinformationen, angegeben ist.',
        hinweise19:
          'Die Bildinformationen und die darin enthaltenen Nutzungs- und Verwendungsbeschränkungen sind zu beachten. Für die aus der Nichtbeachtung resultierenden Schäden haften die Nutzenden. Er hat das BMI insoweit von Ansprüchen Dritter freizustellen.',
        hinweise20:
          'Mit dem Quellennachweis „BMI“ ausgezeichnete Bilder können grundsätzlich kostenfrei heruntergeladen und im Rahmen der Berichterstattung für folgende Zwecke genutzt werden:',
        hinweise21:
          '<ul><li>Presseveröffentlichungen</li><li>Veröffentlichungen in Printmedien</li><li>Veröffentlichungen durch Film und Fernsehen</li><li>Online- und multimediale Veröffentlichungen</li><li>Eine darüber hinausgehende Nutzung für kommerzielle Zwecke, insbesondere für Werbezwecke, ist nicht zulässig.</li>',
        hinweise22:
          'Bilder mit einem anderen Quellennachweis als „BMI“ sind für eine Nutzung durch Dritte nicht freigegeben.',
        hinweise23:
          'Jegliche Bearbeitung, Umgestaltung oder Manipulation der digitalen Bilder, die über Farbkorrekturen, Ausschnitte und Verkleinerungen hinausgehen, ist unzulässig und nur mit vorheriger schriftlicher Zustimmung seitens des BMI erlaubt. Ebenso darf das digitale Bild nicht in einem sinnentstellten Zusammenhang wiedergegeben werden.',
        hinweise24:
          'Eine Entstellung des urheberrechtlichen geschützten Werks in Bild, Wort bzw. jeglicher anderen Form, z.B. durch Nachfotografieren, zeichnerische Verfälschung, Fotocomposing oder elektronische Hilfsmittel ist nicht zulässig. Die Nutzenden tragen die Verantwortung für die Betextung.',
        hinweise25:
          'Die Presse ist insbesondere zur Beachtung der publizistischen Grundsätze des Deutschen Presserates (Pressekodex) verpflichtet. Die Zustimmung zur Nutzung des Bildmaterials umfasst nicht die Zusicherung, dass die abgebildeten Personen, die Inhaberinnen und Inhaber der Rechte an abgebildeten Werken oder die Inhaberinnen und Inhaber von Marken- und sonstigen Schutzrechten die Einwilligung zu einer öffentlichen Wiedergabe erteilt haben. Die Einholung der im Einzelfall notwendigen Einwilligungen Dritter obliegt den Nutzenden. Er hat die Persönlichkeits-, Urheber-, Marken- und sonstigen Schutzrechte von abgebildeten Personen, Werken, Gegenständen oder Zeichen selbst zu beachten. Bei Missachtung solcher Rechte sind allein die Nutzenden etwaigen Dritten gegenüber schadenersatzpflichtig.',
        hinweise26:
          'Das BMI behält sich vor, dem Verdacht einer missbräuchlichen Nutzung oder einer wesentlichen Nutzungsverletzung nachzugehen.',
        hinweise27:
          'Bei Verwendung des digitalen Bildes ist die Quelle „Bundesministerium des Innern und für Heimat“ anzugeben. Dies gilt auch für elektronische Publikationen (z.B. Webseiten). Von jeder Veröffentlichung im Druck ist dem BMI ein Belegexemplar unaufgefordert und kostenfrei zuzusenden: Bundesministerium des Innern und für Heimat, Alt-Moabit 140, 10557 Berlin',
      },
      vsNfd: {
        title: 'Hinweis zu VS-NfD auf der Plattform E-Gesetzgebung',
        text0:
          'Die E-Gesetzgebung wird in den gesicherten Netzen des Bundes betrieben. Derzeit existiert keine VS-NfD-Freigabe für diese Plattform. Insofern dürfen auf der Plattform der E-Gesetzgebung vertrauliche Informationen und Dokumente nur mit Bedacht hochgeladen, erstellt und geteilt werden. Verschlusssachen im Sinne der Verschlusssachenanweisung (VSA) dürfen nicht hochgeladen, erstellt oder geteilt werden.',
      },

      barrierefreiheit: {
        title: 'Zugänglichkeit',
        section1: {
          title: 'Erklärung zur Barrierefreiheit',
          text: 'Die E‑Gesetzgebung soll von möglichst vielen Menschen benutzt werden können. Daher legen wir viel Wert auf zugängliche Benutzung.<br/>Weitere Informationen zur Barrierefreiheit unserer Software finden Sie hier:',
          link: 'Erklärung zur Barrierefreiheit',
        },
        section2: {
          title: 'Nutzungshinweise',
          text: 'Für die konkrete Benutzung finden Sie hier die Tastaturkürzel und Bedienungshinweise für die Arbeit mit unsere Software.',
          link: 'Nutzungsdokumentation der E‑Gesetzgebung',
        },
        section3: {
          title: 'Leichte Sprache',
          text: 'Hier finden Sie Informationen in Leichter Sprache zu unserer Software.',
          link: 'Informationen in Leichter Sprache',
        },
        section4: {
          title: 'Eine Barriere melden',
          text: 'Sie haben in der Benutzung der E‑Gesetzgebung eine Barriere gefunden? Wir freuen uns, wenn Sie uns darüber informieren!<br/>Schreiben Sie eine kurze Nachricht an unser Postfach:',
          link: 'Nachricht an die E‑Gesetzgebung',
        },
        erklaerung: {
          title: 'Erklärung zur Barrierefreiheit',
          pkpBarrierefreiheit: de_contacts.footer.barrierefreiheit.element2b.pkpBarrierefreiheit,
          element1:
            'Die Erklärung zur Barrierefreiheit enthält Informationen über die Zugänglichkeit der Internetseite bzw. Webanwendung gemäß § 12a Behindertengleichstellungsgesetz (BGG). Weiterhin werden Ihnen Kontaktmöglichkeiten zur Meldung von bestehenden Barrieren zur Verfügung gestellt.',
          element1a:
            'Das Bundesministerium des Innern und für Heimat ist bemüht, die E‑Gesetzgebung barrierefrei zugänglich zu machen. Rechtsgrundlage sind das BGG, die Barrierefreie-Informationstechnik-Verordnung (BITV 2.0) und die harmonisierte europäische Norm EN&nbsp;301&nbsp;549 in ihrer jeweils gültigen Fassung.',
          element2: 'Gültigkeitsbereich der Erklärung zur Barrierefreiheit',
          element2a:
            'Die vorliegende Erklärung zur Barrierefreiheit bezieht sich auf alle Anwendungen der E‑Gesetzgebung.',
          element2aLight:
            'Die vorliegende Erklärung zur Barrierefreiheit bezieht sich auf die E‑Gesetzgebung im Internet.',
          element2b:
            'Ausgenommen ist die Registrierungs- und Anmeldemaske, welche gemeinsam mit der Anwendung „Planungs- und Kabinettmanagementprogramm“ (PKP) genutzt wird. Die Barrierefreiheit der Registrierungs- und Anmeldemaske wird von der Maßnahme PKP verantwortet. Für nähere Informationen möchten wir an dieser Stelle auf die <a target="_blank" href="{{pkpBarrierefreiheit}}">Erklärung zur Barrierefreiheit der Anwendung PKP</a> verweisen.',
          element2c:
            'Die Verantwortung für die Barrierefreiheit der unter der Anwendung „Arbeitshilfenbibliothek“ verlinkten Dokumente und Webseiten liegt bei der jeweiligen herausgebenden Stelle der Arbeitshilfen. Diese sind nur in Teilen barrierefrei. Wir bitten Sie, sich für weitere Informationen oder bei Anmerkungen an die herausgebende Stelle zu wenden.',
          element3: 'Stand der Vereinbarkeit mit den Anforderungen',
          element3a: `Die E‑Gesetzgebung wurde zuletzt am 24.05.2024, 28.06.2024 sowie 07.08.2024 einer Barrierefreiheitsprüfung gem. BITV 2.0 unterzogen.<br>
                      Im gegenwärtigen Entwicklungsstand entspricht die E‑Gesetzgebung teilweise den Vorgaben der BITV 2.0. Der Editor ist derzeit nicht barrierefrei zugänglich. Die folgenden Anwendungen sind teilweise barrierefrei bedienbar:
                      <ul>
                        <li>Verfahrensassistent,</li>
                        <li>Arbeitshilfenbibliothek,</li>
                        <li>Vorbereitung,</li>
                        <li>Zeitplanung,</li>
                        <li>Gesetzesfolgenabschätzung (inkl. 14 Teilmodule),</li>
                        <li>Gesetzgebungsportal.</li>
                      </ul>
                      Die Anwendungen Regelungsvorhaben und Abstimmung weisen einen überwiegend barrierefreien Zustand auf. Aufgrund der agilen Entwicklung kann es jedoch zu geringfügigen Barrieren kommen, die fortlaufend behoben werden.`,
          element3aLight: `Im gegenwärtigen Entwicklungsstand entspricht die E‑Gesetzgebung im Internet teilweise den Vorgaben der BITV 2.0. Die Anwendung Zeitplanung ist derzeit nur teilweise barrierefrei.
                           Die Entwicklung erfolgt agil, sodass die E‑Gesetzgebung schrittweise einen Aufwuchs erfährt. Die Anwendung wird während der Entwicklung bereits auf ihre Barrierefreiheit getestet. Die Ergebnisse der Tests sowie die Vorgaben zur Barrierefreiheit werden in der Entwicklung berücksichtigt. Somit ist davon auszugehen, dass in der letzten Ausbaustufe die Gesamtanwendung eine hohe Barrierefreiheit aufweisen wird.`,
          element3b:
            'Die Entwicklung erfolgt agil, sodass die E‑Gesetzgebung schrittweise einen Aufwuchs erfährt. Die Anwendungen werden während der Entwicklung bereits auf ihre Barrierefreiheit getestet. Die Ergebnisse der Tests sowie die Vorgaben zur Barrierefreiheit werden in der Entwicklung berücksichtigt. Somit ist davon auszugehen, dass in der letzten Ausbaustufe die E‑Gesetzgebung insgesamt einen hohen Grad an Barrierefreiheit aufweisen wird.',
          element3c:
            'Die Anwendungen Verfahrensassistent, Arbeitshilfenbibliothek, Zeitplanung und Gesetzesfolgenabschätzung inkl. der 14 Module sollen zum Release April 2025 einen barrierefreien Zustand erreichen. Für den Editor wird bis zum Release Oktober 2025 ein barrierefreier Zustand angestrebt. Das Gesetzgebungsportal soll bis zu seiner Veröffentlichung im Internet Ende 2025 auf Barrierefreiheit überprüft und bei Bedarf dahingehend angepasst werden. Für die Anwendung Vorbereitung wird derzeit evaluiert, ob diese Bestandteil der E‑Gesetzgebung bleiben soll. Abhängig vom Ergebnis wird eine barrierefreie Neukonzeption oder Entfernung der Anwendung angestrebt.',
          element4: 'Erstellung dieser Erklärung zur Barrierefreiheit',
          element4a:
            'Diese Erklärung wurde am {{creationDate}} erstellt.<br>Die Erklärung wurde zuletzt am {{validationDate}} überprüft.',
          element5: 'Feedback und Kontaktangaben',
          element5a:
            'Sind Ihnen Mängel in der Zugänglichkeit von Inhalten aufgefallen? Oder haben Sie Fragen zum Thema Barrierefreiheit? Dann können Sie sich gern bei uns melden:',
          element5b: 'Support der E-Gesetzgebung',
          element5c: 'E‑Mail:&nbsp;<a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>',
          element6: 'Schlichtungsverfahren',
          element6a:
            'Beim Beauftragten der Bundesregierung für die Belange von Menschen mit Behinderungen gibt es eine Schlichtungsstelle gemäß § 16 BGG. Die Schlichtungsstelle hat die Aufgabe, Konflikte zwischen Menschen mit Behinderungen und öffentlichen Stellen des Bundes zu lösen.',
          element6b:
            'Sie können die Schlichtungsstelle einschalten, wenn Sie mit den Antworten aus der oben genannten Kontaktmöglichkeit nicht zufrieden sind. Dabei geht es nicht darum, Gewinner oder Verlierer zu finden. Vielmehr ist es das Ziel, mit Hilfe der Schlichtungsstelle gemeinsam und außergerichtlich eine Lösung für ein Problem zu finden. Das Schlichtungsverfahren ist kostenlos. Sie brauchen auch keinen Rechtsbeistand. Auf der&nbsp;<a target="_blank" href="https://www.schlichtungsstelle-bgg.de/Webs/SchliBGG/DE/AS/startseite/startseite-node.html">Internetseite der Schlichtungsstelle</a> finden Sie alle Informationen zum Schlichtungsverfahren. Dort können Sie nachlesen, wie ein Schlichtungsverfahren abläuft und wie Sie den Antrag auf Schlichtung stellen.',
          element6c: 'Sie erreichen die Schlichtungsstelle unter folgender Adresse:',
          element6d:
            'Schlichtungsstelle nach dem Behindertengleichstellungsgesetz<br>bei dem Beauftragten der Bundesregierung für die Belange von Menschen mit Behinderungen<br>Mauerstraße 53<br>10117 Berlin',
          element6e: 'Telefon: +49 (0)30 18 527 - 2805',
          element6f: 'Fax: +49 (0)30 18 527 - 2901',
          element6g:
            'E‑Mail:&nbsp;<a href="mailto:info@schlichtungsstelle-bgg.de">info@schlichtungsstelle-bgg.de</a><br>Internet:&nbsp;<a target="_blank" href="https://www.schlichtungsstelle-bgg.de/">www.schlichtungsstelle-bgg.de</a>',
        },
        handbuch: {
          title: 'Nutzungshinweise',
          description:
            'Auf dieser Seite befinden sich alle Hinweise, die für Tastaturnutzende der E‑Gesetzgebung relevant sind.',
          subtitle1: 'Shortcuts',
          section1:
            'Folgende Standard-Shortcuts für Windows-Systeme können von Tastaturnutzenden in den Browsern Chrome, Edge sowie Firefox genutzt werden, um eine effizientere Navigation innerhalb der Anwendung zu gewährleisten:',
          tableHeader: 'Tastenkombination;Funktionsbeschreibung',
          table1: [
            'Alt + Umschalt + Druck;Wechselt in den invertierten Modus für stärkere Kontraste.',
            'Windows + Plustaste;Wechselt in die Lupenansicht.',
            'Windows + ESC;Beendet die Lupenansicht.',
            'Bild ab;Führt einen Bildlauf zum Ende der Seite durch.',
            'Bild auf;Führt einen Bildlauf zum Anfang der Seite durch.',
            'Ende;Setzt den Fokus auf das letzte Element des aktuell fokussierten Untermenüs oder des Hauptmenüs.',
            'Enter;Löst fokussierte Elemente aus.',
            'Escape-Taste;Schließt das aktuelle Untermenü.',
            'Home/Pos1;Setzt den Fokus auf das erste Element des aktuell fokussierten Untermenüs oder des Hauptmenüs.',
            'Pfeil hoch;Öffnet ein Untermenü, wenn der Fokus auf einem Hauptmenü-Element mit weiteren Einträgen ist und setzt den Fokus auf das letzte Element des Untermenüs.',
            'Pfeil links;Schließt eventuell offene Untermenüs und setzt den Fokus auf das vorherige Element des Hauptmenüs, welches geöffnet wird.',
            'Pfeil rechts;Schließt eventuell offene Untermenüs und setzt den Fokus auf das nächste Element des Hauptmenüs, welches geöffnet wird.',
            'Pfeil runter;Öffnet ein Untermenü, wenn der Fokus auf einem Hauptmenü-Element mit weiteren Einträgen ist, und setzt den Fokus auf das erste Element des Untermenüs.',
            'Leertaste;Klickt auf fokussierte Buttons und Checkboxen.',
            'Strg + F;Ruft die Standard-Suchfunktion vom Browser auf.',
            'Strg + links/rechts;Navigiert die Screenreader-Ausgabe wortweise nach links/nach rechts. Es wird kein sichtbarer Fokus gesetzt.',
            'Strg + oben/unten;Navigiert die Screenreader-Ausgabe absatzweise nach oben/unten. Es wird kein sichtbarer Fokus gesetzt.',
            'Strg + Pos1;Navigiert zum Textanfang.',
            'Tabulatortaste;Navigiert vorwärts in das nächste Element.',
            'Umschalt + Tabulatortaste;Navigiert rückwärts in das vorige Element.',
          ],
          table2title: 'Neben den Standard-Shortcuts gibt es auch Editor-spezifische Shortcuts:',
          table2: [
            'Backspace im Editierbereich;Löscht Textzeichen bzw. die vorherige Zeile bei einer leeren Textzeile.',
            'Enter im Regelungstext im Editierbereich;Erstellt einen neuen juristischen Absatz oder erweitert eine Aufzählung im juristischen Absatz.',
            'Strg + Ende;Navigiert zum Textende.',
          ],
        },
      },
      leichteSprache: {
        title: 'Leichte Sprache',
        section1: {
          title: '',
          text: `Herzlich willkommen auf unserer Internet-Seite.  <br/>
                Wir sind die E‑Gesetzgebung. <br/>
                <br/>
                Diese Internet-Seite ist in leichter Sprache geschrieben.  <br/>
                Viele Menschen können Texte in leichter Sprache besser verstehen.  <br/>
                Zum Beispiel Menschen, denen Lesen schwerfällt. <br/>
                Oder Menschen, die nicht so gut Deutsch sprechen.  <br/>
                Deshalb gibt es diese Internet-Seite.   
          <br/>
                `,
        },
        section2: {
          title: 'Was ist die E‑Gesetzgebung?',
          text: `Die E‑Gesetzgebung ist ein Software-Programm. <br/>
              Das Software-Programm können Sie mit einem Computer benutzen. <br/>
              <br/>
              Das Software-Programm hat den Namen E‑Gesetzgebung. <br/>
              Der Name kommt von den Wörtern: Elektronisch und Gesetzgebung. <br/>
              Gesetzgebung ist ein Prozess. <br/>
              In diesem Prozess werden Gesetze geschrieben. <br/>
              Gesetze sind Regeln. <br/>
              Alle Menschen in Deutschland müssen sich an diese Regeln halten. <br/>
              <br/>
              Die E‑Gesetzgebung bietet viele Werkzeuge. <br/>
              Menschen können mit den Werkzeugen schneller und besser Gesetze schreiben. <br/>`,
        },
        section3: {
          title: 'Navigationshilfe',
          text: `Die Navigationshilfe hilft Ihnen, besser mit der E‑Gesetzgebung zurechtzukommen.<br/>
          Hier erklären wir mithilfe von Beschreibungen alle Bereiche der E‑Gesetzgebung.<br/>
          Damit Sie schneller das finden, was Sie brauchen.<br/>
        <br/>
          <a href={{urlNavigationshilfe}}>Navigationshilfe</a>`,
        },
        section4: {
          title: 'Werkzeuge der E‑Gesetzgebung',
          text: `Die E‑Gesetzgebung bietet viele Werkzeuge für Menschen, die Gesetze schreiben.<br/>
            Diese Menschen können mit den Werkzeugen schneller und besser Gesetze schreiben.<br/>
            Jedes Werkzeug hat einen Namen.<br/>
            Der Name ist oft ein schweres Wort.<br/>
            Hier finden Sie die Liste der Werkzeuge, die Sie auf dieser Internetseite finden<br/>
          <br/>
            <a href={{urlWerkzeuge}}>Werkzeuge der E‑Gesetzgebung</a>`,
        },
        table: {
          title: 'Werkzeuge der E‑Gesetzgebung',
          header: {
            shortname: 'Abkürzung',
            name: `Name vom Werkzeug<br/>
                (Schweres Wort)`,
            descr: 'Was macht dieses Werkzeug?',
          },
          rv: {
            abkuerzung: 'RV',
            nameWerkzeug: 'Regelungs-Vorhaben',
            erklaerung: `Das Regelungs-Vorhaben ist wie eine Mappe.<br/>
              In diese Mappe können Sie Zettel mit neuen Gesetzen hineinlegen.`,
          },
          zeit: {
            abkuerzung: 'ZEIT',
            nameWerkzeug: 'Zeit-Planung',
            erklaerung: `Ein Gesetz zu schreiben, dauert lange.<br/>
                    Mit der Zeit-Planung können Sie einen Plan machen.<br/>
                    Im Plan steht, wann das Gesetz fertig sein soll.`,
          },
          editor: {
            abkuerzung: 'Editor',
            nameWerkzeug: 'Editor',
            erklaerung: `Der Editor ist ein Text-Programm.<br/>
                      Er funktioniert ähnlich wie andere Text-Programme.<br/>
                      Hier können Sie Ihre Idee für ein Gesetz aufschreiben und bearbeiten.`,
          },
          hra: {
            abkuerzung: 'Abstimmung',
            nameWerkzeug: 'Abstimmung',
            erklaerung: `Manche Menschen werden das neue Gesetz gut finden.<br/>
                        Andere Menschen werden das neue Gesetz nicht gut finden.<br/>
                        Mit diesem Werkzeug können Menschen über das Gesetz abstimmen.<br/>
                        Sie können auch die anderen Menschen fragen: Wie kann ich das Gesetz besser machen?`,
          },
          evor: {
            abkuerzung: 'eVoR',
            nameWerkzeug: 'Vorbereitung',
            erklaerung: `Manchmal brauchen Sie noch eine Idee für ein neues Gesetz.<br/>
                        Dieses Werkzeug hilft Ihnen, neue Ideen zu sammeln.<br/>
                        Sie können festlegen, welches Problem mit dem Gesetz gelöst werden muss.<br/>
                        Sie können Ziele vom Gesetz festlegen.`,
          },
          egfa: {
            abkuerzung: 'eGFA',
            nameWerkzeug: 'Gesetzesfolgen-Abschätzung',
            erklaerung: `Neue Gesetze haben Folgen.<br/>
                Mit diesem Werkzeug können Sie die Folgen vom Gesetz prüfen.<br/>
                Sie prüfen, was passiert, wenn das Gesetz eingeführt wird.`,
          },
          bib: {
            abkuerzung: 'BIB',
            nameWerkzeug: 'Arbeitshilfen-Bibliothek',
            erklaerung: `Die Arbeitshilfen-Bibliothek ist eine Sammlung von Hilfen.<br/>
                          Die Hilfen machen es einfacher, ein Gesetz zu schreiben.`,
          },
          pkp: {
            abkuerzung: 'PKP',
            nameWerkzeug: 'Planungs- und Kabinettsmanagement-Programm (PKP)',
            erklaerung: `Jede Idee für ein Gesetz muss von Ministerinnen und Ministern besprochen werden.<br/>
                          Die Ministerinnen und Minister sind Teil der Regierung.<br/>
                          Sie entscheiden, welche Gesetze eingeführt werden.<br/>
                          Die Entscheidung passiert im Kabinett.<br/>
                          Kabinett ist ein anderes Wort für Bundesregierung.<br/>
                          Das ist das Treffen aller Ministerinnen und Minister.<br/>
                          Mit diesem Werkzeug können Sie einen Plan machen.<br/>
                          Der Plan sagt, wann die Idee für das neue Gesetz besprochen wird.<br/>
                          Mit diesem Werkzeug können Sie außerdem das Gesetz an die Ministerinnen und Minister schicken.`,
          },
          evir: {
            abkuerzung: 'eViR',
            nameWerkzeug: 'Verfahrens-Assistent',
            erklaerung: `Es gibt viele Arten von Gesetzen.<br/>
                        Je nach Thema muss eine andere Art von Gesetz geschrieben werden.<br/>
                        Dieses Werkzeug ist eine Anleitung, wie Gesetze geschrieben werden.<br/>
                        Hier wird erklärt, wie ein gutes Gesetz aussehen muss.`,
          },
          enap: {
            abkuerzung: 'eNAP',
            nameWerkzeug: 'Nachhaltigkeits-Prüfung',
            erklaerung: `Neue Gesetze haben Folgen.<br/>
              Zum Beispiel Folgen im Bereich Nachhaltigkeit.<br/>
              Mit diesem Werkzeug können Sie Folgen vom Gesetz im Bereich Nachhaltigkeit prüfen.`,
          },
        },
        navigationshilfe: {
          title: 'Navigationshilfe',
          text: `Hier erklären wir Ihnen, welche Informationen Sie in der E‑Gesetzgebung finden.<br/>
                Dabei helfen Ihnen die Beschreibungen.<br/>
                Auf den Bildern gibt es Bereiche mit einem roten Rahmen.<br/>
                Das sind die Bereiche, die wir Ihnen gerade erklären.`,
          startseite: {
            title: 'Startseite',
            text: `Hier können Sie aktuelle Informationen nachlesen.<br/>
                Zum Beispiel Informationen zu neuen Werkzeugen.<br/>
                Die Informationen werden immer wieder angepasst.`,
          },
          leichteSprache: {
            title: 'Leichte Sprache',
            text: `Wenn Sie Informationen in leicht verständlicher Sprache wollen,<br/>
                dann klicken Sie auf den Bereich <a href={{urlLeichteSprache}}>Leichte Sprache</a>.<br/>
                Leichte Sprache ist für alle Menschen,<br/>
                die sich schnell und einfach informieren wollen.`,
          },
          barrierefrei: {
            title: 'Barrierefrei',
            text: `Alle Menschen sollen die E‑Gesetzgebung benutzen können.<br/>
                Zum Beispiel Menschen, denen Lesen schwerfällt.<br/>
                Oder Menschen, die blind sind.<br/>
                Oder Menschen, die Farben nicht sehen.<br/>
                Das Wort dafür ist: barrierefrei.`,
          },
          registrieren: {
            title: 'Registrieren',
            text: `Wenn Sie die E‑Gesetzgebung benutzen wollen, müssen Sie sich <a href={{urlRegister}}>registrieren</a>.<br/>
                Registrieren bedeutet: sich zum ersten Mal anmelden.<br/>
                Wenn Sie sich bereits für das Planungs- und Kabinettmanagement-Programm (PKP) registriert haben, müssen Sie sich nicht für die E‑Gesetzgebung neu anmelden.<br/>
                Sie können Ihre Anmeldedaten von PKP verwenden.`,
          },
          anmelden: {
            title: 'Anmelden',
            text: `Wenn Sie sich schon registriert haben, müssen sie sich <a href={{urlLogin}}>anmelden</a>.<br/>
                Klicken Sie hierfür oben rechts auf die blaue Taste <b>Anmelden</b>.<br/>
                Es öffnet sich ein neues Anmeldefenster.<br/>
                Hier können Sie Ihre Anmeldedaten eingeben.`,
          },
          zeit: {
            title: 'Verbleibende Zeit',
            text: `In der Kopfzeile finden Sie oben rechts die Zeit.<br/>
                Kopfzeile bedeutet: Das ist der obere Bereich der Internet-Seite.<br/>
                Die Zeit gibt an, wie lange Sie angemeldet sind.<br/>
                Wenn die Zeit abgelaufen ist, werden Sie automatisch abgemeldet.`,
          },
          neuigkeiten: {
            title: 'Neuigkeiten',
            text: `Daneben befindet sich eine Glocke.<br/>
                Auf die Glocke können Sie klicken.<br/>
                Dann öffnet sich eine Liste mit allen Neuigkeiten.`,
          },
          profil: {
            title: 'Profil',
            text: `Daneben findet sich Ihr Profil.<br/>
                Das Profil ist Ihre persönliche Seite.<br/>
                Da stehen Angaben zu Ihrer Person.<br/>
                Sie können hier Ihr Profil einstellen.`,
          },
          hauptmenue: {
            title: 'Hauptmenü',
            text: `Das Hauptmenü finden Sie links in der Kopfzeile.<br/>
                Wenn Sie auf das Hauptmenü klicken, öffnet sich eine Liste.<br/>
                Das ist die Liste der <a href={{urlWerkzeuge}}>Werkzeuge in der E‑Gesetzgebung</a>.<br/>
                Die Werkzeuge in der Liste finden Sie auf dieser Internet-Seite.`,
          },
          seite: {
            title: 'Über die Seite',
            text: `Wenn Sie auf <a href={{urlUeberDieSeite}}>Über diese Seite</a> klicken, kommen Sie auf eine neue Seite.<br/>
                Hier werden Ihnen allgemeine Informationen zu der Seite zur Verfügung gestellt.`,
          },
          massnahme: {
            title: 'Über das Projekt',
            text: `Unten in der Liste finden Sie weitere Einträge.<br/>
                Wenn Sie auf <a href={{urlUeberDieMassnahme}}>Über das Projekt</a> klicken, kommen Sie auf eine neue Seite.<br/>
                Die Seite erklärt Ihnen, warum es die E‑Gesetzgebung gibt und wie sie entwickelt wurde.`,
          },
          massnahmeLight: {
            title: 'Über das Projekt',
            text: `Wenn Sie auf <a href={{urlUeberDieMassnahme}}>Über das Projekt</a> klicken, kommen Sie auf eine neue Seite.<br/>
                Die Seite erklärt Ihnen, warum es die E‑Gesetzgebung gibt und wie sie entwickelt wurde.`,
          },
          newsletter: {
            title: 'Newsletter Abo',
            text: `Wenn Sie auf Newsletter Abo klicken, öffnet sich ein kleines Fenster.<br/>
                Bei einem „Newsletter Abo“ erhalten Sie regelmäßig neue Informationen.<br/>
                Diese Informationen beziehen sich auf die E‑Gesetzgebung.<br/>
                Sie können sich mit einer E‑Mail anmelden, um diese Informationen zu erhalten.`,
          },
          kontakt: {
            title: 'Kontakt und Feedback',
            text: `Mit Klick auf Kontakt und Feedback öffnet sich ein Fenster.<br/>
                In dem Fenster können Sie auf <a href='mailto:{{eGesetzSupportEmail}}?subject=Feedback zur E-Gesetzgebung'>Nachricht an die E‑Gesetzgebung</a> klicken.<br/>
                Damit öffnet sich Ihr E‑Mail‑Programm und Sie können direkt eine E‑Mail an die E‑Gesetzgebung schreiben.`,
          },
          faq: {
            title: 'Häufige Fragen',
            text: `In dem Bereich <a href={{urlFaq}}>Häufige Fragen</a> können Sie sich häufig gestellte Fragen anzeigen lassen.<br/>
                Diese Fragen sind nach Themen geordnet.`,
          },
          arbeit: {
            title: 'Arbeit mit der E‑Gesetzgebung',
            text: `Wählen Sie die Werkzeuge im Hauptmenü aus.<br/>
                Das Hauptmenü enthält die Namen der Werkzeuge.<br/>
                Die Namen können Sie anklicken.`,
          },
          fusszeile: {
            title: 'Fußzeile',
            text: `Die Fußzeile ist der untere Bereich der Internet-Seite.<br/>
                Da gibt es verschiedene Bereiche.<br/>
                Im Impressum steht, wer die Internet-Seite gemacht hat.<br/>
                Im Bereich Datenschutz erklären wir, wie wir mit Ihren Daten arbeiten.`,
          },
          fusszeileLight: {
            title: 'Fußzeile',
            text: `Die Fußzeile ist ein schweres Wort.<br/>
                Es bedeutet: Das ist der untere Bereich der Internet-Seite.<br/>
                Da gibt es verschiedene Bereiche.<br/>
                Im Impressum steht, wer die Internet-Seite gemacht hat.<br/>
                Im Bereich Datenschutz erklären wir, wie wir mit Ihren Daten arbeiten.`,
          },
        },
      },
      faq: {
        title: 'Häufig gestellte Fragen und Antworten',
        shortTitle: 'Häufige Fragen',
        areaTitleBReg: 'Bereich – Bundesregierung',
        areaTitleBT: 'Bereich – Deutscher Bundestag',
        areaTitleBR: 'Bereich – Bundesrat',
        areaTitleGGP: 'Bereich – Gesetzgebungsportal',
        login: {
          title: 'Benutzerkonto und Login',
          pkpSupportEmail: de_contacts.footer.cockpit.faq.login.pkpSupportEmail,
          pkpFachadministration: de_contacts.footer.cockpit.faq.login.pkpFachadministration,
          loginData: 'Von wem erhalte ich die Login-Daten?',
          loginDataAnswerBase:
            'Die initiale Anmeldung zur E‑Gesetzgebung erfolgt über die <a href="{{urlHome}}">Startseite</a> in den Netzen des Bundes. In der Kopfzeile rechts finden Sie hierfür den Button „<a target="_blank" href="{{urlRegister}}">Registrieren</a>“, der Ihnen die initiale Anmeldung ermöglicht. ',
          loginDataAnswerBReg:
            'Als Mitarbeiterin oder Mitarbeiter der Bundesregierung können Sie sich auf der E‑Gesetzgebung mit Ihrer E‑Mail-Adresse und unter Angabe des jeweiligen Ressorts bzw. des Verfassungsorgans registrieren.',
          loginDataAnswerBT:
            'Als Nutzende aus dem Deutschen Bundestag können Sie sich auf der E‑Gesetzgebung mit Ihrer E‑Mail-Adresse und unter Angabe des Verfassungsorgans registrieren. Es ist die Freigabe der Registrierung durch die Fachadministration im Deutschen Bundestag notwendig. Sie werden anschließend bei Ihrer Anmeldung entsprechend der hinterlegten E‑Mail-Adresse automatisch auf die jeweilige Plattform weitergeleitet.',
          loginDataAnswerBR:
            'Als Mitarbeiterin oder Mitarbeiter des Bundesrates können Sie sich auf der E‑Gesetzgebung mit Ihrer E‑Mail-Adresse und unter Angabe des jeweiligen Ressorts bzw. des Verfassungsorgans registrieren. Für Mitarbeitende des Bundesrates ist die Freigabe der Registrierung durch die Fachadministration im Bundesrat notwendig. Sie werden anschließend bei Ihrer Anmeldung entsprechend der hinterlegten E‑Mail-Adresse automatisch auf die jeweilige Plattform weitergeleitet.',
          problemLogin:
            'Meine Login-Daten werden nicht akzeptiert bzw. habe ich meine Login-Daten vergessen. An wen kann ich mich wenden?',
          problemLoginAnswerBase:
            'Bitte nutzen Sie die Funktion „<a target="_blank" href="{{urlPass}}">Passwort vergessen</a>“. Sollten Sie Ihr Passwort zu häufig falsch eingegeben haben, wird Ihr Konto deaktiviert. Die Funktion „Passwort vergessen“ können Sie in diesem Fall nicht mehr nutzen. ',
          problemLoginAnswerBReg:
            'Die E‑Gesetzgebung hat eine gemeinsame Nutzerverwaltung (Identity & Access Management) mit dem Planungs- und Kabinettmanagement-Programm (PKP). Aus diesem Grund wenden sich Nutzende der Bundesregierung bei Login-Problemen bitte an die zuständige <a target="_blank" href="{{pkpFachadministration}}"> PKP-Fachadministration</a> in ihrem Haus oder an die Support-Adresse <a href="mailto:{{pkpSupportEmail}}">{{pkpSupportEmail}}</a>, um ihr Konto wieder zu aktivieren.',
          problemLoginAnswerBT:
            'Nutzende aus dem Deutschen Bundestag wenden sich bei Login-Problemen bitte via E‑Mail an <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>.',
          problemLoginAnswerBR:
            'Der Bundesrat verfügt über eine eigene Fachadministration für die Bundesratsplattform. Nutzende des Bundesrates wenden sich bei Login-Problemen bitte via E‑Mail an <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>.',
          passwordChange: 'Wie kann ich mein Passwort ändern?',
          passwordChangeAnswer:
            'Bitte nutzen Sie die Funktion „<a target="_blank" href="{{urlPass}}">Passwort vergessen</a>“. Diese Funktion finden Sie auf der Registrierungs- bzw. Login-Maske, nachdem Sie auf der <a href="{{urlHome}}">Startseite</a> entweder auf „<a target="_blank" href="{{urlRegister}}">Registrieren</a>“ oder auf „<a target="_blank" href="{{urlLogin}}">Anmelden</a>“ geklickt haben.',
          personalData: 'Welche personenbezogenen Daten muss ich bei der Registrierung hinterlegen?',
          personalDataAnswer:
            'Die Pflichtfelder sind im <a target="_blank" href="{{urlRegister}}">Eingabeformular</a> für die Registrierung mit einem Stern gekennzeichnet.',
          changeData: 'Wie kann ich meine personenbezogenen Daten bearbeiten?',
          changeDataAnswerBReg:
            'Aktuell können Sie Ihre personenbezogenen Daten (z. B. Name, E‑Mail, Ressort, Funktion) nicht bearbeiten. Die Bundesregierungsplattform hat eine gemeinsame Nutzerverwaltung (Identity & Access Management) mit dem Planungs- und Kabinettmanagement-Programm (PKP). Aus diesem Grund wenden sich Nutzende der Bundesregierung bitte an die zuständige <a target="_blank" href="{{pkpFachadministration}}">PKP-Fachadministration</a> in Ihrem Haus oder an die Support-Adresse <a href="mailto:{{pkpSupportEmail}}">{{pkpSupportEmail}}</a>, um Ihre personenbezogenen Daten zu bearbeiten.',
          changeDataAnswerBT:
            'Aktuell können Sie Ihre personenbezogenen Daten (z. B. Name, E‑Mail, Verfassungsorgan, Funktion) nicht selbst bearbeiten. Nutzende aus dem Deutschen Bundestag wenden sich bitte via E‑Mail an <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>, um ihre personenbezogenen Daten bearbeiten zu lassen.',
          changeDataAnswerBR:
            'Aktuell können Sie Ihre personenbezogenen Daten (z. B. Name, E‑Mail, Ressort, Funktion) nicht bearbeiten. Der Bundesrat verfügt über eine eigene Fachadministration für die Bundesratsplattform. Nutzende des Bundesrates wenden sich bitte via E‑Mail an <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>, um ihre personenbezogenen Daten bearbeiten zu lassen.',
          accountRequirement:
            'Wann benötige ich sowohl für das Planungs- und Kabinettmanagement-Programm als auch für die E‑Gesetzgebung ein Zugangskonto?',
          accountRequirementAnswer:
            'Wenn Sie legistisch in der Bundesregierung tätig sind und Prozesse zur Erstellung und Abstimmung von Regelungsentwürfen begleiten, benötigen Sie ein Zugangskonto für die E‑Gesetzgebung. Das Planungs- und Kabinettmanagement-Programm stellt hingegen Werkzeuge für Vorhabenplanung, Kabinettmanagement und Dokumentenaustausch im Rechtsetzungsverfahren mit dem Bundesrat und dem Deutschen Bundestag zur Verfügung. Eine Zustellung sämtlicher Dokumente eines Regelungsvorhabens aus der E‑Gesetzgebung an das Planungs- und Kabinettmanagement-Programm ist im Zuge der Durchführung des Kabinettverfahrens notwendig. Die E‑Gesetzgebung und das Planungs- und Kabinettmanagement-Programm haben eine gemeinsame Nutzerverwaltung (Identity & Access Management). Mit Ihren Anmeldedaten für das Planungs- und Kabinettmanagement-Programm können Sie sich in der E‑Gesetzgebung anmelden. Ebenso ist es möglich, sich mit Ihren Anmeldedaten für die E‑Gesetzgebung beim Planungs- und Kabinettmanagement-Programm anzumelden. Um das Planungs- und Kabinettmanagement-Programm nutzen zu können, ist zusätzlich eine entsprechende Freischaltung durch die <a target="_blank" href="{{pkpFachadministration}}"> PKP-Fachadministration</a> in Ihrem Haus notwendig.',
          contact: 'Wen kontaktiere ich bei Fragen zur E‑Gesetzgebung?',
          contactAnswer:
            'Bei Fragen zur E‑Gesetzgebung nutzen Sie gerne die Kontaktmöglichkeiten über den Button „Kontakt und Feedback“. Für Fragen und Anregungen steht Ihnen das Projektteam der E‑Gesetzgebung zudem jederzeit über das Supportpostfach <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a> zur Verfügung.',
          ressortWechsel: 'Was muss ich bei einem Ressortwechsel machen?',
          ressortWechselAnswer:
            'Die E‑Gesetzgebung hat eine gemeinsame Nutzerverwaltung (Identity & Access Management) mit dem Planungs- und Kabinettmanagement-Programm (PKP). Aus diesem Grund wenden Sie sich bitte an die zuständige <a target="_blank" href="{{pkpFachadministration}}">PKP-Fachadministration</a> in Ihrem Haus oder an die Support-Adresse <a href="mailto:{{pkpSupportEmail}}">{{pkpSupportEmail}}</a>, um die Daten zu aktualisieren.',
          logout: 'Wie kann ich mich abmelden?',
          logoutAnswer:
            'Solange Sie angemeldet sind, finden Sie in der Kopfzeile rechts Ihre Initialen. Sobald Sie diese anklicken, finden Sie die Möglichkeit zum „Abmelden“. Nach 30 Minuten Inaktivität werden Sie automatisch ausgeloggt.',
          deleteAccount: 'Wie kann ich mein Nutzerkonto löschen lassen?',
          deleteAccountAnswer:
            'Schreiben Sie uns bitte eine E‑Mail an <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a> und teilen Sie uns darin Ihren Wunsch mit, Ihr Nutzerkonto löschen zu lassen.',
          stellvertretungBReg: 'Wie kann ich eine Stellvertretung auf der Bundesregierungsplattform festlegen?',
          stellvertretungBT: 'Wie kann ich eine Stellvertretung auf der Bundestagsplattform festlegen?',
          stellvertretungAnswerBase:
            'Sie können eine Stellvertretung für sich im Bereich „Profil” über „Einstellungen” im Untermenüpunkt „Stellvertretungen” des Menüpunktes „Allgemein” vergeben. Eine Stellvertretung erhält in Ihrem Namen Zugriff auf Ihre E‑Gesetzgebungs-Oberfläche. Sie besitzt die gleichen Berechtigungen wie Sie und kann Ihre Vorgänge in Ihrem Namen bearbeiten.',
          stellvertretungAnswerBReg:
            ' Änderungen an der Federführung eines Regelungsvorhabens sowie an den Einstellungen zur Stellvertretung sind nicht möglich.',
          barrierefreiheit: 'Wo lassen sich die Einstellungen zur Barrierefreiheit vornehmen?',
          barrierefreiheitAnswer:
            'Sie können die Einstellungen zur Barrierefreiheit Ihres Profils im Bereich „Profil” über „Einstellungen” im Untermenüpunkt „Barrierefreiheit” des Menüpunktes „Allgemein” konfigurieren.',
        },
        modules: {
          title: 'Anwendungsspezifische Fragen',
          noAccess: 'Warum kann ich auf einige Anwendungen nicht zugreifen?',
          noAccessAnswer:
            'Die E‑Gesetzgebung steht nur angemeldeten Nutzenden in den Netzen des Bundes in Gänze zur Verfügung. Nach Ihrer Registrierung und Anmeldung auf der für Sie freigeschalteten Plattform haben Sie umfassenden Zugriff auf den gesamten Funktionsumfang der aktuellen Release-Version. Wenn Sie die E‑Gesetzgebung im <a target="_blank" href="https://plattform.egesetzgebung.bund.de">Internet</a> öffnen, haben Sie lediglich Zugriff auf einen eingeschränkten Funktionsumfang der Bundesregierungsplattform. Außerdem werden die eingegebenen Daten nur lokal im Browser und nicht in einer Datenbank gespeichert.',
          missingFunctions: 'Es fehlen wichtige Funktionen. Woran liegt das?',
          missingFunctionsAnswer:
            'Auf Basis der agilen Softwareentwicklung wächst der Funktionsumfang der Anwendungen innerhalb der E‑Gesetzgebung stetig. In zweiwöchigen Zyklen werden neue Funktionen in sog. „Sprints“ umgesetzt und zweimal im Jahr gebündelt als „Release“ ausgeliefert, d. h. für alle Nutzenden zugänglich gemacht. Für die Inhalte der Releases wurden sog. „Visionen“ aus Nutzerperspektive formuliert, in welchen die umzusetzenden Anforderungen der Bundesregierung bis Oktober 2024, für den Deutschen Bundestag bis Oktober 2025, für den Bundesrat in 2026 sowie für das Gesetzgebungsportal zunächst bis Ende 2025 festgehalten sind. Nutzen Sie in diesem Fall gern unsere Kontakt- und Feedback-Funktion oder schreiben Sie eine E‑Mail an <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>.',
          userManual: 'Gibt es ein Benutzerhandbuch für die Anwendungen der E‑Gesetzgebung?',
          userManualAnswer:
            'Für anwendungsspezifische Fragen finden Sie weiterführende Informationen sowie Anwendungshilfen im Bereich der jeweiligen Anwendung. Einige Bereiche befinden sich noch im Aufbau.',
          security: 'Ist die E‑Gesetzgebung sicher vor äußeren Eingriffen?',
          securityAnswer:
            'Die E‑Gesetzgebung implementiert ein Benutzer- und Berechtigungsmanagement (rollenbasierte Zugriffskontrolle). Nur Nutzende mit entsprechenden Rechten können Inhalte einsehen oder verändern. Die Zuweisung von Rechten erfolgt durch die inhaltserstellende Person bzw. durch die Erstellerin oder den Ersteller der Information. Dritte haben auf Ihre Informationen weder Zugriff, noch haben sie die Möglichkeit von den Informationen Kenntnis zu erlangen.<br/><br/>Auf technischer Ebene handelt es sich bei der E‑Gesetzgebung um eine Anwendung in den Netzen des Bundes, die einen Zugriff vor äußeren Eingriffen (aus dem Internet) ausschließt.',
          internalWork: 'Inwieweit greift die E‑Gesetzgebung in die internen Arbeitsabläufe ein?',
          internalWorkAnswer:
            'Mit der E‑Gesetzgebung wird eine digitale Unterstützung für alle am Rechtsetzungsprozess beteiligten Stellen angeboten, wobei keine konkreten Prozesse vorgegeben werden. Die E‑Gesetzgebung ist in Anwendungen unterteilt, die in einer individuellen Reihenfolge nutzbar sind. Interne Abläufe in den Ressorts bzw. der Verfassungsorgane können beibehalten werden.',
          integration: 'Wie ist die E‑Akte Bund in der E‑Gesetzgebung integriert?',
          integrationAnswer:
            'Mithilfe einer Downloadfunktion können durch die Nutzenden ausgewählte/bestimmte (Zwischen-)Ergebnisse der Arbeit in der E‑Gesetzgebung heruntergeladen und in die E‑Akte Bund importiert werden.',
          legalDocML: 'Was ist der Inhaltsdatenstandard LegalDocML.de und wofür wird dieser verwendet?',
          legalDocMLAnswer:
            'Der Inhaltsdatenstandard <a target="_blank" href="{{urlLegalDocML}}">LegalDocML.de</a> definiert durch Metadaten, wie rechtsförmliche Strukturen von Rechtsetzungsdokumenten digital abzubilden sind. Er gibt dazu Regeln vor, wie Dokumente in Datenstrukturen zu überführen sind und umgekehrt aus diesen wieder extrahiert werden können. Damit ist der Inhaltsdatenstandard ein kritischer Bestandteil von drei Kernfunktionen der E‑Gesetzgebung: Erstens ermöglicht er es, beliebige Fachverfahren an die E‑Gesetzgebung anzuschließen, solange Daten im Format des Inhaltsdatenstandards (XML) eingelesen (und ggf. ausgegeben) werden können. Zweitens erlaubt er eine Prüfung von digitalen Rechtsetzungsdokumenten gegen die Vorgaben des Standards, um die formelle Korrektheit des Dokuments zu bestätigen. Drittens können die Maßgaben des Inhaltsdatenstandards verwendet werden, um Nutzende bei der Erzeugung von Dokumenten zu unterstützen, indem Ihnen Vorschläge für rechtsförmlich richtige Konstellationen von Strukturelementen unterbreitet werden.',
          interaction: 'Wie wird die E‑Gesetzgebung mit eNorm interagieren? Muss man künftig beide Anwendungen nutzen?',
          interactionAnswer:
            'Für die E‑Gesetzgebung und all ihre Anwendungen besteht gegenwärtig keine Verpflichtung zur einheitlichen Nutzung. Es ist geplant, über einen Kabinettbeschluss die einheitliche Nutzung der E‑Gesetzgebung für die Bundesregierung zu erwirken. Grundvoraussetzung für ein Inkrafttreten der einheitlichen Nutzung ist, dass die Funktionsbereiche der E‑Gesetzgebung, insbesondere der Editor, mindestens dem funktionalen und technologischen Reifegrad der aktuell genutzten Lösungen aus der Perspektive der Nutzenden entsprechen.<br/><br/>Gemäß der Verwaltungsvereinbarung über die Umsetzung des Projekts „Elektronisches Gesetzgebungsverfahren des Bundes“ vom 4. September 2017 werden in die E‑Gesetzgebung Bestandssysteme in der Bundesregierung unter bestimmten Voraussetzungen integriert. So wird es beispielsweise bis zum Vollausbau des Editors in der E‑Gesetzgebung möglich sein, eNorm-Dokumente in die Anwendung „Abstimmung“ der Plattform hochzuladen. eNorm-Dokumente können darüber hinaus an das Bundeskanzleramt via Planungs- und Kabinettmanagement-Programm zugestellt werden. Bis zur Reife des Editors ist die eNorm-Nutzung unabdingbar für die legistische Arbeit. Sie gilt als eine wichtige Brückentechnologie für das Verfassen von Regelungsentwürfen.',
          operationSystems: 'Welche Betriebssysteme und Browser werden für die Nutzung der E‑Gesetzgebung unterstützt?',
          operationSystemsAnswer:
            'Die E‑Gesetzgebung wird für die aktuellen Betriebssysteme von Windows (≥10) und Apple (MacOS) entwickelt. Es werden derzeit die Browser Google Chrome, Mozilla Firefox und Microsoft Edge in der jeweils aktuellen Version unterstützt. Auf iPads und Android-Tablets kann die E‑Gesetzgebung in den aktuellen Browserversionen von Safari und Google Chrome genutzt werden.',
          applicationsBReg: 'Welche Anwendungen sind in die Bundesregierungsplattform der E‑Gesetzgebung integriert?',
          applicationsAnswerBReg:
            'Für die Bundesregierung, den Deutschen Bundestag und den Bundesrat stellt die E‑Gesetzgebung eigene Plattformen mit spezifischen Anwendungen bereit.<br/>Folgende Anwendungen sind in die Bundesregierungsplattform integriert:<br/><ul><li>Der „Verfahrensassistent“ führt Schritt für Schritt durch das Rechtsetzungsverfahren. Er bietet Informationen zu Gesetzesinitiativen von Bundesregierung sowie zu Rechtsverordnungen und allgemeinen Verwaltungsvorschriften.</li><li>Die „Arbeitshilfenbibliothek“ ermöglicht es, die aktuellen und einschlägigen Arbeitshilfen für den Rechtsetzungsprozess an einer zentralen Stelle zu finden.</li><li>Mithilfe der „Vorbereitung“ werden Legistinnen und Legisten, insbesondere in den federführenden Ressorts, bei der inhaltlichen Vorbereitung eines neuen Regelungsvorhabens unterstützt.</li><li>Nach dem Anlegen eines Regelungsvorhabens unterstützt die Anwendung „Regelungsvorhaben“ die Nutzenden bei der Realisierung des Regelungsvorhabens. Hier werden sinnvoll anknüpfende Prozessschritte angeboten, ohne eine starre Reihenfolge vorzugeben.</li><li>Der „Editor für Rechtsetzungstexte“ der E‑Gesetzgebung ermöglicht die kollaborative, medienbruchfreie und rechtsförmliche Erstellung und Bearbeitung von Regelungsentwürfen.</li><li>Die „Zeitplanung“ ermöglicht es den Legistinnen und Legisten, den kalendarischen Ablauf eines Regelungsvorhabens abzubilden.</li><li>Die „Gesetzesfolgenabschätzung“ leitet strukturiert durch die erforderlichen Prüfschritte für eine systematische Erfassung und Bewertung von erwarteten und evidenten Gesetzesfolgen. Die Ergebnisse können automatisiert in den Regelungsentwurf übernommen werden.</li><li>Mit der Anwendung „Abstimmung“ können die Regelungsentwürfe individuell, hausintern oder ressortübergreifend abgestimmt und so eine Kabinettreife herbeigeführt werden.</li><li>Daran anknüpfend kann die Kabinettvorlage erstellt und an die Hausleitung zur Disposition versandt werden. Hierfür erfolgt eine Zustellung an das Planungs- und Kabinettmanagementprogramm (PKP).</li></ul>',
          applicationsBT: 'Welche Anwendungen sind in die Bundestagsplattform der E‑Gesetzgebung integriert?',
          applicationsAnswerBT:
            'Für die Bundesregierung, den Deutschen Bundestag und den Bundesrat stellt die E‑Gesetzgebung eigene Plattformen mit spezifischen Anwendungen bereit.<br/>Folgende Anwendungen sind in die Plattform des Deutschen Bundestages integriert:<br/><ul><li>Der „Verfahrensassistent“ führt Schritt für Schritt durch das Rechtsetzungsverfahren. Er bietet aktuell Informationen zu Verfahrensschritten bei Gesetzesinitiativen der Bundesregierung sowie Rechtsverordnungen und allgemeinen Verwaltungsvorschriften.</li><li>Die „Arbeitshilfenbibliothek“ ermöglicht es, die aktuellen und einschlägigen Arbeitshilfen der Bundesregierung für den Rechtsetzungsprozess an einer zentralen Stelle einzusehen.</li><li>In der Anwendung „Vorlagen“ können zur Zeit von der Bundesregierung zugeleitete Vorlagen inkl. zugehöriger Metadaten und Dokumente vom Parlamentssekretariat empfangen und angezeigt werden. Künftig sollen in der Anwendung auch eigene Vorlagen angelegt werden können.</li><li>In der Anwendung „Bundestagsdrucksachen“ kann vom Parlamentssekretariat eine Bundestagsdrucksache zu einer Vorlage angelegt und mit den hierfür erforderlichen Daten angereichert werden.</li><li>Die Anwendung „Überweisungen“ ermöglicht die Abfrage und Aufnahme von Beteiligungswünschen der Ausschusssekretariate. Der Begriff „Überweisungswünsche“ wurde durch „Beteiligungswünsche“ ersetzt.</li><li>In der Anwendung „Fachadministration“ haben die benannten Fachadministrationen des Deutschen Bundestages die Möglichkeit zur Zuweisung, Bearbeitung und Entziehung von Rollen registrierter Nutzenden.</li></ul>',
          applicationsBR: 'Welche Anwendungen sind in die Bundesratsplattform der E‑Gesetzgebung integriert?',
          applicationsAnswerBR:
            'Für die Bundesregierung, den Deutschen Bundestag und den Bundesrat stellt die E‑Gesetzgebung eigene Plattformen mit spezifischen Anwendungen bereit.<br/>Folgende Anwendungen sind in die Plattform des Bundesrates integriert:<br/><ul><li>Der „Verfahrensassistent“ führt Schritt für Schritt durch das Rechtsetzungsverfahren. Er bietet aktuell Informationen zu Verfahrensschritten bei Gesetzesinitiativen der Bundesregierung sowie Rechtsverordnungen und allgemeinen Verwaltungsvorschriften.</li><li>Die „Arbeitshilfenbibliothek“ ermöglicht es, die aktuellen und einschlägigen Arbeitshilfen der Bundesregierung für den Rechtsetzungsprozess an einer zentralen Stelle einzusehen.</li><li>In der Anwendung „Vorlagen“ können von der Bundesregierung zugeleitete Vorlagen inkl. zugehöriger Metadaten und Dokumente empfangen und angezeigt werden.</li><li>In der Anwendung „Bundesratsdrucksachen“ kann eine Bundesratsdrucksache zu einer Vorlage angelegt und mit den hierfür erforderlichen Daten angereichert werden. Zur Bundesratsdrucksache hinterlegte Metadaten sind einsehbar und bearbeitbar. Die Reservierung, Vergabe und Bearbeitung von Drucksachennummern sind möglich. Erstellte Bundesratsdrucksachen können verteilt und Druckaufträge ausgelöst werden.</li><li>Die Anwendung „Umlaufverfahren“ ermöglicht die Durchführung eines Umlaufs zur Feststellung der Ausschussbeteiligung und des federführenden Ausschusses zu einer Vorlage im Bundesrat.</li><li>Die Anwendung „Ausschussverfahren“ unterstützt die Mitarbeitenden der Ausschussbüros bei der Vorbereitung und Durchführung von Ausschusssitzungen sowie bei der Ergebnisdokumentation.</li><li>In der Anwendung „Fachadministration“ haben die benannten Fachadministrationen des Bundesrates die Möglichkeit zur Zuweisung, Bearbeitung und Entziehung von Rollen registrierter Nutzenden.</li></ul>',
        },
        project: {
          title: 'Projektspezifische Fragen',
          projectStatus: 'Wo kann ich den derzeitigen Projektstand einsehen?',
          projectStatusAnswer:
            'Die E‑Gesetzgebung, die in den Netzen des Bundes verfügbar ist, bildet die aktuelle Release-Version der Anwendung ab. Der Funktionsumfang wird in halbjährlichen Releases kontinuierlich erweitert. Eine Anmeldung zu den Sprint Reviews der E‑Gesetzgebung ist per formloser E‑Mail an <a href="mailto:eGesetzgebung@bmi.bund.de">eGesetzgebung@bmi.bund.de</a> möglich.',
          strategyQuestions:
            'Ich habe allgemeine Fragen zum Projekt und der strategischen Ausrichtung der E‑Gesetzgebung, an wen kann ich mich wenden?',
          strategyQuestionsAnswer:
            'Bei Fragen rund um das Projekt können Sie sich gerne per E‑Mail an das Funktionspostfach wenden: <a href="mailto:eGesetzgebung@bmi.bund.de">eGesetzgebung@bmi.bund.de</a>.',
          eLegislationResponsibility: 'Wem obliegt die Federführung des Projektes?',
          eLegislationResponsibilityAnswer:
            'Das Projekt E‑Gesetzgebung ist ein Bestandteil des Programms „Dienstekonsolidierung Bund“. Die Projektverantwortung und operative Steuerung obliegen dem Bundesministerium des Innern und für Heimat (BMI).<br/><br/>Detailinformationen finden Sie im <a href="{{urlImpressum}}" target="_blank">Impressum</a>.<br/><br/>Für die Umsetzung der Bundestagsplattform und Bundesratsplattform wurde eine verfassungsorganübergreifende Projektorganisation aufgebaut, über die u. a. die Anforderungen abgestimmt und die Einbindung von Nutzenden gewährleistet werden.',
          goal: 'Was ist das langfristige Ziel der E‑Gesetzgebung?',
          goalAnswer:
            'Durch die E‑Gesetzgebung sollen bisher bestehende Medienbrüche und Redundanzen im Rechtsetzungsprozess innerhalb der Bundesregierung, dem Deutschen Bundestag und dem Bundesrat abgebaut und ein durchgängig elektronisches Rechtsetzungsverfahren implementiert werden.',
          timelineBReg: 'Wann wird der finale Funktionsumfang der Bundesregierungsplattform bereitgestellt?',
          timelineAnswerBReg:
            'Die Projektlaufzeit im Rahmen der IT‑Dienstekonsolidierung Bund endet 2024. Bis dahin wird das Minimum Viable Product im Sinne einer grundlegend funktionstüchtigen Anwendung der E‑Gesetzgebung zur Abbildung der gesetzgebungsrelevanten Prozesse auf Seiten der Bundesregierung entwickelt. Anschließend erfolgt die Pflege und Weiterentwicklung der Bundesregierungsplattform und des Editors im Rahmen der Gemeinsamen IT des Bundes (GIB) unter dem Dach der Nachfragemanagementorganisation (NMO).',
          timelineBT: 'Wann wird der finale Funktionsumfang der Bundestagsplattform bereitgestellt?',
          timelineAnswerBT:
            'Das Minimum Viable Product (MVP) der Bundestagsplattform als grundlegend funktionstüchtige Software soll nach aktueller Planung bis Ende 2025 entwickelt werden. Anschließend erfolgen die Pflege und Weiterentwicklung der Bundestagsplattform.<br/><br/>Es ist geplant, ab dem Release April 2025 den Editor der E‑Gesetzgebung in einer angepassten Version für den Deutschen Bundestag („Bundestagseditor“) bereitzustellen.<br/><br/>Der Zeitpunkt, ab dem die produktive Nutzung der E‑Gesetzgebung im Deutschen Bundestag erfolgt, hängt davon ab, dass die funktionalen Voraussetzungen für die E‑Gesetzgebung bei allen beteiligten Verfassungsorganen erfüllt sind. Der Deutsche Bundestag hat in seinem Ältestenratsbeschluss aus Oktober 2022 festgestellt, dass die einheitliche Nutzung der E‑Gesetzgebung durch alle Verfassungsorgane unabdingbare Voraussetzung für deren Anwendung ist.',
          timelineBR: 'Wann wird der finale Funktionsumfang der Bundesratsplattform bereitgestellt?',
          timelineAnswerBR:
            'Vorbehaltlich aktueller Abstimmungen mit dem Bundesrat soll das MVP der Bundesratsplattform in 2026 bereitgestellt und im Anschluss die Pflege und Weiterentwicklung der Bundesratsplattform eingeleitet werden.<br/><br/>Das MVP soll gewährleisten, dass die Verfassungsaufträge des Bundesrates in digitalisierter Form erfüllt werden - die Mitwirkung der Länder an der Bundesgesetzgebung und die Entscheidung über die Zustimmung zu Verordnungen und Verwaltungsvorschriften ebenso wie die Ausübung des Initiativrechts bezogen auf Gesetze und Rechtsverordnungen und der Mitwirkungsrechte des Bundesrates in Angelegenheiten der Europäischen Union, aber auch die Wahl von Richtern des Bundesverfassungsgerichts und die Mitwirkung bei der Besetzung von verschiedenen Gremien usw.<br/><br/>Mit dem Release Oktober 2024 wird erstmals eine für den Bundesrat mandatierte E‑Gesetzgebung zur Verfügung gestellt, die es den Nutzerinnen und Nutzern des Bundesrats ermöglicht, sich auf einer eigenen Plattform der E‑Gesetzgebung zu registrieren und anzumelden. Zukünftig soll auch der Inhaltsdatenstandard <a href="http://legaldocml.de/" target="_blank">LegalDocML.de</a> für die Bedarfe des Bundesrates weiterentwickelt und der Editor der E‑Gesetzgebung für den Bundesrat bereitgestellt werden.',
        },
        ggp: {
          title: 'Fragen zum Gesetzgebungsportal',
          visibility: 'Warum kann ich das Gesetzgebungsportal nicht sehen?',
          visibilityAnswer:
            'Das Gesetzgebungsportal soll im Zielzustand im Internet veröffentlicht werden. Bis dahin ist das Gesetzgebungsportal zu Testzwecken in den Netzen des Bundes als Modul der E‑Gesetzgebung erreichbar. Es ist in dieser Übergangsphase nur sichtbar, wenn Sie nicht mit einem Benutzerkonto angemeldet sind. Ausnahme ist hierbei die Anmeldung mit einem Nutzenden der Bundesregierung, hier ist das GGP trotzdem sichtbar.',
          information:
            'Woher stammen die Vorhaben und deren Informationen, die im Gesetzgebungsportal angezeigt werden?',
          informationAnswer:
            'Die im Gesetzgebungsportal angezeigten Vorhaben dienen der Illustration der implementierten Funktionalitäten und stellen Testdatensätze dar. Zur Erstellung dieser Datensätze wurde im Sinne eines möglichst hohen Realitätsbezuges auf echte Daten zurückgegriffen. Perspektivisch werden die Informationen aus den Anwendungen der E‑Gesetzgebung genutzt.',
        },
      },
      releaseNotes: {
        welcome: {
          title: 'Was gibt’s Neues?',
          subtitle: 'Release Notes',
          paragraph:
            'Auf dieser Seite finden Sie Informationen zu technischen und inhaltlichen Neuerungen und neuen Funktionalitäten in der E-⁠Gesetzgebung. Diese werden regelmäßig im Rahmen der Releases hinzugefügt.',
          link: 'Zu den Release Notes',
        },
        title: 'Release Notes',
        subtitle: 'Was ist neu?',
        text: 'Auf dieser Seite finden Sie Informationen zu technischen und inhaltlichen Neuerungen und neuen Funktionalitäten in der E‑Gesetzgebung. Diese werden regelmäßig im Rahmen der Releases hinzugefügt.',
        imgAlt: 'Baukran mit HTML-Tag, Laptop, Zahnradsymbolen, Käfer.',
        linkToArchive: 'Zu den Release Notes der vergangenen Releases ',
        items: [
          {
            key: 'bundesregierungsplattform',
            title: 'Bundesregierungsplattform',
            subtitle: 'Release Oktober 2024',
            data: [
              {
                title: 'Übergreifend: Menüband',
                content: `<ul>
                          <li>Die Nutzerfreundlichkeit und Barrierefreiheit des Hauptmenüs wurden optimiert. Dies umfasst einerseits die konsistente Tastatur- und Mausbedienbarkeit und andererseits die problemlose Sprachausgabe der Inhalte mithilfe von assistiven Technologien wie bspw. Screenreader.</li>
                        </ul>`,
              },
              {
                title: 'Übergreifend: Rollen- und Rechtekonzept',
                content: `<ul>
                          <li>Die Gesetzesfolgenabschätzung sowie die Zeitplanung wurden vollständig auf ein rollenbasiertes Zugriffskontrollsystem (Role Based Access Control) umgestellt. Nutzende mit den Rollen „Mitarbeit“ oder „Beobachtung“ für ein Regelungsvorhaben können nun auf zugehörige Zeitplanungen und Gesetzesfolgenabschätzungen zugreifen, ohne dass hierfür eine weitere Berechtigungsvergabe notwendig ist.</li>
                        </ul>`,
              },
              {
                title: 'Übergreifend: E-Mail-Listen',
                content: `<ul>
                          <li>Im „Adresspicker“ können Nutzende sich nun ihre persönlichen E-Mail-Listen unter dem Reiter „Meine E-Mail-Listen“ einblenden lassen. Dies erleichtert den schnellen Zugriff auf eigene E-Mail-Listen überall dort, wo Personen gesucht werden können (z. B. bei der Auswahl von Abstimmungsteilnehmenden oder der Berechtigungsvergabe).</li>
                        </ul>`,
              },
              {
                title: 'Regelungsvorhaben',
                content: `<ul>
                          <li>Das federführende Referat kann Änderungsbedarfe an einer Kabinettvorlage über die Schnittstelle mit PKP (Planungs- und Kabinettmanagement-Programm) an das Bundeskanzleramt übermitteln. Gleichermaßen werden Änderungsbedarfe aus dem Bundeskanzleramt von PKP an die E&#8209;Gesetzgebung gemeldet.</li>
                          <li>Regelungsvorhaben, die zuerst in PKP angelegt wurden, können in die E-Gesetzgebung übernommen werden.</li>
                        </ul>`,
              },
              {
                title: 'Abstimmung',
                content: `<ul>
                          <li>Die Fristen für Abstimmungen können nun als Verschweigensfrist gekennzeichnet werden, sodass etwaig ausbleibende Rückmeldungen als Verzicht auf Widerspruch gewertet werden können.</li>
                          <li>Abstimmungen werden jetzt automatisch zusammen mit dem zugehörigen Regelungsvorhaben archiviert.</li>
                          <li>Das bisherige Modul „Haus- und Ressortabstimmung“ wurde in „Abstimmung“ umbenannt.</li>
                          <li>Abstimmungen können offen oder eingeschränkt eingeleitet werden. Bei eingeschränkten Abstimmungen können Personen, die die Abstimmung weitergeleitet bekommen haben, nur nach Zustimmung teilnehmen. Bei offenen Abstimmungen entfällt hingegen die Notwendigkeit zur Teilnahmeanfrage.</li>
                        </ul>`,
              },
              {
                title: 'Gesetzesfolgenabschätzung',
                content: `<ul>
                          <li>Nutzende haben die Möglichkeit, das Modul „Experimentierklauseln“ in der Gesetzesfolgenabschätzung aufzurufen. Der Rückgriff auf eine Experimentierklausel ermöglicht es, beim Testen von innovativen Ideen und Vorhaben vom allgemeinen rechtlichen Rahmen abzuweichen.</li>
                          <li>Es steht ein Assistent für die Formulierung von Experimentierklauseln bereit. Im nächsten Schritt soll ein Assistent für die Prüfung der Erforderlichkeit von Experimentierklauseln folgen.</li>
                        </ul>`,
              },
              {
                title: 'Fachadministration',
                content: `<ul>
                          <li>Ressorts können nun Fachadministratorinnen und -administratoren benennen. Diese übernehmen für ihr Ressort spezielle administrative Aufgaben und unterstützen die effiziente Verwaltung und Koordination innerhalb der E-Gesetzgebung. Hierfür steht eine neue Anwendung zur Verfügung, welche über das Hauptmenü angesteuert wird. Dort werden der Fachadministration alle Regelungsvorhaben des eigenen Hauses angezeigt.</li>
                          <li>Fachadministratorinnen und -administratoren können die Federführung von Regelungsvorhaben im eigenen Ressort ändern. Diese Funktion ermöglicht es, Regelungsvorhaben an neue Verantwortliche zu übergeben, ohne dass die bisherige Federführung mitwirken muss.</li>
                        </ul>`,
              },
            ],
          },
          {
            key: 'editor',
            title: 'Editor',
            subtitle: 'Release Oktober 2024',
            data: [
              {
                title: 'Stellvertretung',
                content: `<ul>
                          <li>Stellvertretungen können, wie bei der Bundesregierungsplattform, in Ihrem in den „Einstellungen“ zu findenden „Profil“ vergeben werden. Stellvertretende Personen erhalten nun im Editor dieselben Berechtigungen wie die zu vertretende Person.</li>
                        </ul>`,
              },
              {
                title: 'Rechtsförmlichkeitsprüfung',
                content: `<ul>
                          <li>Die Rechtsförmlichkeitsprüfung steht Ihnen nun zunächst in Regelungstexten vom Typ Stammform in der rechten Seitenleiste zur Verfügung. Dabei wird überprüft, ob strukturelle Vorgaben der 4. Auflage des Handbuchs der Rechtsförmlichkeit (HdR) eingehalten sind.</li>
                          <li>Nutzende erhalten Hinweise zu identifizierten HdR-Verletzungen in Form einer Warnmeldung, sowie farblichen Markierungen in der Strukturanzeige (ehemals Inhaltsverzeichnis) und im Inhaltsbereich.</li>
                          <li>Die Prüfung weist Nutzende auf fehlerhafte Gliederungselemente hin, wenn
                            <ul>
                              <li>in einem Gliederungselement kein oder nur ein Element der nächsten Gliederungsebene vorhanden ist oder</li>
                              <li>mehr als fünf juristische Absätze in einer Einzelvorschrift enthalten sind.</li>
                            </ul>
                          </li>
                        </ul>`,
              },
              {
                title: 'Änderungsbefehle',
                content: `<ul>
                          <li>Im Zuge der Änderungsrechtsetzung wird die Erstellung und Ausgabe erster struktureller Änderungsbefehle nach der 4. Auflage des HdR verprobt. </li>
                          <li>Fürs Erste kann ein Regelungstext in Stammform im Bereich „Meine Dokumente“ unter Aktionen als Bestandsrecht festgelegt werden. Dies gilt bis zur baldigen Anbindung des neuen Rechtsinformationssystems des Bundes (NeuRIS).</li>
                          <li>Nach Erstellung einer neuen Version der Dokumentenmappe kann diese in einer Parallelansicht mit dem festgelegten Bestandsrecht verglichen und angepasst werden. So haben Nutzende die Möglichkeit, Änderungen vereinfacht darzustellen.</li>
                          <li>Anpassungen werden automatisiert über die Funktion „Aktualisierung der Änderungsbefehle” als Änderungsbefehle in einer neuen Spalte in Mantelform (in einem aktuell noch nicht speicherbaren Dokument) ausgegeben.
                            <ul>
                              <li>Zunächst können strukturelle Änderungsbefehle für die Operatoren „einfügen”, „streichen” und „ersetzen” unter Ausgabe der Änderungsstelle, des Operators sowie des regelungssprachlichen Teils automatisch generiert werden. </li>
                            </ul>
                          </li>
                        </ul>`,
              },
              {
                title: 'Dynamische Änderungsnachverfolgung',
                content: `<ul>
                          <li>Die dynamische Änderungsnachverfolgung ist nun (mit Ausnahme der Parallelansicht) in die rechte Seitenleiste zu finden. Sie ist von dort aus aktivierbar bzw. deaktivierbar.</li>
                          <li>Die dynamische Änderungsnachverfolgung wurde um die Funktionalität des „Annehmen bzw. Ablehnen aller Änderungen” erweitert.</li>
                        </ul>`,
              },
              {
                title: 'Kommentarfunktion',
                content: `<ul>
                          <li>Schreibberechtigte Nutzende können eigene Kommentare in bearbeitbaren Einzeldokumenten schreiben, einsehen, bearbeiten, beantworten, auflösen und löschen.</li>
                        </ul>`,
              },
              {
                title: 'PDF-Export',
                content: `<ul>
                          <li>Ein Editor-Regelungstext in Stammform kann als einzelnes PDF-Dokument oder mit weiteren Einzeldokumenten der Dokumentenmappe zusammengeführt als ZIP-Datei über die Dokumentenmappe exportiert werden.</li>
                        </ul>`,
              },
              {
                title: 'Parallelansicht',
                content: `<ul>
                          <li>Nutzende können erstmalig in mehr als einer Parallelansicht arbeiten.</li>
                        </ul>`,
              },
            ],
          },
          {
            key: 'bundestagsplattform',
            title: 'Bundestagsplattform',
            subtitle: 'Release Oktober 2024',
            data: [
              {
                title: 'Übergreifend: Stellvertretung',
                content: `<ul>
                          <li>Stellvertretungen können im Bereich „Profil” in den „Einstellungen” vergeben werden. Personen, die als Stellvertretung ausgewählt wurden, erhalten dieselben Berechtigungen, welche die zu vertretende Person innehat. Die Vergabe von Stellvertretungen wird zunächst auf Nutzende aus dem Deutschen Bundestag beschränkt.</li>
                        </ul>`,
              },
              {
                title: 'Übergreifend: E-Mail-Listen',
                content: `<ul>
                          <li>Im Bereich „E-Mail-Listen“ in den „Einstellungen” können Nutzende aus dem Deutschen Bundestag individuelle E‑Mail-Listen anlegen, um nicht nur Einzelpersonen, sondern einen gewünschten Verteilerkreis mit mehreren Personen zu adressieren.</li>
                        </ul>`,
              },
              {
                title: 'Übergreifend: Filterbar',
                content: `<ul>
                          <li>Neue Frontend Designs zur besseren Nutzerführung wurden implementiert, u. a. eine neue Filterleiste, die bestehende verschiedene Filter vereint.</li>
                        </ul>`,
              },
              {
                title: 'Editor',
                content: `<ul>
                          <li>Den Nutzenden aus dem Deutschen Bundestag wird die Anwendung „Editor“ bereits angezeigt. Der initial für die Bundesregierung entwickelte Editor wurde in einem ersten Schritt für die fachspezifischen Bedarfe des Parlamentssekretariats angepasst.</li>
                          <li>Dem Parlamentssekretariat wird die im Rahmen einer Zuleitung der Bundesregierung übermittelte Dokumentenmappe auf der Startseite des Editors angezeigt. Vorerst können ausschließlich die zugeleiteten Editordokumente weiterverarbeitet werden.</li>
                          <li>Über das Aktionsmenü der zugeleiteten Dokumentenmappe kann eine erste beispielhafte Version einer Bundestagsdrucksache als PDF-Datei erstellt werden. Diese wird unter der Dokumentenmappe verlinkt und wird bei Auswahl in einem neuen Tab geöffnet.</li>
                        </ul>`,
              },
              {
                title: 'Vorlagen',
                content: `<ul>
                          <li>Die Anwendung „Vorlagen“ (ehemals: „Regelungsvorhaben“) wurde umbenannt und hinsichtlich der dargestellten Begrifflichkeiten überarbeitet.</li>
                          <li>Das Parlamentssekretariat kann Zuleitungen von Vorlagen der Bundesregierung empfangen (inkl. der zugehörigen von der Bundesregierung angelegten Metadaten, der Editor-Dokumentenmappe und ggf. weiterer externer zugelieferter Dokumente).</li>
                        </ul>`,
              },
              {
                title: 'Bundestagsdrucksachen',
                content: `<ul>
                          <li>Im Reiter „Annahmestelle” wurden umfangreiche Entwicklungen zur Verbesserung der Nutzeroberfläche vorgenommen (z. B. Erweiterung der Sortier- und Filtermöglichkeiten).</li>
                          <li>Die Funktionen beim Anlegen der Reservierung einer Drucksachennummer wurden weiterentwickelt. Für einen besseren Überblick können zudem CSV-Reports über vergebene und reservierte Drucksachennummern vom Parlamentssekretariat erstellt werden.</li>
                          <li>Die Anreicherung von Metadaten bei der Erstellung einer Bundestagsdrucksache wurde weiterentwickelt. Die Eintragung von Sperrfristen für die Verteilung wird auf die Zukunft beschränkt. Kurz vor Ablauf einer Sperrfrist wird das Parlamentssekretariat auf der Startseite der Bundestagsplattform unter „Neuigkeiten” benachrichtigt.</li>
                        </ul>`,
              },
              {
                title: 'Überweisungen',
                content: `<ul>
                          <li>Die neue Anwendung „Überweisungen“ ermöglicht die Abfrage und Aufnahme von Beteiligungswünschen der Ausschusssekretariate.</li>
                          <li>Dem Parlamentssekretariat werden im Reiter „Noch nicht bearbeitete Vorlagen“ vorerst sämtliche Bundestagsdrucksachen angezeigt, deren Überweisung an die Ausschüsse zu klären ist.</li>
                          <li>Im Reiter „Noch nicht bearbeitete Vorlagen” kann das Parlamentssekretariat Vorlagen zur Aufsetzung auf eine Assistenzliste auswählen.</li>
                          <li>Im Reiter „Assistenzliste” kann das Parlamentssekretariat eine konkrete Assistenzliste für die Eintragung von Beteiligungswünschen durch die Ausschusssekretariate freigeben. Hierbei ist eine Rückmeldefrist anzugeben, welche nachträglich bearbeitet werden kann.</li>
                          <li>Den Ausschusssekretariaten werden die mit den freigegebenen Assistenzlisten übermittelten Vorlagen im Reiter „Vorlagenliste” angezeigt. In der Detailansicht mit Informationen zur Vorlage kann der Beteiligungswunsch des Ausschusssekretariats eingetragen, im Rahmen der vorgegebenen Frist bearbeitet und schließlich für das Parlamentssekretariat freigegeben werden. Hier werden ebenso alle übermittelten Beteiligungswünsche der anderen Ausschusssekretariate angezeigt.</li>
                          <li>Die von den Ausschusssekretariaten übermittelten Beteiligungswünsche werden dem Parlamentssekretariat im Reiter „Assistenzliste” konsolidiert angezeigt. Es wird auf doppelte und fehlende Beteiligungswünsche für die federführende Beratung einer Vorlage hingewiesen.</li>
                        </ul>`,
              },
              {
                title: 'Fachadministration & Rechte und Rollen',
                content: `<ul>
                          <li>Die Berechtigung zur Freischaltung der angefragten Registrierungen von Nutzenden aus dem Deutschen Bundestag wurde auf die „Fachadministrationen” begrenzt.</li>
                          <li>Für das Parlamentssekretariat und die Ausschusssekretariate kann die Fachadministration unabhängig die Nutzerverwaltung der zugeordneten Anwendungen ausüben.</li>
                          <li>Die Vergabe der Rolle „Fachadministration” an mindestens einen Nutzenden wurde abgesichert.</li>
                          <li>Die Anwendungen im Hamburger-Menü werden Nutzenden aus dem Deutschen Bundestag entsprechend der Ihnen individuell zugewiesenen Rollenberechtigungen angezeigt.</li>
                          <li>Die Ansicht in der Anwendung „Fachadministration“ wurde überarbeitet. Im Rahmen der Nutzerverwaltung wird neben der Spalte „Berechtigungen” eine zusätzliche Spalte zur Auswahl des jeweiligen Referats angezeigt.</li>
                        </ul>`,
              },
            ],
          },
          {
            key: 'bundesratsplattform',
            title: 'Bundesratsplattform',
            subtitle: 'Release Oktober 2024',
            data: [
              {
                title: 'Veröffentlichung der Bundesratsplattform',
                content: `<ul>
                          <li>Mit dem Release Oktober 2024 wird die E-Gesetzgebung um einen eigenen Mandanten für den Bundesrat erweitert. Für Nutzende mit einer E-Mail-Adresse des Bundesrates ist nun eine auf den Bundesrat angepasste Version der E-Gesetzgebung, die bereits erste bundesratsspezifische Anforderungen bereithält, verfügbar. Neben dem Zugriff auf die Arbeitshilfenbibliothek sowie den Verfahrensassistenten der E&#8209;Gesetzgebung beinhaltet der Mandant die im Folgenden genannten Anwendungen und Funktionen.  </li>
                        </ul>`,
              },
              {
                title: 'Fachadministration',
                content: `<ul>
                          <li>Die Anwendung „Fachadministration“ dient als Grundlage für die Nutzerverwaltung der Bundesratsplattform. Die Fachadministratorinnen und -administratoren des Bundesrates können registrierte Nutzende der Bundesratsplattform suchen und ihnen Rollen zuweisen und entziehen. </li>
                        </ul>`,
              },
              {
                title: 'Regelungsvorhaben',
                content: `<ul>
                          <li>In der Anwendung „Regelungsvorhaben“ werden die zugeleiteten Regelungsvorhaben samt weiterführender Informationen wie z.B. der Kurztitel, der Vorhabentyp, das Eingangs- und Zuleitungsdatum, das federführende Ressort der Bundesregierung sowie die federführenden Ausschüsse im Bundesrat sowie Deutschen Bundestag angezeigt.  </li>
                          <li>Die Regelungsvorhaben können nach Vorhabentyp gefiltert und nach dem Datum der Zuleitung sortiert werden. </li>
                          <li>Im Übersichtsbereich „Nächste Schritte“ eines ausgewählten Regelungsvorhabens, werden die Aufgaben zur weiteren Bearbeitung des zugeleiteten Regelungsvorhabens dargestellt. </li>
                          <li>Im „Datenblatt der Bundesregierung“ können relevante Daten zu dem von der Bundesregierung zugeleiteten Regelungsvorhaben eingesehen werden (z.&nbsp;B. Bezeichnungen, Beteiligte, Vorhabenart). Ein Export als PDF-Datei ist möglich. </li>
                          <li>Unter „Zuleitungen“ sind sämtliche mit einem Regelungsvorhaben zugeleiteten Dokumente, die nicht im Editor erstellt wurden, abrufbar. </li>
                        </ul>`,
              },
              {
                title: 'Bundesratsdrucksachen',
                content: `<ul>
                          <li>Im Reiter „Annahmestelle“ der Anwendung „Bundesratsdrucksachen“ werden die zugeleiteten Regelungsvorhaben angezeigt. Diese können nach dem Zuleitungsdatum sortiert sowie nach Status, Initiantinnen und Initianten und Typ gefiltert werden. </li>
                          <li>Eine Bundesratsdrucksache wird zu einem zugeleiteten Regelungsvorhaben angelegt und mit erforderlichen Daten angereichert werden. Eine Editierung der Drucksachendaten wird ermöglicht. </li>
                          <li>Für zugeleitete Regelungsvorhaben können Bundesratsdrucksachennummern vergeben werden. Dabei ist das aktuelle Jahr bereits vom System vorausgefüllt und die laufende Nummer kann manuell vergeben werden.  </li>
                          <li>Zuleitungs- und Eingangsdatum eines Regelungsvorhabens werden erfasst. </li>
                          <li>Es besteht die Möglichkeit für Mitarbeitende der Annahmestelle, Beschreibungen zur jeweiligen Bundesratsdrucksachennummer einzugeben. </li>
                          <li>Das „Datenblatt der Bundesregierung“ mit relevanten Daten zu dem von der Bundesregierung zugeleiteten Regelungsvorhaben (z.&nbsp;B. Bezeichnungen, Beteiligte, Vorhabenart) kann auch in der Anwendung „Bundesratsdrucksachen“ eingesehen werden. </li>
                          <li>Die bereits verwendeten Nummern für Bundesratsdrucksachen können im Reiter „Drucksachennummern verwalten“ als CSV heruntergeladen werden. </li>
                        </ul>`,
              },
              {
                title: 'Umlaufverfahren',
                content: `<ul>
                          <li>Im Reiter „Meine Umläufe“ in der Anwendung Umlaufverfahren, werden die zu einem Regelungsvorhaben gestarteten Umlaufverfahren sowie die entsprechende Bundesratsdrucksachennummer, das Erstelldatum, der Status und Aktionsmöglichkeiten angezeigt.  </li>
                          <li>Über „Neuen Umlauf einleiten“ kann ein neuer Umlauf zu einem Regelungsvorhaben gestartet werden.</li>
                          <li>Unter „Allgemeine Informationen“ kann das dazugehörige Regelungsvorhaben zu einem Umlauf angegeben werden. Anhand dieser Information wird die Bundesratsdrucksachennummer und die Dokumentenmappe automatisch ermittelt. </li>
                          <li>Unter „Vorschlag zur Federführung“ kann ein Bundesratsausschuss für die Federführung vorgeschlagen werden.  </li>
                          <li>Unter „Adressatinnen und Adressaten“ können neben den für die Teilnahme vorausgewählten Ausschüsse des Bundesrates weitere Adressatinnen und Adressaten zur Kenntnisnahme hinzugefügt werden.  </li>
                          <li>Unter „Einladungs-E-Mail“ kann die zur Teilnahme an einem Umlaufverfahren aufrufende E-Mail gefertigt werden. Neben einem automatisiertem Einladungstext kann der Betreff der Einladung angepasst und zusätzliche Hinweise verfasst werden. Insofern das Regelungsvorhaben bereits ausgewählt wurde, generiert sich ebenfalls der Betreff der Einladung automatisch, kann aber anschließend manuell bearbeitet werden. </li>
                          <li>Sobald alle Pflichtfelder befüllt sind, kann der Umlauf über „Umlauf jetzt einleiten“ eingeleitet und das entsprechende Umlaufverfahren in der Übersicht eingesehen werden. </li>
                        </ul>`,
              },
            ],
          },
          {
            key: 'gesetzgebungsportal',
            title: 'Gesetzgebungsportal',
            subtitle: 'Release Oktober 2024',
            data: [
              {
                title: 'Übergreifend',
                content: `<ul>
                          <li>Das Gesetzgebungsportal wurde zunächst als Modul der E-Gesetzgebung in den Netzen des Bundes umgesetzt.</li>
                          <li>Bei in diesem Release angezeigten Vorhaben, Phasen und Dokumenten handelt es sich nicht um reale Daten, sondern zuvor ausgewählte Testdaten.</li>
                        </ul>`,
              },
              {
                title: 'Übersichtsseite',
                content: `<ul>
                          <li>Die Übersichtsseite stellt die Startseite des Gesetzgebungsportals dar.</li>
                          <li>Auf der Übersichtsseite wurde eine Kachelansicht sowie eine Listenansicht für die im Portal anzuzeigenden Vorhaben implementiert.</li>
                          <li>Nutzende erhalten Informationen zu Typ, Kurztitel, Initiant, zugeordneten Themengebieten, der aktuellen Phase im Gesetzgebungsprozess sowie dem Phasenstatus („Offen“, „In Bearbeitung“ und „Abgeschlossen“).</li>
                        </ul>`,
              },
              {
                title: 'Such- und Filterfunktion',
                content: `<ul>
                          <li>Über eine Navigationsleiste haben die Nutzende die Möglichkeit, die Vorhaben zu filtern, zu sortieren und zu durchsuchen.</li>
                          <li>Die Filterfunktion ermöglicht es den Nutzenden, nach „Initiant“, „Themengebiet“, „aktuelle Phase“ sowie „Eingangsdatum“ zu filtern.</li>
                          <li>Die Suchfunktion umfasst eine Freitext-Suche nach den Titeln der Regelungsvorhaben. Sollte zuvor ein Filter angewandt worden sein, so findet die Suche nur in der gefilterten Datenbasis statt.</li>
                        </ul>`,
              },
              {
                title: 'Detailseite',
                content: `<ul>
                          <li>Die Detailseite bietet den Nutzenden eine detailliertere Ansicht eines Vorhabens.</li>
                          <li>Im oberen Teil der Detailseite werden Typ, Kurztitel, Langtitel, Eingangsdatum im GGP, die Merklistenfunktionalität, Initiant, Federführung, beteiligte Ressorts sowie zugeordnete Themengebiete angezeigt.</li>
                          <li>Im unteren Teil der Detailseite werden die aktuelle Phase inklusive einer allgemeinen Beschreibung der Phase, der Phasenstatus sowie der Phase zugehörige Dokumente dargestellt.</li>
                          <li>Weiterhin wird eine Ansicht aller Dokumente, welche dem Vorhaben zugeordnet und für eine Veröffentlichung im Gesetzgebungsportal vorgesehen sind, angeboten.</li>
                          <li>Eine Downloadfunktion für die im Gesetzgebungsportal vorhandenen Dokumente eines Vorhabens wird angeboten. </li>
                          <li>Nutzende können zwischen den abgeschlossenen und offenen Phasen sowie der aktuell in Bearbeitung befindlichen Phase des Vorhabens wechseln.</li>
                        </ul>`,
              },
              {
                title: 'Merkliste',
                content: `<ul>
                          <li>Die implementierte Merklistenfunktion bietet Nutzenden, die Möglichkeit einzelne Vorhaben zu markieren und sie ihrer Merkliste hinzuzufügen. Die gemerkten Vorhaben werden lokal im Browser gespeichert (Local Storage, ohne Cookies).</li>
                          <li>Die Merkliste kann über den sternförmigen Button im oberen, rechten Bereich des Gesetzgebungsportals erreicht werden.</li>
                          <li>Auf der Merkliste werden alle ausgewählten Vorhaben in einer Kachel- oder Listenansicht angezeigt. Auch hier steht eine Navigationsleiste zur Filterung, Sortierung und zum Suchen bereit.</li>
                          <li>Nutzende werden beim Entfernen eines Vorhabens in einem Hinweisfeld gefragt, ob sie das gewählte Vorhaben wirklich entfernen wollen.</li>
                        </ul>`,
              },
            ],
          },
        ],
        bundesregierungsplattform: {
          title: 'Bundesregierungsplattform',
          subtitle: 'Release Oktober 2024',
          data: [
            {
              title: 'Übergreifend: Menüband',
              content: `<ul>
                          <li>Die Nutzerfreundlichkeit und Barrierefreiheit des Hauptmenüs wurden optimiert. Dies umfasst einerseits die konsistente Tastatur- und Mausbedienbarkeit und andererseits die problemlose Sprachausgabe der Inhalte mithilfe von assistiven Technologien wie bspw. Screenreader.</li>
                        </ul>`,
            },
            {
              title: 'Übergreifend: Rollen- und Rechtekonzept',
              content: `<ul>
                          <li>Die Gesetzesfolgenabschätzung sowie die Zeitplanung wurden vollständig auf ein rollenbasiertes Zugriffskontrollsystem (Role Based Access Control) umgestellt. Nutzende mit den Rollen „Mitarbeit“ oder „Beobachtung“ für ein Regelungsvorhaben können nun auf zugehörige Zeitplanungen und Gesetzesfolgenabschätzungen zugreifen, ohne dass hierfür eine weitere Berechtigungsvergabe notwendig ist.</li>
                        </ul>`,
            },
            {
              title: 'Übergreifend: E-Mail-Listen',
              content: `<ul>
                          <li>Im „Adresspicker“ können Nutzende sich nun ihre persönlichen E-Mail-Listen unter dem Reiter „Meine E-Mail-Listen“ einblenden lassen. Dies erleichtert den schnellen Zugriff auf eigene E-Mail-Listen überall dort, wo Personen gesucht werden können (z. B. bei der Auswahl von Abstimmungsteilnehmenden oder der Berechtigungsvergabe).</li>
                        </ul>`,
            },
            {
              title: 'Regelungsvorhaben',
              content: `<ul>
                          <li>Das federführende Referat kann Änderungsbedarfe an einer Kabinettvorlage über die Schnittstelle mit PKP (Planungs- und Kabinettmanagement-Programm) an das Bundeskanzleramt übermitteln. Gleichermaßen werden Änderungsbedarfe aus dem Bundeskanzleramt von PKP an die E-Gesetzgebung gemeldet.</li>
                          <li>Regelungsvorhaben, die zuerst in PKP angelegt wurden, können in die E-Gesetzgebung übernommen werden.</li>
                        </ul>`,
            },
            {
              title: 'Abstimmung',
              content: `<ul>
                          <li>Die Fristen für Abstimmungen können nun als Verschweigensfrist gekennzeichnet werden, sodass etwaig ausbleibende Rückmeldungen als Verzicht auf Widerspruch gewertet werden können.</li>
                          <li>Abstimmungen werden jetzt automatisch zusammen mit dem zugehörigen Regelungsvorhaben archiviert.</li>
                          <li>Das bisherige Modul „Haus- und Ressortabstimmung“ wurde in „Abstimmung“ umbenannt.</li>
                          <li>Abstimmungen können offen oder eingeschränkt eingeleitet werden. Bei eingeschränkten Abstimmungen können Personen, die die Abstimmung weitergeleitet bekommen haben, nur nach Zustimmung teilnehmen. Bei offenen Abstimmungen entfällt hingegen die Notwendigkeit zur Teilnahmeanfrage.</li>
                        </ul>`,
            },
            {
              title: 'Gesetzesfolgenabschätzung',
              content: `<ul>
                          <li>Nutzende haben die Möglichkeit, das Modul „Experimentierklauseln“ in der Gesetzesfolgenabschätzung aufzurufen. Der Rückgriff auf eine Experimentierklausel ermöglicht es, beim Testen von innovativen Ideen und Vorhaben vom allgemeinen rechtlichen Rahmen abzuweichen.</li>
                          <li>Es steht ein Assistent für die Formulierung von Experimentierklauseln bereit. Im nächsten Schritt soll ein Assistent für die Prüfung der Erforderlichkeit von Experimentierklauseln folgen.</li>
                        </ul>`,
            },
            {
              title: 'Fachadministration',
              content: `<ul>
                          <li>Ressorts können nun Fachadministratorinnen und -administratoren benennen. Diese übernehmen für ihr Ressort spezielle administrative Aufgaben und unterstützen die effiziente Verwaltung und Koordination innerhalb der E-Gesetzgebung. Hierfür steht eine neue Anwendung zur Verfügung, welche über das Hauptmenü angesteuert wird. Dort werden der Fachadministration alle Regelungsvorhaben des eigenen Hauses angezeigt.</li>
                          <li>Fachadministratorinnen und -administratoren können die Federführung von Regelungsvorhaben im eigenen Ressort ändern. Diese Funktion ermöglicht es, Regelungsvorhaben an neue Verantwortliche zu übergeben, ohne dass die bisherige Federführung mitwirken muss.</li>
                        </ul>`,
            },
          ],
        },
        editor: {
          title: 'Editor',
          subtitle: 'Release Oktober 2024',
          data: [
            {
              title: 'Stellvertretung',
              content: `<ul>
                          <li>Stellvertretungen können, wie bei der Bundesregierungsplattform, in Ihrem in den „Einstellungen“ zu findenden „Profil“ vergeben werden. Stellvertretende Personen erhalten nun im Editor dieselben Berechtigungen wie die zu vertretende Person.</li>
                        </ul>`,
            },
            {
              title: 'Rechtsförmlichkeitsprüfung',
              content: `<ul>
                          <li>Die Rechtsförmlichkeitsprüfung steht Ihnen nun zunächst in Regelungstexten vom Typ Stammform in der rechten Seitenleiste zur Verfügung. Dabei wird überprüft, ob strukturelle Vorgaben der 4. Auflage des Handbuchs der Rechtsförmlichkeit (HdR) eingehalten sind.</li>
                          <li>Nutzende erhalten Hinweise zu identifizierten HdR-Verletzungen in Form einer Warnmeldung, sowie farblichen Markierungen in der Strukturanzeige (ehemals Inhaltsverzeichnis) und im Inhaltsbereich.</li>
                          <li>Die Prüfung weist Nutzende auf fehlerhafte Gliederungselemente hin, wenn
                            <ul>
                              <li>in einem Gliederungselement kein oder nur ein Element der nächsten Gliederungsebene vorhanden ist oder</li>
                              <li>mehr als fünf juristische Absätze in einer Einzelvorschrift enthalten sind.</li>
                            </ul>
                          </li>
                        </ul>`,
            },
            {
              title: 'Änderungsbefehle',
              content: `<ul>
                          <li>Im Zuge der Änderungsrechtsetzung wird die Erstellung und Ausgabe erster struktureller Änderungsbefehle nach der 4. Auflage des HdR verprobt. </li>
                          <li>Fürs Erste kann ein Regelungstext in Stammform im Bereich „Meine Dokumente“ unter Aktionen als Bestandsrecht festgelegt werden. Dies gilt bis zur baldigen Anbindung des neuen Rechtsinformationssystems des Bundes (NeuRIS).</li>
                          <li>Nach Erstellung einer neuen Version der Dokumentenmappe kann diese in einer Parallelansicht mit dem festgelegten Bestandsrecht verglichen und angepasst werden. So haben Nutzende die Möglichkeit, Änderungen vereinfacht darzustellen.</li>
                          <li>Anpassungen werden automatisiert über die Funktion „Aktualisierung der Änderungsbefehle” als Änderungsbefehle in einer neuen Spalte in Mantelform (in einem aktuell noch nicht speicherbaren Dokument) ausgegeben.
                            <ul>
                              <li>Zunächst können strukturelle Änderungsbefehle für die Operatoren „einfügen”, „streichen” und „ersetzen” unter Ausgabe der Änderungsstelle, des Operators sowie des regelungssprachlichen Teils automatisch generiert werden. </li>
                            </ul>
                          </li>
                        </ul>`,
            },
            {
              title: 'Dynamische Änderungsnachverfolgung',
              content: `<ul>
                          <li>Die dynamische Änderungsnachverfolgung ist nun (mit Ausnahme der Parallelansicht) in die rechte Seitenleiste zu finden. Sie ist von dort aus aktivierbar bzw. deaktivierbar.</li>
                          <li>Die dynamische Änderungsnachverfolgung wurde um die Funktionalität des „Annehmen bzw. Ablehnen aller Änderungen” erweitert.</li>
                        </ul>`,
            },
            {
              title: 'Kommentarfunktion',
              content: `<ul>
                          <li>Schreibberechtigte Nutzende können eigene Kommentare in bearbeitbaren Einzeldokumenten schreiben, einsehen, bearbeiten, beantworten, auflösen und löschen.</li>
                        </ul>`,
            },
            {
              title: 'PDF-Export',
              content: `<ul>
                          <li>Ein Editor-Regelungstext in Stammform kann als einzelnes PDF-Dokument oder mit weiteren Einzeldokumenten der Dokumentenmappe zusammengeführt als ZIP-Datei über die Dokumentenmappe exportiert werden.</li>
                        </ul>`,
            },
            {
              title: 'Parallelansicht',
              content: `<ul>
                          <li>Nutzende können erstmalig in mehr als einer Parallelansicht arbeiten.</li>
                        </ul>`,
            },
          ],
        },
        bundestagsplattform: {
          title: 'Bundestagsplattform',
          subtitle: 'Release Oktober 2024',
          data: [
            {
              title: 'Übergreifend: Stellvertretung',
              content: `<ul>
                          <li>Stellvertretungen können im Bereich „Profil” in den „Einstellungen” vergeben werden. Personen, die als Stellvertretung ausgewählt wurden, erhalten dieselben Berechtigungen, welche die zu vertretende Person innehat. Die Vergabe von Stellvertretungen wird zunächst auf Nutzende aus dem Deutschen Bundestag beschränkt.</li>
                        </ul>`,
            },
            {
              title: 'Übergreifend: E-Mail-Listen',
              content: `<ul>
                          <li>Im Bereich „E-Mail-Listen“ in den „Einstellungen” können Nutzende aus dem Deutschen Bundestag individuelle E‑Mail-Listen anlegen, um nicht nur Einzelpersonen, sondern einen gewünschten Verteilerkreis mit mehreren Personen zu adressieren.</li>
                        </ul>`,
            },
            {
              title: 'Übergreifend: Filterbar',
              content: `<ul>
                          <li>Neue Frontend Designs zur besseren Nutzerführung wurden implementiert, u. a. eine neue Filterleiste, die bestehende verschiedene Filter vereint.</li>
                        </ul>`,
            },
            {
              title: 'Editor',
              content: `<ul>
                          <li>Den Nutzenden aus dem Deutschen Bundestag wird die Anwendung „Editor“ bereits angezeigt. Der initial für die Bundesregierung entwickelte Editor wurde in einem ersten Schritt für die fachspezifischen Bedarfe des Parlamentssekretariats angepasst.</li>
                          <li>Dem Parlamentssekretariat wird die im Rahmen einer Zuleitung der Bundesregierung übermittelte Dokumentenmappe auf der Startseite des Editors angezeigt. Vorerst können ausschließlich die zugeleiteten Editordokumente weiterverarbeitet werden.</li>
                          <li>Über das Aktionsmenü der zugeleiteten Dokumentenmappe kann eine erste beispielhafte Version einer Bundestagsdrucksache als PDF-Datei erstellt werden. Diese wird unter der Dokumentenmappe verlinkt und wird bei Auswahl in einem neuen Tab geöffnet.</li>
                        </ul>`,
            },
            {
              title: 'Vorlagen',
              content: `<ul>
                          <li>Die Anwendung „Vorlagen“ (ehemals: „Regelungsvorhaben“) wurde umbenannt und hinsichtlich der dargestellten Begrifflichkeiten überarbeitet.</li>
                          <li>Das Parlamentssekretariat kann Zuleitungen von Vorlagen der Bundesregierung empfangen (inkl. der zugehörigen von der Bundesregierung angelegten Metadaten, der Editor-Dokumentenmappe und ggf. weiterer externer zugelieferter Dokumente).</li>
                        </ul>`,
            },
            {
              title: 'Bundestagsdrucksachen',
              content: `<ul>
                          <li>Im Reiter „Annahmestelle” wurden umfangreiche Entwicklungen zur Verbesserung der Nutzeroberfläche vorgenommen (z. B. Erweiterung der Sortier- und Filtermöglichkeiten).</li>
                          <li>Die Funktionen beim Anlegen der Reservierung einer Drucksachennummer wurden weiterentwickelt. Für einen besseren Überblick können zudem CSV-Reports über vergebene und reservierte Drucksachennummern vom Parlamentssekretariat erstellt werden.</li>
                          <li>Die Anreicherung von Metadaten bei der Erstellung einer Bundestagsdrucksache wurde weiterentwickelt. Die Eintragung von Sperrfristen für die Verteilung wird auf die Zukunft beschränkt. Kurz vor Ablauf einer Sperrfrist wird das Parlamentssekretariat auf der Startseite der Bundestagsplattform unter „Neuigkeiten” benachrichtigt.</li>
                        </ul>`,
            },
            {
              title: 'Überweisungen',
              content: `<ul>
                          <li>Die neue Anwendung „Überweisungen“ ermöglicht die Abfrage und Aufnahme von Beteiligungswünschen der Ausschusssekretariate.</li>
                          <li>Dem Parlamentssekretariat werden im Reiter „Noch nicht bearbeitete Vorlagen“ vorerst sämtliche Bundestagsdrucksachen angezeigt, deren Überweisung an die Ausschüsse zu klären ist.</li>
                          <li>Im Reiter „Noch nicht bearbeitete Vorlagen” kann das Parlamentssekretariat Vorlagen zur Aufsetzung auf eine Assistenzliste auswählen.</li>
                          <li>Im Reiter „Assistenzliste” kann das Parlamentssekretariat eine konkrete Assistenzliste für die Eintragung von Beteiligungswünschen durch die Ausschusssekretariate freigeben. Hierbei ist eine Rückmeldefrist anzugeben, welche nachträglich bearbeitet werden kann.</li>
                          <li>Den Ausschusssekretariaten werden die mit den freigegebenen Assistenzlisten übermittelten Vorlagen im Reiter „Vorlagenliste” angezeigt. In der Detailansicht mit Informationen zur Vorlage kann der Beteiligungswunsch des Ausschusssekretariats eingetragen, im Rahmen der vorgegebenen Frist bearbeitet und schließlich für das Parlamentssekretariat freigegeben werden. Hier werden ebenso alle übermittelten Beteiligungswünsche der anderen Ausschusssekretariate angezeigt.</li>
                          <li>Die von den Ausschusssekretariaten übermittelten Beteiligungswünsche werden dem Parlamentssekretariat im Reiter „Assistenzliste” konsolidiert angezeigt. Es wird auf doppelte und fehlende Beteiligungswünsche für die federführende Beratung einer Vorlage hingewiesen.</li>
                        </ul>`,
            },
            {
              title: 'Fachadministration & Rechte und Rollen',
              content: `<ul>
                          <li>Die Berechtigung zur Freischaltung der angefragten Registrierungen von Nutzenden aus dem Deutschen Bundestag wurde auf die „Fachadministrationen” begrenzt.</li>
                          <li>Für das Parlamentssekretariat und die Ausschusssekretariate kann die Fachadministration unabhängig die Nutzerverwaltung der zugeordneten Anwendungen ausüben.</li>
                          <li>Die Vergabe der Rolle „Fachadministration” an mindestens einen Nutzenden wurde abgesichert.</li>
                          <li>Die Anwendungen im Hamburger-Menü werden Nutzenden aus dem Deutschen Bundestag entsprechend der Ihnen individuell zugewiesenen Rollenberechtigungen angezeigt.</li>
                          <li>Die Ansicht in der Anwendung „Fachadministration“ wurde überarbeitet. Im Rahmen der Nutzerverwaltung wird neben der Spalte „Berechtigungen” eine zusätzliche Spalte zur Auswahl des jeweiligen Referats angezeigt.</li>
                        </ul>`,
            },
          ],
        },
        bundesratsplattform: {
          title: 'Bundesratsplattform',
          subtitle: 'Release Oktober 2024',
          data: [
            {
              title: 'Veröffentlichung der Bundesratsplattform',
              content: `<ul>
                          <li>Mit dem Release Oktober 2024 wird die E-Gesetzgebung um einen eigenen Mandanten für den Bundesrat erweitert. Für Nutzende mit einer E-Mail-Adresse des Bundesrates ist nun eine auf den Bundesrat angepasste Version der E-Gesetzgebung, die bereits erste bundesratsspezifische Anforderungen bereithält, verfügbar. Neben dem Zugriff auf die Arbeitshilfenbibliothek sowie den Verfahrensassistenten der E&#8209;Gesetzgebung beinhaltet der Mandant die im Folgenden genannten Anwendungen und Funktionen.  </li>
                        </ul>`,
            },
            {
              title: 'Fachadministration',
              content: `<ul>
                          <li>Die Anwendung „Fachadministration“ dient als Grundlage für die Nutzerverwaltung der Bundesratsplattform. Die Fachadministratorinnen und -administratoren des Bundesrates können registrierte Nutzende der Bundesratsplattform suchen und ihnen Rollen zuweisen und entziehen. </li>
                        </ul>`,
            },
            {
              title: 'Regelungsvorhaben',
              content: `<ul>
                          <li>In der Anwendung „Regelungsvorhaben“ werden die zugeleiteten Regelungsvorhaben samt weiterführender Informationen wie z.B. der Kurztitel, der Vorhabentyp, das Eingangs- und Zuleitungsdatum, das federführende Ressort der Bundesregierung sowie die federführenden Ausschüsse im Bundesrat sowie Deutschen Bundestag angezeigt.  </li>
                          <li>Die Regelungsvorhaben können nach Vorhabentyp gefiltert und nach dem Datum der Zuleitung sortiert werden. </li>
                          <li>Im Übersichtsbereich „Nächste Schritte“ eines ausgewählten Regelungsvorhabens, werden die Aufgaben zur weiteren Bearbeitung des zugeleiteten Regelungsvorhabens dargestellt. </li>
                          <li>Im „Datenblatt der Bundesregierung“ können relevante Daten zu dem von der Bundesregierung zugeleiteten Regelungsvorhaben eingesehen werden (z.&nbsp;B. Bezeichnungen, Beteiligte, Vorhabenart). Ein Export als PDF-Datei ist möglich. </li>
                          <li>Unter „Zuleitungen“ sind sämtliche mit einem Regelungsvorhaben zugeleiteten Dokumente, die nicht im Editor erstellt wurden, abrufbar. </li>
                        </ul>`,
            },
            {
              title: 'Bundesratsdrucksachen',
              content: `<ul>
                          <li>Im Reiter „Annahmestelle“ der Anwendung „Bundesratsdrucksachen“ werden die zugeleiteten Regelungsvorhaben angezeigt. Diese können nach dem Zuleitungsdatum sortiert sowie nach Status, Initiantinnen und Initianten und Typ gefiltert werden. </li>
                          <li>Eine Bundesratsdrucksache wird zu einem zugeleiteten Regelungsvorhaben angelegt und mit erforderlichen Daten angereichert werden. Eine Editierung der Drucksachendaten wird ermöglicht. </li>
                          <li>Für zugeleitete Regelungsvorhaben können Bundesratsdrucksachennummern vergeben werden. Dabei ist das aktuelle Jahr bereits vom System vorausgefüllt und die laufende Nummer kann manuell vergeben werden.  </li>
                          <li>Zuleitungs- und Eingangsdatum eines Regelungsvorhabens werden erfasst. </li>
                          <li>Es besteht die Möglichkeit für Mitarbeitende der Annahmestelle, Beschreibungen zur jeweiligen Bundesratsdrucksachennummer einzugeben. </li>
                          <li>Das „Datenblatt der Bundesregierung“ mit relevanten Daten zu dem von der Bundesregierung zugeleiteten Regelungsvorhaben (z.&nbsp;B. Bezeichnungen, Beteiligte, Vorhabenart) kann auch in der Anwendung „Bundesratsdrucksachen“ eingesehen werden. </li>
                          <li>Die bereits verwendeten Nummern für Bundesratsdrucksachen können im Reiter „Drucksachennummern verwalten“ als CSV heruntergeladen werden. </li>
                        </ul>`,
            },
            {
              title: 'Umlaufverfahren',
              content: `<ul>
                          <li>Im Reiter „Meine Umläufe“ in der Anwendung Umlaufverfahren, werden die zu einem Regelungsvorhaben gestarteten Umlaufverfahren sowie die entsprechende Bundesratsdrucksachennummer, das Erstelldatum, der Status und Aktionsmöglichkeiten angezeigt.  </li>
                          <li>Über „Neuen Umlauf einleiten“ kann ein neuer Umlauf zu einem Regelungsvorhaben gestartet werden.</li>
                          <li>Unter „Allgemeine Informationen“ kann das dazugehörige Regelungsvorhaben zu einem Umlauf angegeben werden. Anhand dieser Information wird die Bundesratsdrucksachennummer und die Dokumentenmappe automatisch ermittelt. </li>
                          <li>Unter „Vorschlag zur Federführung“ kann ein Bundesratsausschuss für die Federführung vorgeschlagen werden.  </li>
                          <li>Unter „Adressatinnen und Adressaten“ können neben den für die Teilnahme vorausgewählten Ausschüsse des Bundesrates weitere Adressatinnen und Adressaten zur Kenntnisnahme hinzugefügt werden.  </li>
                          <li>Unter „Einladungs-E-Mail“ kann die zur Teilnahme an einem Umlaufverfahren aufrufende E-Mail gefertigt werden. Neben einem automatisiertem Einladungstext kann der Betreff der Einladung angepasst und zusätzliche Hinweise verfasst werden. Insofern das Regelungsvorhaben bereits ausgewählt wurde, generiert sich ebenfalls der Betreff der Einladung automatisch, kann aber anschließend manuell bearbeitet werden. </li>
                          <li>Sobald alle Pflichtfelder befüllt sind, kann der Umlauf über „Umlauf jetzt einleiten“ eingeleitet und das entsprechende Umlaufverfahren in der Übersicht eingesehen werden. </li>
                        </ul>`,
            },
          ],
        },
        gesetzgebungsportal: {
          title: 'Gesetzgebungsportal',
          subtitle: 'Release Oktober 2024',
          data: [
            {
              title: 'Übergreifend',
              content: `<ul>
                          <li>Das Gesetzgebungsportal wurde zunächst als Modul der E-Gesetzgebung in den Netzen des Bundes umgesetzt.</li>
                          <li>Bei in diesem Release angezeigten Vorhaben, Phasen und Dokumenten handelt es sich nicht um reale Daten, sondern zuvor ausgewählte Testdaten.</li>
                        </ul>`,
            },
            {
              title: 'Übersichtsseite',
              content: `<ul>
                          <li>Die Übersichtsseite stellt die Startseite des Gesetzgebungsportals dar.</li>
                          <li>Auf der Übersichtsseite wurde eine Kachelansicht sowie eine Listenansicht für die im Portal anzuzeigenden Vorhaben implementiert.</li>
                          <li>Nutzende erhalten Informationen zu Typ, Kurztitel, Initiant, zugeordneten Themengebieten, der aktuellen Phase im Gesetzgebungsprozess sowie dem Phasenstatus („Offen“, „In Bearbeitung“ und „Abgeschlossen“).</li>
                        </ul>`,
            },
            {
              title: 'Such- und Filterfunktion',
              content: `<ul>
                          <li>Über eine Navigationsleiste haben die Nutzende die Möglichkeit, die Vorhaben zu filtern, zu sortieren und zu durchsuchen.</li>
                          <li>Die Filterfunktion ermöglicht es den Nutzenden, nach „Initiant“, „Themengebiet“, „aktuelle Phase“ sowie „Eingangsdatum“ zu filtern.</li>
                          <li>Die Suchfunktion umfasst eine Freitext-Suche nach den Titeln der Regelungsvorhaben. Sollte zuvor ein Filter angewandt worden sein, so findet die Suche nur in der gefilterten Datenbasis statt.</li>
                        </ul>`,
            },
            {
              title: 'Detailseite',
              content: `<ul>
                          <li>Die Detailseite bietet den Nutzenden eine detailliertere Ansicht eines Vorhabens.</li>
                          <li>Im oberen Teil der Detailseite werden Typ, Kurztitel, Langtitel, Eingangsdatum im GGP, die Merklistenfunktionalität, Initiant, Federführung, beteiligte Ressorts sowie zugeordnete Themengebiete angezeigt.</li>
                          <li>Im unteren Teil der Detailseite werden die aktuelle Phase inklusive einer allgemeinen Beschreibung der Phase, der Phasenstatus sowie der Phase zugehörige Dokumente dargestellt.</li>
                          <li>Weiterhin wird eine Ansicht aller Dokumente, welche dem Vorhaben zugeordnet und für eine Veröffentlichung im Gesetzgebungsportal vorgesehen sind, angeboten.</li>
                          <li>Eine Downloadfunktion für die im Gesetzgebungsportal vorhandenen Dokumente eines Vorhabens wird angeboten. </li>
                          <li>Nutzende können zwischen den abgeschlossenen und offenen Phasen sowie der aktuell in Bearbeitung befindlichen Phase des Vorhabens wechseln.</li>
                        </ul>`,
            },
            {
              title: 'Merkliste',
              content: `<ul>
                          <li>Die implementierte Merklistenfunktion bietet Nutzenden, die Möglichkeit einzelne Vorhaben zu markieren und sie ihrer Merkliste hinzuzufügen. Die gemerkten Vorhaben werden lokal im Browser gespeichert (Local Storage, ohne Cookies).</li>
                          <li>Die Merkliste kann über den sternförmigen Button im oberen, rechten Bereich des Gesetzgebungsportals erreicht werden.</li>
                          <li>Auf der Merkliste werden alle ausgewählten Vorhaben in einer Kachel- oder Listenansicht angezeigt. Auch hier steht eine Navigationsleiste zur Filterung, Sortierung und zum Suchen bereit.</li>
                          <li>Nutzende werden beim Entfernen eines Vorhabens in einem Hinweisfeld gefragt, ob sie das gewählte Vorhaben wirklich entfernen wollen.</li>
                        </ul>`,
            },
          ],
        },
      },
      previousReleases: {
        title: 'Release Notes der vergangenen Releases',
        text: 'Auf dieser Seite finden Sie Informationen zu technischen und  inhaltlichen Neuerungen und neuen Funktionalitäten der vergangenen Releases in der E-Gesetzgebung.',
        imgAlt: 'Baukran mit HTML-Tag, Laptop, Zahnradsymbolen, Käfer.',
        linkBack: 'Zurück',
        releases: [
          {
            releaseTitle: 'April 2024',
            releaseKey: 'april2024',
            items: [
              {
                key: 'bundesregierungsplattform',
                title: 'Bundesregierungsplattform',
                subtitle: 'Release April 2024',
                data: [
                  {
                    title: 'Übergreifend: Menüband',
                    content: `<ul>
                          <li>Die Barrierefreiheit wurde weiterentwickelt: Die Anwendungsbezeichnungen sind ausgeschrieben und die Farben der Gesamtanwendung sichern einen besseren Kontrast.</li>
                        </ul>`,
                  },
                  {
                    title: 'Übergreifend: Rollen- und Rechtekonzept',
                    content: `<ul>
                          <li>Ein einheitliches Rollen- und Rechtekonzept wurde umgesetzt.</li>
                          <li>Hierfür wurden drei Rollen definiert: ,Federführung‘, ,Mitarbeit‘ und ,Beobachtung‘. Die weitreichendsten Berechtigungen liegen bei ,Federführung‘ und umfassen u. a. die Vergabe von Schreibrechten an die Rolle ,Mitarbeit‘ und Leserechten an die Rolle ,Beobachtung‘.</li>
                          <li>Die Berechtigungsvergabe gilt derzeit nur für Regelungsvorhaben sowie die zugeordneten Zeitplanungen.</li>
                        </ul>`,
                  },
                  {
                    title: 'Übergreifend: E-Mail-Listen',
                    content: `<ul>
                          <li>In den Einstellungen können E-Mail-Listen erstellt und aktualisiert werden. Die E-Mail-Listen kommen in den Anwendungen Regelungsvorhaben, Editor, Abstimmung sowie Zeitplanungsvorlagen zum Einsatz, um weitere Personen am Vorhaben zu beteiligen.</li>
                        </ul>`,
                  },
                  {
                    title: 'Regelungsvorhaben',
                    content: `<ul>
                          <li>Das Formular ,Regelungsvorhaben anlegen‘ wurde anhand des fachlichen Nutzerfeedbacks überarbeitet.</li>
                          <li>Mit der Archivierung des Regelungsvorhabens werden alle zugeordneten Zeitplanungen archiviert und für die Bearbeitung gesperrt. Das Datenblatt eines archivierten Regelungsvorhabens bleibt angezeigt.</li>
                        </ul>`,
                  },
                  {
                    title: 'Zeitplanung',
                    content: `<ul>
                          <li>Die Phasenlängen sowie Texte wurden entsprechend der Praxis angepasst.</li>
                          <li>Bei der Anlage einer Zeitplanung werden automatisch die Sitzungswochen von Deutschem Bundestag und Bundesrat sowie weitere terminliche Vorgaben wie Kabinettsitzungen berücksichtigt.</li>
                          <li>Außerdem wurden die Hinweise bei den Regelungsvorhabentypen ‚Gesetz‘ und ‚Verordnung‘ erweitert. Diese Hinweise beschreiben im jeweiligen Kontext die zu beachtenden Vorgaben und Besonderheiten.</li>
                          <li>Neben der Archivierung über das Regelungsvorhaben können einzelne Zeitplanungen archiviert werden.</li>
                        </ul>`,
                  },
                  {
                    title: 'Gesetzesfolgenabschätzung',
                    content: `<ul>
                          <li>Die automatische Übertragung und Aktualisierung der Ergebnisse der Gesetzesfolgenabschätzung ist auf zwei Wegen möglich:
                            <ul>
                              <li>über das Aktionsmenü auf der Startseite des Editors auf Ebene der Dokumentenmappe oder</li>
                              <li>über den Menüpunkt ‚Datei‘ aus dem im Editor geöffneten Einzeldokument.</li>
                            </ul>
                          </li>
                          <li>In einer Übersicht werden der Status der Fertigstellung sowie die letzte Übertragung der Inhalte in den Editor angezeigt.</li>
                          <li>Falls die Nutzenden im Editor neue Inhalte zwischen den aus der Gesetzesfolgenabschätzung übertragenen Elementen erstellt haben, werden diese Inhalte nach der veranlassten Modulaktualisierung vor den übertragenen Inhalten des Moduls platziert. Dabei wird anhand der Farbe unterschieden, welche Inhalte ihren Ursprung in der Gesetzesfolgenabschätzung haben und welche Inhalte manuell im Einzeldokument durch die Nutzenden erstellt sind.</li>
                          <li>Bei den Auswirkungen auf mittelständische Unternehmen (KMU-Test) wurde die Fertigstellungsseite grundlegend überarbeitet.</li>
                        </ul>`,
                  },
                ],
              },
              {
                key: 'editor',
                title: 'Editor',
                subtitle: 'Release April 2024',
                data: [
                  {
                    title: 'Startseite des Editors',
                    content: `<ul>
                          <li>Die Startseite des Editors zeigt die drei zuletzt bearbeiteten Einzeldokumente pro Dokumentenmappe an. </li>
                          <li>Die zuletzt angelegte Version einer Dokumentenmappe wird automatisch auf der Startseite des Editors durch ein Icon ‚Aktuellste Version‘ in der Spalte ‚Eigenschaften‘ gekennzeichnet.</li>
                          <li>Nutzende haben Zugriff auf die Versionshistorie eines Regelungsvorhabens. In dieser sind alle Dokumentenmappen enthalten, die zum jeweiligen Regelungsvorhaben erstellt wurden.</li>
                          <li>Für Dokumentenmappen unter ‚Meine Dokumente‘ ist es möglich, eine Version an die Startseite anzuheften. Wenn die angeheftete Dokumentenmappe von der Startseite gelöst wird, verschiebt sie sich automatisch in die Versionshistorie.</li>
                          <li>Bei Anlage einer neuen Version kann im Modaldialog entschieden werden, ob die bisherige Dokumentenmappe angeheftet oder in die Versionshistorie verschoben werden soll.</li>
                        </ul>`,
                  },
                  {
                    title: 'Sequentielle Kollaboration',
                    content: `<ul>
                          <li>Schreibrechte können für eine ausgewählte Version einer Dokumentenmappe mit dem Status ‚in Bearbeitung‘ vergeben werden. Mit der Vergabe der Schreibrechte hat die zuvor schreibberechtigte Person nur noch Leserechte auf die jeweiligen Dokumentenmappe.</li>
                          <li>Schreibrechte können nur an eine Person weitergegeben. Die Vergabe der Schreibrechte kann befristet werden.</li>
                          <li>Das Entziehen von Schreibrechten ist jederzeit durch die Person möglich, die zuvor Schreibrechte erteilt hat. </li>
                        </ul>`,
                  },
                  {
                    title: 'Dynamische Änderungsnachverfolgung',
                    content: `<ul>
                          <li>Eine dynamische Änderungsnachverfolgung wurde (mit Ausnahme der Parallelansicht) umgesetzt. Sie kann über den Menüpunkt ‚Ansicht‘ aktiviert oder deaktiviert werden.</li>
                          <li>Gelöschte Inhalte werden in rot und durchgestrichen, neue Inhalte in grün dargestellt. </li>
                        </ul>`,
                  },
                  {
                    title: 'Statischer Änderungsvergleich',
                    content: `<ul>
                          <li>Für den Regelungstext in Stammform kann über den Menüpunkt ‚Synopse‘ ein statischer Änderungsvergleich angezeigt werden, um einen Regelungstext mit einer anderen Version des Regelungstextes zu vergleichen. Bei der erstellten Ansicht handelt es sich um ein nicht-bearbeitbares Dokument in Tabellenform. </li>
                          <li>In der Ansicht kann zwischen einer HdR-konformen Anzeige und einer farblichen Anzeige der Änderungen gewechselt werden. Gelöschte Inhalte werden in rot und durchgestrichen, neue Inhalte in grün angezeigt. </li>
                        </ul>`,
                  },
                  {
                    title: 'PDF-Export',
                    content: `<ul>
                          <li>Vorblatt und Begründung können als einzelne PDF Dokumente oder zusammengeführt als .zip-Datei exportiert werden.</li>
                        </ul>`,
                  },
                  {
                    title: 'Gegenüberstellung in der Parallelansicht',
                    content: `<ul>
                          <li>Mehrere Einzeldokumente desselben Typs können mit der ‚Parallelansicht‘ auf der jeweiligen Gliederungselementebene komparabel angezeigt werden. </li>
                          <li>Die Höhe der jeweiligen Zeilen wird automatisch angepasst, sobald an einem der Dokumente weitergearbeitet wird und z. B. neue Gliederungselemente angelegt werden.</li>
                          <li>Im Rahmen von Abstimmungen können somit Rückmeldungen neben einer eigenen Dokumentenmappe geöffnet und innerhalb der Zellstruktur leichter miteinander verglichen und bearbeitet werden.</li>
                        </ul>`,
                  },
                  {
                    title: 'Vereinfachte Arbeit mit Dokumenten',
                    content: `<ul>
                          <li>Den Nutzenden steht nun eine ,Rückgängig/Wiederholen‘-Funktion (mit Ausnahme der Übernahme von Gesetzesfolgenabschätzungen) zur Verfügung. </li>
                          <li>Der Text ,Platzhalter‘ verschwindet beim Klicken auf ebendiesen, ohne dass ein manuelles Löschen notwendig ist. </li>
                          <li>Die Anlage von Tabellen und das Hinzufügen sowie Löschen von Elementen wurde vereinfacht.</li>
                        </ul>`,
                  },
                ],
              },
              {
                key: 'bundestagsplattform',
                title: 'Bundestagsplattform',
                subtitle: 'Release April 2024',
                data: [
                  {
                    title: 'Hinweise',
                    content: `<ul>
                          <li>Den Nutzenden der Bundestagsplattform wird bereits die Anwendung „Editor“ angezeigt. Hierbei handelt es sich um den Bundesregierungseditor, welcher im aktuellen Entwicklungsstand noch nicht spezifisch für den Deutschen Bundestag genutzt werden kann.</li>
                          <li>Die im Folgenden aufgelisteten Funktionen bilden überwiegend Arbeitsprozesse im Parlamentssekretariat des Deutschen Bundestages ab.</li>
                        </ul>`,
                  },
                  {
                    title: 'Menüband',
                    content: `<ul>
                          <li>Die Barrierefreiheit wurde weiterentwickelt: Die Anwendungsbezeichnungen sind ausgeschrieben und die Farben der Gesamtanwendung sichern einen besseren Kontrast.</li>
                        </ul>`,
                  },
                  {
                    title: 'Regelungsvorhaben',
                    content: `<ul>
                          <li>In der Anwendung „Regelungsvorhaben“ werden die zugeleiteten Regelungsvorhaben angezeigt. Die Regelungsvorhaben können nach Vorhabentyp gefiltert werden.</li>
                          <li>Im Übersichtsbereich ,Nächste Schritte‘ eines ausgewählten Regelungsvorhabens werden die Aufgaben zur weiteren Bearbeitung des zugeleiteten Regelungsvorhabens dargestellt.</li>
                          <li>Im ,Datenblatt der Bundesregierung‘ können relevante Daten zu dem von der Bundesregierung zugeleiteten Regelungsvorhaben eingesehen werden (z.&nbsp;B. Bezeichnungen, Beteiligte, Vorhabenart). Ein Export als PDF-Datei ist möglich.</li>
                          <li>Unter ,Zuleitungen‘ sind sämtliche mit einem Regelungsvorhaben zugeleiteten Dokumente, die nicht im Editor erstellt wurden, abrufbar.</li>
                        </ul>`,
                  },
                  {
                    title: 'Bundestagsdrucksachen',
                    content: `<ul>
                          <li>Im Reiter ,Annahmestelle‘ der Anwendung „Bundestagsdrucksachen“ werden die zugeleiteten Regelungsvorhaben angezeigt. Diese können nach Status, Initiantinnen und Initianten gefiltert sowie nach Zuleitungs- und Verteildatum sortiert werden.</li>
                          <li>Eine Bundestagsdrucksache kann aus einem zugeleiteten Regelungsvorhaben angelegt und mit erforderlichen Daten angereichert werden. Eine Editierung wird ermöglicht.
                            <ul>
                              <li>Für zugeleitete Regelungsvorhaben können Bundestagsdrucksachennummern vergeben werden. Dabei ist die aktuelle Wahlperiode als Standardwert vorgegeben, es können jedoch manuell andere Wahlperioden ausgewählt werden.</li>
                              <li>Eine externe Anlage zu einer Bundestagsdrucksache kann als Verschlusssache markiert und ein etwaiger Europabezug der Bundestagsdrucksache kann eingetragen werden.</li>
                              <li>Zuleitungs- und Eingangsdatum eines Regelungsvorhabens werden erfasst.</li>
                              <li>Die Bundestagsdrucksache kann mit einer Sperrfrist für die Verteilung versehen werden.</li>
                              <li>Es besteht die Möglichkeit für Mitarbeitende der Annahmestelle, Bemerkungen zur jeweiligen Bundestagsdrucksache einzugeben. Die Sichtbarkeit dieser Bemerkungen kann auf das Parlamentssekretariat beschränkt werden.</li>
                            </ul>
                          </li>
                          <li>Das ,Datenblatt der Bundesregierung‘ mit relevanten Daten zu dem von der Bundesregierung zugeleiteten Regelungsvorhaben (z.&nbsp;B. Bezeichnungen, Beteiligte, Vorhabenart) kann auch in der Anwendung „Bundestagsdrucksachen“ eingesehen werden.</li>
                          <li>Die Nummern für Bundestagsdrucksachen können im Reiter ,Drucksachennummern verwalten‘ reserviert werden. Es wird eine Bundestagsdrucksachennummer vorgeschlagen, es können jedoch auch individuell Bundestagsdrucksachennummern reserviert werden. Die reservierten Bundestagsdrucksachennummern werden in einer Listenansicht dargestellt und können später einem Regelungsvorhaben zugeordnet werden. Die Reservierung kann editiert und gelöscht werden.</li>
                        </ul>`,
                  },
                  {
                    title: 'Fachadministration',
                    content: `<ul>
                          <li>Die Anwendung „Fachadministration“ dient als Grundlage für die Nutzerverwaltung der Bundestagsplattform. Die Fachadministratorinnen und -administratoren des Deutschen Bundestages können registrierte Nutzende der Bundestagsplattform suchen und ihnen Rollen zuweisen und entziehen.</li>
                        </ul>`,
                  },
                ],
              },
            ],
          },
        ],
      },
    },
    menuItems: {
      cockpit: { label: 'Startseite', menuItem: 'Startseite' },
      hilfen: {
        label: 'Arbeitshilfenbibliothek',
        hint: 'Arbeitshilfenbibliothek',
        menuItem: 'Arbeitshilfenbibliothek',
      },

      evor: {
        label: 'Vorbereitung',
        hint: 'Vorbereitung von Regelungsentwürfen',
        menuItem: 'Vorbereitung',
      },
      enorm: {
        label: 'Abstimmung',
        hint: 'Abstimmung von Regelungsentwürfen',
        menuItem: 'Abstimmung',
      },
      pkp: {
        label: 'PKP',
        hint: 'Planungs- und Kabinettmanagement-Programm (PKP)',
        menuItem: 'Planungs- und Kabinettmanagement-Programm (PKP)',
      },
      zeit: { label: 'Zeitplanung', hint: 'Zeitplanung', menuItem: 'Zeitplanung' },
      egfa: {
        label: 'Gesetzesfolgenabschätzung',
        hint: 'Gesetzesfolgenabschätzung',
        menuItem: 'Gesetzesfolgenabschätzung',
      },
      enap: {
        label: 'Nachhaltigkeitsprüfung',
        hint: 'Nachhaltigkeitsprüfung',
        menuItem: 'Nachhaltigkeitsprüfung',
      },
      rv: { label: 'Regelungsvorhaben', hint: 'Regelungsvorhaben', menuItem: 'Regelungsvorhaben' },
      lea: { label: 'Editor', hint: 'Editor der E-Gesetzgebung', menuItem: 'Editor' },
      evir: {
        label: 'Verfahrensassistent',
        hint: 'Verfahrensassistent im Rechtsetzungsprozess',
        menuItem: 'Verfahrensassistent',
      },
      ggp: {
        label: 'Gesetzgebungsportal',
        hint: 'Gesetzgebungsportal',
        menuItem: 'Gesetzgebungsportal',
      },
      admin: {
        label: 'Fachadministration',
        hint: 'Fachadministration',
        menuItem: 'Fachadministration',
      },
    },
    pkp: {
      title: 'Planungs- und Kabinettmanagement-Programm (PKP)',
      text: 'Das gemeinsame Planungs- und Kabinettmanagement-Programm (PKP) der Bundesregierung.',
      textLink: 'Externer Link zu PKP',
      textLink2: ', welcher sich in einem separaten Browser Tab öffnet.',
    },
    lea: {
      title: 'Editor',
      text: 'Editor der E-Gesetzgebung',
      textLink: 'Externer Link zum Editor, welcher sich in einem separaten Browser Tab öffnet.',
    },
    welcome: {
      head: {
        subTitle: 'Bereitstellung Release April 2024',
        title: 'Willkommen bei der E‑Gesetzgebung!',
        login: 'Anmeldung',
        imgAlt:
          'Screenshot der eingeloggten Startseite versehen mit den neuen Funktionen Module an Editor senden, Bundestag, Kabinettverfahren und Barrierefreiheit.',
      },
      hinweis: {
        title: 'Änderungen am Rechte- und Rollensystem',
        content:
          'Mit dem neuen Release gehen wir weitere Schritte in Richtung eines neuen Berechtigungssystems. Wenn Sie bisher Lesezugriff auf einzelne Module von Gesetzesfolgenabschätzungen hatten (z. B. das Modul Erfüllungsaufwand), können Sie im Zuge der Umstellung nicht mehr hierauf zugreifen. Bitten Sie in diesen Fällen die Federführung des dazugehörigen Regelungsvorhabens um entsprechende Zugriffsrechte auf das Regelungsvorhaben.',
      },
      functionsPreview: {
        login: 'Zur Anmeldung',
        releaseNotes: 'Mehr dazu in den Release Notes',
        subTitle: 'Im Überblick',
        title: 'Folgende neue Funktionalitäten stehen ab jetzt für Sie zur Verfügung',
        egfa: {
          heading: '"Gesetzesfolgenabschätzung" – Abbildung der gesamten Prüfanforderungen der GGO',
          content:
            'Die Anwendung "Gesetzesfolgenabschätzung" zur Gesetzesfolgenabschätzung bildet nun in zwölf Modulen die Gesamtheit der Anforderungen der Gemeinsamen Geschäftsordnung der Bundesministerien an die Folgenabschätzung bei Regelungsvorhaben der Bundesregierung ab. Die Module leiten Sie Schritt für Schritt durch die Prüfungen – von den Auswirkungen auf die öffentlichen Haushalte über den Erfüllungsaufwand und die Nachhaltigkeitsprüfung bis zur nun neu verfügbaren Evaluierung. Nach Abschluss der Module können Sie die Ergebnisse in das Vorblatt und die Begründung Ihres Regelungsentwurfs übertragen.',
        },
        erweiterungen: {
          heading: 'Erweiterungen in der Plattform',
          content:
            'Die Anwendungen der E-Gesetzgebung wurden um verschiedene Funktionalitäten erweitert. Im Rahmen der Anwendung "Abstimmung" können Sie nun Fristverlängerungen beantragen und Unterabstimmungen einleiten. In der Anwendung "Zeitplanung" sind ab jetzt auch Zeichnungen möglich. Außerdem können Sie Schulferien, Sitzungstermine und bundeseinheitliche Feiertage in der Kalenderansicht Ihrer Zeitplanung anzeigen. Unter den Einstellungen zu Ihrem Profil haben Sie die Möglichkeit E-Mail-Benachrichtigungen zu aktivieren bzw. zu deaktivieren.',
        },
        internet: {
          heading: 'Die <span class="no-wrap">E-Gesetzgebung</span> im Internet',
          content: `Für die interessierte Öffentlichkeit bietet die schlanke Version der E-Gesetzgebung im Internet auch außerhalb der Netze des Bundes Einblicke in die Praxis der Rechtsetzung. Sie informiert über die Ziele, das Vorgehen und die aktuellen Entwicklungen im Projekt. Das Angebot umfasst darüber hinaus gegenwärtig vier Anwendungen:
            <ul>
              <li>"Nachhaltigkeitsprüfung", die Nachhaltigkeitsprüfung</li>
              <li>"Vorbereitung", die Vorbereitung von Regelungsentwürfen</li>
              <li>"Verfahrensassistent", der Verfahrensassistent im Rechtsetzungsprozess und</li>
              <li>"Arbeitshilfenbibliothek", die Bibliothek der Arbeitshilfen.</li>
            </ul>`,
        },
        bReg: {
          heading: 'Bundesregierungsplattform',
          content:
            'Mit dem Release April 2024 wurde die Zusammenarbeit an Regelungsvorhaben erheblich vereinfacht. Es können jetzt Listen erstellt werden, um mehrere Personen gleichzeitig zu berechtigen oder sie zu einer Abstimmung hinzuzufügen.',
          imgAlt: 'Schematische Darstellung des Dialogs zum Anlegen und Verwalten von E-Mail-Listen.',
        },
        editor: {
          heading: 'Editor',
          content:
            'Im Editor wurde mit dem Release April 2024 die sequenzielle Kollaboration ausgebaut. Bei der Bearbeitung einer Dokumentenmappe ist nun die Weitergabe von Schreibrechten an eine andere Person möglich. Während die Schreibrechte für eine Dokumentenversion an eine Person gebunden sind, können mehrere Personen zum Lesen berechtigt werden.',
          imgAlt: 'Schematische Darstellung des Dialogs zur Bearbeitung von Lese- und Schreibrechten im Editor.',
        },

        bundestag: {
          heading: 'Bundestagsplattform',
          content:
            'Das Release April 2024 bringt für die Bundestagsplattform eine neue Anwendung zur Verwaltung der Bundestagsdrucksachen mit. Regelungsvorhaben können nun unter anderem mit Drucksachendaten angereichert, mit Bemerkungen versehen oder als Verschlusssache markiert werden. Außerdem können Sperrfristen für ein Regelungsvorhaben gesetzt werden.',
          imgAlt:
            'Schematische Darstellung der Oberfläche zur Bearbeitung von Sperrfristen sowie von Bemerkungen bei Bundestagsdrucksachen.',
        },
        bitv: {
          heading: 'Barrierefreiheit',
          content:
            'Zur weiteren Steigerung der Barrierefreiheit wurde mit dem Release April 2024 die aktuelle Seite zur „Leichten Sprache“ ausgebaut. Nun gibt es eine neue Navigationshilfe zur Nutzung der Bundesregierungsplattform sowie eine leicht verständliche Übersicht über die Anwendungen der E‑Gesetzgebung.',
          imgAlt: `Schematische Darstellung der Erweiterungen für die Barrierefreiheit mit Nennung
der Leichten Sprache, der Navigationshilfe und weiterer Werkzeuge der E-Gesetzgebung.`,
        },
        leichteSprache: {
          heading: 'Leichte Sprache',
          content:
            'Die E-Gesetzgebung ist auch in leichter Sprache verfügbar. Leichte Sprache ist eine vereinfachte Version der deutschen Standardsprache. Der Sprachstil ist verständlicher: Man verzichtet auf komplizierte Satzstrukturen oder wenig bekannte Fremdwörter. Zielgruppe der einfachen Sprache sind Personen, die weniger gut lesen können oder für die Deutsch eine Fremdsprache ist. Mit dem Release April 2023 kann man nun in der Fußleiste der Webseite „Leichte Sprache“ auswählen. Dort wird die Anwendung E-Gesetzgebung für Nutzende in einfacher Sprache erklärt.',
        },
      },
      promo: {
        mainTitle: 'Die E-Gesetzgebung',
        secondTitle: 'Navigation zur Anmeldung',
        thirdTitle: 'Das Rechtsetzungsverfahren auf Bundesebene - elektronisch, medienbruchfrei und interoperabel',
        text1: 'Über die linke Menüleiste haben Sie Zugriff auf die Anwendungen.',
        imgAlt1: 'Screenshot des seitlichen Menübands.',
        text2: 'Auf einen Teil der Anwendungen haben Sie nur im angemeldeten Bereich Zugriff.',
        imgAlt2: 'Screenshot der Anmelden-Schalftfläche in der Kopfleiste.',
        text3: 'Die Kopfleiste ermöglicht Ihnen die Verwaltung Ihres Benutzerkontos.',
        imgAlt3: 'Screenshot der Benutzerverwaltung in der Kopfleiste.',
        summary:
          'Die Startseite der E-Gesetzgebung ist Ihr <strong>Portal zu den Anwendungen</strong>, die Ihnen die Arbeiten im Rechtsetzungsprozess vereinfachen.',
      },
      vision: {
        subTitle: 'Die Funktionalitäten',
        title: 'Von der Vorbereitung über den Entwurf bis zur Abstimmung ',
        imgAlt:
          'Symbolische Darstellung eines Laptops neben einer ausgeglichenen Waage, einer Sprechblase, einem Symbol für Zeitverlauf und einem Paragrafensymbol.',
        edit: {
          title: 'Regelungsentwürfe editieren',
          text: 'Einfache Erstellung und Bearbeitung von Regelungsentwürfen mit Kommentarfunktion.',
        },
        support: {
          title: 'Interaktive Unterstützung',
          text: 'Arbeitshilfen wie die Gesetzesfolgenabschätzung oder die Arbeitshilfenbiliothek unterstützen Sie bei Ihrem Rechtsetzungsvorhaben.',
        },
        processualSupport: {
          title: 'Prozessuale Begleitung',
          text: 'Von der Vorbereitung über den Entwurf bis zur Abstimmung begleiten und unterstützen Sie die Anwendungen der E‑Gesetzgebung durch den gesamten Rechtsetzungsprozess.',
        },
        timeManagment: {
          title: 'Effizientes Zeitmanagement',
          text: 'Die elektronische Zeitplanung erleichtert Ihnen die Planung und das Monitoring Ihres Vorhabens.',
        },
      },
      functions: {
        subTitle: 'Der Funktionsaufwuchs',
        title: 'Die Maßnahme <span class="no-wrap">E-Gesetzgebung</span> und die nächsten Schritte',
        imgAlt: 'Symbolische Darstellung mit mehreren Würfeln auf unterschiedlichen Höhen.',
        text1:
          'Die Maßnahme stellt die Anwendungen der E‑Gesetzgebung stufenweise bereit, sodass eine frühzeitige Nutzung bereits verfügbarer Anwendungen ermöglicht wird. Alle Funktionalitäten werden im Rahmen des agilen Vorgehens nutzerzentriert realisiert und feedbackorientiert weiterentwickelt.',
        text2:
          'Hierbei werden die Module, wie der Editor und die Gesetzesfolgenabschätzung, sowie die Plattform stetig weiterentwickelt und im Wirkbetrieb zur Verfügung gestellt.',
        text3:
          'Die <a href="/cockpit/#/ueberDasProjekt">Maßnahme der E-Gesetzgebung</a> setzt hierbei bewusst auf einen Open‑Source‑Ansatz und somit die Veröffentlichung ihres Quellcodes auf der Plattform Open CoDE.',
      },
      feedback: {
        title: 'Kontakt und Feedback',
        text: 'Sie haben Anregungen, Verbesserungsvorschläge oder einen Fehler in einer der Anwendungen der <span class="no-wrap">E-Gesetzgebung</span> entdeckt? Wir freuen uns über Ihr Feedback.',
        link: 'Nachricht an die <span class="no-wrap">E-Gesetzgebung</span>',
      },
    },
    dashboard: {
      gutenMorgen: 'Guten Morgen',
      gutenTag: 'Guten Tag',
      gutenAbend: 'Guten Abend',
      today: 'Heute',
      eintraege: 'Einträge',
      eintrag: 'Eintrag',
      von: 'von',
      meineRegelungsvorhaben: 'Meine Regelungsvorhaben',
      activeRegelungsvorhaben: 'Aktive Regelungsvorhaben',
      regelungsvorhabenoeffnen: 'Regelungsvorhaben öffnen',
      alleEinblenden: 'Alle einblenden',
      ersteZeilereduzieren: 'Auf die erste Zeile reduzieren',
      farbeZuweisen: 'Farbe zuweisen',
      farben: {
        blau: 'Blau',
        tuerkis: 'Türkis',
        lila: 'Lila',
        rosa: 'Rosa',
        gruen: 'Grün',
        orange: 'Orange',
      },
      emptyRvMessage:
        '<p>Derzeit haben Sie keine Regelungsvorhaben.</p> <p>Sobald Sie eines angelegt haben, können Sie hier direkt darauf zugreifen.</p>',
      rvAnlegen: 'Regelungsvorhaben anlegen',
      schnellzugriffTitle: 'Schnellzugriff',
      tabs: {
        zuletztBearbeitet: {
          tabNav: 'Von mir zuletzt verwendet',
          emptyMessage:
            '<p><strong>Noch keine Dateien verwendet</strong></p><p>Sobald Sie Dokumente in den Anwendungen "Regelungsvorhaben", "Abstimmung" oder "Zeitplanung" angelegt oder geöffnet haben, werden diese hier angezeigt.</p>',
          table: {
            tableHeads: {
              dokument: 'Dokument',
              art: 'Art',
              zuletztVerwendet: 'Zuletzt verwendet',
            },
          },
        },
        neuigkeiten: {
          tabNav: 'Neuigkeiten',
          emptyMessage:
            '<p><strong>Noch keine Neuigkeiten vorhanden</strong></p><p>Hier erscheinen automatisch Ihre Neuigkeiten, damit Sie alle Aktivitäten im Blick behalten können.</p>',
          eintraegeAlsGelesen: 'Alle Einträge als gelesen markieren',
          eintraegeLoeschen: 'Alle Einträge löschen',
          zuAllenNeuigkeiten: 'Zu allen Neuigkeiten',
          table: {
            alsUngelesenMarkieren: 'Als ungelesen markieren',
            alsGelesenMarkieren: 'Als gelesen markieren',
            eintragLoeschen: 'Eintrag löschen',
            tableHeads: {
              neuigkeiten: 'Neuigkeiten',
              eingang: 'Eingang',
              aktionen: 'Aktionen',
            },
          },
        },
        aufgaben: {
          tabNav: 'Aufgaben',
          neueAufgabe: 'Neue Aufgabe erstellen',
          aufgabenLoeschen: 'Erledigte Aufgaben löschen',
          zuAllenAufgaben: 'Zu allen Aufgaben',
          successCreatMessage: 'Die Aufgabe wurde erfolgreich erstellt.',
          successModifiedMessage: 'Die Aufgabe wurde erfolgreich geändert.',
          emptyMessage:
            '<p><strong>Noch keine Aufgaben vorhanden</strong></p><p>Erstellen Sie sich Aufgaben, damit Sie Ihre To-dos im Blick haben.</p>',
          emptyMessageLink: 'Erste Aufgabe erstellen',
          table: {
            bearbeiten: 'Bearbeiten',
            checkedHover: 'Erledigte Aufgabe',
            unCheckedHover: 'Unerledigte Aufgabe',
            noFrist: 'keine Frist',
            erledigtAm: 'Erledigt am',
            alsErledigtMarkieren: 'Aufgabe als erledigt markieren',
            alsUnerledigtMarkieren: 'Aufgabe als unerledigt markieren',
            aufgabeLoeschen: 'Aufgaben löschen',
            tableHeads: {
              status: 'Status',
              aufgabe: 'Aufgabe',
              frist: 'Frist',
              aktionen: 'Aktionen',
            },
          },
          modal: {
            creatTitle: 'Aufgabe erstellen',
            modifyTitle: 'Aufgabe bearbeiten',
            btnModifyTitle: 'Änderung speichern',
            creatContentTitle: 'Neue Aufgabe erstellen',
            modifyContentTitle: 'Aufgabe bearbeiten',
            contentDescription: 'Pflichtfelder sind mit einem * gekennzeichnet.',
            aufgabeLabel: 'Aufgabe',
            anmerkungenLabel: 'Anmerkungen',
            anmerkungenLengthError: 'Die maximal zulässige Anzahl von {{maxChars}} Zeichen wurde überschritten.',
            aufgabeMissedError: 'Bitte geben Sie den Aufgabentitel an.',
            cancel: 'Abbrechen',
            deadline: {
              title: 'Frist',
              dateLabel: 'Datum (tt.mm.jjjj)',
              timeLabel: 'Uhrzeit (hh:mm)',
              errorDate: 'Bitte wählen Sie ein Datum aus.',
              errorTime: 'Bitte wählen Sie eine Uhrzeit aus.',
              errorTimeFormat: 'Bitte geben Sie eine gültige Uhrzeit im Format „hh:mm“ ein.',
              errorDateFormat: 'Bitte geben Sie ein gültiges Datum im Format „tt.mm.jjjj“ ein.',
              errorTimeFuture: 'Die Frist muss in der Zukunft liegen.',
              errorTimeBeforeMin: 'Die Frist muss nach {{date}} enden.',
            },
          },
        },
      },
    },
    vorstellungLight: {
      title: 'Über diese Seite',
      notice:
        'Hinweis: Die E‑Gesetzgebung im Internet bildet einen reduzierten Funktionsumfang der Bundesregierungsplattform ab. Der vollständige Funktionsumfang der Anwendungen der E‑Gesetzgebung für die Bundesregierung, den Deutschen Bundestag und Bundesrat ist in den Netzen des Bundes über folgenden Link aufrufbar: <a href=https://egesetz.de>egesetz.de</a>.',
      content: {
        section1: {
          title: 'E-Gesetzgebung im Einsatz',
          paragraph1:
            'Das Projekt „Elektronisches Gesetzgebungsverfahren (E‑Gesetzgebung)“ hat zum Ziel, das Rechtsetzungsverfahren des Bundes auf eine neue IT‑Grundlage zu stellen. Bisher bestehende Medienbrüche im Prozess innerhalb und zwischen der Bundesregierung, dem Deutschen Bundestag und dem Bundesrat sollen abgebaut werden. Das Rechtsetzungsverfahren auf Bundesebene soll vollständig elektronisch, medienbruchfrei und interoperabel sein.',
          paragraph2:
            'Vom Kleinen zum ganz Großen: Die Anwendungen der E‑Gesetzgebung für die Bundesregierung werden nach und nach bis Ende des Jahres 2024 für Rechtsetzungsreferentinnen und -referenten entwickelt und in den Netzen des Bundes bereitgestellt. Neben der laufenden Bereitstellung der E‑Gesetzgebung für die Bundesregierung wurde die E­‑Gesetzgebung mit dem Release Oktober 2023 um einen eigenen Mandanten für den Deutschen Bundestag erweitert. Im Zuge des Releases Oktober 2024 wurde ein eigener Mandant für den Bundesrat etabliert. Gesonderte Benutzeroberflächen bilden die Funktionen entsprechend den verfassungsorganspezifischen Geschäftsabläufen ab.',
          paragraph3:
            'Mit der E‑Gesetzgebung im Internet haben Sie die Möglichkeit, einen Blick hinter die Kulissen zu werfen. Ausgewählte Module unserer Anwendungen der Bundesregierungsplattform stellen wir zum Anschauen, Ausprobieren und Anwenden zur Verfügung.',
          paragraph4: 'Die Funktionen der bereitgestellten Anwendungen ermöglichen Ihnen,',
          ziele: [
            'mithilfe der „Anwendung zur Vorbereitung von Regelungsentwürfen“ inhaltliche Vorbereitungen eines neuen Regelungsvorhaben vorzunehmen,',
            'mithilfe der Orientierungshilfe der Zeitplanung Regelungsvorhaben intern planen zu können,',
            'mithilfe der „Nachhaltigkeitsprüfung“ zu untersuchen, ob die Wirkungen eines Vorhabens einer nachhaltigen Entwicklung entsprechen,',
            'sich Schritt für Schritt durch das Rechtsetzungsverfahren vom „Verfahrensassistenten“ begleiten zu lassen,',
            'und die aktuellen Arbeitshilfen für den Rechtsetzungsprozess an einer zentralen Stelle in der „Arbeitshilfenbibliothek“ zu finden.',
          ],
          contact:
            'Für Fragen, Anregungen oder Kritik freuen wir uns über eine formlose E‑Mail an: <a href="mailto:{{eGesetzSupportEmail}}">{{eGesetzSupportEmail}}</a>',
        },
      },
    },
    ueberDieMassnahme: {
      title: 'Über das Projekt',
      content: {
        section1: {
          title: 'Ausgangslage und Handlungsbedarf',
          paragraph1:
            'In die Gesetzgebungsprozesse des Bundes sind viele Akteurinnen und Akteure aus unterschiedlichen Verfassungsorganen eingebunden. Bisher wird das Rechtsetzungsverfahren jedoch nur uneinheitlich durch IT unterstützt. Vom ersten Entwurf bis zur Verkündung eines Gesetzes kommen unterschiedliche Softwarelösungen und Anwendungen zum Einsatz. Zugleich steigen die inhaltlichen Anforderungen der zu regelnden Sachverhalte stetig. In der Konsequenz kommt es zu intensiven und langen Abstimmungswegen.',
          paragraph2:
            'Diese komplexe Ausgangslage belegt und verdeutlicht auch das Gutachten des Nationalen Normenkontrollrats <a target="_blank" href="https://www.normenkontrollrat.bund.de/Webs/NKR/SharedDocs/Downloads/DE/Gutachten/2019-erst-der-inhalt-dann-die-paragraphen.pdf?__blob=publicationFile&v=5">„Erst der Inhalt, dann die Paragraphen“</a>. Ein durchgängiges und einfach zu handhabendes elektronisches Rechtsetzungsverfahren hat viele Vorteile:',
          vorteile: [
            'Einzelne Arbeitsschritte werden unterstützt bzw. vollständig elektronisch abgebildet.',
            'Einmal erhobene Daten werden dauerhaft nachnutzbar.',
            'Medienbrüche und redundante Aufwände werden vermieden.',
            'Bearbeitungsstände und Bearbeitungsschritte bleiben dauerhaft nachvollziehbar.',
          ],
        },
        section2: {
          title: 'Projektziele',
          paragraph1:
            'Das Projekt „Elektronisches Gesetzgebungsverfahren des Bundes“ (E‑Gesetzgebung) ist Bestandteil des vom Bundesministerium des Innern und für Heimat (BMI) verantworteten Programms <a target="_blank" href="https://www.cio.bund.de/Web/DE/Dienstekonsolidierung/Programm_Dienstekonsolidierung/programm_dienstekonsolidierung_node.html">„Dienstekonsolidierung Bund“</a> und hat zum Ziel,',
          ziele: [
            'bisher bestehende Medienbrüche innerhalb und zwischen der Bundesregierung, dem Deutschen Bundestag und dem Bundesrat abzubauen,',
            'das Rechtsetzungsverfahren auf Bundesebene vollständig elektronisch und interoperabel abzubilden und',
            'durch die Orientierung an den aktuellen technologischen Entwicklungen die Gesetzgebungsarbeit modern und zukunftssicher aufzustellen.',
          ],
          imgAlt: 'Symbolische Darstellung der E‑Gesetzgebung und LegalDocML.de im Rechtsetzungskreislauf.',
          imgTitle: 'Abbildung 1: E‑Gesetzgebung und LegalDocML.de im Rechtsetzungskreislauf',
          titleImage: 'Der Rechtsetzungskreislauf',
          par2: 'Abbildung 1 veranschaulicht schematisch die Digitalisierung des Rechtsetzungskreislaufs, welche wesentlich von der E‑Gesetzgebung vorangetrieben wird.',
          par3: 'Die Bundesregierungsplattform ist die Umgebung für die Anwendungen zur elektronischen und medienbruchfreien Abbildung des Rechtsetzungsverfahrens für die Bundesregierung. Diese umfasst die elektronische Erarbeitung, Kommentierung und Abstimmung von Regelungsentwürfen bis hin zur Zuleitung zum Bundeskabinett.',
          par4: 'Die Bundestagsplattform soll zukünftig das Erstellen und Einbringen von Vorlagen, das Anlegen und Verwalten von Bundestags- und Ausschussdrucksachen sowie die Vor- und Nachbereitung der Beratung dieser Drucksachen im Plenum und in den Ausschüssen des Deutschen Bundestages ermöglichen. Ebenso sieht die Bundestagsplattform perspektivisch die Zuleitung von Gesetzesbeschlüssen an den Bundesrat und die Unterrichtung über diese und über weitere Beschlüsse an die Bundesregierung vor. ',
          par5: 'Die Bundesratsplattform soll es zukünftig ermöglichen, Vorlagen der Verfassungsorgane zu erhalten und relevante Informationen für die weitere Verwendung in bundesratsspezifischen Prozessen und Dokumenten zu extrahieren. Über die Plattform werden perspektivisch die beteiligten Ausschüsse in einem Umlaufverfahren ermittelt. Für die Arbeit in den Ausschüssen sowie im Plenum werden die vorbereitenden Drucksachen angelegt, nach Durchführung der jeweiligen Beratungen die Beschlüsse erstellt und an den Deutschen Bundestag und die Bundesregierung zugeleitet.',
          par6: 'Jedes Verfassungsorgan erhält eine eigene Benutzeroberfläche, die die Funktionen entsprechend den verfassungsorganspezifischen Geschäftsabläufen abbildet. Abhängig vom registrierten Nutzerkonto gelangen die Nutzenden automatisch auf die für sie bestimmte Benutzeroberfläche. ',
          par7: 'In einem zukünftigen Gesetzgebungsportal im Internet sollen für Bürgerinnen und Bürger zudem Gesetzesvorhaben, deren Status, weiterführende Informationen und Verlinkungen zu weiteren Informationsangeboten der Verfassungsorgane dargestellt werden.',
          par8: 'Nur durch die enge Verzahnung mit der Maßnahme E‑Verkündung (Bundesministerium der Justiz, BMJ), dem Planungs- und Kabinettmanagementprogramm (PKP, Bundeskanzleramt) und dem Projekt „Neues Rechtsinformationssystem des Bundes“ (NeuRIS, Bundesamt für Justiz, BfJ) kann der Kreislauf geschlossen werden.',
        },
        section3: {
          title: 'Vorgehen',
          paragraph1:
            'Das Projekt stellt die Anwendungen der E‑Gesetzgebung stufenweise bereit, sodass eine frühzeitige Nutzung bereits verfügbarer Dienste ermöglicht wird. Alle Funktionalitäten werden im Rahmen dieses agilen Vorgehens nutzerzentriert realisiert und feedbackorientiert weiterentwickelt. Maßgebliche Unterstützung leisten im Rahmen des Projekts die Nutzenden. Bedarfs- und anlassbezogen werden mandatierte und nicht-mandatierte Nutzende zu ressort- und verfassungsorganübergreifenden Formaten (z. B. Key-User-Fokusgruppen, Gremiensitzungen, Fachaustausche, Anforderungsworkshops) eingeladen und bezüglich fachlicher Fragen zur Umsetzung konsultiert. Nutzende haben auch die Möglichkeit, Feedback zum aktuellen Entwicklungsstand im Rahmen sogenannter „Sprint Reviews“ zu geben.',
          paragraph2:
            'Neben der laufenden Bereitstellung der E‑Gesetzgebung für die Bundesregierung wird seit Mai 2023 die Einbindung des Deutschen Bundestages und seit Januar 2024 die Einbindung des Bundesrates umgesetzt. Bis Ende 2024 sollen im Rahmen der Dienstekonsolidierung die Anwendungsfälle der Bundesregierung abgebildet werden. Die Einbindung des Deutschen Bundestages erfolgt nach aktueller Planung bis Ende 2025. Die Einbindung des Bundesrates erfolgt in 2026. Mit der Etablierung einer Pflege- und Betriebsorganisation soll die langfristige Weiterentwicklung und Pflege gesichert werden.',
          paragraph3:
            'Zudem ist für die E‑Gesetzgebung mit LegalDocML.de ein eigener Inhaltsdatenstandard erarbeitet worden. Ziel des Inhaltsdatenstandards ist es, den rechtsförmlichen Aufbau und die Struktur von Regelungstexten maschinenlesbar abzubilden sowie deren Bearbeitung und Weitergabe prozessübergreifend zu ermöglichen. LegalDocML.de kann insofern als das Fundament der E‑Gesetzgebung bezeichnet werden und stellt die Basis für die Digitalisierung von Rechtsetzungsverfahren dar. Zur übergreifenden Verwendung des Inhaltsdatenstandards LegalDocML.de wird dieser für die Bedarfe des Deutschen Bundestages und des Bundesrates weiterentwickelt.',
          linkText: 'Zu weiteren Informationen zu LegalDocML.de',
        },

        section6: {
          title: 'Open Source-Software als wesentlicher Erfolgsfaktor',
          imgAlt: 'Symbolische Darstellung der Startseite der E‑Gesetzgebung.',
          imgTitle: 'Abb. 2: Die Startseite der „E‑Gesetzgebung“',
          paragraph1:
            'Die Maßnahme setzt bewusst auf einen Open Source‑Ansatz und somit auf die Stärkung der digitalen Souveränität der Bundesrepublik Deutschland. Mit der Veröffentlichung des Quellcodes wird die Nachnutzbarkeit, insbesondere durch föderale und supranationale Ebenen, eröffnet. Der Quellcode der Plattform der E‑Gesetzgebung wurde im Juni 2022 erstmalig als eines der ersten Softwareprojekte des Bundes unter einer Open Source‑Lizenz auf der Plattform Open CoDE veröffentlicht. Mit dem Go-Live von Open CoDE erreichte auch die E‑Gesetzgebung einen Meilenstein. ',
          paragraph2:
            'Im Zuge des Release Oktober 2024 ist eine weitere Veröffentlichung des Quellcodes der Bundesregierungsplattform, des Editors, der Bundestagsplattform und der Bundesratsplattform im <a target="_blank" href="https://gitlab.opencode.de/bmi/e-gesetzgebung/documentation">Repository der E‑Gesetzgebung auf Open CoDE</a> geplant.',
        },
      },
    },
    legalDoc: {
      title: 'LegalDocML.de als Standard für einen digitalen Rechtsetzungsprozess',
      section1: {
        paragraph1:
          'Rechtsetzungsprozesse zeichnen sich durch eine hohe Komplexität und Vielschichtigkeit aus. Dies gilt nicht nur für die Kommunikation zwischen den beteiligten Akteuren, sondern bereits für den Inhalt des Regelungsentwurfes und die damit verbundenen Erstellungs- und Abstimmungsprozesse. Für eine digitale und medienbruchfreie Rechtsetzung ist es daher unerlässlich, gemeinsame Regeln, Verfahren und Formate zwischen den beteiligten Akteuren festzulegen. Dadurch wird sichergestellt, dass Daten und Informationen stets einheitlich interpretiert werden. Ein grundlegender Standard für digitale Rechtsetzungsdokumente ist der Inhaltsdatenstandard LegalDocML.de. Er basiert auf dem internationalen Standard LegalDocML und wird im Zuge der E‑Gesetzgebung an die Spezifika der Bundesrechtsetzung angepasst (vgl. Abb. 1).',
        imgTitle1: 'Abbildung 1: Das Verhältnis von LegalDocML und LegalDocML.de',
        paragraph2:
          'Die Grundlage der Erarbeitung des Standards ist der Beschluss des Strategischen Steuerungsgremiums von Juli 2019:',
        paragraph3:
          '„Die Maßnahme E‑Gesetzgebung entwickelt auf der Grundlage des Datenformats LegalDocML einen Inhaltsdatenstandard für die E‑Gesetzgebung, […]. Die Spezifikation des Inhaltsdatenstandards umfasst sowohl die Regelungsentwürfe mitsamt den zugehörigen Dokumenttypen der Bundesregierung und der Verfassungsorgane als auch das Bestandsrecht.“',
        paragraph4:
          'Ziel des Inhaltsdatenstandards LegalDocML.de ist es, den rechtsförmlichen Aufbau und die Struktur von Rechtsetzungsdokumenten als XML-Datenschema abzubilden. Der rechtsförmliche Aufbau und die Struktur werden u. a. durch das Handbuch der Rechtsförmlichkeit (HdR), die Gemeinsame Geschäftsordnung der Bundesministerien (GGO) oder das Handbuch zur Vorbereitung von Rechts- und Verwaltungsvorschriften (HVRV) vorgegeben. Die Abbildung dieser Vorgaben in einem technologieunabhängigen XML-Format stellt sicher, dass Regelungsentwürfe zukünftig einheitlich und in einer für alle Beteiligten nachvollziehbaren Struktur aufgebaut sind. Die einheitliche Auszeichnung spezifischer Struktur- und Inhaltselemente innerhalb eines Rechtsetzungsdokumentes ermöglicht perspektivisch, Bearbeitungsprozesse intelligent und nutzerorientiert zu unterstützen. Dafür wird im Rahmen der Maßnahme E‑Gesetzgebung eigens ein legislativer Editor entwickelt.',
        imgTitle2: 'Abbildung 2: Auszeichnung von Inhaltselementen in Rechtsetzungsdokumenten mit LegalDocML.de',
        paragraph5:
          'Insbesondere die Kommentierung, die Konsolidierung von Anmerkungen sowie das Erstellen von Synopsen und Änderungsbefehlen soll zukünftig durch eine einheitliche semantische und syntaktische Verwendung von Inhaltselementen erleichtert und weitestgehend automatisiert werden. Rechtsetzungsreferentinnen und –referenten werden dadurch bei der täglichen Arbeit an Rechtsetzungsdokumenten unterstützt. Im Ergebnis bleibt so mehr Zeit für die inhaltliche Befassung.',
        paragraph6:
          'Neben der spezifischen Unterstützung der Rechtsetzungsreferentinnen und –referenten dient der Standard als ein wesentlicher Baustein für die Umsetzung eines durchgängig digitalisierten Rechtsetzungsverfahrens. Auch weitere Maßnahmen, die an der Digitalisierung des Rechtsetzungskreislaufes arbeiten, wie die elektronische Verkündung (E‑Verkündung) und das neue Rechtsinformationssystem des Bundes (NeuRIS) haben sich auf die Nutzung von LegalDocML.de verständigt.',
      },
      section2: {
        paragraph1:
          'Die kontinuierliche Pflege, Anpassung und Weiterentwicklung dieses Standards wird seit Anfang 2024 durch die Koordinierungsstelle für IT‑Standards (KoSIT) unter Beteiligung relevanter Stakeholder durchgeführt.',
      },
      section3: {
        title: 'Steckbrief zu LegalDocML.de',
        paragraph1:
          'Der derzeitige Stand des Standards kann auf der <a target="_blank" href="https://gitlab.opencode.de/bmi/e-gesetzgebung/ldml_de">Plattform Open CoDE</a> eingesehen werden.',
        steckbrief: [
          {
            title: 'Zielstellung:',
            content:
              'Etablierung eines digitalen, rechtsetzungsspezifischen Dokumentenformats für den gesamten Rechtsetzungskreislauf auf Bundesebene.',
            image: 'Zielstellung',
          },
          {
            title: 'Umfang:',
            content:
              'Als Inhaltsdatenstandard der gesamten Bundesrechtsetzung umfasst LegalDocML.de alle bisher relevanten Dokumente sowohl in der Entwurfs- als auch in der Verkündungsfassung. LegalDocML.de umfasst all jene Dokumente der Bundesrechtsetzung, die final im Bundesgesetzblatt (Teil I und Teil II) verkündet werden und erfasst auch Strukturen des Bestandsrechts. Zudem können auch bestimmte Dokumente des Deutschen Bundestages, welche im parlamentarischen Prozess erstellt werden, in LegalDocML.de erfasst werden.',
            image: 'Umfang',
          },
          {
            title: 'Format:',
            content: 'OASIS-Standard LegalDocML (auch bekannt als Akoma Ntoso)',
            image: 'Format',
          },
          {
            title: 'Technologie:',
            content: 'XML-basiertes Datenformat, technologieunabhängig nutzbar',
            image: 'Technologie',
          },
          {
            title: 'Verfügbarkeit:',
            content:
              'Als Open Source verfügbar unter der <a target="_blank" href="https://gitlab.opencode.de/bmi/e-gesetzgebung/ldml_de/-/blob/main/LICENSE?ref_type=heads">Lizenz Creative Commons CC BY 3.0</a>',
            image: 'Verfuegbarkeit',
          },
        ],
      },
    },
    welcomeLight: {
      intro: {
        alt: 'Symbolische Darstellung der Plattform der E-Gesetzgebung.',
        linkText: 'Weitere Informationen',
        title: 'Das Rechtsetzungsverfahren auf Bundesebene – elektronisch, medienbruchfrei und interoperabel',
        subTitle: 'Die E-Gesetzgebung im Internet',
      },
      functionsPreview: {
        title: 'Folgende Funktionalitäten stehen für Sie zur Verfügung',
        subTitle: 'Anwendungen im Internet',
        anwendungLink: 'Zur Anwendung',
        evor: {
          heading: 'Vorbereitung',
          content:
            'die Anwendung "Vorbereitung" ermöglicht die inhaltliche Vorbereitung von Regelungsentwürfen in der Frühphase des Regelungsvorhabens. Die Anwendung begleitet die federführenden Ressorts von der Analyse des Regelungsfeldes, über die Operationalisierung von Zielen, der Entwicklung von Regelungsalternativen bis hin zur Abschätzung ihrer Vor- und Nachteile. ',
          imgAlt: 'Schematische Darstellung der Anwendung Vorbereitung.',
        },
        enap: {
          heading: 'Nachhaltigkeitsprüfung',
          content:
            'Mit der Anwendung "Nachhaltigkeitsprüfung" wird geprüft, inwiefern die Wirkungen eines Vorhabens einer nachhaltigen Entwicklung entsprechen. Die Nachhaltigkeitsprüfung ist gemäß § 44 Abs. 1 S. 4 GGO im Rahmen der Gesetzesfolgenabschätzung durchzuführen. Zentraler Bezugspunkt für die Prüfung ist die   <a target="_blank" href="https://www.bundesregierung.de/resource/blob/998006/1873516/3d3b15cd92d0261e7a0bcdc8f43b7839/2021-03-10-dns-2021-finale-langfassung-nicht-barrierefrei-data.pdf?download=1">Deutsche Nachhaltigkeitsstrategie</a> in der im Jahr 2021 weiterentwickelten Fassung.',
          imgAlt: 'Schematische Darstellung der Anwendung Nachhaltigkeitsprüfung.',
        },
        bib: {
          heading: 'Arbeitshilfenbibliothek',
          content:
            'Die Anwendung "Arbeitshilfenbibliothek" ist die Bibliothek der Arbeitshilfen, die Legistinnen und Legisten im Rechtssetzungsverfahren nutzen können. Die in der "Arbeitshilfenbibliothek" bereitgestellten Arbeitshilfen werden von unterschiedlichen Stellen verantwortet. Sie umfassen Leitfäden, Richtlinien, Berechnungswerkzeuge und Checklisten. Durch die Bereitstellung auf der Plattform sind Arbeitshilfen in ihrer aktuellen Form an einem Ort gesammelt. Dadurch entfällt Rechercheaufwand und Reibungsverluste werden vermieden.',
          imgAlt: 'Schematische Darstellung der Anwendung Arbeitshilfenbibliothek.',
        },
        evir: {
          heading: 'Verfahrensassistent',
          content: `Die Anwendung "Verfahrensassistent" bietet wertvolle Informationen zum Verfahrensablauf. Legistinnen und Legisten dient die Anwendung "Verfahrensassistent" als Orientierungspunkt in allen Phasen des Rechtsetzungsverfahrens. Entlang des Prozesses werden relevante Anwendungen und einschlägige Arbeitshilfen der E-Gesetzgebung bedarfsgerecht zur Verfügung gestellt. So werden die zu erledigenden Schritte und Aufgaben auf einen Blick deutlich, wodurch eine zielgerichtete und fristgerechte Bearbeitung ermöglicht wird.`,
          imgAlt: 'Schematische Darstellung der Anwendung Verfahrensassistent.',
        },
        zeit: {
          heading: 'Zeitplanung',
          content: `Um die Dauer eines Vorhabens besser abzuschätzen, können Sie nun Zeitplanungen anlegen sowie angelegte Zeitplanungen bearbeiten. Als Grundlage Ihrer Zeitplanung können Sie eine Vorlage auswählen. Darin sind bereits alle wesentlichen Phasen, Termine und Hinweise enthalten. Die Zeitplanung kann sowohl in einer barrierefreien Tabelle als auch in einer nicht-barrierefreien Kalenderansicht dargestellt werden. Für die Nachnutzung können Sie Zeitplanungen als PDF exportieren.`,
          imgAlt: 'Schematische Darstellung der Anwendung Zeitplanung.',
        },
      },
      ueberUns: {
        subTitle: 'Über uns',
        title: 'Der Hintergrund zum Projekt <span style="white-space: nowrap;">E-Gesetzgebung</span>',
        content:
          'Vom Kleinen zum ganz Großen: die Anwendungen der E-Gesetzgebung werden nach und nach bis zum Jahr 2024 für Sie entwickelt und bereit gestellt. Dazu gehört z. B. die Möglichkeit, sich am System anzumelden, Regelungsentwürfe abzustimmen oder Sachverständige und Länder zu beteiligen.',
        linkName: 'Weitere Informationen',
        imgAlt: 'Symbolische Darstellung mit mehreren Würfeln.',
      },
    },
    logoutTimer: {
      mainLabel: `Ihre Sitzung läuft in {{timeLeftLabel}} min ab.`,
      btnReset: 'Timer zurücksetzen',
      modal: {
        title: `Hinweis: Sie werden in {{timeLeftLabel}} Minuten automatisch ausgeloggt. Sind Sie noch da?`,
        text: 'Sie werden in Kürze automatisch zu Ihrer eigenen Sicherheit von der E-Gesetzgebung abgemeldet.',
        btnReset: 'Timer zurücksetzen und weiterarbeiten',
        btnLogout: 'Jetzt abmelden',
      },
      title: 'Sie wurden automatisch ausgeloggt.',
      paragraph1:
        'Aus Sicherheitsgründen werden Sie bei Inaktivität automatisch nach Ablauf der eingestellten Zeitspanne abgemeldet.',
      loginButton: 'Erneut anmelden',
      btnHomepage: 'Zurück zur Startseite',
      resetSuccessMsg: 'Die Sitzungszeit wurde auf 29 Minuten zurückgesetzt.',
    },
  },
};
