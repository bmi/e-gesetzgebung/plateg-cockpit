// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const bt = {
  cockpit: {
    langCode: 'de-DE',
    dashboard: {
      activeRegelungsvorhaben: 'Aktive Vorlagen',
      emptyRvMessage:
        '<p>Derzeit haben Sie keine Vorlagen.</p> <p>Sobald Ihnen eine Vorlage zugeleitet wurde, können Sie hier direkt darauf zugreifen.</p>',
      meineRegelungsvorhaben: 'Meine Vorlagen',
      regelungsvorhabenoeffnen: 'Vorlage öffnen',
    },
    header: {
      titleAddition: ' | Bundestag',
    },
    menuItems: {
      rv: { label: 'Vorlagen', hint: 'Vorlagen', menuItem: 'Vorlagen' },
      drucksachen: {
        label: 'Bundestagsdrucksachen',
        hint: 'Bundestagsdrucksachen',
        menuItem: 'Bundestagsdrucksachen',
      },
      admin: {
        label: 'Fachadministration',
        hint: 'Fachadministration',
        menuItem: 'Fachadministration',
      },
      ueberweisung: {
        label: 'Überweisung',
        hint: 'Überweisung',
        menuItem: 'Überweisung',
      },
    },
  },
};
