// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { de as administration_de } from '@plateg/administration/src/messages/de/index';
import { de as editor_de } from '@plateg/editor/src/i18n/messages/de/index';
import { de as egfa_de } from '@plateg/egfa/src/messages/de/index';
import { de as enorm_de } from '@plateg/enorm/src/messages/de/index';
import { de as evir_de } from '@plateg/evir/src/messages/de/index';
import { de as evor_de } from '@plateg/evor/src/messages/de/index';
import { de as zeit_de } from '@plateg/period-calculator/src/messages/de/index';
import { de as print_de } from '@plateg/print/src/messages/de/index';
import { de as rv_de } from '@plateg/rv/src/messages/de/index';
import { de as theme_de } from '@plateg/theme/src/messages/de/index';
import { de as working_aids_de } from '@plateg/working-aids/src/messages/de/index';
import { de as umlauf_de } from '@plateg/umlauf/src/messages/de/index';
import { de as ueberweisung_de } from '@plateg/ueberweisung/src/messages/de/index';

import { de } from './de';

export const messages = {
  de: {
    // eslint-disable-next-line
    translation: {
      ...de,
      ...evor_de,
      ...enorm_de,
      ...egfa_de,
      ...working_aids_de,
      ...rv_de,
      ...zeit_de,
      ...evir_de,
      ...theme_de,
      ...editor_de,
      ...administration_de,
      ...print_de,
      ...umlauf_de,
      ...ueberweisung_de,
    },
  },
};
