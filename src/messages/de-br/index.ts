// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export const br = {
  cockpit: {
    langCode: 'de-DE',
    dashboard: {
      activeRegelungsvorhaben: 'Aktive Vorlagen',
      emptyRvMessage:
        '<p>Derzeit haben Sie keine Vorlagen.</p> <p>Sobald Ihnen eine Vorlage zugeleitet wurde, können Sie hier direkt darauf zugreifen.</p>',
      meineRegelungsvorhaben: 'Meine Vorlagen',
      regelungsvorhabenoeffnen: 'Vorlage öffnen',
    },
    header: {
      titleAddition: ' | Bundesrat',
    },
    menuItems: {
      admin: {
        label: 'Fachadministration',
        hint: 'Fachadministration',
        menuItem: 'Fachadministration',
      },
      drucksachen: {
        label: 'Bundesratsdrucksachen',
        hint: 'Bundesratsdrucksachen',
        menuItem: 'Bundesratsdrucksachen',
      },
      rv: { label: 'Vorlagen', hint: 'Vorlagen', menuItem: 'Vorlagen' },
      umlauf: {
        label: 'Umlaufverfahren',
        hint: 'Umlaufverfahren',
        menuItem: 'Umlaufverfahren',
      },
    },
  },
};
