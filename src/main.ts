// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import {
  AufgabeControllerApi,
  Configuration,
  DataControllerApi,
  LoggingControllerApi,
  NeuigkeitControllerApi,
  RegelungsvorhabenControllerApi,
  SucheControllerApi,
  UserControllerApi,
} from '@plateg/rest-api';
import { EmaillisteControllerApi } from '@plateg/rest-api/apis/EmaillisteControllerApi';
import { configureRestApi, displayMessage, GlobalDI } from '@plateg/theme';

function loadApp(configRestCalls: Configuration) {
  GlobalDI.getOrRegister('userController', () => new UserControllerApi(configRestCalls));
  GlobalDI.getOrRegister('sucheController', () => new SucheControllerApi(configRestCalls));
  GlobalDI.getOrRegister('regelungsvorhabenController', () => new RegelungsvorhabenControllerApi(configRestCalls));
  GlobalDI.getOrRegister('neuigkeitController', () => new NeuigkeitControllerApi(configRestCalls));
  GlobalDI.getOrRegister('aufgabeController', () => new AufgabeControllerApi(configRestCalls));
  GlobalDI.getOrRegister('loggingController', () => new LoggingControllerApi(configRestCalls));
  GlobalDI.getOrRegister('emaillisteControllerApi', () => new EmaillisteControllerApi(configRestCalls));
  GlobalDI.getOrRegister('dataControllerApi', () => new DataControllerApi(configRestCalls));
  import('./react.main');
}

configureRestApi(loadApp);

// Used to send frontend errors to the backend for logging
window.onerror = function (_message, _source, _lineno, _colno, error) {
  const loggerCtrl = GlobalDI.get<LoggingControllerApi>('loggingController');
  loggerCtrl
    .saveLog({ body: `location: ${location.href}, error: ${error?.stack || ''}` || 'No error provided' })
    .subscribe({
      error: () => {
        displayMessage(
          `Es ist ein unerwarteter Fehler aufgetreten. Bitte wenden Sie sich an den Support. Wir entschuldigen uns für entstandene Umstände. ${
            error?.name as string
          } `,
          'error',
        );
        console.log('Error while sending frontend logs to the backend', error);
      },
    });
};
