// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './style.less';
import './shares/i18n';

import { ConfigProvider } from 'antd';
import deDE from 'antd/es/locale/de_DE';
import React, { version } from 'react';
import { createRoot } from 'react-dom/client';
import { HashRouter as Router } from 'react-router-dom';

import { GlobalDI, plategThemeConfig, ScrollToTop } from '@plateg/theme';
import { store, StoreProvider } from '@plateg/theme/src/components/store';

import { AppLight } from './components/app/app.light';

GlobalDI.register('Framework', {
  name: 'React',
  version,
});

// necessary in order to use date-fns instead of moment
deDE.DatePicker.lang.locale = 'de';

const htmlDivElement: HTMLDivElement | null = document.querySelector('div#app');

if (htmlDivElement === null) {
  throw new Error('Root container missing in index.html');
}
const root = createRoot(htmlDivElement);
root.render(
  <ConfigProvider theme={plategThemeConfig} locale={deDE}>
    <Router>
      <ScrollToTop />
      <StoreProvider store={store}>
        <AppLight />
      </StoreProvider>
    </Router>
  </ConfigProvider>,
);
