// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';
import { throwError } from 'rxjs';
import sinon from 'sinon';

import { AppType, CmsControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { loadingStatusSetStub } from '../general.test';
import BackendPlugin, { deepMerge, TranslationObject } from './BackendPlugin';

describe('test backend i18n plugin', () => {
  const pluginCtrl = new BackendPlugin();
  const cms_controller = GlobalDI.getOrRegister('cmsController', () => new CmsControllerApi());
  const getCmsTextStub = sinon.stub(cms_controller, 'getCmsText');
  it('backend returns error', (done) => {
    getCmsTextStub.returns(throwError(() => new Error('test')));
    const callbackStub = sinon.stub();
    pluginCtrl.read('test1', 'test2', callbackStub);
    setTimeout(() => {
      sinon.assert.calledTwice(loadingStatusSetStub);
      sinon.assert.calledOnce(callbackStub);
      sinon.assert.callCount(getCmsTextStub, 10);
      getCmsTextStub.resetBehavior();
      done();
    }, 30);
  });

  it('backend returns text for each application', (done) => {
    getCmsTextStub({ appType: AppType.Rv });
    const callbackStub = sinon.stub();
    pluginCtrl.read('de', 'translation', callbackStub);
    sinon.assert.called(getCmsTextStub);
    getCmsTextStub.restore();
    done();
  });

  it('backend text bt', (done) => {
    getCmsTextStub({ appType: AppType.Rv });
    const callbackStub = sinon.stub();
    pluginCtrl.read('de', 'bt', callbackStub);
    sinon.assert.called(getCmsTextStub);
    getCmsTextStub.restore();
    done();
  });

  it('backend text br', (done) => {
    getCmsTextStub({ appType: AppType.Rv });
    const callbackStub = sinon.stub();
    pluginCtrl.read('de', 'br', callbackStub);
    sinon.assert.called(getCmsTextStub);
    getCmsTextStub.restore();
    done();
  });
});

describe('deepMerge', () => {
  it('should merge two flat objects', () => {
    const target: TranslationObject = { key1: 'value1' };
    const source: TranslationObject = { key2: 'value2' };
    const result = deepMerge(target, source);
    expect(result).to.deep.equal({ key1: 'value1', key2: 'value2' });
  });

  it('should overwrite properties from the target with properties from the source', () => {
    const target: TranslationObject = { key1: 'value1' };
    const source: TranslationObject = { key1: 'value2' };
    const result = deepMerge(target, source);
    expect(result).to.deep.equal({ key1: 'value2' });
  });

  it('should recursively merge nested objects', () => {
    const target: TranslationObject = { key1: { subkey1: 'value1' } };
    const source: TranslationObject = { key1: { subkey2: 'value2' } };
    const result = deepMerge(target, source);
    expect(result).to.deep.equal({ key1: { subkey1: 'value1', subkey2: 'value2' } });
  });

  it('should not merge non-object properties', () => {
    const target: TranslationObject = { key1: 'value1' };
    const source: TranslationObject = { key2: 2 };
    const result = deepMerge(target, source);
    expect(result).to.deep.equal({ key1: 'value1', key2: 2 });
  });
});
