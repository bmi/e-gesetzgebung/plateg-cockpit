// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { forkJoin } from 'rxjs';

import { AppType, CmsControllerApi, Configuration } from '@plateg/rest-api';
import { displayMessage, getMiddleware, GlobalDI, LoadingStatusController } from '@plateg/theme';

import { de as ggpDe } from '@plateg/ggp/src/messages/de';
import { messages } from '../messages';

export interface TranslationObject {
  [key: string]: string | TranslationObject;
}
const isObject = (item: unknown): item is TranslationObject => {
  return item !== null && typeof item === 'object' && !Array.isArray(item);
};

export const deepMerge = (target: TranslationObject, source: TranslationObject): TranslationObject => {
  if (!isObject(target) || !isObject(source)) {
    console.log('Both target and source must be objects');
  }

  const output: TranslationObject = { ...target };

  Object.keys(source).forEach((key) => {
    if (isObject(source[key])) {
      if (!(key in target)) {
        output[key] = source[key];
      } else {
        output[key] = deepMerge(target[key] as TranslationObject, source[key] as TranslationObject);
      }
    } else {
      output[key] = source[key];
    }
  });

  return output;
};
class BackendPlugin {
  public read(language: string, namespace: string, callback: (firstArg: null, messages: any) => void): void {
    const configRestCalls = new Configuration({ middleware: getMiddleware() });
    const cms_controller = GlobalDI.getOrRegister('cmsController', () => new CmsControllerApi(configRestCalls));
    const loadingStatusCtrl = GlobalDI.getOrRegister('loadingStatusController', () => new LoadingStatusController());
    loadingStatusCtrl.setLoadingStatus(true);
    const appTypes = [
      AppType.Evir,
      AppType.Cockpit,
      AppType.Evor,
      AppType.Egfa,
      AppType.Arbeitshilfen,
      AppType.Rv,
      AppType.Hra,
      AppType.Theme,
      AppType.Editor,
      AppType.Zeit,
    ];

    forkJoin(appTypes.map((x: AppType) => cms_controller.getCmsText({ appType: x })) as []).subscribe({
      next: (messageFilesStream: Blob[]) => {
        let msgs: TranslationObject = {
          ...messages['de']['translation'],
        };

        if (namespace === 'bt' || namespace === 'br') {
          if (msgs.rv) {
            msgs = {
              ...msgs,
              rv: deepMerge(
                msgs.rv as TranslationObject,
                ((msgs.rv as TranslationObject)[namespace] as TranslationObject).rv as TranslationObject,
              ),
            };
          }
          if (msgs.cockpit) {
            msgs = {
              ...msgs,
              cockpit: deepMerge(
                msgs.cockpit as TranslationObject,
                ((msgs.cockpit as TranslationObject)[namespace] as TranslationObject).cockpit as TranslationObject,
              ),
            };
          }
          if (msgs.print && namespace === 'br') {
            msgs = {
              ...msgs,
              print: deepMerge(
                msgs.print as TranslationObject,
                ((msgs.print as TranslationObject)[namespace] as TranslationObject).print as TranslationObject,
              ),
            };
          }
        }

        const promises = messageFilesStream.map((msg) => {
          return msg.text().then((text: string) => {
            try {
              return JSON.parse(text) as { de: { translation: {} } };
            } catch (error) {
              console.error('Could not parse JSON: ', error);
              displayMessage('Fehler beim Einlesen der JSON-Datei (translation)', 'error');
              return undefined;
            }
          });
        });
        void Promise.all(promises).then((values) => {
          values?.forEach((value) => {
            if (value) {
              msgs = { ...msgs, ...value['de']['translation'] };
            }
          });
          msgs.ggp = ggpDe.ggp; // Temporäre Lösung um Übersetzungen von GGP zu laden bis GGP eine eigenständige App wird
          callback(null, msgs);
          loadingStatusCtrl.setLoadingStatus(false);
        });
      },
      error: (error) => {
        displayMessage('Fehler beim Laden der JSON-Datei (translation) aus dem Backend', 'error');
        console.error('Could not load messages from backend, using local messages: ', error);
        callback(null, messages['de']['translation']);
        loadingStatusCtrl.setLoadingStatus(false);
      },
    });
  }
}
BackendPlugin.type = 'backend';
export default BackendPlugin;
