// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { lazy } from 'react';

import { RoutingItem } from './component.react';
import { ReleaseNotesArchive } from './footer/releaseNotes/releaseArchive/component.react';

const Welcome = lazy(() =>
  import('./welcome/component.react').then((module) => {
    return { default: module.Welcome };
  }),
);
const Datenschutz = lazy(() =>
  import('./footer/datenschutz/component.react').then((module) => {
    return { default: module.Datenschutz };
  }),
);
const VsNfd = lazy(() =>
  import('./footer/vs-nfd/component.react').then((module) => {
    return { default: module.VsNfd };
  }),
);
const FAQ = lazy(() =>
  import('./footer/faq/component.react').then((module) => {
    return { default: module.FAQ };
  }),
);
const BarrierefreiheitErklaerungFull = lazy(() =>
  import('./footer/barrierefreiheit/erklaerung/component.react').then((module) => {
    return { default: module.BarrierefreiheitErklaerungFull };
  }),
);
const LeichteSpracheNavFull = lazy(() =>
  import('./footer/leichte-sprache/navigationshilfe/component.react').then((module) => {
    return { default: module.LeichteSpracheNavFull };
  }),
);
const LeichteSpracheWerkzeugeFull = lazy(() =>
  import('./footer/leichte-sprache/werkzeuge/component.react').then((module) => {
    return { default: module.LeichteSpracheWerkzeugeFull };
  }),
);
const SettingsComponent = lazy(() =>
  import('./settings-component/component.react').then((module) => {
    return { default: module.SettingsComponent };
  }),
);
const LoginComponent = lazy(() =>
  import('./login-component/component.react').then((module) => {
    return { default: module.LoginComponent };
  }),
);
const LogoutSuccess = lazy(() =>
  import('./logout-success/component.react').then((module) => {
    return { default: module.LogoutSuccess };
  }),
);
const ReleaseNotes = lazy(() =>
  import('./footer/releaseNotes/component.react').then((module) => {
    return { default: module.ReleaseNotes };
  }),
);
const VersionenComponent = lazy(() =>
  import('./footer/versionen/component.react').then((module) => {
    return { default: module.VersionenComponent };
  }),
);
export const routingItemsCommon: RoutingItem[] = [
  {
    id: 12,
    path: '/barrierefreiheit/erklaerung',
    component: <BarrierefreiheitErklaerungFull />,
    fullscreen: true,
  },
  {
    id: 13,
    path: '/leichteSprache/navigationshilfe',
    component: <LeichteSpracheNavFull />,
    fullscreen: true,
  },
  {
    id: 20,
    path: '/leichteSprache/werkzeuge',
    component: <LeichteSpracheWerkzeugeFull />,
    fullscreen: true,
  },
  {
    id: 14,
    path: '/datenschutz',
    component: <Datenschutz />,
    fullscreen: true,
  },

  {
    id: 16,
    path: '/cockpit',
    component: <Welcome />,
    fullscreen: true,
  },
  {
    id: 17,
    path: '/logoutSuccess',
    component: <LogoutSuccess />,
  },
  {
    id: 18,
    path: '/einstellungen',
    component: (
      <LoginComponent>
        <SettingsComponent />
      </LoginComponent>
    ),
    fullscreen: true,
  },
  {
    id: 19,
    path: '/vsnfd',
    component: <VsNfd />,
    fullscreen: true,
  },
  { id: 63, path: '/versionen', component: <VersionenComponent />, fullscreen: true },
  {
    id: 26,
    path: '/releaseNotes/previousReleases',
    component: <ReleaseNotesArchive />,
    fullscreen: true,
  },
  {
    id: 27,
    path: '/releaseNotes/:sectionRelease?',
    component: <ReleaseNotes />,
    fullscreen: true,
  },
  {
    id: 102,
    path: '/faq',
    label: 'cockpit.login.faqButton', //'Häufige Fragen',
    menuItem: 'cockpit.login.faqButton',
    hint: 'cockpit.login.faqButton',
    component: <FAQ />,
    fullscreen: true,
  },
  {
    id: 103,
    path: '/releaseNotes',
    label: 'cockpit.login.releaseNotes', // Release Notes
    menuItem: 'cockpit.login.releaseNotes',
    hint: 'cockpit.login.releaseNotes',
    component: <ReleaseNotes />,
    fullscreen: true,
  },
];
