// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import sinon from 'sinon';

import { EinstellungenControllerApi, UserControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

import { SettingsController } from './controller';

describe('TEST: SettingsController', () => {
  const ctrl = GlobalDI.getOrRegister('settingsController', () => new SettingsController());
  const userCtrl = GlobalDI.getOrRegister('userController', () => new UserControllerApi());
  const einstellungenControllerApi = GlobalDI.getOrRegister(
    'einstellungenControllerApi',
    () => new EinstellungenControllerApi(),
  );
  const getUserEinstellungStub = sinon.stub(userCtrl, 'getUserEinstellung');
  const setUserSettingsStub = sinon.stub(userCtrl, 'setUserSettings');
  const getStellvertreterCallStub = sinon.stub(userCtrl, 'getStellvertreter');
  const setStellvertreterCallStub = sinon.stub(userCtrl, 'setStellvertreter');
  const getEinstellungenCallStub = sinon.stub(einstellungenControllerApi, 'getEinstellungen');
  const setUserCallStub = sinon.stub(userCtrl, 'getUserWithStellvertreter');
  const resetUserCallStub = sinon.stub(userCtrl, 'resetUser');

  beforeEach(() => {
    getUserEinstellungStub.resetHistory();
    setUserSettingsStub.resetHistory();
    getEinstellungenCallStub.resetHistory();
  });

  after(() => {
    getUserEinstellungStub.reset();
    setUserSettingsStub.reset();
    getEinstellungenCallStub.reset();
  });

  it('TEST: getSettingsCall', () => {
    ctrl.getSettingsCall();
    sinon.assert.calledOnce(getUserEinstellungStub);
  });

  it('TEST: setSettingsCall', () => {
    ctrl.setSettingsCall({});
    sinon.assert.calledOnce(setUserSettingsStub);
  });

  it('TEST: getStellvertreterCall', () => {
    ctrl.getStellvertreterCall();
    sinon.assert.calledOnce(getStellvertreterCallStub);
  });

  it('TEST: setStellvertreterCall', () => {
    ctrl.setStellvertreterCall([]);
    sinon.assert.calledOnce(setStellvertreterCallStub);
  });
  it('TEST: getStellvertretungenCall', () => {
    ctrl.getStellvertretungenCall();
    sinon.assert.calledOnce(getStellvertreterCallStub);
  });
  it('TEST: getEinstellungen', () => {
    einstellungenControllerApi.getEinstellungen();
    sinon.assert.calledOnce(getEinstellungenCallStub);
  });
  it('TEST: setUserCall', () => {
    ctrl.setUserCall('userId');
    sinon.assert.calledOnce(setUserCallStub);
  });
  it('TEST: resetUserCall', () => {
    ctrl.resetUserCall();
    sinon.assert.calledOnce(resetUserCallStub);
  });
});
