// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Observable } from 'rxjs';

import {
  EinstellungenControllerApi,
  EinstellungenDTO,
  UserControllerApi,
  UserEinstellungEntityDTO,
  UserEntityResponseDTO,
  UserEntityWithStellvertreterResponseDTO,
} from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';

export class SettingsController {
  private readonly controller = GlobalDI.getOrRegister('userController', () => new UserControllerApi());

  private readonly einstellungenControllerApi = GlobalDI.getOrRegister(
    'einstellungenControllerApi',
    () => new EinstellungenControllerApi(),
  );

  public getSettingsCall(): Observable<UserEinstellungEntityDTO> {
    return this.controller.getUserEinstellung();
  }

  public setSettingsCall(settings: UserEinstellungEntityDTO): Observable<void> {
    return this.controller.setUserSettings({ userEinstellungEntityDTO: settings });
  }

  public getStellvertreterCall(): Observable<UserEntityResponseDTO[]> {
    return this.controller.getStellvertreter();
  }

  public getStellvertretungenCall(): Observable<UserEntityResponseDTO[]> {
    return this.controller.getStellvertretungen();
  }

  public setStellvertreterCall(requestBody: string[]): Observable<void> {
    return this.controller.setStellvertreter({ requestBody });
  }

  public resetUserCall(): Observable<UserEntityWithStellvertreterResponseDTO> {
    return this.controller.resetUser();
  }

  public setUserCall(userId: string): Observable<UserEntityWithStellvertreterResponseDTO> {
    return this.controller.getUserWithStellvertreter({ userId });
  }

  public getEinstellungen(): Observable<EinstellungenDTO> {
    return this.einstellungenControllerApi.getEinstellungen();
  }
}
