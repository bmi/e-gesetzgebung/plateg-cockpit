// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Affix, Layout, Spin } from 'antd';
import { Content } from 'antd/es/layout/layout';
import i18n from 'i18next';
import React, { useEffect, useState } from 'react';

import { UserEinstellungEntityDTO } from '@plateg/rest-api';
import {
  GlobalDI,
  HeaderComponent,
  HeaderController,
  MenuCollapseController,
  NavbarComponent,
  NavBarItem,
  SiderWrapper,
} from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { SettingsContentComponent } from './settings-content/component.react';

export function SettingsComponent(): React.ReactElement {
  const { Header } = Layout;
  const headerController = GlobalDI.getOrRegister('headerController', () => new HeaderController());
  const [isCollapsed, setIsCollapsed] = useState<boolean>(false);
  const [loadingStatus] = useState<boolean>(false);
  const [menuOffsetTop, setMenuOffsetTop] = useState<number>(0);
  const [userSettings, setUserSettings] = useState<UserEinstellungEntityDTO>();
  const menuCollapseController = new MenuCollapseController(isCollapsed, setIsCollapsed);
  const appStore = {
    setting: useAppSelector((state) => state.setting),
    user: useAppSelector((state) => state.user),
  };
  const kurzbezeichnung = appStore.user.user?.dto.ressort?.kurzbezeichnung;
  const customTranslation = (name: string, dynamicName?: string) => {
    return dynamicName ? dynamicName : i18n.t(`cockpit.settings.nav.${name}`);
  };

  const navBarItems: NavBarItem[] = [
    {
      submenuEntry: { key: 'allgemein', isLink: false },
      menuKeys: [
        { key: 'allgemein.Barrierefreiheit', dynamicName: i18n.t('cockpit.settings.nav.allgemeinBarrierefreiheit') },
        ...(kurzbezeichnung === 'BR'
          ? []
          : [{ key: 'allgemein.Rechtevergabe', dynamicName: i18n.t('cockpit.settings.nav.allgemeinRechtevergabe') }]),
      ],
    },
    {
      menuKeys: [
        { key: 'startseite' },
        ...(kurzbezeichnung === 'BT' ? [{ key: 'emailList' }] : []),
        ...(kurzbezeichnung !== 'BT' && kurzbezeichnung !== 'BR'
          ? [{ key: 'hra' }, { key: 'zeit' }, { key: 'egfa' }, { key: 'emailList' }]
          : []),
      ],
    },
  ];

  useEffect(() => {
    menuCollapseController.updateButtonPosition();
  }, []);

  useEffect(() => {
    headerController.setHeaderProps({
      setMenuOffset: setMenuOffsetTop,
    });
    menuCollapseController.registerListener();
    return () => {
      menuCollapseController.removeListener();
    };
  }, []);
  useEffect(() => {
    if (appStore.setting) {
      setUserSettings(appStore.setting.setting);
    }
  }, [appStore.setting]);
  useEffect(() => {
    menuCollapseController.configureCollapseButton();
  }, [isCollapsed]);

  return (
    <Spin
      {...{ role: loadingStatus ? 'progressbar' : undefined }}
      aria-busy={loadingStatus}
      spinning={loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Layout
        style={{ height: '100%', display: loadingStatus === true ? 'none' : 'flex' }}
        className="site-layout-background"
      >
        <Header className="header-component site-layout-background">
          <HeaderComponent ctrl={headerController} />
        </Header>
        <Layout id="plateg-content-section" className="main-content-area has-drawer">
          <SiderWrapper
            breakpoint="lg"
            collapsedWidth="30"
            onCollapse={() => setIsCollapsed(!isCollapsed)}
            collapsible
            width={280}
            collapsed={isCollapsed}
            className="navbar-component"
          >
            <Affix offsetTop={menuOffsetTop}>
              <span>{!isCollapsed && <NavbarComponent navBarItems={navBarItems} trans={customTranslation} />}</span>
            </Affix>
          </SiderWrapper>
          <Content style={{ padding: '24px 0' }} className="site-layout-background">
            <SettingsContentComponent userSettings={userSettings} setUserSettings={setUserSettings} />
          </Content>
        </Layout>
      </Layout>
    </Spin>
  );
}
