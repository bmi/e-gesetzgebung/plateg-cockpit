// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './settings-item.less';

import { Form, FormInstance, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EinstellungDrawerBreiteType } from '@plateg/rest-api';
import { SelectWrapper } from '@plateg/theme';

export interface SettingsItemComponentProps {
  title: string;
  subtitle: string;
  name: string;
  form?: FormInstance<any>;
  setIsDirty?: (isDirty: boolean) => void;
}

export function SettingsItemComponent(props: SettingsItemComponentProps): React.ReactElement {
  const [isActive, setIsActive] = useState(false);
  const [isSelected, setIsSelected] = useState(EinstellungDrawerBreiteType.Schmal);
  const { t } = useTranslation();

  const onChangeIsSwitch = props.name !== 'barrierefreiheitInformationsleisteBreite';
  useEffect(() => {
    const state = props.form?.getFieldValue(props.name) as boolean | undefined;
    if (state !== undefined) {
      // check if its switch or dropdown
      if (onChangeIsSwitch) {
        setIsActive(props.form?.getFieldValue(props.name) as boolean);
      } else {
        setIsSelected(props.form?.getFieldValue(props.name) as EinstellungDrawerBreiteType);
      }
    }
  }, [props.form?.getFieldsValue()]);

  const onChange = (val: boolean) => {
    setIsActive(val);
    props.setIsDirty?.(true);
  };

  const onChangeSelect = (val: EinstellungDrawerBreiteType) => {
    setIsSelected(val);
    props.setIsDirty?.(true);
  };

  const settingClass = `settings-item ${!onChangeIsSwitch && `non-flex`}`;

  return (
    <div className={settingClass}>
      <div className="setting-description">
        <label htmlFor={props.name} className="setting-item-label">
          {props.title}
        </label>
        <p className="ant-typography p-no-style">{props.subtitle}</p>
      </div>
      {onChangeIsSwitch ? (
        <div className="setting-trigger">
          <label>{t(`cockpit.settings.settingsItem.active.${isActive.toString()}`)}</label>
          <Form.Item valuePropName="checked" name={props.name}>
            <Switch onChange={onChange} />
          </Form.Item>
        </div>
      ) : (
        <div className="setting-select-div">
          <Form.Item
            labelCol={{ span: 24 }}
            name={props.name}
            label={t(`cockpit.settings.drawerWidth.label`).toString()}
          >
            <SelectWrapper
              popupClassName="setting-select-options"
              onChange={onChangeSelect}
              value={isSelected}
              className="setting-select"
              options={[
                {
                  label: (
                    <span aria-label={t(`cockpit.settings.drawerWidth.small`).toString()}>
                      {t(`cockpit.settings.drawerWidth.small`).toString()}
                    </span>
                  ),
                  value: EinstellungDrawerBreiteType.Schmal,
                  title: t(`cockpit.settings.drawerWidth.small`).toString(),
                },
                {
                  label: (
                    <span aria-label={t(`cockpit.settings.drawerWidth.normal`).toString()}>
                      {t(`cockpit.settings.drawerWidth.normal`).toString()}
                    </span>
                  ),
                  value: EinstellungDrawerBreiteType.Normal,
                  title: t(`cockpit.settings.drawerWidth.normal`).toString(),
                },
                {
                  label: (
                    <span aria-label={t(`cockpit.settings.drawerWidth.big`).toString()}>
                      {t(`cockpit.settings.drawerWidth.big`).toString()}
                    </span>
                  ),
                  value: EinstellungDrawerBreiteType.Breit,
                  title: t(`cockpit.settings.drawerWidth.big`).toString(),
                },
              ]}
            />
          </Form.Item>
        </div>
      )}
    </div>
  );
}
