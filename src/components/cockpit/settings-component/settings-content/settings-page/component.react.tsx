// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './settings-page.less';

import { Button, Form } from 'antd';
import Title from 'antd/lib/typography/Title';
import i18n from 'i18next';
import React, { useEffect, useState } from 'react';
import { AjaxError } from 'rxjs/ajax';

import { UserEinstellungEntityDTO } from '@plateg/rest-api';
import { displayMessage, ErrorController, GlobalDI, LoadingStatusController, PageLeavePrompt } from '@plateg/theme';
import { useAppDispatch } from '@plateg/theme/src/components/store';
import { setSetting } from '@plateg/theme/src/components/store/slices/settingSlice';

import { SettingsController } from '../../controller';
import { SettingsItemComponent, SettingsItemComponentProps } from './settings-item/component.react';

interface SettingsPageComponentProps {
  title: string;
  subtitle?: string;
  userSettings?: UserEinstellungEntityDTO;
  setUserSettings: (userSettings: UserEinstellungEntityDTO) => void;
  items?: SettingsItemComponentProps[];
  activePage?: string;
}

export function SettingsPageComponent(props: SettingsPageComponentProps): React.ReactElement {
  const settingsController = GlobalDI.getOrRegister('settingsController', () => new SettingsController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [isDirty, setIsDirty] = useState(false);
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (props.userSettings && props.activePage) {
      form.setFieldsValue({ ...props.userSettings });
      setIsDirty(false);
    }
  }, [props.activePage, props.userSettings]);

  if (!props.userSettings) {
    return <></>;
  }

  const onFinish = (values: UserEinstellungEntityDTO) => {
    const newSettings = { ...props.userSettings, ...values };
    settingsController.setSettingsCall(newSettings).subscribe({
      next: () => {
        displayMessage(i18n.t('cockpit.settings.pages.successMsg'), 'success');
        props.setUserSettings(newSettings);
        loadingStatusController.setLoadingStatus(false);
        dispatch(setSetting(newSettings));
        setIsDirty(false);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
        console.error('Could not save settings', error);
        loadingStatusController.setLoadingStatus(false);
      },
    });
  };

  return (
    <div className="settings-page">
      <Title level={1}>{props.title}</Title>
      <Title level={2}> {props.subtitle}</Title>
      <PageLeavePrompt
        key={`page-leave-confirm-${props.title}`}
        projectName={`Einstellungen für ${props.title}`}
        saveDraft={() => onFinish(form.getFieldsValue() as UserEinstellungEntityDTO)}
        shouldSave={() => isDirty}
        shouldStay={() => form.getFieldsError().some((item) => item.errors.length > 0)}
      />
      <Form onFinish={onFinish} form={form} initialValues={props.userSettings}>
        {props.items?.map((item) => {
          return <SettingsItemComponent key={item.name} {...{ ...item, form: form, setIsDirty: setIsDirty }} />;
        })}
        <div className="form-control-buttons">
          <Button id="cockpit-saveSettings-btn" type="primary" htmlType="submit" size="large">
            {i18n.t('cockpit.settings.pages.saveBtn').toString()}
          </Button>
        </div>
      </Form>
    </div>
  );
}
