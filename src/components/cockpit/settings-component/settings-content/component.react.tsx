// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Row } from 'antd';
import i18n from 'i18next';
import React, { useEffect } from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';

import { UserEinstellungEntityDTO } from '@plateg/rest-api';
import { BreadcrumbComponent, GlobalDI, HeaderController, StandardContentCol } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { SettingsEmailListComponent } from './settings-email-list/component.react';
import { SettingsPageComponent } from './settings-page/component.react';
import { SettingsRightsAssignmentComponent } from './settings-rights-assignment/component.react';
import { StellvertreterViewRightsAssignmentComponent } from './stellvertreter-view-settings-right-assignment/component.react';

interface SettingsComponentProps {
  userSettings?: UserEinstellungEntityDTO;
  setUserSettings: (userSettings: UserEinstellungEntityDTO) => void;
  isPKPEnabled?: boolean;
}

export function SettingsContentComponent(props: SettingsComponentProps): React.ReactElement {
  const headerController = GlobalDI.get<HeaderController>('headerController');
  const routeMatcherVorbereitung = useRouteMatch<{ pageName: string; subPageName: string | undefined }>(
    '/einstellungen/:pageName/:subPageName?',
  );

  const appStore = useAppSelector((state) => state.user);

  useEffect(() => {
    let pageName = routeMatcherVorbereitung?.params?.pageName as string;
    const subPageName = routeMatcherVorbereitung?.params?.subPageName as string;
    if (subPageName) {
      pageName += subPageName;
    }
    headerController.setHeaderProps({
      headerLeft: [
        <BreadcrumbComponent
          key="breadcrumb"
          items={[
            <Link id="cockpit-settings-link" key="cockpit-settings" to="/einstellungen">
              {i18n.t('cockpit.settings.breadcrumb.settings').toString()}
            </Link>,
            <span key={`evor-${pageName}`}>{i18n.t(`cockpit.settings.nav.${pageName}`).toString()}</span>,
          ]}
        />,
      ],
      headerCenter: [],
      headerRight: [],
      headerLast: [],
    });
  }, [routeMatcherVorbereitung]);

  const showRouteIfNot = (excludedIdentifiers: string[]) => {
    const kurzbezeichnung = appStore.user?.dto.ressort?.kurzbezeichnung;
    return kurzbezeichnung !== undefined && !excludedIdentifiers.includes(kurzbezeichnung);
  };

  return (
    <Row>
      <StandardContentCol>
        <Switch>
          <Route
            exact
            path="/einstellungen"
            render={() => <Redirect to={`/einstellungen/allgemein/Barrierefreiheit`} />}
          />
          <Route exact path="/einstellungen/allgemein/Barrierefreiheit">
            {/* Einstellungen Barrierefreiheit  */}
            <SettingsPageComponent
              activePage={routeMatcherVorbereitung?.params?.pageName}
              userSettings={props.userSettings}
              setUserSettings={props.setUserSettings}
              title={i18n.t('cockpit.settings.pages.barrierefreiheit.title')}
              items={[
                {
                  title: i18n.t('cockpit.settings.pages.barrierefreiheit.tabellenAnmerkung.title'),
                  subtitle: i18n.t('cockpit.settings.pages.barrierefreiheit.tabellenAnmerkung.content'),
                  name: 'barrierefreiheitGeoeffneteAnmerkungenInTabellen',
                },
                {
                  title: i18n.t('cockpit.settings.pages.barrierefreiheit.navigationspunkte.title'),
                  subtitle: i18n.t('cockpit.settings.pages.barrierefreiheit.navigationspunkte.content'),
                  name: 'barrierefreiheitMehrzeiligeNavigationspunkte',
                },
                {
                  title: i18n.t('cockpit.settings.pages.barrierefreiheit.vereinfachteMehrauswahl.title'),
                  subtitle: i18n.t('cockpit.settings.pages.barrierefreiheit.vereinfachteMehrauswahl.content'),
                  name: 'barrierefreiheitAeussererScrollbalken',
                },
                {
                  title: i18n.t('cockpit.settings.pages.barrierefreiheit.infoDrawer.title'),
                  subtitle: i18n.t('cockpit.settings.pages.barrierefreiheit.infoDrawer.content'),
                  name: 'barrierefreiheitInformationsleisteBreite',
                },
              ]}
            />
          </Route>

          {showRouteIfNot(['BR']) && (
            <Route exact path="/einstellungen/allgemein/Rechtevergabe">
              {appStore.user?.dto.stellvertreter ? (
                <StellvertreterViewRightsAssignmentComponent />
              ) : (
                <SettingsRightsAssignmentComponent />
              )}
            </Route>
          )}
          {showRouteIfNot(['BR', 'BT']) && [
            <Route exact path="/einstellungen/hra" key="/einstellungen/hra">
              <SettingsPageComponent
                activePage={routeMatcherVorbereitung?.params?.pageName}
                userSettings={props.userSettings}
                setUserSettings={props.setUserSettings}
                title={i18n.t('cockpit.settings.pages.hra.title')}
                subtitle={i18n.t('cockpit.settings.pages.hra.subtitle')}
                items={[
                  {
                    title: i18n.t('cockpit.settings.pages.hra.meineAbstimmungen.title'),
                    subtitle: i18n.t('cockpit.settings.pages.hra.meineAbstimmungen.content'),
                    name: 'emailAbstimmungen',
                  },
                  {
                    title: i18n.t('cockpit.settings.pages.hra.bitteUmMitzeichnung.title'),
                    subtitle: i18n.t('cockpit.settings.pages.hra.bitteUmMitzeichnung.content'),
                    name: 'emailMitzeichnungen',
                  },
                  {
                    title: i18n.t('cockpit.settings.pages.hra.fristverlaengerungen.title'),
                    subtitle: i18n.t('cockpit.settings.pages.hra.fristverlaengerungen.content'),
                    name: 'emailAnfragen',
                  },
                  {
                    title: i18n.t('cockpit.settings.pages.hra.teilnahmeanfragen.title'),
                    subtitle: i18n.t('cockpit.settings.pages.hra.teilnahmeanfragen.content'),
                    name: 'emailTeilnahmeAnfragen',
                  },
                ]}
              />
            </Route>,
            <Route exact path="/einstellungen/zeit" key="/einstellungen/zeit">
              <SettingsPageComponent
                activePage={routeMatcherVorbereitung?.params?.pageName}
                userSettings={props.userSettings}
                setUserSettings={props.setUserSettings}
                title={i18n.t('cockpit.settings.pages.zeit.title')}
                subtitle={i18n.t('cockpit.settings.pages.zeit.subtitle')}
                items={[
                  {
                    title: i18n.t('cockpit.settings.pages.zeit.zeitplanungen.title'),
                    subtitle: i18n.t('cockpit.settings.pages.zeit.zeitplanungen.content'),
                    name: 'emailZeitplanungen',
                  },
                  {
                    title: i18n.t('cockpit.settings.pages.zeit.zeitplanungsvorlagen.title'),
                    subtitle: i18n.t('cockpit.settings.pages.zeit.zeitplanungsvorlagen.content'),
                    name: 'emailZeitplanungsvorlagen',
                  },
                ]}
              />
            </Route>,
            <Route exact path="/einstellungen/egfa" key="/einstellungen/egfa">
              <SettingsPageComponent
                activePage={routeMatcherVorbereitung?.params?.pageName}
                userSettings={props.userSettings}
                setUserSettings={props.setUserSettings}
                title={i18n.t('cockpit.settings.pages.egfa.title')}
                subtitle={i18n.t('cockpit.settings.pages.egfa.subtitle')}
                items={[
                  {
                    title: i18n.t('cockpit.settings.pages.egfa.schnellzugriff.title'),
                    subtitle: i18n.t('cockpit.settings.pages.egfa.schnellzugriff.content'),
                    name: 'emailEgfa',
                  },
                ]}
              />
            </Route>,
          ]}
          <Route exact path="/einstellungen/startseite" key="/einstellungen/startseite">
            <SettingsPageComponent
              activePage={routeMatcherVorbereitung?.params?.pageName}
              userSettings={props.userSettings}
              setUserSettings={props.setUserSettings}
              title={i18n.t('cockpit.settings.pages.startseite.title')}
              items={[
                {
                  title: i18n.t('cockpit.settings.pages.startseite.schnellzugriff.title'),
                  subtitle: i18n.t('cockpit.settings.pages.startseite.schnellzugriff.content'),
                  name: 'startseiteSchnellzugriffAufAktiveRvs',
                },
              ]}
            />
          </Route>
          {showRouteIfNot(['BR']) && (
            <Route exact path="/einstellungen/emailList" key="/einstellungen/emailList">
              <SettingsEmailListComponent />
            </Route>
          )}
        </Switch>
      </StandardContentCol>
    </Row>
  );
}
