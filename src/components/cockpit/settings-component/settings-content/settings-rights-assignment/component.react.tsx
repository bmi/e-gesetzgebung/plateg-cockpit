// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './settings-rights-assignment.less';

import { Button, Form } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import {
  Constants,
  displayMessage,
  EmailsSearchComponent,
  ErrorBox,
  ErrorController,
  GeneralFormWrapper,
  GlobalDI,
  InfoComponent,
  LoadingStatusController,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { SettingsController } from '../../controller';

export function SettingsRightsAssignmentComponent(): React.ReactElement {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const [isFormChanged, setIsFormChanged] = useState<boolean>(false);
  const [selectedUsersEmails, setSelectedUsersEmails] = useState<string[] | null>(null);
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const settingsController = GlobalDI.getOrRegister('settingsController', () => new SettingsController());
  const appStore = useAppSelector((state) => state.user);
  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const subscription = settingsController.getStellvertreterCall().subscribe({
      next: (data) => {
        setSelectedUsersEmails(data.map((user) => user.dto.email));
        form.setFieldsValue({ stellvertretungen: data.map((user) => user.dto.email) });
        loadingStatusController.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
        console.error('Could not fetch settings', error);
        loadingStatusController.setLoadingStatus(false);
      },
    });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  const onFinish = () => {
    const emailsList = form.getFieldValue('stellvertretungen') as string[];
    loadingStatusController.setLoadingStatus(true);
    let successMsg = t(`cockpit.settings.pages.rechtevergabe.successMsg`);
    if (selectedUsersEmails && emailsList.every((elem) => selectedUsersEmails.includes(elem))) {
      successMsg = t(`cockpit.settings.pages.rechtevergabe.deleteSuccessMsg`);
    }
    settingsController.setStellvertreterCall(emailsList).subscribe({
      next: () => {
        setSelectedUsersEmails([...emailsList]);
        loadingStatusController.setLoadingStatus(false);
        displayMessage(successMsg, 'success');
        setIsFormChanged(false);
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
        console.error('Could not save settings', error);
        loadingStatusController.setLoadingStatus(false);
      },
    });
  };

  const onFinishFailed = () => {
    document?.getElementById('errorBox')?.focus();
  };

  const mailValidator = (values: any, userEmail: string, msg: string) => {
    return userEmail && (values as string[]).includes(userEmail) ? Promise.reject(msg) : Promise.resolve();
  };

  return (
    <>
      {selectedUsersEmails && (
        <div className="settings-rights-assignment">
          <Title level={1}>{t('cockpit.settings.pages.rechtevergabe.title')}</Title>
          <p
            style={{ marginBottom: 36 }}
            className="ant-typography p-no-style"
            dangerouslySetInnerHTML={{
              __html: t('cockpit.settings.pages.rechtevergabe.content', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <GeneralFormWrapper
            form={form}
            layout="vertical"
            scrollToFirstError={true}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            initialValues={{ stellvertretungen: selectedUsersEmails }}
            noValidate
          >
            <Form.Item shouldUpdate noStyle>
              {() => <ErrorBox fieldsErrors={form.getFieldsError()} />}
            </Form.Item>
            <div className="emails-search-wrapper">
              <Title level={2}>
                {t('cockpit.settings.pages.rechtevergabe.title2')}
                <InfoComponent title={t('cockpit.settings.pages.rechtevergabe.title2')}>
                  <p
                    dangerouslySetInnerHTML={{
                      __html: t('cockpit.settings.pages.rechtevergabe.title2Drawer'),
                    }}
                  />
                </InfoComponent>
              </Title>

              <Form.Item shouldUpdate>
                <EmailsSearchComponent
                  name="stellvertretungen"
                  form={form}
                  texts={{
                    searchLabel: t('cockpit.settings.pages.rechtevergabe.searchLabel'),
                    searchDrawer: (
                      <InfoComponent title={t('cockpit.settings.pages.rechtevergabe.searchLabel')}>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: t('cockpit.settings.pages.rechtevergabe.searchLabelDrawer'),
                          }}
                        />
                      </InfoComponent>
                    ),
                    searchHint: t('theme.freigabe.searchHint'),
                    addressLabel: t('cockpit.settings.pages.rechtevergabe.addressLabel'),
                    addressDrawer: (
                      <InfoComponent title={t('cockpit.settings.pages.rechtevergabe.addressLabel')}>
                        <p
                          dangerouslySetInnerHTML={{
                            __html: t('cockpit.settings.pages.rechtevergabe.addressLabelDrawer'),
                          }}
                        />
                      </InfoComponent>
                    ),
                  }}
                  rules={[
                    {
                      pattern: Constants.EMAIL_REGEXP_PATTERN,
                      message: t('theme.freigabe.errorShouldBeEmail'),
                    },
                    {
                      validator: (_rule, values) => {
                        return mailValidator(
                          values,
                          appStore.user?.dto.email || '',
                          t('cockpit.settings.pages.rechtevergabe.errorParticipant'),
                        );
                      },
                    },
                  ]}
                  updateFormField={(fieldName: string, value: string[]) => {
                    form.setFieldsValue({
                      [fieldName]: value,
                    });
                    setIsFormChanged(true);
                    void form.validateFields(['stellvertretungen']);
                  }}
                  currentUserEmail={appStore.user?.dto.email || ''}
                  isRowView={true}
                  searchOnlyWithRessortId={appStore.user?.dto.ressort?.id}
                />
              </Form.Item>
              <Button
                id="cockpit-rechtevergabeSave-btn"
                type="primary"
                htmlType="submit"
                className="btn-submit"
                size="large"
                disabled={!isFormChanged}
              >
                {t('cockpit.settings.pages.rechtevergabe.btnSubmit')}
              </Button>
            </div>
          </GeneralFormWrapper>
        </div>
      )}
    </>
  );
}
