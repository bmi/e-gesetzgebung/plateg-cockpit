// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form, Input } from 'antd';
import Title from 'antd/lib/typography/Title';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { EmaillisteSaveDTO } from '@plateg/rest-api';
import { Constants, FormItemWithInfo, GeneralFormWrapper, HiddenInfoComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

export interface NewEmailListTitleComponentComponentProps {
  name: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  setEmailListTitle: (title: string) => void;
  newList: EmaillisteSaveDTO;
  existedTitles: string[];
}
export function Page1NewEmailListTitleComponent(props: NewEmailListTitleComponentComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const handleError = (errorInfo: ValidateErrorEntity) => {
    (form.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function })?.focus();
  };

  useEffect(() => {
    form.setFieldsValue({ titel: props.newList.titel });
  }, [props.newList]);

  const isTitleExistValidator = (value: string, msg: string) => {
    const isTitleExist = props.existedTitles.includes(value.toLowerCase());
    return isTitleExist ? Promise.reject(msg) : Promise.resolve();
  };

  return (
    <>
      <GeneralFormWrapper
        onFinishFailed={handleError}
        form={form}
        id={'page1'}
        name="page1"
        layout="vertical"
        onFinish={(values: { titel: string }) => {
          props.setEmailListTitle(values.titel);
          props.setActivePageIndex(props.activePageIndex + 1);
        }}
      >
        <Title level={2}>{t('cockpit.settings.pages.emailList.modalNewList.page1.title')}</Title>
        <p className="ant-typography">{t('cockpit.settings.pages.emailList.modalNewList.page1.requiredInfo')}</p>
        <FormItemWithInfo
          label={
            <span>
              {t('cockpit.settings.pages.emailList.modalNewList.page1.label')}
              <span style={{ position: 'absolute', marginLeft: '10px' }}>
                <HiddenInfoComponent
                  title={t('cockpit.settings.pages.emailList.modalNewList.page1.info.title')}
                  text={t('cockpit.settings.pages.emailList.modalNewList.page1.info.text')}
                />
              </span>
            </span>
          }
          rules={[
            {
              required: true,
              whitespace: true,
              message: t('cockpit.settings.pages.emailList.modalNewList.page1.requiredMsg'),
            },
            {
              max: Constants.TEXT_BOX_LENGTH,
              message: t('cockpit.settings.pages.emailList.modalNewList.page1.textLength', {
                maxChars: Filters.numberToDecimalString(Constants.TEXT_BOX_LENGTH),
              }),
            },
            {
              validator: (_rule, value: string) => {
                return isTitleExistValidator(
                  value,
                  t('cockpit.settings.pages.emailList.modalNewList.page1.errorUniqueTitle'),
                );
              },
              validateTrigger: 'submit',
            },
          ]}
          name="titel"
          customName={`titel`}
        >
          <Input type="text" />
        </FormItemWithInfo>
      </GeneralFormWrapper>
    </>
  );
}
