// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './new-email-list-review.less';

import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { EmaillisteSaveDTO, UserControllerApi, UserEntityShortDTO } from '@plateg/rest-api';
import { ErrorController, GlobalDI, LoadingStatusController, UserFormattingController } from '@plateg/theme';

export interface Page3ReviewComponentProps {
  newList: EmaillisteSaveDTO;
  setSelectedPersonen: (personen: UserEntityShortDTO[]) => void;
}

export function Page3NewEmailListReviewComponent(props: Page3ReviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const userFormattingController = GlobalDI.getOrRegister<UserFormattingController>(
    'userFormattingController',
    () => new UserFormattingController(),
  );

  const userController = GlobalDI.get<UserControllerApi>('userController');
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const [teilnehmer, setTeilnehmer] = useState<string[]>([]);

  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const getUsersSub = userController.getUserShortListByEmails({ emails: props.newList.emails }).subscribe({
      next: (data) => {
        loadingStatusController.setLoadingStatus(false);
        const personen = data.dtos;
        props.setSelectedPersonen(personen);

        setTeilnehmer(userFormattingController.formatUsers(personen).split(';'));
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'enorm.generalErrorMsg');
        loadingStatusController.setLoadingStatus(false);
      },
    });

    return () => {
      loadingStatusController.setLoadingStatus(false);
      getUsersSub.unsubscribe();
    };
  }, [props.newList]);

  return (
    <div className="metadata-section">
      <Title level={2}>{t('cockpit.settings.pages.emailList.modalNewList.page3.title')}</Title>
      <dl>
        <div className="review-row">
          <dt>{t('cockpit.settings.pages.emailList.modalNewList.page3.listTitleLabel')}</dt>
          <dd>{props.newList.titel}</dd>
        </div>
        <div className="review-row">
          <dt>{t('cockpit.settings.pages.emailList.modalNewList.page3.usersLabel')}</dt>
          <dd>
            {teilnehmer.map((email) => {
              return <div key={email}>{email}</div>;
            })}
          </dd>
        </div>
      </dl>
    </div>
  );
}
