// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Form } from 'antd';
import Title from 'antd/lib/typography/Title';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { EmaillisteSaveDTO, UserEntityDTO } from '@plateg/rest-api';
import { Constants, EmailsSearchComponent, GeneralFormWrapper, HiddenInfoComponent } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

export interface NewEmailListTitleComponentComponentProps {
  name: string;
  activePageIndex: number;
  setActivePageIndex: (activePageIndex: number) => void;
  setEmailListUsers: (emails: string[]) => void;
  newList: EmaillisteSaveDTO;
}
export function Page2NewEmailListUsersComponent(props: NewEmailListTitleComponentComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const appStore = useAppSelector((state) => state.user);
  const [usersData, setUsersData] = useState<UserEntityDTO[]>([]);

  const handleError = (errorInfo: ValidateErrorEntity) => {
    (form.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function })?.focus();
  };
  const mailValidator = (values: any, msg: string) => {
    return appStore.user?.dto.email && (values as string[]).includes(appStore.user?.dto.email)
      ? Promise.reject(msg)
      : Promise.resolve();
  };

  useEffect(() => {
    if (usersData.length) {
      void form.validateFields(['emails']);
    }
  }, [usersData]);

  return (
    <>
      <GeneralFormWrapper
        onFinishFailed={handleError}
        form={form}
        id={props.name}
        name={props.name}
        layout="vertical"
        initialValues={{ emails: props.newList.emails || [] }}
        onFinish={(values: { emails: string[] }) => {
          props.setEmailListUsers(values.emails);
          props.setActivePageIndex(props.activePageIndex + 1);
        }}
      >
        <Title level={2}>{t('cockpit.settings.pages.emailList.modalNewList.page2.title')}</Title>
        <p className="ant-typography">{t('cockpit.settings.pages.emailList.modalNewList.page2.requiredInfo')}</p>
        <EmailsSearchComponent
          name="emails"
          form={form}
          currentUserEmail={appStore.user?.dto.email || ''}
          setUsersDataOnParent={setUsersData}
          texts={{
            searchLabel: t('cockpit.settings.pages.emailList.modalNewList.page2.searchLabel'),
            searchDrawer: (
              <HiddenInfoComponent
                title={t('cockpit.settings.pages.emailList.modalNewList.page2.searchDrawerLabel')}
                text={t('cockpit.settings.pages.emailList.modalNewList.page2.searchDrawer')}
              />
            ),
            searchHint: t('cockpit.settings.pages.emailList.modalNewList.page2.searchHint'),
            addressLabel: t('cockpit.settings.pages.emailList.modalNewList.page2.addressLabel'),
            addressDrawer: (
              <HiddenInfoComponent
                title={t('cockpit.settings.pages.emailList.modalNewList.page2.addressDrawerLabel')}
                text={t('cockpit.settings.pages.emailList.modalNewList.page2.addressDrawer')}
              />
            ),
            noAddresses: t('cockpit.settings.pages.emailList.modalNewList.page2.noSelectedPerson'),
          }}
          rules={[
            { required: true, message: t('cockpit.settings.pages.emailList.modalNewList.page2.error') },
            {
              pattern: Constants.EMAIL_REGEXP_PATTERN,
              message: t('enorm.vote.emailAddress.errorShouldBeEmail'),
            },
            {
              validator: (_rule, values) => {
                return mailValidator(values, t('cockpit.settings.pages.emailList.modalNewList.page2.errorParticipant'));
              },
            },
          ]}
          updateFormField={(fieldName: string, value: string[]) => {
            props.setEmailListUsers(value);
            form.setFieldsValue({
              [fieldName]: value,
            });
            void form.validateFields(['emails']);
          }}
        />
      </GeneralFormWrapper>
    </>
  );
}
