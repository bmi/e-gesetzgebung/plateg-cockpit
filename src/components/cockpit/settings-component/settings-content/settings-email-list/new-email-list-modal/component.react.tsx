// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { EmaillisteResponseDTO, EmaillisteSaveDTO, UserEntityShortDTO } from '@plateg/rest-api';
import { EmaillisteControllerApi } from '@plateg/rest-api/apis/EmaillisteControllerApi';
import {
  displayMessage,
  ErrorController,
  GlobalDI,
  LoadingStatusController,
  MultiPageModalComponent,
} from '@plateg/theme';

import { Page3NewEmailListReviewComponent } from './new-email-list-review/component.react';
import { Page1NewEmailListTitleComponent } from './new-email-list-title/component.react';
import { Page2NewEmailListUsersComponent } from './new-email-list-users/component.react';

export interface NewEmailListModalComponentProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  updateEmailList: (list: EmaillisteResponseDTO) => void;
  selectedList?: EmaillisteResponseDTO;
  existedTitles: string[];
}

export function NewEmailListModalComponent(props: NewEmailListModalComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const emailListController = GlobalDI.get<EmaillisteControllerApi>('emaillisteControllerApi');

  const emptyEmailList: EmaillisteSaveDTO = {
    titel: '',
    emails: [],
  };

  const [activePageIndex, setActivePageIndex] = useState<number>(0);
  const [newEmailList, setNewEmailList] = useState<EmaillisteSaveDTO>(emptyEmailList);
  const [isDraftSubmitted, setIsDraftSubmitted] = useState<boolean>(false);
  const [selectedPersonen, setSelectedPersonen] = useState<UserEntityShortDTO[]>([]);

  useEffect(() => {
    if (props.isVisible && props.selectedList) {
      const list: EmaillisteSaveDTO = {
        titel: props.selectedList?.titel,
        emails: props.selectedList?.personen.map((person) => person.email),
      };
      setNewEmailList(list);
    } else {
      setActivePageIndex(0);
      setNewEmailList(emptyEmailList);
    }
    setIsDraftSubmitted(false);
  }, [props.isVisible]);

  return (
    <>
      <MultiPageModalComponent
        key={`new-email-list-${props.isVisible.toString()}`}
        isVisible={props.isVisible}
        setIsVisible={props.setIsVisible}
        activePageIndex={activePageIndex}
        setActivePageIndex={setActivePageIndex}
        title={t(`cockpit.settings.pages.emailList.modalNewList.mainTitle${props.selectedList ? 'Edit' : ''}`)}
        cancelBtnText={t('cockpit.settings.pages.emailList.modalNewList.btnCancel')}
        nextBtnText={t('cockpit.settings.pages.emailList.modalNewList.btnNext')}
        prevBtnText={t('cockpit.settings.pages.emailList.modalNewList.btnPrev')}
        componentReferenceContext={`new-email-list-${props.isVisible.toString()}`}
        pages={[
          {
            formName: 'page1',
            content: (
              <Page1NewEmailListTitleComponent
                name="page1"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                setEmailListTitle={(titel: string) => {
                  setNewEmailList({
                    ...newEmailList,
                    titel,
                  });
                }}
                newList={newEmailList}
                existedTitles={props.existedTitles}
              />
            ),
          },
          {
            formName: 'page2',
            content: (
              <Page2NewEmailListUsersComponent
                name="page2"
                activePageIndex={activePageIndex}
                setActivePageIndex={setActivePageIndex}
                setEmailListUsers={(emails: string[]) => {
                  setNewEmailList({
                    ...newEmailList,
                    emails,
                  });
                }}
                newList={newEmailList}
              />
            ),
            primaryInsteadNextBtn: {
              buttonText: t('cockpit.settings.pages.emailList.modalNewList.page2.btnNext'),
              shouldNavToNextPage: true,
            },
          },
          {
            content: (
              <Page3NewEmailListReviewComponent newList={newEmailList} setSelectedPersonen={setSelectedPersonen} />
            ),
            primaryInsteadNextBtn: {
              buttonText: t(
                `cockpit.settings.pages.emailList.modalNewList.page3.btnNext${props.selectedList ? 'Edit' : ''}`,
              ),
              disabled: isDraftSubmitted,
            },
            nextOnClick: () => {
              setIsDraftSubmitted(true);
              const observable = props.selectedList
                ? emailListController.modifyEmailliste({ id: props.selectedList.id, emaillisteSaveDTO: newEmailList })
                : emailListController.createEmailliste({ emaillisteSaveDTO: newEmailList });
              const sub = observable.subscribe({
                next: (id) => {
                  loadingStatusController.setLoadingStatus(false);
                  displayMessage(
                    t(`cockpit.settings.pages.emailList.modalNewList.successMsg${props.selectedList ? 'Edit' : ''}`, {
                      listName: newEmailList.titel,
                    }),
                    'success',
                  );
                  props.updateEmailList({
                    titel: newEmailList.titel,
                    id: id,
                    personen: selectedPersonen,
                    bearbeitetAm: new Date().toISOString(),
                    erstelltAm: new Date().toISOString(),
                  });
                  props.setIsVisible(false);
                  sub.unsubscribe();
                },
                error: (error: AjaxError) => {
                  loadingStatusController.setLoadingStatus(false);
                  errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
                  sub.unsubscribe();
                },
              });
            },
          },
        ]}
      />
    </>
  );
}
