// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './settings-email-list.less';

import { Button, Collapse, CollapseProps } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { createRef, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { EmaillisteResponseDTO, UserEntityShortDTO, UserEntityShortDTOStatusEnum } from '@plateg/rest-api';
import { EmaillisteControllerApi } from '@plateg/rest-api/apis/EmaillisteControllerApi';
import {
  displayMessage,
  DropdownMenu,
  ErrorController,
  ExclamationCircleFilled,
  GlobalDI,
  LoadingStatusController,
  PlusOutlined,
} from '@plateg/theme';
import { DirectionRightOutlined } from '@plateg/theme/src/components/icons/DirectionRightOutlined';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { NewEmailListConfirmDeleteModalComponent } from './new-email-list-confirm-delete/component.react';
import { NewEmailListModalComponent } from './new-email-list-modal/component.react';

interface ListRefsInterface {
  [s: string]: React.RefObject<HTMLDivElement>;
}

export function SettingsEmailListComponent(): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  const emailListController = GlobalDI.get<EmaillisteControllerApi>('emaillisteControllerApi');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [emailList, setEmailList] = useState<EmaillisteResponseDTO[]>([]);
  const [selectedList, setSelectedList] = useState<EmaillisteResponseDTO>();
  const [emailListCollapse, setEmailListCollapse] = useState<CollapseProps['items']>([]);
  const [existedTitles, setExistedTitles] = useState<string[]>([]);
  const emailListWrapperRef = useRef<HTMLDivElement>(null);
  const listRef = useRef<ListRefsInterface>({} as ListRefsInterface);
  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const sub = emailListController.getEmaillisten().subscribe({
      next: (data) => {
        setEmailList(data.dtos);
        loadingStatusController.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        loadingStatusController.setLoadingStatus(false);
        errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
      },
    });

    return () => {
      sub.unsubscribe();
    };
  }, []);

  useEffect(() => {
    // Set focus to the same element from that was modal opened
    if ((!isDeleteModalVisible || !isModalVisible) && selectedList) {
      ((listRef as unknown as ListRefsInterface)[selectedList.id].current?.children[0] as HTMLElement)?.focus();
    }
    // IF element doesn't exist anymore set focus to main holder.
    if (!isModalVisible && !selectedList) {
      emailListWrapperRef.current?.focus();
      window.scrollTo(0, 0);
    }
  }, [isDeleteModalVisible, isModalVisible]);

  useEffect(() => {
    const collapsedList = emailList
      .sort((a, b) => a.titel.localeCompare(b.titel))
      .map((item, ind) => {
        (listRef as unknown as ListRefsInterface)[item.id] = createRef<HTMLDivElement>();
        return {
          ref: (listRef as unknown as ListRefsInterface)[item.id],
          key: item.id,
          label: getListName(item),
          children: (
            <>
              {isListOutdated(item) && (
                <div className="actualize-hint-holder">
                  {t('cockpit.settings.pages.emailList.labelActualize')}
                  <Button
                    id={`cockpit-email-list--actualize-btn-${item.id}`}
                    type="link"
                    size="large"
                    onClick={() => {
                      actualizeList(item);
                    }}
                  >
                    {t('cockpit.settings.pages.emailList.linkActualize')}
                  </Button>
                </div>
              )}
              {item.personen.length > 0 && (
                <dl className="personen-holder">
                  <dt className="title">{t('cockpit.settings.pages.emailList.labelRessort')}</dt>
                  <dd className="title">{t('cockpit.settings.pages.emailList.labelBeteiligt')}</dd>
                  {item.personen.map((person) => {
                    const name = person.name || person.email;
                    const ressort = person.ressortKurzbezeichnung ?? null;
                    const abteilungString = person.abteilung ? `, ${person.abteilung}` : '';
                    const fachreferatString = person.abteilung && person.fachreferat ? `, ${person.fachreferat}` : '';
                    const ressortString = ressort ? ` ${ressort}${abteilungString}${fachreferatString}` : '';
                    return (
                      <React.Fragment key={name}>
                        <dt>
                          {isPersonNotRegister(person) && <ExclamationCircleFilled />}
                          {ressortString} &nbsp;
                        </dt>
                        <dd>
                          {name}{' '}
                          {isPersonNotRegister(person) && <i>{t('cockpit.settings.pages.emailList.userNotActive')}</i>}
                        </dd>
                      </React.Fragment>
                    );
                  })}
                </dl>
              )}
            </>
          ),
          extra: getListMenu(item, ind),
        };
      });

    setEmailListCollapse(collapsedList);
  }, [emailList]);

  useEffect(() => {
    setExistedTitles(
      emailList.map((list) => list.titel.toLowerCase()).filter((title) => title !== selectedList?.titel.toLowerCase()),
    );
  }, [emailList, selectedList]);

  const getListMenu = (item: EmaillisteResponseDTO, ind: number) => {
    const items: DropdownMenuItem[] = [
      {
        element: t('cockpit.settings.pages.emailList.btnEdit'),
        onClick: () => {
          setSelectedList(item);
          setIsModalVisible(true);
        },
      },
      {
        element: t('cockpit.settings.pages.emailList.btnDelete'),
        onClick: () => {
          setSelectedList(item);
          setIsDeleteModalVisible(true);
        },
      },
    ];
    if (isListOutdated(item)) {
      items.unshift({
        element: t('cockpit.settings.pages.emailList.btnActualize'),
        onClick: () => {
          actualizeList(item);
        },
      });
    }
    return (
      <div
        key={ind}
        onClick={(e) => {
          e.stopPropagation();
        }}
        onKeyDown={(e) => {
          if (e.key === 'Enter') {
            e.stopPropagation();
          }
        }}
        aria-hidden
      >
        <DropdownMenu
          getPopupContainer={() => document.querySelector('.ant-layout-has-sider')}
          items={items}
          elementId={`menu-${item.id}`}
        />
      </div>
    );
  };

  const actualizeList = (list: EmaillisteResponseDTO) => {
    loadingStatusController.setLoadingStatus(true);
    const updatedPesonen = list.personen.filter((person) => !isPersonNotRegister(person));
    const updatedList = { ...list, personen: updatedPesonen };
    const sub = emailListController
      .modifyEmailliste({
        id: list.id,
        emaillisteSaveDTO: {
          titel: list.titel,
          emails: updatedPesonen.map((person) => person.email),
        },
      })
      .subscribe({
        next: () => {
          updateEmailList(updatedList);
          loadingStatusController.setLoadingStatus(false);
          displayMessage(
            t(`cockpit.settings.pages.emailList.modalNewList.successMsgEdit`, {
              listName: list.titel,
            }),
            'success',
          );
          sub.unsubscribe();
        },
        error: (error: AjaxError) => {
          loadingStatusController.setLoadingStatus(false);
          errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
          sub.unsubscribe();
        },
      });
  };

  const getListName = (list: EmaillisteResponseDTO): React.ReactElement => {
    let listName = <>{list.titel}</>;
    if (isListOutdated(list)) {
      listName = (
        <div className="list-title-outdated">
          <ExclamationCircleFilled />
          {list.titel}
          <br /> {t('cockpit.settings.pages.emailList.labelOutdated')}
        </div>
      );
    }
    return <>{listName}</>;
  };

  const isListOutdated = (list: EmaillisteResponseDTO) => {
    return list.personen.some((person) => isPersonNotRegister(person));
  };

  const isPersonNotRegister = (person: UserEntityShortDTO) => {
    return person.status !== UserEntityShortDTOStatusEnum.Ok;
  };

  const updateEmailList = (item: EmaillisteResponseDTO) => {
    if (emailList.some((el) => el.id === item.id)) {
      setEmailList(emailList.map((list) => (list.id === item.id ? item : list)));
    } else {
      setEmailList([...emailList, item]);
    }
    setSelectedList(undefined);
  };

  const removeEmailList = (id: string) => {
    setEmailList(emailList.filter((el) => el.id !== id));
    setIsDeleteModalVisible(false);
    setSelectedList(undefined);
  };

  return (
    <>
      <Title level={1}>{t('cockpit.settings.pages.emailList.title')}</Title>
      <p
        className="ant-typography"
        dangerouslySetInnerHTML={{
          __html: t('cockpit.settings.pages.emailList.content', {
            interpolation: { escapeValue: false },
          }),
        }}
      ></p>
      <div className="email-list-wrapper" ref={emailListWrapperRef} tabIndex={-1}>
        <Title level={2}>{t('cockpit.settings.pages.emailList.title2')}</Title>
        <Collapse items={emailListCollapse} expandIcon={() => <DirectionRightOutlined />} />
        <Button
          id="cockpit-new-email-list-btn"
          type="text"
          icon={<PlusOutlined />}
          size="large"
          className="btn-new-list"
          onClick={() => {
            setSelectedList(undefined);
            setIsModalVisible(true);
          }}
        >
          {t('cockpit.settings.pages.emailList.btnAddNewList')}
        </Button>
      </div>

      <NewEmailListModalComponent
        isVisible={isModalVisible}
        setIsVisible={setIsModalVisible}
        updateEmailList={updateEmailList}
        selectedList={selectedList}
        existedTitles={existedTitles}
      />
      <NewEmailListConfirmDeleteModalComponent
        isVisible={isDeleteModalVisible}
        setIsVisible={setIsDeleteModalVisible}
        removeEmailList={removeEmailList}
        selectedList={selectedList}
      />
    </>
  );
}
