// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { EmaillisteResponseDTO } from '@plateg/rest-api';
import { EmaillisteControllerApi } from '@plateg/rest-api/apis/EmaillisteControllerApi';
import {
  displayMessage,
  ErrorController,
  ExclamationCircleFilled,
  GlobalDI,
  LoadingStatusController,
  ModalWrapper,
} from '@plateg/theme';

export interface NewEmailListConfirmDeleteModalProps {
  isVisible: boolean;
  setIsVisible: (isVisible: boolean) => void;
  removeEmailList: (id: string) => void;
  selectedList?: EmaillisteResponseDTO;
}
export function NewEmailListConfirmDeleteModalComponent(
  props: NewEmailListConfirmDeleteModalProps,
): React.ReactElement {
  const { t } = useTranslation();
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const emailListController = GlobalDI.get<EmaillisteControllerApi>('emaillisteControllerApi');

  return (
    <>
      {props.selectedList && (
        <ModalWrapper
          className="delete-email-list-confirm-modal"
          open={props.isVisible}
          onOk={() => {
            loadingStatusController.setLoadingStatus(true);
            const sub = emailListController.deleteEmailliste({ id: props.selectedList.id }).subscribe({
              next: (data) => {
                loadingStatusController.setLoadingStatus(false);
                displayMessage(
                  t('cockpit.settings.pages.emailList.deleteModal.successMsg', { listName: props.selectedList.titel }),
                  'success',
                );
                props.removeEmailList(props.selectedList.id);
                props.setIsVisible(false);
                sub.unsubscribe();
              },
              error: (error: AjaxError) => {
                loadingStatusController.setLoadingStatus(false);
                errorCtrl.displayErrorMsg(error, 'egfa.generalErrorMsg');
                sub.unsubscribe();
              },
            });
          }}
          onCancel={() => props.setIsVisible(false)}
          okText={t('cockpit.settings.pages.emailList.deleteModal.okText')}
          cancelText={t('cockpit.settings.pages.emailList.deleteModal.cancelText')}
        >
          <h3 className="delete-confirm-title">
            <ExclamationCircleFilled /> {t('cockpit.settings.pages.emailList.deleteModal.title')}
          </h3>
          <p className="delete-confirm-content">{t('cockpit.settings.pages.emailList.deleteModal.content')}</p>
        </ModalWrapper>
      )}
    </>
  );
}
