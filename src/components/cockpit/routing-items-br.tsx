// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { lazy } from 'react';

import {
  AdministrationMenuIcon,
  StartseiteMenuIcon,
  RVMenuIcon,
  HRAMenuIcon,
  DrucksachennummerMenuIcon,
} from '@plateg/theme';

import { RoutingItem } from './component.react';
import { routingItemsCommon } from './routing-items-common';
import { Welcome } from './welcome/component.react';
import { RolleGlobalType } from '@plateg/rest-api';
import { routingItemsGeneral } from '../general/routing-items-general';

const AppRv = lazy(() =>
  import('@plateg/rv/').then((module) => {
    return { default: module.AppRv };
  }),
);
const LoginComponent = lazy(() =>
  import('./login-component/component.react').then((module) => {
    return { default: module.LoginComponent };
  }),
);
const PrintApp = lazy(() =>
  import('@plateg/print/').then((module) => {
    return { default: module.PrintApp };
  }),
);
const UmlaufApp = lazy(() =>
  import('@plateg/umlauf/').then((module) => {
    return { default: module.UmlaufApp };
  }),
);
const AdministrationApp = lazy(() =>
  import('@plateg/administration/').then((module) => {
    return { default: module.AdministrationApp };
  }),
);

export const routingItemsBRSpecific: RoutingItem[] = [
  {
    id: 1,
    path: '/cockpit',
    label: 'cockpit.menuItems.cockpit.label' /* 'cockpit' */,
    hint: 'cockpit.header.logo.title' /* 'cockpit' */,
    menuItem: 'cockpit.menuItems.cockpit.menuItem',
    component: <Welcome />,
    moduleName: 'cockpit',
    fullscreen: true,
    icon: <StartseiteMenuIcon />,
  },
  {
    id: 4,
    path: '/regelungsvorhaben',
    label: 'cockpit.menuItems.rv.label' /* 'RV' */,
    hint: 'cockpit.menuItems.rv.hint' /* 'Regelungsvorhaben' */,
    menuItem: 'cockpit.menuItems.rv.menuItem',
    component: (
      <LoginComponent>
        <AppRv />
      </LoginComponent>
    ),
    moduleName: 'rv-bt',
    fullscreen: true,
    icon: <RVMenuIcon />,
  },
  {
    id: 6,
    path: '/drucksachen',
    label: 'cockpit.menuItems.drucksachen.label' /* 'Bundesratsdrucksachen' */,
    hint: 'cockpit.menuItems.drucksachen.hint' /* 'Bundesratsdrucksachen' */,
    menuItem: 'cockpit.menuItems.drucksachen.menuItem',
    component: (
      <LoginComponent>
        <PrintApp />
      </LoginComponent>
    ),
    moduleName: 'drucksachen',
    fullscreen: true,
    icon: <DrucksachennummerMenuIcon />,
  },
  {
    id: 24,
    path: '/umlauf',
    label: 'cockpit.menuItems.umlauf.label' /* 'Umlaufverfahren' */,
    hint: 'cockpit.menuItems.umlauf.hint' /* 'Umlaufverfahren' */,
    menuItem: 'cockpit.menuItems.umlauf.menuItem',
    component: (
      <LoginComponent>
        <UmlaufApp />
      </LoginComponent>
    ),
    moduleName: 'umlauf',
    fullscreen: true,
    icon: <HRAMenuIcon />,
  },
  {
    id: 25,
    path: '/fachadministration',
    label: 'cockpit.menuItems.admin.label' /* 'Administration' */,
    hint: 'cockpit.menuItems.admin.hint' /* 'Administration' */,
    menuItem: 'cockpit.menuItems.admin.menuItem',
    component: (
      <LoginComponent>
        <AdministrationApp />
      </LoginComponent>
    ),
    moduleName: 'admin',
    fullscreen: true,
    icon: <AdministrationMenuIcon />,
  },
];

export const routingItemsBR: RoutingItem[] = [...routingItemsBRSpecific, ...routingItemsGeneral, ...routingItemsCommon];

const unavailableRoutesForRole: Partial<Record<RolleGlobalType, number[]>> & { default: number[] } = {
  [RolleGlobalType.BrAdministration]: [2, 3, 4, 6, 24],
  [RolleGlobalType.BrBundesrat]: [],
  [RolleGlobalType.BrFachadministrationP1]: [2, 3, 4, 6, 24],
  [RolleGlobalType.BrFachadministrationAusschuesse]: [2, 3, 4, 6, 24],
  default: [25], // Unavailable: /fachadministration
};

export const getBRRoutes = (role: RolleGlobalType | undefined) => {
  const unavailableRoutes = unavailableRoutesForRole[role ?? 'default'] || unavailableRoutesForRole.default;
  return routingItemsBR.filter((item) => !unavailableRoutes.includes(item.id));
};
