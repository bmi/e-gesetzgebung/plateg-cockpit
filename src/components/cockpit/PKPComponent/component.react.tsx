// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

export function PKPComponent(): React.ReactElement {
  const { t } = useTranslation();
  const appStore = {
    appSetting: useAppSelector((state) => state.appSetting),
  };

  const PKPUrl = appStore.appSetting.appSetting?.iamEinstellungen.pkpUrl || '';
  return (
    <Row>
      <StandardContentCol>
        <Title level={1}>{t('cockpit.pkp.title')}</Title>
        <p>{t('cockpit.pkp.text')}</p>
        <a id="pkp-link" target="_blank" href={`${PKPUrl}`}>
          {t('cockpit.pkp.textLink')}
        </a>
        {t('cockpit.pkp.textLink2')}
      </StandardContentCol>
    </Row>
  );
}
