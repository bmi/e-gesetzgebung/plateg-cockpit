// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';

import { UserEntityWithStellvertreterResponseDTO } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';
import { useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { CockpitLayout } from '../general/cockpit-layout/component.react';
import { Footer } from '../general/footer/component.react';
import { LoginTimeoutController } from '../general/header/subcomponents/logout-timer/controller';
import { Header } from './header/component.react';
import { getBRRoutes, routingItemsBR } from './routing-items-br';
import { getBRegRoutes, routingItemsBReg } from './routing-items-breg';
import { getBTRoutes, routingItemsBT } from './routing-items-bt';

export interface RoutingItem {
  id: number;
  label?: string;
  menuItem?: string;
  hint?: string;
  path: string;
  component: React.ReactElement;
  moduleName?: string;
  fullscreen?: boolean;
  nowrap?: boolean;
  icon?: React.ReactElement;
  disabled?: boolean;
}

export function CockpitFull(): React.ReactElement {
  const appStore = useAppSelector((state) => state.user);
  const ressortMappings: Record<string, string> = {
    BT: 'Bundestag',
    BR: 'Bundesrat',
    default: 'Bundesregierung',
  };
  const kurzbezeichnung = appStore.user?.dto.ressort?.kurzbezeichnung;
  const titleRessort = ressortMappings[kurzbezeichnung ?? 'default'] || ressortMappings['default'];
  const mainTitle = appStore.user?.dto.ressort?.kurzbezeichnung ? `E-Gesetzgebung - ${titleRessort}` : 'E-Gesetzgebung';
  const loginTimeoutCtrl = GlobalDI.getOrRegister('loginTimeoutController', () => new LoginTimeoutController());
  const [routingItems, setRoutingItems] = useState<RoutingItem[]>([
    ...routingItemsBT,
    ...routingItemsBR,
    ...routingItemsBReg,
  ]);
  const [logoutTimer, setLogouttimer] = useState<string | null>(localStorage.getItem('logoutTimer'));
  const { keycloak } = useKeycloak();

  const getRoutesForUser = (user: UserEntityWithStellvertreterResponseDTO | undefined) => {
    if (!user) {
      return [...routingItemsBReg];
    }
    const userRessort = user.dto.ressort?.kurzbezeichnung;
    const role = user.dto.rolle;
    if (userRessort === 'BT') {
      return [...getBTRoutes(role)];
    }
    if (userRessort === 'BR') {
      return [...getBRRoutes(role)];
    }
    return [...getBRegRoutes(role)];
  };

  useEffect(() => {
    let items: RoutingItem[];
    if (logoutTimer && logoutTimer.length) {
      items = getRoutesForUser(appStore.user);
    } else {
      // set logged out routing items
      items = [...routingItemsBReg];
    }
    items.sort((a, b) => a.id - b.id);
    setRoutingItems(items);
  }, [appStore.user, logoutTimer]);

  useEffect(() => {
    const timestamp = localStorage.getItem('logoutTimer');
    setLogouttimer(timestamp);
  }, [routingItems]);

  useEffect(() => {
    window.addEventListener('storage', (e) => {
      if (e.key == 'logoutTimer') {
        const localstorageLastTimeActivity = loginTimeoutCtrl.getLastActivityTimeFromLocalStorage();
        if (!keycloak.authenticated && localstorageLastTimeActivity) {
          keycloak.login().catch((error) => {
            console.log(error);
          });
        }
      }
    });
  }, []);

  const cockpitLayout = (
    <CockpitLayout
      footer={<Footer isFull />}
      header={<Header routingItems={routingItems} />}
      mainTitle={mainTitle}
      routingItems={routingItems}
    />
  );

  return <>{keycloak.authenticated ? <>{appStore.user && cockpitLayout}</> : cockpitLayout}</>;
}
