// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { lazy } from 'react';

import { RolleGlobalType } from '@plateg/rest-api';
import {
  AdministrationMenuIcon,
  DrucksachennummerMenuIcon,
  EditorMenuIcon,
  RVMenuIcon,
  StartseiteMenuIcon,
  UeberweisungMenuIcon,
} from '@plateg/theme';

import { routingItemsGeneral } from '../general/routing-items-general';
import { RoutingItem } from './component.react';
import { routingItemsCommon } from './routing-items-common';
import { Welcome } from './welcome/component.react';

const AppRv = lazy(() =>
  import('@plateg/rv/').then((module) => {
    return { default: module.AppRv };
  }),
);
const Editor = lazy(() =>
  import('@plateg/editor').then((module) => {
    return { default: module.Editor };
  }),
);
const AdministrationApp = lazy(() =>
  import('@plateg/administration/').then((module) => {
    return { default: module.AdministrationApp };
  }),
);
const PrintApp = lazy(() =>
  import('@plateg/print/').then((module) => {
    return { default: module.PrintApp };
  }),
);
const LoginComponent = lazy(() =>
  import('./login-component/component.react').then((module) => {
    return { default: module.LoginComponent };
  }),
);
const UeberweisungApp = lazy(() =>
  import('@plateg/ueberweisung/').then((module) => {
    return { default: module.UeberweisungApp };
  }),
);
export const routingItemsBTSpecific: RoutingItem[] = [
  {
    id: 1,
    path: '/cockpit',
    label: 'cockpit.menuItems.cockpit.label' /* 'cockpit' */,
    hint: 'cockpit.header.logo.title' /* 'cockpit' */,
    menuItem: 'cockpit.menuItems.cockpit.menuItem',
    component: <Welcome />,
    moduleName: 'cockpit',
    fullscreen: true,
    icon: <StartseiteMenuIcon />,
  },
  {
    id: 4,
    path: '/regelungsvorhaben',
    label: 'cockpit.menuItems.rv.label' /* 'RV' */,
    hint: 'cockpit.menuItems.rv.hint' /* 'Regelungsvorhaben' */,
    menuItem: 'cockpit.menuItems.rv.menuItem',
    component: (
      <LoginComponent>
        <AppRv />
      </LoginComponent>
    ),
    moduleName: 'rv-bt',
    fullscreen: true,
    icon: <RVMenuIcon />,
  },
  {
    id: 5,
    path: '/editor',
    label: 'cockpit.menuItems.lea.label' /* 'LEA' */,
    hint: 'cockpit.menuItems.lea.hint' /* 'LEA' */,
    menuItem: 'cockpit.menuItems.lea.menuItem',
    component: (
      <LoginComponent>
        <Editor />
      </LoginComponent>
    ),
    moduleName: 'lea',
    nowrap: true,
    fullscreen: true,
    icon: <EditorMenuIcon />,
  },
  {
    id: 6,
    path: '/drucksachen',
    label: 'cockpit.menuItems.drucksachen.label' /* 'Bundestagsdrucksachen' */,
    hint: 'cockpit.menuItems.drucksachen.hint' /* 'Bundestagsdrucksachen' */,
    menuItem: 'cockpit.menuItems.drucksachen.menuItem',
    component: (
      <LoginComponent>
        <PrintApp />
      </LoginComponent>
    ),
    moduleName: 'drucksachen',
    fullscreen: true,
    icon: <DrucksachennummerMenuIcon />,
  },
  {
    id: 7,
    path: '/ueberweisung',
    label: 'cockpit.menuItems.ueberweisung.label' /* 'Bundestagsueberweisung' */,
    hint: 'cockpit.menuItems.ueberweisung.hint' /* 'Bundestagsueberweisung' */,
    menuItem: 'cockpit.menuItems.ueberweisung.menuItem',
    component: (
      <LoginComponent>
        <UeberweisungApp />
      </LoginComponent>
    ),
    moduleName: 'ueberweisung',
    fullscreen: true,
    icon: <UeberweisungMenuIcon />,
  },
  {
    id: 8,
    path: '/fachadministration',
    label: 'cockpit.menuItems.admin.label' /* 'Administration' */,
    hint: 'cockpit.menuItems.admin.hint' /* 'Administration' */,
    menuItem: 'cockpit.menuItems.admin.menuItem',
    component: (
      <LoginComponent>
        <AdministrationApp />
      </LoginComponent>
    ),
    moduleName: 'admin',
    fullscreen: true,
    icon: <AdministrationMenuIcon />,
  },
];

export const routingItemsBT: RoutingItem[] = [...routingItemsBTSpecific, ...routingItemsGeneral, ...routingItemsCommon];

const unavailableRoutesForRole: Partial<Record<RolleGlobalType, number[]>> & { default: number[] } = {
  [RolleGlobalType.Administration]: [6], // Unavailable: /drucksachen
  [RolleGlobalType.AdministrationUndSachbearbeitung]: [],
  [RolleGlobalType.Sachbearbeitung]: [8], // Unavailable: /fachadministration
  [RolleGlobalType.BtFachadministrationPd1]: [4, 5, 6], // Unavailable: /drucksachen /RV /Editor
  [RolleGlobalType.BtFachadministrationAusschuesse]: [4, 5, 6], // Unavailable: /drucksachen /RV /Editor
  [RolleGlobalType.Pd1Annahmestelle]: [8], // Unavailable: /fachadministration
  [RolleGlobalType.Pd1Lektorat]: [8], // Unavailable: /fachadministration
  [RolleGlobalType.Bundestagsfraktion]: [6, 8], // Unavailable: /drucksachen, /fachadministration
  [RolleGlobalType.Ausschusssekretariat]: [6, 8], // Unavailable: /drucksachen, /fachadministration
  [RolleGlobalType.Ausschussmitglied]: [6, 8], // Unavailable: /drucksachen, /fachadministration
  default: [6, 8], // Unavailable: /drucksachen /fachadministration
};

export const getBTRoutes = (role: RolleGlobalType | undefined) => {
  const unavailableRoutes = unavailableRoutesForRole[role ?? 'default'] || unavailableRoutesForRole.default;
  return routingItemsBT.filter((item) => !unavailableRoutes.includes(item.id));
};
