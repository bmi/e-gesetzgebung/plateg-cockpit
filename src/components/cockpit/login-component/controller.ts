// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { UserControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';
export class LoginController {
  public getLoginUrl(): string {
    const userController: UserControllerApi = GlobalDI.get('userController');
    return `${userController.configuration.basePath}/oauth2/authorization/keycloak?redirectUri=${encodeURIComponent(
      window.location.href,
    )}`;
  }
}
