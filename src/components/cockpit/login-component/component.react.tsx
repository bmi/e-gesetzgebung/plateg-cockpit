// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './login.less';

import { Button, Image } from 'antd';
import Title from 'antd/lib/typography/Title';
import i18n from 'i18next';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ExternalLinkOutlined, GlobalDI, ImageModule } from '@plateg/theme';
import { useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { useAppDispatch } from '@plateg/theme/src/components/store';
import { clearUser } from '@plateg/theme/src/components/store/slices/userSlice';

import { LoginTimeoutController } from '../../general/header/subcomponents/logout-timer/controller';

interface LoginComponentProps {
  children: React.ReactElement;
}
export function LoginComponent(props: LoginComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const target = props.children;
  const [signedIn, setSignedIn] = useState<boolean | undefined>(undefined);
  const [loggedOutByTimeout, setLoggedOutByTimeout] = useState<boolean | undefined>(undefined);
  const loginTimeoutCtrl = GlobalDI.getOrRegister('loginTimeoutController', () => new LoginTimeoutController());
  const dispatch = useAppDispatch();
  const { keycloak } = useKeycloak();
  useEffect(() => {
    // Listen to event about automatic logout by timeout
    loginTimeoutCtrl.subscribeStatusLogoutByTimeout().subscribe((status) => {
      if (status) {
        setLoggedOutByTimeout(true);
        dispatch(clearUser());
      }
    });
  }, []);
  useEffect(() => {
    setSignedIn(keycloak.authenticated);
  }, [keycloak.authenticated]);

  const loginGuard = (
    <div className="login-page-content">
      <Image
        loading="lazy"
        alt=""
        height={242}
        preview={false}
        src={(require(`../../../media/login-page/Illustration_Anmeldescreen.svg`) as ImageModule).default}
        className="login-image"
      />
      <Title level={1}>{i18n.t(`cockpit.${loggedOutByTimeout ? 'logoutTimer' : 'login'}.title`).toString()}</Title>
      <p className="ant-typography p-no-style paragraph-1">
        {t(`cockpit.${loggedOutByTimeout ? 'logoutTimer' : 'login'}.paragraph1`)}
      </p>
      <Button id="plateg-login-btn" onClick={() => void keycloak.login()} className="login-button" type="primary">
        {t(`cockpit.${loggedOutByTimeout ? 'logoutTimer' : 'login'}.loginButton`)}
        <ExternalLinkOutlined />
      </Button>
      {loggedOutByTimeout && (
        <Button id="plateg-login-homepage-btn" href={'#/cockpit'} className="login-button" type="default">
          {t(`cockpit.logoutTimer.btnHomepage`)}
        </Button>
      )}
    </div>
  );

  // If undefined -> backend call has not yet returned -> empty screen
  // If true -> show target
  // If false -> show request to login
  if (signedIn === undefined) {
    return <></>;
  }
  return signedIn ? target : loginGuard;
}
