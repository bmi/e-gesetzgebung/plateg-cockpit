// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './dashboard.less';

import { Button, Col, Image, Row, Spin, TabsProps, Typography } from 'antd';
import { Content } from 'antd/es/layout/layout';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { forkJoin } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { HomepageTableDTO, RegelungsvorhabenEntityResponseShortDTO, UserEntityResponseDTO } from '@plateg/rest-api';
import { ErrorController, GlobalDI, ImageModule, TabsWrapper } from '@plateg/theme';
import { Loading } from '@plateg/theme/src/components/icons/Loading';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import { setNeuigkeiten } from '@plateg/theme/src/components/store/slices/neuigkeitenSlice';

import WatermarkBundesrat from '../../../media/welcome-page/Grafik-Adler_Bundesrat.svg';
import WatermarkBundestag from '../../../media/welcome-page/Grafik-Adler_Bundestag.svg';
import { HeaderComponentController } from '../header/controller';
import { ActiveRegelungsVorhaben } from './active-regelungsVorhaben/component.react';
import { AufgabenTabComponent } from './aufgaben-tab/component.react';
import { DashboardController } from './controller.react';
import { NeuigkeitenTabComponent } from './neuigkeiten-tab/component.react';
import { ZuletztBearbeitetTabComponent } from './zuletzt-bearbeitet-tab/component.react';

interface DashboardProps {
  currentUser: UserEntityResponseDTO;
}
interface DashboardState {
  activeTab: string;
  zuletztBearbeitetData: HomepageTableDTO[];
  loadingStatus: boolean;
  showRvs: boolean | undefined;
  currentDate: Date;
  activeRegelungsvorhaben?: RegelungsvorhabenEntityResponseShortDTO[];
}
export function Dashboard(props: DashboardProps): React.ReactElement {
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const { t } = useTranslation();
  const { Title } = Typography;
  const history = useHistory();
  const routeMatcherVorbereitung = useRouteMatch<{ tabKey: string }>('/cockpit/:tabKey');
  const headerCtrl = GlobalDI.getOrRegister('cockpitHeaderComponentController', () => new HeaderComponentController());
  const dashboardController = GlobalDI.getOrRegister('dashboardController', () => new DashboardController());
  const dispatch = useAppDispatch();
  const appStore = {
    setting: useAppSelector((state) => state.setting),
    neuigkeiten: useAppSelector((state) => state.neuigkeiten),
    user: useAppSelector((state) => state.user),
  };
  const defaultActiveTab: string =
    appStore.user?.user?.dto.ressort?.kurzbezeichnung === 'BT' ? 'neuigkeiten' : 'zuletztBearbeitet';

  const [dashboardState, setDashboardState] = useState<DashboardState>({
    activeTab:
      routeMatcherVorbereitung?.params?.tabKey != null ? routeMatcherVorbereitung?.params?.tabKey : defaultActiveTab,
    zuletztBearbeitetData: [],
    loadingStatus: false,
    showRvs: false,
    currentDate: new Date(),
  });

  useEffect(() => {
    setDashboardState({ ...dashboardState, loadingStatus: true });
    const loadContentSubscription = forkJoin({
      latestActivities: dashboardController.getZuletztBearbeitetCall(),
      activeRegelungsVorhaben: dashboardController.getActiveRegelungsvorhaben(),
    }).subscribe({
      next: (results) => {
        results.latestActivities.sort((activity1, activity2) =>
          dashboardController.dateSort(activity1.bearbeitetAm, activity2.bearbeitetAm),
        );
        results.activeRegelungsVorhaben.dtos.sort((rv1, rv2) =>
          dashboardController.dateSort(rv1.dto.bearbeitetAm, rv2.dto.bearbeitetAm),
        );
        setDashboardState({
          ...dashboardState,
          zuletztBearbeitetData: results.latestActivities,
          activeRegelungsvorhaben: results.activeRegelungsVorhaben.dtos,
          activeTab:
            routeMatcherVorbereitung?.params?.tabKey != null
              ? routeMatcherVorbereitung?.params?.tabKey
              : defaultActiveTab,
          currentDate: new Date(),
          loadingStatus: false,
        });
      },
      error: (error: AjaxError) => {
        setDashboardState({
          ...dashboardState,
          activeTab:
            routeMatcherVorbereitung?.params?.tabKey != null
              ? routeMatcherVorbereitung?.params?.tabKey
              : defaultActiveTab,
          currentDate: new Date(),
          loadingStatus: false,
        });
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
    return function cleanup() {
      loadContentSubscription.unsubscribe();
    };
  }, []);
  useEffect(() => {
    setDashboardState({
      ...dashboardState,
      showRvs: appStore.setting.setting?.startseiteSchnellzugriffAufAktiveRvs,
    });
  }, [appStore.setting, dashboardState.activeRegelungsvorhaben]);
  const getGreetingKey = () => {
    const hours = dashboardState.currentDate.getHours();
    if (hours >= 4 && hours <= 10) {
      return t(`cockpit.dashboard.gutenMorgen`);
    }
    if ((hours >= 18 && hours <= 23) || (hours >= 0 && hours <= 3)) {
      return t(`cockpit.dashboard.gutenAbend`);
    }
    return t(`cockpit.dashboard.gutenTag`);
  };

  const tabItems: TabsProps['items'] = [
    ...(appStore.user?.user?.dto.ressort?.kurzbezeichnung !== 'BT'
      ? [
          {
            key: 'zuletztBearbeitet',
            label: t('cockpit.dashboard.tabs.zuletztBearbeitet.tabNav'),
            children: !dashboardState.zuletztBearbeitetData.length ? (
              <div className="empty-tab-content">
                <Image
                  loading="lazy"
                  height={135}
                  preview={false}
                  src={(require(`../../../media/welcome-page/empty_zuletztBearbeitet.svg`) as ImageModule).default}
                  alt=""
                />
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.dashboard.tabs.zuletztBearbeitet.emptyMessage'),
                  }}
                />
              </div>
            ) : (
              <ZuletztBearbeitetTabComponent zuletztBearbeitetData={dashboardState.zuletztBearbeitetData} />
            ),
          },
        ]
      : []),
    {
      key: 'neuigkeiten',
      label: (
        <div className="neuigkeiten-tab-header">
          <span>{t('cockpit.dashboard.tabs.neuigkeiten.tabNav')}</span>
          {appStore.neuigkeiten.ungeleseneNeuigkeiten > 0 && (
            <span className="neuigkeiten-number">{appStore.neuigkeiten.ungeleseneNeuigkeiten}</span>
          )}
        </div>
      ),
      children: !appStore.neuigkeiten.neuigkeitenData.length ? (
        <div className="empty-tab-content">
          <Image
            loading="lazy"
            height={103}
            width={131}
            preview={false}
            src={(require(`../../../media/welcome-page/empty_neuigkeiten.svg`) as ImageModule).default}
            alt=""
          />
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.dashboard.tabs.neuigkeiten.emptyMessage'),
            }}
          />
        </div>
      ) : (
        <NeuigkeitenTabComponent all={false} setNeuigkeitenData={(data) => dispatch(setNeuigkeiten(data))} />
      ),
    },
    {
      key: 'aufgaben',
      label: t('cockpit.dashboard.tabs.aufgaben.tabNav'),
      children: <AufgabenTabComponent all={false} />,
    },
  ];

  const layoutstyles = {
    containerBT: {
      width: '520px',
      height: '453.025px',
      backgroundImage: `url(${WatermarkBundestag})`,
      backgroundRepeat: 'no-repeat',
      display: 'inline-flex',
      padding: '96px 0px 56px 0px',
      position: 'absolute',
      right: '0',
      opacity: '0.3',
    },
    containerBR: {
      width: '520px',
      height: '452.2px',
      backgroundImage: `url(${WatermarkBundesrat})`,
      backgroundRepeat: 'no-repeat',
      display: 'inline-flex',
      padding: '96px 0px 56px 0px',
      position: 'absolute',
      right: '0',
      opacity: '0.3',
    },
  } as const;

  const getLogo = () => {
    switch (appStore.user?.user?.dto.ressort?.kurzbezeichnung) {
      case 'BT':
        return <div style={layoutstyles.containerBT}></div>;
      case 'BR':
        return <div style={layoutstyles.containerBR}></div>;
      default:
        return;
    }
  };

  const getEmptyRvContent = () => {
    const emptyRvMessage = t('cockpit.dashboard.emptyRvMessage');
    let newRvButton = (
      <Button
        type="link"
        onClick={() => history.push('/regelungsvorhaben/meineRegelungsvorhaben/neuesRegelungsvorhaben')}
      >
        {`${t('cockpit.dashboard.rvAnlegen')} >`}
      </Button>
    );
    if (appStore.user?.user?.dto.ressort?.kurzbezeichnung === 'BT') {
      newRvButton = <></>;
    }
    return (
      <div className="empty-rv-content">
        <Image
          loading="lazy"
          height={92}
          preview={false}
          src={(require(`../../../media/welcome-page/RV-init-icon_EmptyState.svg`) as ImageModule).default}
          alt=""
        />
        <p
          dangerouslySetInnerHTML={{
            __html: emptyRvMessage,
          }}
        />
        {newRvButton}
      </div>
    );
  };
  return (
    <Spin
      {...{ role: dashboardState.loadingStatus ? 'progressbar' : undefined }}
      aria-busy={dashboardState.loadingStatus}
      spinning={dashboardState.loadingStatus}
      size="large"
      tip="Laden..."
      wrapperClassName="loading-screen"
      indicator={<Loading />}
    >
      <Content
        style={{
          display: dashboardState.loadingStatus === true ? 'none' : 'block',
          paddingTop: '67px',
        }}
        className="main-content-area has-drawer"
      >
        {getLogo()}
        <div className="dashboard">
          <Row>
            <Col xs={{ span: 18, offset: 3 }}>
              <div className="welcome-meine-rv">
                <div className="day-date">
                  <span>
                    {dashboardState.currentDate.toLocaleDateString(t('cockpit.langCode'), {
                      weekday: 'long',
                      year: 'numeric',
                      month: 'numeric',
                      day: 'numeric',
                    })}
                  </span>
                </div>
                <div className="greeting-user">
                  <span className="user-avatar">{headerCtrl.computeInitials(props.currentUser.dto.name)}</span>
                  <Title level={1} className="greeting-message">
                    <span>{`${getGreetingKey()},`}</span>
                    <span>{props.currentUser.dto.name}</span>
                  </Title>
                </div>
                {dashboardState.showRvs && (
                  <>
                    {dashboardState.activeRegelungsvorhaben?.length ? (
                      <ActiveRegelungsVorhaben
                        activeRegelungsvorhaben={dashboardState.activeRegelungsvorhaben}
                        setActiveRegelungsvorhaben={(rvs: RegelungsvorhabenEntityResponseShortDTO[]) =>
                          setDashboardState({ ...dashboardState, activeRegelungsvorhaben: rvs })
                        }
                      />
                    ) : (
                      <div className="empty-dashboard-rv">
                        <Title level={2} className="dashboard-rv-title">
                          {t('cockpit.dashboard.meineRegelungsvorhaben')}
                        </Title>
                        {getEmptyRvContent()}
                      </div>
                    )}
                  </>
                )}
              </div>
            </Col>
          </Row>

          <Row className="schnellzugriff-row">
            <Col xs={{ span: 18, offset: 3 }}>
              <div className="schnellzugriff">
                <Title className="schnellzugriff-title" level={2}>
                  {t('cockpit.dashboard.schnellzugriffTitle')}
                </Title>
                <TabsWrapper
                  items={tabItems}
                  activeKey={dashboardState.activeTab}
                  className="standard-tabs"
                  onChange={(key: string) => {
                    setDashboardState({ ...dashboardState, activeTab: key });
                    history.push(`/cockpit/${key}`);
                  }}
                  moduleName={t('cockpit.dashboard.mainTitle')}
                />
              </div>
            </Col>
          </Row>
        </div>
      </Content>
    </Spin>
  );
}
