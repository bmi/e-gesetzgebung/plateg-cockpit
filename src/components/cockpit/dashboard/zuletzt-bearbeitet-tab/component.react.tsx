// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { HomepageTableDTO } from '@plateg/rest-api/models/';
import { GlobalDI, TableComponent, TableComponentProps } from '@plateg/theme';

import { DashboardController } from '../controller.react';

interface ZuletztBearbeitetTabComponentInterface {
  zuletztBearbeitetData: HomepageTableDTO[];
}

export function ZuletztBearbeitetTabComponent(props: ZuletztBearbeitetTabComponentInterface): React.ReactElement {
  const { t } = useTranslation();
  const dashboardController = GlobalDI.getOrRegister('dashboardController', () => new DashboardController());

  const [zuletztBearbeitetData, setZuletztBearbeitetData] = useState(props.zuletztBearbeitetData);
  const [zuletztBearbeitetTableVals, setZuletztBearbeitetTableVals] = useState<TableComponentProps<HomepageTableDTO>>({
    id: 'zuletzt-bearbeitet-table',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });
  const [zuletztBearbeitetTableContent, setZuletztBearbeitetTableContent] = useState<HomepageTableDTO[]>([]);
  useEffect(() => {
    setZuletztBearbeitetData(props.zuletztBearbeitetData);
  }, [props.zuletztBearbeitetData]);

  useEffect(() => {
    setZuletztBearbeitetTableVals(dashboardController.getZuletztBearbeitetTableVals(zuletztBearbeitetData));
  }, [zuletztBearbeitetData]);
  useEffect(() => {
    setZuletztBearbeitetTableContent(zuletztBearbeitetTableVals.content);
  }, [zuletztBearbeitetTableVals]);
  return (
    <>
      {zuletztBearbeitetTableContent.length > 0 && (
        <>
          <div className="dashboard-table-top">
            <span>{`${zuletztBearbeitetTableContent.length} ${
              zuletztBearbeitetTableContent.length === 1
                ? t(`cockpit.dashboard.eintrag`)
                : t(`cockpit.dashboard.eintraege`)
            }`}</span>
          </div>
          <TableComponent
            id={zuletztBearbeitetTableVals.id}
            columns={zuletztBearbeitetTableVals.columns}
            content={zuletztBearbeitetTableContent}
            expandable={zuletztBearbeitetTableVals.expandable}
            className={zuletztBearbeitetTableVals.className}
            hiddenRowCount
          />
        </>
      )}
    </>
  );
}
