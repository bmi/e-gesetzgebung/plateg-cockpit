// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './aufgaben-tab.less';

import { Button, Image, Table } from 'antd';
import { ExpandableConfig } from 'antd/es/table/interface';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';
import { v4 as uuidv4 } from 'uuid';

import { AufgabeEntityDTO, AufgabeEntityListResponseDTO } from '@plateg/rest-api/models/';
import {
  displayMessage,
  DropdownMenu,
  ErrorController,
  GlobalDI,
  ImageModule,
  InfoComponent,
  TableComponentProps,
} from '@plateg/theme';
import { DirectionDownOutlined } from '@plateg/theme/src/components/icons/DirectionDownOutlined';
import { DirectionRightOutlined } from '@plateg/theme/src/components/icons/DirectionRightOutlined';
import { useAppSelector } from '@plateg/theme/src/components/store';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { DashboardController } from '../controller.react';
import { AufgabenFormValues, AufgabenModal } from './aufgaben-modal/component.react';

interface AufgabenTabComponentInterface {
  all: boolean;
  setAufgaben?: (val: AufgabeEntityListResponseDTO) => void;
  aufgaben?: AufgabeEntityListResponseDTO;
}

export function AufgabenTabComponent(props: AufgabenTabComponentInterface): React.ReactElement {
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const appStore = useAppSelector((state) => state.setting);
  const { t } = useTranslation();
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [expandAll, setExpandAll] = useState<boolean>(false);
  const [internalAufgaben, setInternalAufgaben] = useState<AufgabeEntityListResponseDTO>();
  const [tableExpandedRowKeys, setTableExpandedRowKeys] = useState<string[]>([]);
  const dashboardController = GlobalDI.getOrRegister('dashboardController', () => new DashboardController());
  const [aufgabenTableVals, setAufgabenTableVals] = useState<TableComponentProps<AufgabeEntityDTO>>({
    id: 'aufgaben-table',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });
  const [selectedAufgabe, setSelectedAufgabe] = useState<AufgabeEntityDTO | undefined>();

  let aufgaben: AufgabeEntityListResponseDTO | undefined;
  let setAufgaben: (val: AufgabeEntityListResponseDTO) => void;
  if (props.aufgaben && props.setAufgaben) {
    aufgaben = props.aufgaben;
    setAufgaben = props.setAufgaben;
  } else {
    aufgaben = internalAufgaben;
    setAufgaben = setInternalAufgaben;
  }

  const deleteCheckedAufgabenEvent = () => {
    dashboardController.deleteAufgaben().subscribe({
      next: () => {
        getAufgaben();
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };
  const setAufgabeCheckedEvent = (id: string, checked: boolean) => {
    dashboardController.setAufgabeChecked(id, checked).subscribe({
      next: () => {
        if (aufgaben) {
          setAufgaben({
            ...aufgaben,
            dtos: aufgaben.dtos.map((item) => {
              if (item.id === id) {
                return { ...item, erledigtAm: checked ? new Date().toString() : '' };
              }
              return item;
            }),
          });
        }
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };
  const saveAufgabe = (aufgabe: AufgabenFormValues) => {
    const toSaveAufgabe: AufgabeEntityDTO = !selectedAufgabe
      ? {
          id: uuidv4(),
          aufgabeText: aufgabe.aufgabeText,
          anmerkung: aufgabe.anmerkung,
          fristAm: dashboardController.generateFristenString(aufgabe.fristDate, aufgabe.fristTime),
        }
      : {
          ...selectedAufgabe,
          aufgabeText: aufgabe.aufgabeText,
          anmerkung: aufgabe.anmerkung,
          fristAm: dashboardController.generateFristenString(aufgabe.fristDate, aufgabe.fristTime),
        };

    dashboardController.saveAufgabe(toSaveAufgabe, !!selectedAufgabe).subscribe({
      next: () => {
        getAufgaben();
        setIsModalVisible(false);
        displayMessage(
          t(`cockpit.dashboard.tabs.aufgaben.${selectedAufgabe ? 'successModifiedMessage' : 'successCreatMessage'}`),
          'success',
        );
      },
      error: (error: AjaxError) => {
        setIsModalVisible(false);
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };

  const deleteaufgabeEvent = (id: string) => {
    dashboardController.deleteAufgabe(id).subscribe({
      next: () => {
        if (aufgaben) {
          setAufgaben({
            aufgabenInsgesamt: aufgaben.aufgabenInsgesamt - 1,
            dtos: aufgaben.dtos.filter((item) => item.id !== id),
          });
        }
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };
  const showEditModal = (aufgabe: AufgabeEntityDTO) => {
    setSelectedAufgabe(aufgabe);
    setIsModalVisible(true);
  };
  useEffect(() => {
    if (!isModalVisible) {
      setSelectedAufgabe(undefined);
    }
  }, [isModalVisible]);
  useEffect(() => {
    if (aufgaben) {
      setAufgabenTableVals(
        dashboardController.geAufgabenTableVals(
          props.all ? aufgaben.dtos : aufgaben.dtos.slice(0, 15),
          setAufgabeCheckedEvent,
          deleteaufgabeEvent,
          showEditModal,
        ),
      );
    }
  }, [aufgaben]);
  useEffect(() => {
    const sub = getAufgaben();

    return () => {
      sub.unsubscribe();
    };
  }, []);
  useEffect(() => {
    if (appStore.setting) {
      setExpandAll(!!appStore.setting.barrierefreiheitGeoeffneteAnmerkungenInTabellen);
    }
  }, [appStore.setting?.barrierefreiheitGeoeffneteAnmerkungenInTabellen]);

  useEffect(() => {
    if (aufgaben && expandAll) {
      setTableExpandedRowKeys(aufgaben.dtos.filter((item) => item.erledigtAm || item.anmerkung).map((item) => item.id));
    }
  }, [aufgaben, expandAll]);
  const getAufgaben = () => {
    return dashboardController.getAufgabenCall(false).subscribe({
      next: (data) => {
        const withFristDtos = data.dtos.filter((item) => item.fristAm);
        const withoutFristDtos = data.dtos.filter((item) => !item.fristAm);
        withFristDtos.sort((a, b) => {
          return Number(new Date(a.fristAm ?? '')) - Number(new Date(b.fristAm ?? ''));
        });
        withoutFristDtos.sort((a, b) => {
          return Number(new Date(a.erstelltAm ?? '')) - Number(new Date(b.erstelltAm ?? ''));
        });

        setAufgaben({ ...data, dtos: [...withFristDtos, ...withoutFristDtos] });
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };

  const eintragText =
    aufgaben?.aufgabenInsgesamt === 1 ? t(`cockpit.dashboard.eintrag`) : t(`cockpit.dashboard.eintraege`);
  const dropDownItems: DropdownMenuItem[] = [
    {
      element: t(`cockpit.dashboard.tabs.aufgaben.neueAufgabe`),
      onClick: () => setIsModalVisible(true),
    },
    {
      element: t(`cockpit.dashboard.tabs.aufgaben.aufgabenLoeschen`),
      onClick: deleteCheckedAufgabenEvent,
    },
  ];

  let topContent = (
    <>
      <span>{`${aufgaben && aufgaben.dtos.length > 15 ? '15' : aufgaben?.dtos.length || ''} ${t(
        `cockpit.dashboard.von`,
      )} ${aufgaben?.aufgabenInsgesamt || ''} ${eintragText}`}</span>
      {aufgaben && aufgaben.aufgabenInsgesamt > 15 && (
        <InfoComponent
          prefilledWidth={'80vw'}
          buttonText={t(`cockpit.dashboard.tabs.aufgaben.zuAllenAufgaben`)}
          buttonId="dashboard-aufgaben-drawer-btn"
          title={t(`cockpit.dashboard.tabs.aufgaben.tabNav`)}
          id="dashboard-aufgaben-drawer"
          titleWithoutPrefix
          getContainer=".ant-layout-has-sider"
        >
          <div className="aufgaben-drawer-content">
            <AufgabenTabComponent all={true} setAufgaben={setAufgaben} aufgaben={aufgaben} />
          </div>
        </InfoComponent>
      )}
    </>
  );

  if (props.all && aufgaben) {
    topContent = <span>{` ${aufgaben.aufgabenInsgesamt} ${eintragText}`}</span>;
  }

  const changeRowsKey = (expanded: boolean, record: AufgabeEntityDTO) => {
    if (!expanded) {
      setTableExpandedRowKeys([...tableExpandedRowKeys, record.id]);
    } else {
      setTableExpandedRowKeys(tableExpandedRowKeys.filter((id) => id !== record.id));
    }
  };
  const openButton = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-openDetailView-btn-${elementId}`}
      aria-label="Detailansicht (zuklappbar) wird in der nächsten Tabellenzeile angezeigt"
      onClick={onClick}
      type="text"
    >
      <DirectionDownOutlined />
    </Button>
  );
  const closeButton = (onClick: React.MouseEventHandler<HTMLElement>, elementId: string) => (
    <Button
      id={`theme-generic-styleGuide-expnadDetailView-btn-${elementId}`}
      aria-label="Detailsicht aufklappen"
      onClick={onClick}
      type="text"
    >
      <DirectionRightOutlined />
    </Button>
  );
  const expandedConfig: ExpandableConfig<AufgabeEntityDTO> = {
    expandedRowRender: (record: AufgabeEntityDTO) => {
      return (
        <div className="anmerkungen-container">
          {record.anmerkung && (
            <div>
              <Title level={4}>{t(`cockpit.dashboard.tabs.aufgaben.modal.anmerkungenLabel`)}:</Title>
              <p>{record.anmerkung}</p>
            </div>
          )}
          {record.erledigtAm && (
            <div>
              <Title level={4}>{t(`cockpit.dashboard.tabs.aufgaben.table.erledigtAm`)}:</Title>
              <p>{dashboardController.dateTimeFormat(record.erledigtAm)}</p>
            </div>
          )}
        </div>
      );
    },
    rowExpandable: (record: AufgabeEntityDTO) => {
      {
        return !!record.anmerkung || !!record.erledigtAm;
      }
    },
    expandIcon: ({ expanded, record }) => {
      if (record.anmerkung || record.erledigtAm) {
        if (expanded) {
          return openButton(() => changeRowsKey(expanded, record), record.id);
        } else {
          return closeButton(() => changeRowsKey(expanded, record), record.id);
        }
      }
    },
  };

  return (
    <>
      <div
        // Fix to avoid propagation arrow keys pushing to tabs menu only in popup
        onKeyDown={(e) => {
          e.stopPropagation();
        }}
      >
        <AufgabenModal
          isModalVisible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
          saveAufgabe={saveAufgabe}
          toEditAufgabe={selectedAufgabe}
        />
      </div>
      {aufgaben?.dtos.length ? (
        <div className="aufgaben-container">
          {aufgabenTableVals.content.length > 0 && (
            <>
              <div className="dashboard-table-top" style={{ marginBottom: '19px' }}>
                {topContent}
                <span style={{ marginLeft: 'auto' }}>
                  <DropdownMenu
                    getPopupContainer={() => document.querySelector('.ant-layout-has-sider')}
                    items={dropDownItems}
                    elementId="menu-dashboard-aufgaben"
                  />
                </span>
              </div>

              <Table
                expandedRowKeys={tableExpandedRowKeys}
                id={aufgabenTableVals.id}
                rowKey={(record) => record.id}
                columns={aufgabenTableVals.columns}
                dataSource={aufgabenTableVals.content}
                pagination={{
                  defaultPageSize: 20,
                  position: ['bottomCenter'],
                  hideOnSinglePage: true,
                  showSizeChanger: false,
                  showTitle: true,
                }}
                expandable={expandedConfig}
                className={aufgabenTableVals.className}
              />
            </>
          )}
        </div>
      ) : (
        <div className="empty-tab-content empty-aufgaben">
          <Image
            loading="lazy"
            height={103}
            width={131}
            preview={false}
            src={(require(`../../../../media/welcome-page/empty_aufgaben.svg`) as ImageModule).default}
            alt=""
          />
          <div>
            <p
              dangerouslySetInnerHTML={{
                __html: t('cockpit.dashboard.tabs.aufgaben.emptyMessage'),
              }}
            />
            <Button onClick={() => setIsModalVisible(true)} type="link">
              {t('cockpit.dashboard.tabs.aufgaben.emptyMessageLink')}
            </Button>
          </div>
        </div>
      )}
    </>
  );
}
