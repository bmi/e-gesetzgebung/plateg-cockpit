// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './aufgaben-modal.less';

import { Form, Input, InputRef } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import Title from 'antd/es/typography/Title';
import { format } from 'date-fns';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { AufgabeEntityDTO } from '@plateg/rest-api';
import { Constants, GeneralFormWrapper, MultiPageModalComponent } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

import { DateTimeComponent } from './dateTimeComponent/component.react';

interface AufgabenModalProps {
  isModalVisible: boolean;
  setIsModalVisible: (value: boolean) => void;
  saveAufgabe: (aufgabe: AufgabenFormValues) => void;
  toEditAufgabe?: AufgabeEntityDTO;
}
export interface AufgabenFormValues {
  aufgabeText: string;
  fristDate: string | undefined;
  fristTime: string | undefined;
  anmerkung: string | undefined;
}

export function AufgabenModal(props: AufgabenModalProps): React.ReactElement {
  const { t } = useTranslation();
  const aufgabeTextRef = useRef<InputRef>(null);
  const [form] = Form.useForm();
  let timer: NodeJS.Timeout;
  useEffect(() => {
    if (props.isModalVisible) {
      if (!props.toEditAufgabe) {
        form.resetFields();
      }
      timer = setTimeout(() => {
        aufgabeTextRef.current?.focus();
      }, 400);
    }
    return () => clearTimeout(timer);
  }, [props.isModalVisible]);
  const handleError = (errorInfo: ValidateErrorEntity) => {
    (form.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function }).focus();
  };
  const onFinish = () => {
    const values = form.getFieldsValue() as AufgabenFormValues;

    props.saveAufgabe({
      anmerkung: values.anmerkung,
      aufgabeText: values.aufgabeText,
      fristDate: form.getFieldValue('fristDate-date-placeholder') as string | undefined,
      fristTime: form.getFieldValue('fristTime-placeholder') as string | undefined,
    });
  };

  useEffect(() => {
    if (props.toEditAufgabe) {
      form.setFieldsValue({
        aufgabeText: props.toEditAufgabe.aufgabeText,
        'fristDate-date-placeholder': props.toEditAufgabe.fristAm
          ? format(new Date(props.toEditAufgabe.fristAm), 'dd.MM.yyyy')
          : undefined,
        'fristTime-placeholder': props.toEditAufgabe.fristAm
          ? format(new Date(props.toEditAufgabe.fristAm), 'HH:mm')
          : undefined,
        anmerkung: props.toEditAufgabe.anmerkung,
      });
    }
  }, [props.toEditAufgabe]);
  const modalTitle = props.toEditAufgabe
    ? t('cockpit.dashboard.tabs.aufgaben.modal.modifyTitle')
    : t('cockpit.dashboard.tabs.aufgaben.modal.creatTitle');
  const saveBtnLabel = props.toEditAufgabe
    ? t('cockpit.dashboard.tabs.aufgaben.modal.btnModifyTitle')
    : t('cockpit.dashboard.tabs.aufgaben.modal.creatTitle');
  return (
    <MultiPageModalComponent
      rootClassName="aufgaben-modal-root"
      className="aufgaben-modal"
      key={'aufgaben-modal' + props.isModalVisible.toString()}
      componentReferenceContext={'aufgaben-modal' + props.isModalVisible.toString()}
      isVisible={props.isModalVisible}
      setIsVisible={props.setIsModalVisible}
      title={modalTitle}
      cancelBtnText={t('cockpit.dashboard.tabs.aufgaben.modal.cancel')}
      nextBtnText={saveBtnLabel}
      prevBtnText=""
      activePageIndex={0}
      pages={[
        {
          content: (
            <>
              <Title level={2}>
                {t(
                  `cockpit.dashboard.tabs.aufgaben.modal.${
                    props.toEditAufgabe ? 'modifyContentTitle' : 'creatContentTitle'
                  }`,
                )}
              </Title>
              <p>{t('cockpit.dashboard.tabs.aufgaben.modal.contentDescription')}</p>
              <GeneralFormWrapper
                onFinishFailed={handleError}
                form={form}
                id={`aufgaben-modal`}
                name="page1"
                className="aufgaben-modal-form"
                layout="vertical"
                onFinish={onFinish}
              >
                <Form.Item
                  name="aufgabeText"
                  label={<span>{t('cockpit.dashboard.tabs.aufgaben.modal.aufgabeLabel')}</span>}
                  rules={[
                    {
                      required: true,
                      message: t('cockpit.dashboard.tabs.aufgaben.modal.aufgabeMissedError'),
                      validateTrigger: 'submit',
                    },
                  ]}
                >
                  <Input type="text" ref={aufgabeTextRef} />
                </Form.Item>
                <Form.Item
                  name="anmerkung"
                  label={<span>{t('cockpit.dashboard.tabs.aufgaben.modal.anmerkungenLabel')}</span>}
                  rules={[
                    {
                      max: Constants.TEXT_AREA_LENGTH,
                      message: t('cockpit.dashboard.tabs.aufgaben.modal.anmerkungenLengthError', {
                        maxChars: Filters.numberToDecimalString(Constants.TEXT_AREA_LENGTH),
                      }),
                      validateTrigger: 'submit',
                    },
                  ]}
                >
                  <TextArea rows={5} />
                </Form.Item>
                <Title level={2}>{t('cockpit.dashboard.tabs.aufgaben.table.tableHeads.frist')}</Title>
                <DateTimeComponent
                  aliasDate="fristDate"
                  aliasTime="fristTime"
                  form={form}
                  minDate={new Date().toString()}
                  value={props.toEditAufgabe?.fristAm}
                />
              </GeneralFormWrapper>
            </>
          ),
          primaryInsteadNextBtn: {
            buttonText: saveBtnLabel,
            shouldNavToNextPage: false,
          },
          nextOnClick: () => {
            form.submit();
          },
        },
      ]}
    />
  );
}
