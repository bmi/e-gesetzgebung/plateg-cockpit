// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Col, Form, Input, Row } from 'antd';
import { FormInstance } from 'antd/lib/form';
import { set } from 'date-fns';
import endOfDay from 'date-fns/endOfDay';
import isBefore from 'date-fns/isBefore';
import isSameDay from 'date-fns/isSameDay';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { CustomDatepicker, getDateTimeString } from '@plateg/theme';

interface DateTimeComponentProps {
  aliasDate: string;
  aliasTime: string;
  form: FormInstance;
  index?: number;
  minDate?: string;
  value?: string;
  onChange?: () => void;
}
const TIME_REGEX = /^([01]\d|2[0-3]):([0-5]\d)$/;
export function DateTimeComponent(props: DateTimeComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const [date, setDate] = useState<Date | null>();
  const ref = useRef<HTMLDivElement>(null);
  const timePlaceholderName = `${props.aliasTime}-placeholder`;

  // Gets value that should be checked and returns boolean
  const disabledDate = (possibleDate: Date) => {
    const current = new Date();
    return (
      isBefore(endOfDay(possibleDate), endOfDay(current)) ||
      (props.minDate && isBefore(endOfDay(possibleDate), endOfDay(new Date(props.minDate))))
    );
  };

  useEffect(() => {
    if (props.form.getFieldValue(`${props.aliasDate}-date-placeholder`)) {
      props.onChange?.();
    }
  }, [date]);

  const errorTextDate = t('cockpit.dashboard.tabs.aufgaben.modal.deadline.errorDate');
  const errorTextTime = t('cockpit.dashboard.tabs.aufgaben.modal.deadline.errorTime');
  const errorTextMin = t(`cockpit.dashboard.tabs.aufgaben.modal.deadline.errorTimeBeforeMin`, {
    date: props.minDate ? getDateTimeString(props.minDate) : '',
  });
  const errorTextTimeFormat = t(`cockpit.dashboard.tabs.aufgaben.modal.deadline.errorTimeFormat`, {
    index: props.index,
  });
  const errorTextDateFormat = t(`cockpit.dashboard.tabs.aufgaben.modal.deadline.errorDateFormat`, {
    index: props.index,
  });
  const errorTextTimeFuture = t(`cockpit.dashboard.tabs.aufgaben.modal.deadline.errorTimeFuture`, {
    index: props.index,
  });

  const isTimeDisabled = (value: string) => {
    const current = new Date();
    const result = prepareSelectedTime(value);
    if (!date) {
      return false;
    }
    if (
      (isSameDay(date, current) && isBefore(result, current)) ||
      isBefore(date.setHours(0, 0, 0, 0), current.setHours(0, 0, 0, 0))
    ) {
      return true;
    } else {
      return false;
    }
  };

  const isBeforeCustomMin = (value: string, customMin?: string) => {
    const time = prepareSelectedTime(value);
    return customMin && date
      ? isBefore(set(date, { hours: time.getHours(), minutes: time.getMinutes() }), new Date(customMin))
      : false;
  };

  const prepareSelectedTime = (value: string) => {
    const parsedDate = value.split(':');
    return set(new Date(), { hours: Number(parsedDate[0]), minutes: Number(parsedDate[1]), seconds: 0 });
  };

  return (
    <Row ref={ref} gutter={8} className="date-time-holder">
      <Col span={12}>
        <CustomDatepicker
          textPlaceholder=""
          disabledDate={disabledDate}
          setDate={setDate}
          getPopupContainer={() => ref.current as HTMLElement}
          aliasDate={props.aliasDate}
          aliasTime={props.aliasTime}
          form={props.form}
          timePlaceholderName={timePlaceholderName}
          errorRequired={t(`cockpit.dashboard.tabs.aufgaben.modal.deadline.errorDate`, { index: props.index })}
          errorDateTime={errorTextDate}
          errorInvalidFormat={errorTextDateFormat}
          errorDisabledDate={errorTextTimeFuture}
          additionalValidators={[
            //check if val is before maxDate
            {
              // eslint-disable-next-line @typescript-eslint/no-misused-promises
              validator: (_rule, value: string) => {
                if (value && props.minDate && isBeforeCustomMin(value, props.minDate)) {
                  return Promise.reject(errorTextDate);
                } else {
                  return Promise.resolve();
                }
              },
              message: errorTextMin,
              validateTrigger: 'submit',
            },
          ]}
        />
      </Col>
      <Col span={12}>
        <Form.Item
          className="time-field"
          name={timePlaceholderName}
          dependencies={[props.aliasDate]}
          label={<span>{t('cockpit.dashboard.tabs.aufgaben.modal.deadline.timeLabel')}</span>}
          rules={[
            {
              // Überprüft, dass auch eine Uhrzeit eingegeben wird, wenn ein Datum eingegeben wurde
              validator: () => {
                if (
                  // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                  props.form.getFieldValue(`${props.aliasDate}-date-placeholder`) !== undefined &&
                  // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                  props.form.getFieldValue(`${props.aliasDate}-date-placeholder`) !== '' &&
                  (props.form.getFieldValue(timePlaceholderName) === '' ||
                    props.form.getFieldValue(timePlaceholderName) === undefined)
                ) {
                  return Promise.reject(errorTextTime);
                } else {
                  return Promise.resolve();
                }
              },
              message: errorTextTime,
              validateTrigger: 'submit',
            },
            {
              // Validate format on submit
              validator: (_rule, value: string) => {
                if (value && !TIME_REGEX.test(value)) {
                  return Promise.reject(errorTextTimeFormat);
                } else {
                  return Promise.resolve();
                }
              },
              validateTrigger: 'submit',
            },
            {
              // Validate time should be in future
              validator: (_rule, value: string) => {
                if (value && value.length > 4 && isTimeDisabled(value)) {
                  return Promise.reject(errorTextTimeFuture);
                } else {
                  return Promise.resolve();
                }
              },
              validateTrigger: 'submit',
            },
            //check if val is before maxDate

            //check if val is after end of min date
            {
              validator: (_rule, value: string) => {
                if (value && value.length > 4 && props.minDate && isBeforeCustomMin(value, props.minDate)) {
                  return Promise.reject();
                } else {
                  return Promise.resolve();
                }
              },
              message: errorTextMin,
              validateTrigger: 'submit',
            },
          ]}
        >
          <Input type="text" />
        </Form.Item>
      </Col>
    </Row>
  );
}
