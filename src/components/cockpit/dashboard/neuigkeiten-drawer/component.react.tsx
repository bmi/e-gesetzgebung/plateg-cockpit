// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './neuigkeiten-drawer.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { InfoComponent } from '@plateg/theme';
import { NeuigkeitenState } from '@plateg/theme/src/components/store/slices/neuigkeitenSlice';

import { NeuigkeitenTabComponent } from '../neuigkeiten-tab/component.react';

interface NeuigkeitenDrawerProps {
  id: string;
  buttonText: string | React.ReactElement;
  buttonId: string;
  setNeuigkeitenData: (neuigkeiten: NeuigkeitenState) => void;
  getContainer?: string;
  buttonAriaLabel?: string;
}
export function NeuigkeitenDrawer(props: NeuigkeitenDrawerProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <InfoComponent
      prefilledWidth={'80vw'}
      buttonText={props.buttonText}
      buttonId={props.buttonId}
      title={t(`cockpit.dashboard.tabs.neuigkeiten.tabNav`)}
      id={props.id}
      titleWithoutPrefix
      getContainer={props.getContainer}
      buttonAriaLabel={props.buttonAriaLabel}
    >
      <div className="neuigkeiten-drawer-content">
        <NeuigkeitenTabComponent setNeuigkeitenData={props.setNeuigkeitenData} all={true} />
      </div>
    </InfoComponent>
  );
}
