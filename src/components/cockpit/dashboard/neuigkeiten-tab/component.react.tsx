// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './neuigkeiten-tab.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import { NeuigkeitEntityDTO } from '@plateg/rest-api/models/';
import { DropdownMenu, ErrorController, GlobalDI, TableComponent, TableComponentProps } from '@plateg/theme';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import {
  deleteNeuigkeit,
  deleteNeuigkeiten,
  NeuigkeitenState,
  setNeuigkeitenGelesen,
  setNeuigkeitGelesen,
  setNeuigkeitLesen,
} from '@plateg/theme/src/components/store/slices/neuigkeitenSlice';
import {
  setPaginationFilterInfo,
  setPaginationResult,
} from '@plateg/theme/src/components/store/slices/tablePaginationSlice';
import { DropdownMenuItem } from '@plateg/theme/src/components/table-component/table-sub-components/dropdown-button-component/component.react';

import { DashboardController } from '../controller.react';
import { NeuigkeitenDrawer } from '../neuigkeiten-drawer/component.react';

interface NeuigkeitenTabComponentInterface {
  setNeuigkeitenData: (neuigkeiten: NeuigkeitenState) => void;
  all: boolean;
}

export function NeuigkeitenTabComponent(props: NeuigkeitenTabComponentInterface): React.ReactElement {
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());

  const { t } = useTranslation();
  const dispatch = useAppDispatch();
  const appStore = useAppSelector((state) => state.neuigkeiten);
  const dashboardController = GlobalDI.getOrRegister('dashboardController', () => new DashboardController());
  const history = useHistory();
  const [neuigkeitenTableVals, setNeuigkeitenTableVals] = useState<TableComponentProps<NeuigkeitEntityDTO>>({
    id: 'neuigkeiten-table',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });
  const [neuigkeitenTableContent, setNeuigkeitenTableContent] = useState<NeuigkeitEntityDTO[]>([]);
  const [neuigkeitenDrawerVals, setNeuigkeitenDrawerVals] = useState<TableComponentProps<NeuigkeitEntityDTO>>({
    id: 'neuigkeiten-drawer',
    columns: [],
    content: [],
    expandable: false,
    sorterOptions: [],
    customDefaultSortIndex: 0,
    expandableCondition: undefined,
    className: '',
  });
  const [neuigkeitenDrawerContent, setNeuigkeitenDrawerContent] = useState<NeuigkeitEntityDTO[]>([]);
  const pagDataRequest = useAppSelector((state) => state.tablePagination.tabs['neuigkeiten']).request;

  const setNeuigkeitenGelesenEvent = () => {
    dashboardController.setNeuigkeitenGelesen().subscribe({
      next: () => {
        dispatch(setNeuigkeitenGelesen());
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };
  const deleteNeuigkeitenEvent = () => {
    dashboardController.deleteNeuigkeiten().subscribe({
      next: () => {
        dispatch(deleteNeuigkeiten());
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };
  const onNeuigkeitClick = (link: string, neuigkeit: NeuigkeitEntityDTO) => {
    if (!neuigkeit.gelesen) {
      dashboardController.setNeuigkeitGelesen(neuigkeit.id, false).subscribe({
        next: () => {
          dispatch(setNeuigkeitGelesen({ id: neuigkeit.id }));
          history.push(link);
        },
        error: (error: AjaxError) => {
          errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
          history.push(link);
        },
      });
    } else {
      history.push(link);
    }
  };
  const setNeuigkeitLesenEvent = (id: string, gelesen: boolean) => {
    dashboardController.setNeuigkeitGelesen(id, !gelesen).subscribe({
      next: () => {
        dispatch(setNeuigkeitLesen({ id, gelesen }));
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };

  const deleteNeuigkeitEvent = (id: string) => {
    dashboardController.deleteNeuigkeit(id).subscribe({
      next: () => {
        dispatch(deleteNeuigkeit({ id }));
      },
      error: (error: AjaxError) => {
        errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
      },
    });
  };
  useEffect(() => {
    setNeuigkeitenTableVals(
      dashboardController.getNeuigkeitenTableVals(
        appStore.neuigkeitenHomeData,
        onNeuigkeitClick,
        setNeuigkeitLesenEvent,
        deleteNeuigkeitEvent,
      ),
    );
    setNeuigkeitenDrawerVals(
      dashboardController.getNeuigkeitenTableVals(
        appStore.neuigkeitenData,
        onNeuigkeitClick,
        setNeuigkeitLesenEvent,
        deleteNeuigkeitEvent,
      ),
    );
  }, [appStore]);

  useEffect(() => {
    setNeuigkeitenTableContent(neuigkeitenTableVals.content);
    setNeuigkeitenDrawerContent(neuigkeitenDrawerVals.content);
  }, [neuigkeitenTableVals, neuigkeitenDrawerVals]);

  useEffect(() => {
    loadContent(pagDataRequest.currentPage || 0);
  }, [pagDataRequest.currentPage]);

  const loadContent = (pageNumber: number) => {
    dashboardController.getNeuigkeitenCall(pageNumber).subscribe({
      next: (neuigkeitendata) => {
        neuigkeitendata.dtos.sort((neuigkeit1, neuigkeit2) =>
          dashboardController.dateSort(neuigkeit1.erstelltAm, neuigkeit2.erstelltAm),
        );

        props.setNeuigkeitenData({
          neuigkeitenData: neuigkeitendata.dtos,
          neuigkeitenHomeData: appStore.neuigkeitenHomeData,
          neuigkeitenInsgesamt: neuigkeitendata.neuigkeitenInsgesamt,
          ungeleseneNeuigkeiten: neuigkeitendata.ungeleseneNeuigkeitenInsgesamt,
        });

        dispatch(
          setPaginationResult({
            tabKey: 'neuigkeiten',
            totalItems: neuigkeitendata.neuigkeitenInsgesamt,
            currentPage: pageNumber,
          }),
        );
        dispatch(setPaginationFilterInfo({ tabKey: 'neuigkeiten', vorhabenartFilter: [], filterNames: [] }));
      },
    });
  };

  const eintragText =
    appStore.neuigkeitenInsgesamt === 1 ? t(`cockpit.dashboard.eintrag`) : t(`cockpit.dashboard.eintraege`);
  let dropDownItems: DropdownMenuItem[] = [
    {
      element: t(`cockpit.dashboard.tabs.neuigkeiten.eintraegeAlsGelesen`),
      onClick: setNeuigkeitenGelesenEvent,
    },
  ];
  const btnText = t(`cockpit.dashboard.tabs.neuigkeiten.zuAllenNeuigkeiten`);

  let topContent = (
    <>
      <span>{`${appStore.neuigkeitenData.length > 15 ? '15' : appStore.neuigkeitenData.length} ${t(
        `cockpit.dashboard.von`,
      )} ${appStore.neuigkeitenInsgesamt} ${eintragText}`}</span>
      <NeuigkeitenDrawer
        id="dashboard-neuigkeiten-drawer"
        buttonText={btnText}
        buttonId="dashboard-neuigkeiten-drawer-btn"
        setNeuigkeitenData={props.setNeuigkeitenData}
        getContainer=".ant-layout-has-sider"
        buttonAriaLabel={`${btnText}, Informationsleiste`}
      />
    </>
  );

  if (props.all) {
    topContent = <span>{` ${appStore.neuigkeitenInsgesamt} ${eintragText}`}</span>;

    dropDownItems = [
      {
        element: t(`cockpit.dashboard.tabs.neuigkeiten.eintraegeLoeschen`),
        onClick: deleteNeuigkeitenEvent,
      },
      ...dropDownItems,
    ];
  }

  return (
    <div className="neuigkeiten-container">
      {neuigkeitenTableContent.length > 0 && (
        <>
          <div className="dashboard-table-top">
            {topContent}
            <span style={{ marginLeft: 'auto' }}>
              <DropdownMenu
                getPopupContainer={() => document.querySelector('.ant-layout-has-sider')}
                items={dropDownItems}
                elementId="menu-dashboard-neuigkeiten"
              />
            </span>
          </div>

          <TableComponent
            tabKey="neuigkeiten"
            rowClassName={(record: NeuigkeitEntityDTO) => (!record.gelesen ? 'new-row' : '')}
            id={neuigkeitenTableVals.id}
            columns={neuigkeitenTableVals.columns}
            content={props.all ? neuigkeitenDrawerContent : neuigkeitenTableContent}
            expandable={neuigkeitenTableVals.expandable}
            className={neuigkeitenTableVals.className}
            hiddenRowCount
            autoHeight
            scrollElement={props.all ? document.querySelector('.ant-drawer-body') : undefined}
            bePagination={props.all}
          />
        </>
      )}
    </div>
  );
}
