// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Checkbox, Tooltip } from 'antd';
import { CheckboxChangeEvent } from 'antd/es/checkbox';
import { ColumnsType } from 'antd/es/table/interface';
import Text from 'antd/lib/typography/Text';
import Title from 'antd/lib/typography/Title';
import { lightFormat, parse, set } from 'date-fns';
import { zonedTimeToUtc } from 'date-fns-tz';
import i18n from 'i18next';
import React, { ReactElement } from 'react';
import { Observable } from 'rxjs';

import {
  AufgabeControllerApi,
  AufgabeEntityDTO,
  AufgabeEntityListResponseDTO,
  FarbeType,
  HomepageTableDTO,
  NeuigkeitControllerApi,
  NeuigkeitEntityDTO,
  NeuigkeitEntityListResponseDTO,
  RegelungsvorhabenControllerApi,
  RegelungsvorhabenEntityListResponseShortDTO,
  UserControllerApi,
} from '@plateg/rest-api';
import { DropdownMenu, GlobalDI, TableComponentProps } from '@plateg/theme';
import { Filters } from '@plateg/theme/src/shares/filters';

export class DashboardController {
  private regelungsvorhabenController = GlobalDI.getOrRegister<RegelungsvorhabenControllerApi>(
    'regelungsvorhabenController',
    () => new RegelungsvorhabenControllerApi(),
  );
  private neuigkeitController = GlobalDI.getOrRegister<NeuigkeitControllerApi>(
    'neuigkeitController',
    () => new NeuigkeitControllerApi(),
  );
  private aufgabeController = GlobalDI.getOrRegister<AufgabeControllerApi>(
    'aufgabeController',
    () => new AufgabeControllerApi(),
  );
  private userController = GlobalDI.get<UserControllerApi>('userController');
  public dateTimeFormat(dateString: string): string {
    const datePart =
      new Date().toDateString() === new Date(dateString).toDateString()
        ? i18n.t('cockpit.dashboard.today')
        : Filters.date(new Date(dateString));

    return `${datePart}  ·  ${Filters.time(new Date(dateString))}`;
  }
  public getZuletztBearbeitetTableVals(content: HomepageTableDTO[]): TableComponentProps<HomepageTableDTO> {
    const columns: ColumnsType<HomepageTableDTO> = [
      {
        title: i18n.t('cockpit.dashboard.tabs.zuletztBearbeitet.table.tableHeads.dokument').toString(),
        key: 'bezeichnung',
        fixed: 'left',
        render: (record: HomepageTableDTO): ReactElement => {
          return <a href={record.link}>{record.titel}</a>;
        },
      },
      {
        title: i18n.t('cockpit.dashboard.tabs.zuletztBearbeitet.table.tableHeads.art').toString(),
        key: 'art',
        render: (record: HomepageTableDTO): ReactElement => {
          return <Text>{record.art}</Text>;
        },
      },

      {
        title: i18n.t('cockpit.dashboard.tabs.zuletztBearbeitet.table.tableHeads.zuletztVerwendet').toString(),
        key: 'zuletztBearbeitet',
        render: (record: HomepageTableDTO): ReactElement => {
          return <Text>{this.dateTimeFormat(record.bearbeitetAm)}</Text>;
        },
      },
    ];
    return {
      id: 'zuletzt-bearbeitet-table',
      expandable: false,
      columns,
      content,
    };
  }
  public getNeuigkeitenTableVals(
    content: NeuigkeitEntityDTO[],
    onNeuigkeitClick: (link: string, neuigkeit: NeuigkeitEntityDTO) => void,
    setNeuigkeitLesen: (id: string, gelesen: boolean) => void,
    deleteNeuigkeit: (id: string) => void,
  ): TableComponentProps<NeuigkeitEntityDTO> {
    const columns: ColumnsType<NeuigkeitEntityDTO> = [
      {
        title: i18n.t('cockpit.dashboard.tabs.neuigkeiten.table.tableHeads.neuigkeiten').toString(),
        key: 'neuigkeiten',
        render: (record: NeuigkeitEntityDTO): ReactElement => {
          const link = `/hra/anfragen/`;
          const isFristverlaengerungAnfrage = record.nachricht.includes('Fristverlängerung');
          return (
            <div className="neuigkeiten-nachricht">
              <div
                dangerouslySetInnerHTML={{
                  __html: record.nachricht.slice(0, record.nachricht.indexOf('<a')),
                }}
              />
              <a
                onClick={(e: React.MouseEvent) => {
                  e.preventDefault();
                  isFristverlaengerungAnfrage
                    ? onNeuigkeitClick(link, record)
                    : onNeuigkeitClick(
                        record.nachricht.slice(record.nachricht.indexOf('href="') + 6, record.nachricht.indexOf('">')),
                        record,
                      );
                }}
                href={record.nachricht.slice(record.nachricht.indexOf('href="') + 6, record.nachricht.indexOf('">'))}
              >
                {record.nachricht.slice(record.nachricht.indexOf('">') + 2, record.nachricht.indexOf('</a>'))}
              </a>
              <span
                dangerouslySetInnerHTML={{
                  __html: record.nachricht.slice(record.nachricht.indexOf('</a>'), record.nachricht.length),
                }}
              />
            </div>
          );
        },
      },
      {
        title: i18n.t('cockpit.dashboard.tabs.neuigkeiten.table.tableHeads.eingang').toString(),
        key: 'eingang',
        render: (record: NeuigkeitEntityDTO): ReactElement => {
          return <Text>{this.dateTimeFormat(record.erstelltAm)}</Text>;
        },
      },

      {
        title: i18n.t('cockpit.dashboard.tabs.neuigkeiten.table.tableHeads.aktionen').toString(),
        key: 'aktionen',
        render: (record: NeuigkeitEntityDTO): ReactElement => {
          return (
            <DropdownMenu
              getPopupContainer={() => document.querySelector('.ant-layout-has-sider')}
              items={[
                {
                  element: record.gelesen
                    ? i18n.t('cockpit.dashboard.tabs.neuigkeiten.table.alsUngelesenMarkieren')
                    : i18n.t('cockpit.dashboard.tabs.neuigkeiten.table.alsGelesenMarkieren'),
                  onClick: () => {
                    setNeuigkeitLesen(record.id, !record.gelesen);
                  },
                },
                {
                  element: i18n.t('cockpit.dashboard.tabs.neuigkeiten.table.eintragLoeschen'),
                  onClick: () => {
                    deleteNeuigkeit(record.id);
                  },
                },
              ]}
              elementId={`menu-${record.id}`}
            />
          );
        },
      },
    ];
    return {
      id: 'neuigkeiten-table',
      expandable: false,
      columns,
      content,
    };
  }
  public geAufgabenTableVals(
    content: AufgabeEntityDTO[],
    setAufgabechecked: (id: string, checked: boolean) => void,
    deleteAufgabe: (id: string) => void,
    setSelectedAufgabe: (val: AufgabeEntityDTO) => void,
  ): TableComponentProps<AufgabeEntityDTO> {
    const columns: ColumnsType<AufgabeEntityDTO> = [
      {
        title: i18n.t('cockpit.dashboard.tabs.aufgaben.table.tableHeads.status').toString(),
        width: '5%',
        key: 'status',
        render: (record: AufgabeEntityDTO): ReactElement => {
          return (
            <Tooltip
              title={
                record.erledigtAm
                  ? i18n.t('cockpit.dashboard.tabs.aufgaben.table.checkedHover').toString()
                  : i18n.t('cockpit.dashboard.tabs.aufgaben.table.unCheckedHover').toString()
              }
            >
              <Checkbox
                className="erledigt-check"
                checked={!!record.erledigtAm}
                onChange={(e: CheckboxChangeEvent) => {
                  e.preventDefault();
                  setAufgabechecked(record.id, e.target.checked);
                }}
              />
            </Tooltip>
          );
        },
      },
      {
        title: i18n.t('cockpit.dashboard.tabs.aufgaben.table.tableHeads.aufgabe').toString(),
        key: 'aufgabe',
        width: '55%',
        render: (record: AufgabeEntityDTO): ReactElement => {
          return <Title level={3}>{record.aufgabeText}</Title>;
        },
      },
      {
        title: i18n.t('cockpit.dashboard.tabs.aufgaben.table.tableHeads.frist').toString(),
        key: 'frist',
        width: '30%',
        render: (record: AufgabeEntityDTO): ReactElement => {
          return (
            <Text className="aufgabe-frist">
              {record.fristAm ? (
                this.dateTimeFormat(record.fristAm)
              ) : (
                <span>{i18n.t('cockpit.dashboard.tabs.aufgaben.table.noFrist').toString()}</span>
              )}
            </Text>
          );
        },
      },

      {
        title: i18n.t('cockpit.dashboard.tabs.aufgaben.table.tableHeads.aktionen').toString(),
        key: 'aktionen',
        width: '10%',
        render: (record: AufgabeEntityDTO): ReactElement => {
          return (
            <DropdownMenu
              getPopupContainer={() => document.querySelector('.ant-layout-has-sider')}
              items={[
                {
                  element: record.erledigtAm
                    ? i18n.t('cockpit.dashboard.tabs.aufgaben.table.alsUnerledigtMarkieren')
                    : i18n.t('cockpit.dashboard.tabs.aufgaben.table.alsErledigtMarkieren'),
                  onClick: () => {
                    setAufgabechecked(record.id, !record.erledigtAm);
                  },
                },
                {
                  element: i18n.t('cockpit.dashboard.tabs.aufgaben.table.aufgabeLoeschen'),
                  onClick: () => {
                    deleteAufgabe(record.id);
                  },
                },
                {
                  element: i18n.t('cockpit.dashboard.tabs.aufgaben.table.bearbeiten'),
                  disabled: () => !(record.erledigtAm === '' || record.erledigtAm === null),
                  onClick: () => {
                    setSelectedAufgabe(record);
                  },
                },
              ]}
              elementId={`menu-${record.id}`}
            />
          );
        },
      },
    ];
    return {
      id: 'aufgaben-table',
      className: 'table-component',
      expandable: false,
      columns,
      content,
    };
  }
  public getZuletztBearbeitetCall(): Observable<HomepageTableDTO[]> {
    return this.userController.getHomepageTable();
  }
  public getNeuigkeitenCall(pageNumber: number): Observable<NeuigkeitEntityListResponseDTO> {
    return this.neuigkeitController.getNeuigkeitenList({ pageNumber });
  }
  public getActiveRegelungsvorhaben(): Observable<RegelungsvorhabenEntityListResponseShortDTO> {
    return this.regelungsvorhabenController.getRegelungsvorhabenShortListForStartseite();
  }
  public setNeuigkeitGelesen(id: string, gelesen: boolean): Observable<void> {
    return this.neuigkeitController.setElementMarkAsRead({ id, setUngelesen: gelesen });
  }
  public setNeuigkeitenGelesen(): Observable<void> {
    return this.neuigkeitController.setElementeMarkAsRead();
  }
  public deleteNeuigkeiten(): Observable<void> {
    return this.neuigkeitController.deleteAllElements1();
  }
  public deleteNeuigkeit(id: string): Observable<void> {
    return this.neuigkeitController.deleteAllElements({ id });
  }
  public getAufgabenCall(shortList: boolean): Observable<AufgabeEntityListResponseDTO> {
    return this.aufgabeController.getAufgabenList({ shortList });
  }
  public setAufgabeChecked(id: string, checked: boolean): Observable<void> {
    return this.aufgabeController.setElementAsResolved({ id, setErledigt: !checked });
  }
  public deleteAufgabe(id: string): Observable<void> {
    return this.aufgabeController.deleteElement({ id });
  }
  public deleteAufgaben(): Observable<void> {
    return this.aufgabeController.deleteAllResolvedElements();
  }
  public saveAufgabe(aufgabe: AufgabeEntityDTO, modify: boolean): Observable<void> {
    if (modify) {
      return this.aufgabeController.modifyAufgabe({ aufgabeEntityDTO: aufgabe });
    }
    return this.aufgabeController.createAufgabe({ aufgabeEntityDTO: aufgabe });
  }
  public generateFristenString = (stringDate?: string, stringTime?: string): string | undefined => {
    if (stringDate && stringTime) {
      let utcDate;
      try {
        const date = parse(stringDate, 'dd.MM.yyyy', new Date());
        const parsedDate = stringTime.split(':');
        const time = set(new Date(), { hours: Number(parsedDate[0]), minutes: Number(parsedDate[1]), seconds: 0 });
        const dateFormatted = lightFormat(date || new Date(), 'yyyy-MM-dd');
        const timeFormatted = lightFormat(time || new Date(1900, 0, 1, 18), 'HH:mm:ss');
        const localDateTime = `${dateFormatted} ${timeFormatted}`;
        utcDate = zonedTimeToUtc(localDateTime, 'Europe/Berlin');
      } catch (e) {
        return undefined;
      }
      return utcDate.toISOString();
    }
    return undefined;
  };

  public setActiveRegelungsvorhabenFarbe(
    rvId: string,
    farbe: FarbeType,
  ): Observable<RegelungsvorhabenEntityListResponseShortDTO> {
    return this.regelungsvorhabenController.modifyRvFarbe({ id: rvId, farbeType: farbe });
  }
  public dateSort(date1: string, date2: string) {
    return new Date(date2).getTime() - new Date(date1).getTime();
  }
}
