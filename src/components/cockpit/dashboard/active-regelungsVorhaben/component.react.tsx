// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Typography } from 'antd';
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { AjaxError } from 'rxjs/ajax';

import { FarbeType, RegelungsvorhabenEntityResponseShortDTO } from '@plateg/rest-api';
import { CheckOutlined, DropdownMenu, ErrorController, GlobalDI } from '@plateg/theme';
import { BriefcaseDashboard } from '@plateg/theme/src/components/icons/BriefcaseDashboard';

import { DashboardController } from '../controller.react';

interface ActiveRegelungsVorhabenProps {
  activeRegelungsvorhaben: RegelungsvorhabenEntityResponseShortDTO[];
  setActiveRegelungsvorhaben: (rvs: RegelungsvorhabenEntityResponseShortDTO[]) => void;
}

export function ActiveRegelungsVorhaben(props: ActiveRegelungsVorhabenProps): React.ReactElement {
  const { Title } = Typography;
  const { t } = useTranslation();
  const history = useHistory();
  const [showAllRegelungsvorhaben, setShowAllRegelungsvorhaben] = useState<boolean>(false);
  const dashboardController = GlobalDI.getOrRegister('dashboardController', () => new DashboardController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const onChangeColorClick = (rv: RegelungsvorhabenEntityResponseShortDTO, color: FarbeType) => {
    if (rv.dto.farbe != color) {
      dashboardController.setActiveRegelungsvorhabenFarbe(rv.base.id, color).subscribe({
        next: () => {
          rv.dto.farbe = color;
          props.setActiveRegelungsvorhaben([...props.activeRegelungsvorhaben]);
        },
        error: (error: AjaxError) => {
          errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
        },
      });
    }
  };

  /* Smoothly scroll to top when rv list is collapsed */
  useEffect(() => {
    if (!showAllRegelungsvorhaben) {
      window.scrollTo(0, 0);
    }
  }, [showAllRegelungsvorhaben]);

  return (
    <div className="active-dashboard-rv">
      <Title level={2} className="active-rv-title">
        {t('cockpit.dashboard.activeRegelungsvorhaben')}
      </Title>
      <div className="rvs-container">
        {props.activeRegelungsvorhaben
          .slice(0, showAllRegelungsvorhaben ? props.activeRegelungsvorhaben.length : 3)
          .map((rv) => {
            return (
              <div key={rv.base.id} className={`rv-card rv-card-${rv.dto.farbe.toLowerCase()}`}>
                <div className="rv-card-container">
                  <div className="rv-card-head">
                    <div className="rv-card-title">
                      <div>
                        <BriefcaseDashboard />
                      </div>

                      <Link to={`/regelungsvorhaben/meineRegelungsvorhaben/${rv.base.id}/naechsteschritte`}>
                        <span>{rv.dto.abkuerzung}</span>
                      </Link>
                    </div>
                    <div className="rv-card-context-menu">
                      <DropdownMenu
                        items={[
                          {
                            element: t('cockpit.dashboard.regelungsvorhabenoeffnen'),
                            onClick: () => {
                              history.push(`/regelungsvorhaben/meineRegelungsvorhaben/${rv.base.id}/naechsteschritte`);
                            },
                          },
                          {
                            element: t('cockpit.dashboard.farbeZuweisen'),
                            children: Object.entries(t('cockpit.dashboard.farben', { returnObjects: true })).map(
                              (item) => {
                                return {
                                  element: (
                                    <div className="rv-card-context-sub-menu-item">
                                      <div className={`square square-${item[0]}`}></div>
                                      <span>{item[1]}</span>
                                      {rv.dto.farbe.toLowerCase() === item[0] && <CheckOutlined />}
                                    </div>
                                  ),
                                  onClick: () => {
                                    onChangeColorClick(rv, item[0].toUpperCase() as FarbeType);
                                  },
                                };
                              },
                            ),
                          },
                        ]}
                        elementId={`active-rv-$${rv.base.id}`}
                      />
                    </div>
                  </div>
                  <div className="rv-card-content">{rv.dto.kurzbezeichnung}</div>
                </div>
              </div>
            );
          })}
      </div>
      {props.activeRegelungsvorhaben.length > 3 && (
        <Button type="link" onClick={() => setShowAllRegelungsvorhaben(!showAllRegelungsvorhaben)}>
          {t(`cockpit.dashboard.${showAllRegelungsvorhaben ? 'ersteZeilereduzieren' : 'alleEinblenden'}`)}
        </Button>
      )}
    </div>
  );
}
