// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './vision.less';

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

const imageItems = ['edit', 'support', 'processualSupport', 'timeManagment'];

export function VisionComponent(): React.ReactElement {
  const { Title } = Typography;

  const { t } = useTranslation();

  return (
    <section className="section-vision">
      <div className="vision-component">
        <div className="holder info-wrapper">
          <div>
            <strong className="sub-title">{t('cockpit.welcome.vision.subTitle')}</strong>
            <Title level={2}> {t('cockpit.welcome.vision.title')}</Title>
            <div className="process-list">
              {imageItems.map((item) => (
                <div className={`process-item`} key={item}>
                  <Image
                    width={36}
                    loading="lazy"
                    preview={false}
                    src={(require(`../../../../media/welcome-page/${item}.svg`) as ImageModule).default}
                    alt={t(`cockpit.welcome.vision.${item}.title`)}
                  />
                  <div className="text">
                    <Title style={{ marginTop: 20 }} level={3}>
                      {t(`cockpit.welcome.vision.${item}.title`)}
                    </Title>
                    <p
                      className="ant-typography p-no-style"
                      dangerouslySetInnerHTML={{
                        __html: t(`cockpit.welcome.vision.${item}.text`),
                      }}
                    />
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
