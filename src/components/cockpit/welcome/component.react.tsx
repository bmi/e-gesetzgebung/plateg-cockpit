// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './welcome.less';

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { UserEntityResponseDTO } from '@plateg/rest-api';
import { HinweisComponent, ImageModule } from '@plateg/theme';
import { useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { Dashboard } from '../dashboard/component.react';
import { LoginComponent } from '../login-component/component.react';
import { HeadComponent } from './head/component.react';
import { ReleaseInfoComponent } from './releaseBottom/component.react';
import { VisionComponent } from './vision/component.react';

interface WelcomeState {
  signedIn: boolean | undefined;
  user?: UserEntityResponseDTO;
}
export function Welcome(): React.ReactElement {
  const { t } = useTranslation();
  const [state, setState] = useState<WelcomeState>({
    signedIn: undefined,
  });
  const appStore = useAppSelector((state) => state.user);
  const { keycloak } = useKeycloak();
  useEffect(() => {
    if (appStore.user?.base.id != null) {
      setState({ signedIn: true, user: appStore.user });
    } else {
      setState({ signedIn: false });
    }
  }, [appStore.user]);

  if (state.signedIn === undefined) {
    return <></>;
  }

  if (state.signedIn && state.user) {
    return (
      <LoginComponent>
        <Dashboard currentUser={state.user} />
      </LoginComponent>
    );
  }

  return (
    <div className="welcome-page">
      <HeadComponent
        title={{
          title: t('cockpit.welcome.head.title'),
        }}
        button={{
          text: t('cockpit.welcome.head.login'),
          onClickHandler: () => void keycloak.login(),
          isHidden: false,
        }}
        img={{
          img: require(`../../../media/welcome-page/Grafik-April-Release-2024_Intro.svg`) as ImageModule,
          alt: t('cockpit.welcome.head.imgAlt'),
        }}
      />
      <section className="section-welcome-hinweis">
        <div className="holder">
          <HinweisComponent
            mode="warning"
            styleDiv={{ paddingLeft: 0, paddingRight: 0 }}
            title={t('cockpit.welcome.hinweis.title')}
            content={<p style={{ marginTop: 6 }}>{t('cockpit.welcome.hinweis.content')}</p>}
          />
        </div>
      </section>
      <VisionComponent />
      <ReleaseInfoComponent
        button={{
          text: t('cockpit.footer.releaseNotes.welcome.link'),
          link: '/releaseNotes',
          isHidden: false,
        }}
        title={{
          title: t('cockpit.footer.releaseNotes.welcome.title'),
          subtitle: t('cockpit.footer.releaseNotes.welcome.subtitle'),
          text: t('cockpit.footer.releaseNotes.welcome.paragraph'),
        }}
        img={{
          img: require(`../../../media/welcome-page/ReleaseNotes.svg`) as ImageModule,
          alt: t('cockpit.footer.releaseNotes.imgAlt'),
        }}
      />
    </div>
  );
}
