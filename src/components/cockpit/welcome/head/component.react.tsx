// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './head.less';

import { Button, Image, Typography } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

import { RightArrow } from '@plateg/ggp/src/media/ggp-detail-view/RightArrow';
import { ImageModule } from '@plateg/theme';
import { RightDirectionArrow } from '@plateg/theme/src/components/icons';

interface HeadComponentProps {
  title: {
    title: string | React.ReactElement;
    subtitle?: string;
    text?: string | React.ReactElement;
  };
  button?: {
    text: string;
    link?: string;
    onClickHandler?: () => void;
    isHidden?: boolean;
  };
  img: {
    img: ImageModule;
    alt: string;
  };
  linkBack?: {
    text: string;
    link: string;
  };
}

export function HeadComponent(props: HeadComponentProps): React.ReactElement {
  const { Title } = Typography;
  return (
    <section
      style={{
        paddingBottom: 80,
        paddingTop: 62,
        background: '#fff',
      }}
    >
      {props.linkBack && (
        <div className="link-back-head-holder">
          <Link id="head-link-back" className="link-back" to={props.linkBack.link}>
            <RightDirectionArrow />
            {props.linkBack.text}
          </Link>
        </div>
      )}
      <div className="holder head-component-wrapper">
        <div className="head-component">
          {props.title.subtitle && <strong className="sub-title">{props.title.subtitle}</strong>}
          <Title level={1}>{props.title.title}</Title>

          {props.title.text && <p className="info-text">{props.title.text}</p>}
          {props.button && !props.button.isHidden && (
            <Button
              type="primary"
              style={{ textDecoration: 'none' }}
              href={props.button.link}
              onClick={props.button.onClickHandler}
            >
              {props.button.text}
              <RightArrow />
            </Button>
          )}
        </div>
        <Image preview={false} src={props.img.img.default} alt={props.img.alt} loading="lazy" />
      </div>
    </section>
  );
}
