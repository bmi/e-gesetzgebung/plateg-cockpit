// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './promo.less';

import { Typography } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EmptyContentComponent, ImageModule } from '@plateg/theme';

export function PromoComponent(): React.ReactElement {
  const { Paragraph } = Typography;

  const { t } = useTranslation();
  return (
    <section>
      <div className="holder">
        <div className="promo-component">
          <strong className="sub-title">{t('cockpit.welcome.promo.mainTitle')}</strong>
          <Title level={2}>
            {t('cockpit.welcome.promo.secondTitle')}
            {<br />}
            {t('cockpit.welcome.promo.thirdTitle')}
          </Title>
          <EmptyContentComponent
            images={[
              {
                label: t('cockpit.welcome.promo.text1'),
                imgSrc: require(`../../../../media/welcome-page/Grafik-Onboarding_Schritt1.svg`) as ImageModule,
                imgAlt: t('cockpit.welcome.promo.imgAlt1'),
              },
              {
                label: t('cockpit.welcome.promo.text2'),
                imgSrc: require(`../../../../media/welcome-page/Grafik-Onboarding_Schritt2.svg`) as ImageModule,
                imgAlt: t('cockpit.welcome.promo.imgAlt2'),
              },
              {
                label: t('cockpit.welcome.promo.text3'),
                imgSrc: require(`../../../../media/welcome-page/Grafik-Onboarding_Schritt3.svg`) as ImageModule,
                imgAlt: t('cockpit.welcome.promo.imgAlt3'),
              },
            ]}
            noIndex
          >
            <Paragraph className="info-text">
              <p
                dangerouslySetInnerHTML={{
                  __html: t('cockpit.welcome.promo.summary', {
                    interpolation: { escapeValue: false },
                  }),
                }}
              ></p>
            </Paragraph>
          </EmptyContentComponent>
        </div>
      </div>
    </section>
  );
}
