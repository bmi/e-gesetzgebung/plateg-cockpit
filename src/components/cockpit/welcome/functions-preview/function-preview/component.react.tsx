// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './function-preview.less';

import { Button, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { GlobalDI } from '@plateg/theme';
import { RightDirectionArrow } from '@plateg/theme/src/components/icons';

import { LoginController } from '../../../login-component/controller';

const { Title } = Typography;
export interface FunctionPreviewComponentProps {
  text: { heading?: string | React.ReactElement; content: string | React.ReactElement };
  image: React.ReactElement;
  textIsOnTheRight: boolean;
  customLink?: { name?: string; link?: string; asLink?: boolean; isExternal?: boolean; isHidden?: boolean };
  releaseLink?: string;
}

export function FunctionPreviewComponent(props: FunctionPreviewComponentProps): React.ReactElement {
  const { t } = useTranslation();
  const ctrl = GlobalDI.getOrRegister('cockpitLoginController', () => new LoginController());
  const targetAttrValue = props.customLink?.isExternal ? '_blank' : '_self';
  const externalLinkClassName = props.customLink?.isExternal ? 'external-link' : '';
  const link = props.customLink?.asLink ? (
    <a
      target={targetAttrValue}
      className={`custom-link ${externalLinkClassName}`}
      href={props.customLink?.link ?? ctrl.getLoginUrl()}
    >
      {props.customLink?.name ?? t('cockpit.welcome.functionsPreview.login')}
    </a>
  ) : (
    <Button type="primary" size="large" href={props.customLink?.link ?? ctrl.getLoginUrl()}>
      {props.customLink?.name ?? t('cockpit.welcome.functionsPreview.login')}
    </Button>
  );

  return (
    <div
      className={`function-preview-component info-wrapper ${props.textIsOnTheRight ? 'inverted-content-order' : ''}`}
    >
      <div className="function-preview-text-section">
        <div className="function-preview-text">
          <Title level={2}> {props.text.heading}</Title>
          {props.text.content}
          {!props.customLink?.isHidden && link}
          {props.releaseLink && (
            <Link className="custom-link" to={`/releaseNotes/${props.releaseLink}`}>
              <span style={{ marginRight: 8 }}>{t('cockpit.welcome.functionsPreview.releaseNotes')}</span>
              <RightDirectionArrow />
            </Link>
          )}
        </div>
      </div>
      <div className="function-preview-image">{props.image}</div>
    </div>
  );
}
