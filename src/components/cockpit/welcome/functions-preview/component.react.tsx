// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

import { FunctionPreviewComponent } from './function-preview/component.react';

const { Title } = Typography;
interface FuncPreviewType {
  topic: string;
  imgFilename: string;
  textIsOnTheRight: boolean;
  releaseLink?: string;
}

const previewData: FuncPreviewType[] = [
  {
    topic: 'bReg',
    imgFilename: `Grafik-April-Release-2024_BReg.svg`,
    textIsOnTheRight: false,
    releaseLink: 'bundesregierungsplattform',
  },
  {
    topic: 'editor',
    imgFilename: `Grafik-April-Release-2024_Editor.svg`,
    textIsOnTheRight: false,
    releaseLink: 'editor',
  },

  {
    topic: 'bundestag',
    imgFilename: 'Grafik-April-Release-2024_Bundestagsplattform.svg',
    textIsOnTheRight: false,
    releaseLink: 'bundestagsplattform',
  },
  {
    topic: 'bitv',
    imgFilename: 'Grafik-April-Release-2024_Barrierefreiheit.svg',
    textIsOnTheRight: false,
  },
];

export function FunctionsPreviewComponent(): React.ReactElement {
  const { t } = useTranslation();

  const displayFuncPreviewComp = previewData.map((item) => (
    <FunctionPreviewComponent
      key={item.imgFilename}
      textIsOnTheRight={item.textIsOnTheRight}
      text={{
        heading: t(`cockpit.welcome.functionsPreview.${item.topic}.heading`),
        content: (
          <p
            dangerouslySetInnerHTML={{
              __html: t(`cockpit.welcome.functionsPreview.${item.topic}.content`, {
                interpolation: { escapeValue: false },
              }),
            }}
          />
        ),
      }}
      image={
        <Image
          loading="lazy"
          preview={false}
          src={(require(`../../../../media/welcome-page/${item.imgFilename}`) as ImageModule).default}
          alt={t(`cockpit.welcome.functionsPreview.${item.topic}.imgAlt`)}
        />
      }
      customLink={{ isHidden: true }}
      releaseLink={item.releaseLink}
    />
  ));

  return (
    <section>
      <div className="holder">
        <div className="functions-preview-component">
          <strong className="sub-title">{t('cockpit.welcome.functionsPreview.subTitle')}</strong>
          <Title level={2}> {t('cockpit.welcome.functionsPreview.title')}</Title>
          {displayFuncPreviewComp}
        </div>
      </div>
    </section>
  );
}
