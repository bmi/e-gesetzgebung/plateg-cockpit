// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './release.less';

import { Image, Typography } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';

import { ImageModule } from '@plateg/theme';
import { RightDirectionArrow } from '@plateg/theme/src/components/icons';

interface HeadComponentProps {
  title: {
    title: string | React.ReactElement;
    subtitle?: string;
    text?: string | React.ReactElement;
  };
  button?: {
    text: string;
    link: string;
    isHidden?: boolean;
  };
  img: {
    img: ImageModule;
    alt: string;
  };
  welcomePage?: boolean;
  notHeader?: boolean;
}

export function ReleaseInfoComponent(props: HeadComponentProps): React.ReactElement {
  const { Title } = Typography;
  return (
    <section
      style={{
        paddingBottom: 80,
        paddingTop: 62,
        background: '#fff',
      }}
    >
      <div className="holder release-head-component-wrapper">
        <div className="head-component">
          {props.title.subtitle && <strong className="sub-title">{props.title.subtitle}</strong>}
          <Title style={{ marginBottom: 16 }} level={2}>
            {props.title.title}
          </Title>

          {props.title.text && <p className="info-text">{props.title.text}</p>}
          {props.button && !props.button.isHidden && (
            <Link className="custom-link" to={props.button.link}>
              <span style={{ marginRight: 8 }}>{props.button.text}</span>
              <RightDirectionArrow />
            </Link>
          )}
        </div>
        <Image preview={false} src={props.img.img.default} alt={props.img.alt} loading="lazy" />
      </div>
    </section>
  );
}
/* info: this component was copied from HeadComponent and added to welcome page bottom  */
