// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next/';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../../general/static-page-wrapper/component.react';

export function VsNfd(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.vsNfd.title')}</Title>
          <p dangerouslySetInnerHTML={{ __html: t('cockpit.footer.vsNfd.text0') }} />
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
