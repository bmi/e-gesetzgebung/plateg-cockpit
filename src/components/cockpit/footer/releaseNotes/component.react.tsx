// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './releaseNotes.less';

import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';

import { ImageModule } from '@plateg/theme';
import { RightDirectionArrow } from '@plateg/theme/src/components/icons';

import { StaticPageWrapper } from '../../../general/static-page-wrapper/component.react';
import { HeadComponent } from '../../welcome/head/component.react';
import { NotesSectionProps, ReleaseNotesSection } from './releaseNotes-section/component.react';

export function ReleaseNotes(): React.ReactElement {
  const { t } = useTranslation();

  const { sectionRelease } = useParams<{ sectionRelease: string }>();
  const releaseNotesList: NotesSectionProps[] = t('cockpit.footer.releaseNotes.items', { returnObjects: true }) || [];
  const sectionsNameList = releaseNotesList.map((section) => section.key);
  const bundesregierungsplattformRef = useRef<HTMLDivElement>(null);
  const editorRef = useRef<HTMLDivElement>(null);
  const bundestagsplattformRef = useRef<HTMLDivElement>(null);
  const bundesratsplattformRef = useRef<HTMLDivElement>(null);
  const gesetzgebungsportalRef = useRef<HTMLDivElement>(null);
  const refMap: { [key: string]: React.RefObject<HTMLDivElement> } = {
    bundesregierungsplattform: bundesregierungsplattformRef,
    editor: editorRef,
    bundestagsplattform: bundestagsplattformRef,
    bundesratsplattform: bundesratsplattformRef,
    gesetzgebungsportal: gesetzgebungsportalRef,
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (sectionsNameList.includes(sectionRelease)) {
        scrollToSection(refMap[sectionRelease]);
      }
    }, 300);
    return () => {
      clearTimeout(timeout);
    };
  }, [sectionRelease]);

  const scrollToSection = (sectionRef: React.RefObject<HTMLDivElement>) => {
    if (sectionRef.current) {
      const rect = sectionRef.current.getBoundingClientRect();
      const offset = 140;
      window.scrollTo({
        top: rect.top - offset,
        behavior: 'smooth',
      });
    }
  };

  return (
    <StaticPageWrapper>
      <div className="releaseNotes">
        <div className="releaseNotesHeader">
          <HeadComponent
            title={{
              title: (
                <span
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.footer.releaseNotes.title', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
              subtitle: t('cockpit.footer.releaseNotes.subtitle'),
              text: (
                <span
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.footer.releaseNotes.text', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            }}
            img={{
              img: require(`../../../../media/welcome-page/ReleaseNotes.svg`) as ImageModule,
              alt: t('cockpit.footer.releaseNotes.imgAlt'),
            }}
          />
        </div>
        <div className="releaseNotes-body">
          {releaseNotesList.map((section) => (
            <ReleaseNotesSection
              key={section.key}
              refDiv={refMap[section.key]}
              subtitle={section.subtitle}
              title={section.title}
              data={section.data}
            />
          ))}
          <div className="link-holder">
            <Link id="release-archive-link" className="link-to-archive" to="/releaseNotes/previousReleases">
              {t('cockpit.footer.releaseNotes.linkToArchive')}
              <RightDirectionArrow />
            </Link>
          </div>
        </div>
      </div>
    </StaticPageWrapper>
  );
}
