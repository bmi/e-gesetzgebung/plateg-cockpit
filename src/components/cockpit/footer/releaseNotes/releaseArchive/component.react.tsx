// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { TabsProps } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule, TabsWrapper } from '@plateg/theme';

import { StaticPageWrapper } from '../../../../general/static-page-wrapper/component.react';
import { HeadComponent } from '../../../welcome/head/component.react';
import { NotesSectionProps, ReleaseNotesSection } from '../releaseNotes-section/component.react';

interface PastReleaseItem {
  releaseKey: string;
  releaseTitle: string;
  items: NotesSectionProps[];
}
export function ReleaseNotesArchive(): React.ReactElement {
  const { t } = useTranslation();
  const releases: PastReleaseItem[] = t('cockpit.footer.previousReleases.releases', { returnObjects: true }) || [];
  const tabItems: TabsProps['items'] = releases.map((release) => {
    return {
      key: release.releaseKey,
      label: release.releaseTitle,
      children: release.items.map((item) => {
        return <ReleaseNotesSection key={item.key} subtitle={item.subtitle} title={item.title} data={item.data} />;
      }),
    };
  });
  const [activeTab, setActiveTab] = useState(releases[0].releaseKey || '');
  return (
    <StaticPageWrapper>
      <div className="releaseNotes">
        <div className="releaseNotesHeader">
          <HeadComponent
            title={{
              title: (
                <span
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.footer.previousReleases.title', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
              text: (
                <span
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.footer.previousReleases.text', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            }}
            img={{
              img: require(`../../../../../media/welcome-page/ReleaseNotes.svg`) as ImageModule,
              alt: t('cockpit.footer.previousReleases.imgAlt'),
            }}
            linkBack={{ text: t('cockpit.footer.previousReleases.linkBack'), link: '/releaseNotes' }}
          />
        </div>
        <div className="releaseNotes-tabs releaseNotes-body">
          <TabsWrapper
            items={tabItems}
            activeKey={activeTab}
            onChange={(key: string) => {
              setActiveTab(key);
            }}
            className="standard-tabs"
            moduleName={'previousReleases'}
          />
        </div>
      </div>
    </StaticPageWrapper>
  );
}
