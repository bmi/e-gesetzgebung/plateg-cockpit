// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse, CollapseProps } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect } from 'react';

import { AriaController } from '@plateg/theme';
import { DirectionRightOutlinedNew } from '@plateg/theme/src/components/icons';
import { Filters } from '@plateg/theme/src/shares/filters';

export interface NotesSectionProps {
  key: string;
  title: string;
  subtitle: string;
  data: NotesItem[];
  refDiv?: React.RefObject<HTMLDivElement>;
}
interface NotesItem {
  title: string;
  content: React.ReactElement;
}
export function ReleaseNotesSection(props: NotesSectionProps): React.ReactElement {
  useEffect(() => {
    AriaController.setAriaLabelsByClassName('ant-collapse-arrow', '');
  }, []);

  const sanitizedTitle = Filters.sanitizeStringForCSS(props.title);

  const items: CollapseProps['items'] = props.data.map((item: NotesItem) => {
    return {
      children: <div dangerouslySetInnerHTML={{ __html: item.content }} />,
      label: <p dangerouslySetInnerHTML={{ __html: item.title }} />,
      key: crypto.randomUUID(),
    };
  });
  return (
    <div ref={props.refDiv} id={`release-notes-section-${sanitizedTitle}`}>
      <strong className="sub-title">{props.subtitle}</strong>
      <Title level={2}>{props.title}</Title>
      <Collapse
        items={items}
        bordered={false}
        expandIcon={({ isActive }) => (
          <DirectionRightOutlinedNew
            style={{ width: 32, height: 32, marginTop: 10 }}
            className={isActive ? 'svg-transition svg-rotate-up' : 'svg-transition svg-rotate-down'}
          />
        )}
        expandIconPosition={'end'}
      />
    </div>
  );
}
