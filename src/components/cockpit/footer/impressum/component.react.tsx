// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../../general/static-page-wrapper/component.react';
export function Impressum(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.impressum.title')}</Title>
          <p>{t('cockpit.footer.impressum.einleitung')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.impressum.impressum', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <Title level={2}>{t('cockpit.footer.impressum.subtitle')}</Title>
          <p>{t('cockpit.footer.impressum.hinweise1')}</p>
          <p>{t('cockpit.footer.impressum.hinweise2')}</p>
          <Title level={3}>{t('cockpit.footer.impressum.hinweise3')}</Title>
          <p>{t('cockpit.footer.impressum.hinweise4')}</p>
          <p>{t('cockpit.footer.impressum.hinweise5')}</p>
          <Title level={3}>{t('cockpit.footer.impressum.hinweise6')}</Title>
          <p>{t('cockpit.footer.impressum.hinweise7')}</p>
          <p>{t('cockpit.footer.impressum.hinweise8')}</p>
          <Title level={3}>{t('cockpit.footer.impressum.hinweise9')}</Title>
          <p>{t('cockpit.footer.impressum.hinweise10')}</p>
          <Title level={3}>{t('cockpit.footer.impressum.hinweise11')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.impressum.hinweise12', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <Title level={3}>{t('cockpit.footer.impressum.hinweise13')}</Title>
          <p>{t('cockpit.footer.impressum.hinweise14')}</p>
          <p>{t('cockpit.footer.impressum.hinweise15')}</p>
          <p>{t('cockpit.footer.impressum.hinweise16')}</p>
          <Title level={3}>{t('cockpit.footer.impressum.hinweise17')}</Title>
          <ol type="1">
            <li>{t('cockpit.footer.impressum.hinweise18')}</li>
            <li>{t('cockpit.footer.impressum.hinweise19')}</li>
            <li>
              <p>{t('cockpit.footer.impressum.hinweise20')}</p>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('cockpit.footer.impressum.hinweise21', {
                    interpolation: { escapeValue: false },
                  }),
                }}
              ></p>
              <p>{t('cockpit.footer.impressum.hinweise22')}</p>
            </li>
            <li>{t('cockpit.footer.impressum.hinweise23')}</li>
            <li>{t('cockpit.footer.impressum.hinweise24')}</li>
            <li>{t('cockpit.footer.impressum.hinweise25')}</li>
            <li>{t('cockpit.footer.impressum.hinweise26')}</li>
            <li>{t('cockpit.footer.impressum.hinweise27')}</li>
          </ol>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
