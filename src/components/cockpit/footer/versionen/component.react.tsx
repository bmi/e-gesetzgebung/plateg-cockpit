// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Typography } from 'antd';
import { Row } from 'antd/lib';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AjaxError } from 'rxjs/ajax';

import { ActuatorApi } from '@plateg/rest-api';
import { ErrorController, GlobalDI, LoadingStatusController, StandardContentCol } from '@plateg/theme';

import deps from '../../../../../package.json';
import { StaticPageWrapper } from '../../../general/static-page-wrapper/component.react';

const { Title } = Typography;

interface PlatformBeApi {
  details: {
    'app.version': string;
    'authorization.version': string;
  };
}

interface EditorBeApi {
  details: {
    version: string;
  };
}

interface HealthApiType {
  components: {
    editor: EditorBeApi;
    properties: PlatformBeApi;
  };
}

interface VersionInfo {
  name: string;
  version?: string;
}

interface Props {
  light?: boolean;
}

const appVersion = '$$APP_VERSION$$';

export function VersionenComponent(props: Readonly<Props>): React.ReactElement {
  const { t } = useTranslation();

  const [healthApi, setHealthApi] = useState<HealthApiType | object>();

  const healthController = GlobalDI.getOrRegister<ActuatorApi>('actuatorApi', () => new ActuatorApi());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');

  useEffect(() => {
    loadingStatusController.setLoadingStatus(true);
    const sub = healthController.health().subscribe({
      next: (data) => {
        setHealthApi(data);
        loadingStatusController.setLoadingStatus(false);
      },
      error: (error: AjaxError) => {
        // error 503 happens when the health API reports any service as unavailable but we can still fetch the version data from the response
        if (error.status === 503 && (error.response as HealthApiType)?.components) {
          setHealthApi(error.response as HealthApiType);
        } else {
          errorCtrl.displayErrorMsg(error, 'generalErrorMsg');
        }
        loadingStatusController.setLoadingStatus(false);
      },
    });
    return () => {
      sub.unsubscribe();
    };
  }, []);

  const versionsInfos: VersionInfo[] = [
    {
      name: t('cockpit.footer.versionen.plFE'),
      version: appVersion,
    },
    { name: t('cockpit.footer.versionen.edFE'), version: deps.dependencies['@plateg/editor'] },
    {
      name: t('cockpit.footer.versionen.plBE'),
      version: (healthApi as HealthApiType)?.components.properties?.details['app.version'],
    },
    {
      name: t('cockpit.footer.versionen.edBE'),
      version: (healthApi as HealthApiType)?.components.editor?.details?.version,
    },
    {
      name: t('cockpit.footer.versionen.rbac'),
      version: (healthApi as HealthApiType)?.components.properties?.details['authorization.version'],
    },
  ];

  const versionsInfosLight: VersionInfo[] = [
    {
      name: t('cockpit.footer.versionen.plFE'),
      version: appVersion,
    },
    {
      name: t('cockpit.footer.versionen.plBE'),
      version: (healthApi as HealthApiType)?.components.properties?.details['app.version'],
    },
  ];

  const displayVersion = (versionsInfo: VersionInfo[]) =>
    versionsInfo.map((item) => (
      <p key={crypto.randomUUID()}>
        {item.name} {item.version}
      </p>
    ));

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}> {t('cockpit.footer.versionen.title')}</Title>
          {props.light ? displayVersion(versionsInfosLight) : displayVersion(versionsInfos)}
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
