// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../faq.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { UserType } from '../component.react';
import { getGGPQuestions, getLoginQuestions, getModulesQuestions, getProjectQuestions } from '../controller.react';
import { FAQSection } from '../faq-section/component.react';

export function FAQLogout(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <>
      <Title className="h1-style" level={2}>
        {t('cockpit.footer.faq.areaTitleBReg')}
      </Title>
      <FAQSection
        title={t('cockpit.footer.faq.login.title')}
        titleLevel={3}
        qaItems={getLoginQuestions(UserType.BREG)}
      />
      <FAQSection
        title={t('cockpit.footer.faq.modules.title')}
        titleLevel={3}
        qaItems={getModulesQuestions(UserType.BREG)}
      />
      <FAQSection
        title={t('cockpit.footer.faq.project.title')}
        titleLevel={3}
        qaItems={getProjectQuestions(UserType.BREG)}
        lastElement
      />
      <Title className="h1-style" level={2}>
        {t('cockpit.footer.faq.areaTitleBT')}
      </Title>
      <FAQSection title={t('cockpit.footer.faq.login.title')} titleLevel={3} qaItems={getLoginQuestions(UserType.BT)} />
      <FAQSection
        title={t('cockpit.footer.faq.modules.title')}
        titleLevel={3}
        qaItems={getModulesQuestions(UserType.BT)}
      />
      <FAQSection
        title={t('cockpit.footer.faq.project.title')}
        titleLevel={3}
        qaItems={getProjectQuestions(UserType.BT)}
        lastElement
      />
      <Title className="h1-style" level={2}>
        {t('cockpit.footer.faq.areaTitleBR')}
      </Title>
      <FAQSection title={t('cockpit.footer.faq.login.title')} titleLevel={3} qaItems={getLoginQuestions(UserType.BR)} />
      <FAQSection
        title={t('cockpit.footer.faq.modules.title')}
        titleLevel={3}
        qaItems={getModulesQuestions(UserType.BR)}
      />
      <FAQSection
        title={t('cockpit.footer.faq.project.title')}
        titleLevel={3}
        qaItems={getProjectQuestions(UserType.BR)}
        lastElement
      />
      <Title className="h1-style" level={2}>
        {t('cockpit.footer.faq.areaTitleGGP')}
      </Title>
      <FAQSection
        title={t('cockpit.footer.faq.modules.title')}
        titleLevel={3}
        qaItems={getGGPQuestions()}
        lastElement
      />
    </>
  );
}
