// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../faq.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { UserType } from '../component.react';
import { getGGPQuestions, getLoginQuestions, getModulesQuestions, getProjectQuestions } from '../controller.react';
import { FAQSection } from '../faq-section/component.react';

export function FAQLogin(props: { userType: UserType }): React.ReactElement {
  const { t } = useTranslation();
  return (
    <>
      <FAQSection title={t('cockpit.footer.faq.login.title')} qaItems={getLoginQuestions(props.userType, true)} />
      <FAQSection title={t('cockpit.footer.faq.modules.title')} qaItems={getModulesQuestions(props.userType, true)} />
      <FAQSection title={t('cockpit.footer.faq.project.title')} qaItems={getProjectQuestions(props.userType, true)} />
      <FAQSection title={t('cockpit.footer.faq.ggp.title')} qaItems={getGGPQuestions()} lastElement />
    </>
  );
}
