// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Collapse, CollapseProps } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect } from 'react';

import { AriaController } from '@plateg/theme';
import { DirectionRightOutlinedNew } from '@plateg/theme/src/components/icons';
import { Filters } from '@plateg/theme/src/shares/filters';

interface FAQSectionProps {
  title: string;
  titleLevel?: 1 | 2 | 3 | 4 | 5;
  qaItems: QAItem[];
  lastElement?: boolean;
}
interface QAItem {
  question: string;
  answer: string;
}
export function FAQSection(props: FAQSectionProps): React.ReactElement {
  useEffect(() => {
    AriaController.setAriaLabelsByClassName('ant-collapse-arrow', '');
  }, []);

  const sanitizedTitle = Filters.sanitizeStringForCSS(props.title);

  const items: CollapseProps['items'] = props.qaItems.map((item: QAItem) => {
    return {
      children: <p dangerouslySetInnerHTML={{ __html: item.answer }} />,
      label: <p dangerouslySetInnerHTML={{ __html: item.question }} />,
      key: crypto.randomUUID(),
    };
  });

  return (
    <div id={`faq-section-${sanitizedTitle}`}>
      <Title className={props.titleLevel && 'h2-style'} level={props.titleLevel ?? 2}>
        {props.title}
      </Title>
      <Collapse
        items={items}
        bordered={false}
        expandIcon={({ isActive }) => (
          <DirectionRightOutlinedNew
            style={{ width: 22, height: 22, marginTop: -3 }}
            className={isActive ? 'svg-transition svg-rotate-down' : 'svg-transition'}
          />
        )}
      />
      {!props.lastElement && <hr />}
    </div>
  );
}
