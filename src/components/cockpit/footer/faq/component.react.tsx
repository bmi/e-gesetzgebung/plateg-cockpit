// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './faq.less';

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { StaticPageWrapper } from '../../../general/static-page-wrapper/component.react';
import { FAQLogin } from './faq-login/component.react';
import { FAQLogout } from './faq-logout/component.react';

export enum UserType {
  BREG = 'BReg',
  BT = 'BT',
  BR = 'BR',
}

export function FAQ(): React.ReactElement {
  const { t } = useTranslation();
  const [userType, setUserType] = useState<UserType>();
  const appStore = {
    user: useAppSelector((state) => state.user.user),
  };

  useEffect(() => {
    if (appStore.user) {
      switch (appStore.user?.dto.ressort?.kurzbezeichnung) {
        case 'BT':
          setUserType(UserType.BT);
          break;
        case 'BR':
          setUserType(UserType.BR);
          break;
        default:
          setUserType(UserType.BREG);
      }
    } else {
      setUserType(undefined);
    }
  }, [appStore.user]);

  return (
    <StaticPageWrapper>
      <div className="faq content-area-faq">
        <Row>
          <StandardContentCol>
            <div className="faqHeader">
              <Title level={1}>{t('cockpit.footer.faq.title')}</Title>
            </div>
          </StandardContentCol>
        </Row>
        <div className="faqBody">
          <Row>
            <StandardContentCol>{userType ? <FAQLogin userType={userType} /> : <FAQLogout />}</StandardContentCol>
          </Row>
        </div>
      </div>
    </StaticPageWrapper>
  );
}
