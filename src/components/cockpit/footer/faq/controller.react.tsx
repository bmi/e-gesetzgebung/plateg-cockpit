// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import { useHistory } from 'react-router';

import { UserControllerApi } from '@plateg/rest-api';
import { GlobalDI } from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { LoginController } from '../../login-component/controller';
import { UserType } from './component.react';

export const getLoginQuestions = (userType: UserType, login?: boolean) => {
  const history = useHistory();
  const loginCtrl = GlobalDI.getOrRegister('cockpitLoginController', () => new LoginController());
  const userController: UserControllerApi = GlobalDI.get('userController');
  const appStore = {
    appSetting: useAppSelector((state) => state.appSetting.appSetting),
  };
  const AnchorTagType = {
    HOME: history.createHref({ pathname: '/cockpit' }),
    REGISTER: `${userController.configuration.basePath}/user/register`,
    PASSWORT: appStore.appSetting?.iamEinstellungen
      ? `${appStore.appSetting.iamEinstellungen.authServerUrl}/realms/bund/login-actions/reset-credentials`
      : '#',
    LOGIN: loginCtrl.getLoginUrl(),
  };

  const getUrl = (urlType: string) => urlType;

  const loginData = {
    question: i18n.t('cockpit.footer.faq.login.loginData'),
    answer:
      i18n.t('cockpit.footer.faq.login.loginDataAnswerBase', {
        urlHome: getUrl(AnchorTagType.HOME),
        urlRegister: getUrl(AnchorTagType.REGISTER),
      }) + i18n.t(`cockpit.footer.faq.login.loginDataAnswer${userType}`),
  };
  const problemLogin = {
    question: i18n.t('cockpit.footer.faq.login.problemLogin'),
    answer:
      i18n.t('cockpit.footer.faq.login.problemLoginAnswerBase', {
        urlPass: getUrl(AnchorTagType.PASSWORT),
        urlRegister: getUrl(AnchorTagType.REGISTER),
      }) +
      i18n.t(`cockpit.footer.faq.login.problemLoginAnswer${userType}`, {
        eGesetzSupportEmail: i18n.t('cockpit.eGesetzSupportEmail'),
        pkpFachadministration: i18n.t('cockpit.footer.faq.login.pkpFachadministration'),
        pkpSupportEmail: i18n.t('cockpit.footer.faq.login.pkpSupportEmail'),
      }),
  };
  const passwordChange = {
    question: i18n.t('cockpit.footer.faq.login.passwordChange'),
    answer: i18n.t('cockpit.footer.faq.login.passwordChangeAnswer', {
      urlHome: getUrl(AnchorTagType.HOME),
      urlPass: getUrl(AnchorTagType.PASSWORT),
      urlRegister: getUrl(AnchorTagType.REGISTER),
      urlLogin: getUrl(AnchorTagType.LOGIN),
    }),
  };
  const personalData = {
    question: i18n.t('cockpit.footer.faq.login.personalData'),
    answer: i18n.t('cockpit.footer.faq.login.personalDataAnswer', {
      urlRegister: getUrl(AnchorTagType.REGISTER),
    }),
  };
  const changeData = {
    question: i18n.t('cockpit.footer.faq.login.changeData'),
    answer: i18n.t(`cockpit.footer.faq.login.changeDataAnswer${userType}`, {
      eGesetzSupportEmail: i18n.t('cockpit.eGesetzSupportEmail'),
      pkpSupportEmail: i18n.t('cockpit.footer.faq.login.pkpSupportEmail'),
      pkpFachadministration: i18n.t('cockpit.footer.faq.login.pkpFachadministration'),
    }),
  };
  const accountRequirement = {
    question: i18n.t('cockpit.footer.faq.login.accountRequirement'),
    answer: i18n.t('cockpit.footer.faq.login.accountRequirementAnswer', {
      pkpFachadministration: i18n.t('cockpit.footer.faq.login.pkpFachadministration'),
    }),
  };
  const contact = {
    question: i18n.t('cockpit.footer.faq.login.contact'),
    answer: i18n.t('cockpit.footer.faq.login.contactAnswer', {
      eGesetzSupportEmail: i18n.t('cockpit.eGesetzSupportEmail'),
    }),
  };
  const ressortWechsel = {
    question: i18n.t('cockpit.footer.faq.login.ressortWechsel'),
    answer: i18n.t('cockpit.footer.faq.login.ressortWechselAnswer', {
      urlRegister: getUrl(AnchorTagType.REGISTER),
      pkpSupportEmail: i18n.t('cockpit.footer.faq.login.pkpSupportEmail'),
      pkpFachadministration: i18n.t('cockpit.footer.faq.login.pkpFachadministration'),
    }),
  };
  const logout = {
    question: i18n.t('cockpit.footer.faq.login.logout'),
    answer: i18n.t('cockpit.footer.faq.login.logoutAnswer'),
  };
  const deleteAccount = {
    question: i18n.t('cockpit.footer.faq.login.deleteAccount'),
    answer: i18n.t('cockpit.footer.faq.login.deleteAccountAnswer', {
      eGesetzSupportEmail: i18n.t('cockpit.eGesetzSupportEmail'),
    }),
  };
  const stellvertretung = {
    question: i18n.t(`cockpit.footer.faq.login.stellvertretung${userType}`),
    answer:
      i18n.t('cockpit.footer.faq.login.stellvertretungAnswerBase') +
      (userType === UserType.BREG ? i18n.t('cockpit.footer.faq.login.stellvertretungAnswerBReg') : ''),
  };
  const barrierefreiheit = {
    question: i18n.t('cockpit.footer.faq.login.barrierefreiheit'),
    answer: i18n.t('cockpit.footer.faq.login.barrierefreiheitAnswer'),
  };

  const loginQuestions = [loginData, problemLogin, passwordChange, personalData, contact, deleteAccount];
  if (userType === UserType.BREG) {
    loginQuestions.splice(4, 0, accountRequirement);
    loginQuestions.splice(6, 0, ressortWechsel);
  }
  if (login) {
    loginQuestions.splice(4, 0, changeData);
    loginQuestions.splice(userType === UserType.BREG ? 8 : 6, 0, logout);
    (userType === UserType.BREG || userType === UserType.BT) && loginQuestions.push(stellvertretung);
    loginQuestions.push(barrierefreiheit);
  }
  return loginQuestions;
};

export const getModulesQuestions = (userType: UserType, login?: boolean) => {
  const history = useHistory();
  const noAccess = {
    question: i18n.t('cockpit.footer.faq.modules.noAccess'),
    answer: i18n.t('cockpit.footer.faq.modules.noAccessAnswer'),
  };
  const missingFunctions = {
    question: i18n.t('cockpit.footer.faq.modules.missingFunctions'),
    answer: i18n.t('cockpit.footer.faq.modules.missingFunctionsAnswer', {
      eGesetzSupportEmail: i18n.t('cockpit.eGesetzSupportEmail'),
    }),
  };
  const userManual = {
    question: i18n.t('cockpit.footer.faq.modules.userManual'),
    answer: i18n.t('cockpit.footer.faq.modules.userManualAnswer'),
  };
  const security = {
    question: i18n.t('cockpit.footer.faq.modules.security'),
    answer: i18n.t('cockpit.footer.faq.modules.securityAnswer'),
  };
  const internalWork = {
    question: i18n.t('cockpit.footer.faq.modules.internalWork'),
    answer: i18n.t('cockpit.footer.faq.modules.internalWorkAnswer'),
  };
  const integration = {
    question: i18n.t('cockpit.footer.faq.modules.integration'),
    answer: i18n.t('cockpit.footer.faq.modules.integrationAnswer'),
  };
  const legalDocML = {
    question: i18n.t('cockpit.footer.faq.modules.legalDocML'),
    answer: i18n.t('cockpit.footer.faq.modules.legalDocMLAnswer', {
      urlLegalDocML: history.createHref({ pathname: '/ueberDasProjekt/legaldoc' }),
    }),
  };
  const interaction = {
    question: i18n.t('cockpit.footer.faq.modules.interaction'),
    answer: i18n.t('cockpit.footer.faq.modules.interactionAnswer'),
  };
  const operationSystems = {
    question: i18n.t('cockpit.footer.faq.modules.operationSystems'),
    answer: i18n.t('cockpit.footer.faq.modules.operationSystemsAnswer'),
  };
  const applications = {
    question: i18n.t(`cockpit.footer.faq.modules.applications${userType}`),
    answer: i18n.t(`cockpit.footer.faq.modules.applicationsAnswer${userType}`),
  };

  const modulesQuestions = [noAccess];
  if (login)
    modulesQuestions.push(
      missingFunctions,
      userManual,
      security,
      internalWork,
      legalDocML,
      operationSystems,
      applications,
    );
  if (userType === UserType.BREG && login) {
    modulesQuestions.splice(5, 0, integration);
    modulesQuestions.splice(7, 0, interaction);
  }
  return modulesQuestions;
};

export const getProjectQuestions = (userType: UserType, login?: boolean) => {
  const history = useHistory();
  const projectStatus = {
    question: i18n.t('cockpit.footer.faq.project.projectStatus'),
    answer: i18n.t('cockpit.footer.faq.project.projectStatusAnswer'),
  };
  const strategyQuestions = {
    question: i18n.t('cockpit.footer.faq.project.strategyQuestions'),
    answer: i18n.t('cockpit.footer.faq.project.strategyQuestionsAnswer'),
  };
  const eLegislationResponsibility = {
    question: i18n.t('cockpit.footer.faq.project.eLegislationResponsibility'),
    answer: i18n.t('cockpit.footer.faq.project.eLegislationResponsibilityAnswer', {
      urlImpressum: history.createHref({ pathname: '/impressum' }),
    }),
  };
  const goal = {
    question: i18n.t('cockpit.footer.faq.project.goal'),
    answer: i18n.t('cockpit.footer.faq.project.goalAnswer'),
  };
  const timeline = {
    question: i18n.t(`cockpit.footer.faq.project.timeline${userType}`),
    answer: i18n.t(`cockpit.footer.faq.project.timelineAnswer${userType}`),
  };

  const projectQuestions = [projectStatus, strategyQuestions];
  if (login) projectQuestions.push(eLegislationResponsibility, goal, timeline);

  return projectQuestions;
};

export const getGGPQuestions = () => {
  const visibility = {
    question: i18n.t('cockpit.footer.faq.ggp.visibility'),
    answer: i18n.t('cockpit.footer.faq.ggp.visibilityAnswer'),
  };
  const information = {
    question: i18n.t('cockpit.footer.faq.ggp.information'),
    answer: i18n.t('cockpit.footer.faq.ggp.informationAnswer'),
  };

  return [visibility, information];
};
