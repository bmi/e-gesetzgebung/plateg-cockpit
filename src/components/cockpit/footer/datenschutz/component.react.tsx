// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next/';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../../general/static-page-wrapper/component.react';

export function Datenschutz(): React.ReactElement {
  const { t } = useTranslation();

  const generateListEntry = (index: number) => {
    return (
      <li key={crypto.randomUUID()}>
        <Title level={4}>{t(`cockpit.footer.datenschutz.listTitle7${index}`)}</Title>
        <p className="ant-typography p-no-style">{t(`cockpit.footer.datenschutz.listContent7${index}`)}</p>
      </li>
    );
  };

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.datenschutz.title')}</Title>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle1')}</Title>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle11')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf111')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.datenschutz.block112', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf112')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.datenschutz.block113', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf114')}</p>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle12')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf121')}</p>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle2')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf21')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf22')}</p>
          <Title level={2}>{t('cockpit.footer.datenschutz.listTitle23')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.datenschutz.list231', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <Title level={2}>{t('cockpit.footer.datenschutz.listTitle24')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.datenschutz.list241', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle3')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf31')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf32')}</p>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle4')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf41')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf42')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf43')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf44')}</p>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle5')}</Title>
          <Title level={2}>{t('cockpit.footer.datenschutz.listTitle51')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.datenschutz.list511', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.datenschutz.paragraf52', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle6')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf61')}</p>
          <Title level={2}>{t('cockpit.footer.datenschutz.subtitle7')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf71')}</p>
          <ul>{[2, 3, 4, 5, 6, 7].map(generateListEntry)}</ul>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf78')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.datenschutz.paragraf79', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <p className="ant-typography p-no-style">{t('cockpit.footer.datenschutz.paragraf80')}</p>
        </StandardContentCol>{' '}
      </Row>
    </StaticPageWrapper>
  );
}
