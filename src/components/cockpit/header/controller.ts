// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

export class HeaderComponentController {
  public computeInitials = (name?: string): string => {
    const nameParts = (name ?? '').split(' ');
    if (nameParts.length > 1) {
      return nameParts[0].charAt(0) + nameParts[nameParts.length - 1].charAt(0);
    } else {
      return '';
    }
  };
}
