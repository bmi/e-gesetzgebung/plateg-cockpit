// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './logged-in-menu-dropdown.less';

import { Avatar, Button, Menu } from 'antd';
import { ItemType } from 'antd/lib/menu/hooks/useItems';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { Subscription } from 'rxjs';

import { UserEntityDTO, UserEntityResponseDTO, UserEntityWithStellvertreterDTO } from '@plateg/rest-api';
import { CheckOutlined, displayMessage, GlobalDI } from '@plateg/theme';
import { logoutAction, useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { DirectionRightOutlinedNew } from '@plateg/theme/src/components/icons';
import { useAppDispatch } from '@plateg/theme/src/components/store';
import { clearUser, setUser } from '@plateg/theme/src/components/store/slices/userSlice';

import { SettingsController } from '../../../settings-component/controller';
import { HeaderComponentController } from '../../controller';

import type { MenuProps } from 'rc-menu';
interface LoggedInMenuDropdownProps {
  isMenuVisible: boolean;
  setIsMenuVisible: (value: boolean) => void;
  userInfo: UserEntityWithStellvertreterDTO;
  userMenuRef: React.MutableRefObject<HTMLAnchorElement | null>[];
  hilfenRef: React.RefObject<HTMLAnchorElement>;
}

export function LoggedInMenuDropdown(props: LoggedInMenuDropdownProps): React.ReactElement {
  const location = useLocation();
  const history = useHistory();
  const dispatch = useAppDispatch();
  const { keycloak } = useKeycloak();
  const [settingIsSelected, setSettingIsSelected] = useState('');
  useEffect(() => {
    if (location.pathname.startsWith('/einstellungen')) {
      setSettingIsSelected('1');
    } else {
      setSettingIsSelected('');
    }
  }, [location.pathname]);
  const { t } = useTranslation();
  const settingsController = GlobalDI.getOrRegister('settingsController', () => new SettingsController());
  const headerCtrl = GlobalDI.getOrRegister('cockpitHeaderComponentController', () => new HeaderComponentController());

  const [originalUser, setOriginalUser] = useState<UserEntityDTO | UserEntityWithStellvertreterDTO>();
  const [initials, setInitials] = useState<string>('');

  const initMenuItems: MenuProps['items'] = [
    {
      label: (
        <Link
          id="logged-in-plattform-settings-btn"
          to="/einstellungen"
          onKeyDown={(event) =>
            event.key === 'Enter' && !props.userInfo?.stellvertreter ? history.replace('/einstellungen') : ''
          }
          onClick={(event) => {
            if (props.userInfo?.stellvertreter) {
              event.preventDefault();
            }
          }}
        >
          {t('cockpit.userMenu.settings')}
        </Link>
      ),
      key: 'menu-item-1',
      disabled: props.userInfo?.stellvertreter,
    },
    { type: 'divider' },
    {
      label: (
        <Link
          to="/#"
          id="logged-in-logout-button"
          type="text"
          ref={props.userMenuRef[0]}
          onClick={() => {
            dispatch(clearUser());
            logoutAction(keycloak);
          }}
          onKeyDown={(event) => {
            if (event.key === 'Enter') {
              logoutAction(keycloak);
            }
          }}
        >
          {t('cockpit.userMenu.logout')}
        </Link>
      ),
      key: 'menu-item-2',
    },
  ];
  const [menuItems, setMenuItems] = useState<MenuProps['items']>(initMenuItems);

  const reloadPlattform = () => {
    history.push('/cockpit');
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  };

  useEffect(() => {
    if (props.isMenuVisible) {
      props.userMenuRef[1].current?.focus();
    }
  }, [props.isMenuVisible]);

  const handleStellvertreterChange = (entry: UserEntityResponseDTO) => {
    settingsController.setUserCall(entry.base.id).subscribe({
      next: (userResponse) => {
        displayMessage(
          t('cockpit.stellvertretung.changeSuccess', {
            name: entry.dto.name || entry.dto.email,
          }),
          'success',
        );
        dispatch(setUser(userResponse));
        localStorage.setItem('stellvertretungActiveId', userResponse.base.id);
        reloadPlattform();
      },
      error: () => {
        displayMessage(t('cockpit.stellvertretung.changeError'), 'error');
      },
    });
  };

  const createSVSubMenu = (data: UserEntityResponseDTO[]) => {
    const subMenuSV = {
      popupClassName: 'konto-wechseln-holder',
      key: 'SubMenu',
      label: 'Konto wechseln',
      popupOffset: [-615, 0],
      // this property creates a react console error -> issue https://github.com/ant-design/ant-design/discussions/36399
      children: [
        {
          type: 'group',
          children: [
            {
              label: (
                <Button
                  type="text"
                  id="stellvertreter-header-button"
                  onClick={() =>
                    settingsController.resetUserCall().subscribe({
                      next: (userResponse) => {
                        displayMessage(
                          t('cockpit.stellvertretung.resetSuccess', {
                            name: props.userInfo?.name || props.userInfo?.email,
                          }),
                          'success',
                        );
                        dispatch(setUser(userResponse));
                        localStorage.removeItem('stellvertretungActiveId');
                        reloadPlattform();
                      },
                      error: () => {
                        displayMessage(t('cockpit.stellvertretung.resetError'), 'error');
                      },
                    })
                  }
                >
                  <span className="icon-holder">
                    {`${originalUser?.name || originalUser?.email || ''} (Sie)`}{' '}
                    {!props.userInfo?.stellvertreter && (
                      <span className="icon">
                        <CheckOutlined />
                      </span>
                    )}
                  </span>
                </Button>
              ),
              className: 'stellvertreter-menu-item',
              key: 'stellvertreter-header',
              onKeyDown: (e: KeyboardEvent) =>
                e.key === 'Enter'
                  ? settingsController.resetUserCall().subscribe({
                      next: (userResponse) => {
                        displayMessage(
                          t('cockpit.stellvertretung.resetSuccess', {
                            name: props.userInfo?.name || props.userInfo?.email,
                          }),
                          'success',
                        );
                        dispatch(setUser(userResponse));
                        localStorage.removeItem('stellvertretungActiveId');
                        reloadPlattform();
                      },
                      error: () => {
                        displayMessage(t('cockpit.stellvertretung.resetError'), 'error');
                      },
                    })
                  : null,
            },
            { type: 'divider' },
            ...data.map((entry, index) => {
              return {
                className: 'stellvertreter-menu-item',
                key: `stellvertreter-entry-${index}`,
                onKeyDown: (e: KeyboardEvent) => {
                  if (e.key === 'Enter') {
                    handleStellvertreterChange(entry);
                  }
                },
                label: (
                  <Button
                    type="text"
                    key={`stellvertreter-entry-${index}-button`}
                    onClick={() => {
                      handleStellvertreterChange(entry);
                    }}
                  >
                    <span className="icon-holder">
                      {entry.dto.name || entry.dto.email}
                      {props.userInfo?.stellvertreter &&
                        (props.userInfo?.name === entry.dto.name || props.userInfo?.email === entry.dto.email) && (
                          <span className="icon">
                            <CheckOutlined />
                          </span>
                        )}
                    </span>
                  </Button>
                ),
              };
            }),
          ],
        },
      ],
    };
    const divider: ItemType = {
      type: 'divider',
      className: 'divider-sv',
    };
    setMenuItems([subMenuSV, divider, ...initMenuItems]);
  };
  let getStellvertretungenCallSub: Subscription;
  useEffect(() => {
    if (originalUser) {
      getStellvertretungenCallSub = settingsController.getStellvertretungenCall().subscribe({
        next: (dataSV) => {
          if (dataSV.length > 0) {
            // stellverter could not be tested in the new menu version (because of pkp?), should be retested when possible
            createSVSubMenu(dataSV);
          }
        },
        error: () => {
          console.error('Stellvertreter could not be loaded');
        },
      });
    }
    return () => {
      getStellvertretungenCallSub?.unsubscribe();
    };
  }, [originalUser]);

  useEffect(() => {
    if (props.userInfo?.stellvertreter) {
      setOriginalUser(props.userInfo?.stelleninhaber?.dto);
    } else {
      setOriginalUser(props.userInfo);
    }
  }, [props.userInfo]);

  useEffect(() => {
    setInitials(headerCtrl.computeInitials(originalUser?.name));
  }, [originalUser]);

  return (
    <>
      <div className="bitv-btn-wrapper">
        <Button
          ref={props.hilfenRef}
          shape="circle"
          className={props.isMenuVisible ? 'initials-button-active' : 'initials-button'}
          aria-label="Nutzermenü"
          aria-expanded={props.isMenuVisible}
          onClick={() => props.setIsMenuVisible(!props.isMenuVisible)}
          id="btn-user-menu"
        >
          {initials}
        </Button>
        <span className="bitv-avatar-circle"></span>
      </div>
      <div className="user-menu-container" hidden={!props.isMenuVisible}>
        <div className="upper-section">
          <div className="avatar-container">
            <Avatar size={60}>{initials}</Avatar>
          </div>
          <div className="name-div">
            <p className="full-name">{originalUser?.name}</p>
          </div>
        </div>
        <Menu
          expandIcon={<DirectionRightOutlinedNew style={{ width: 22, height: 22, marginTop: 1 }} />}
          items={menuItems}
          className="user-menu"
          mode="vertical"
          onClick={() => {
            props.setIsMenuVisible(false);
          }}
          selectedKeys={[settingIsSelected]}
        />
      </div>
    </>
  );
}
