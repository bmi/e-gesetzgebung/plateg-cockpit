// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Menu } from 'antd';
import React from 'react';

import { IAMButton } from '../iam-button/component.react';
interface LoggedOutMenuProps {
  myRefLoggedout: React.MutableRefObject<HTMLAnchorElement | null>[];
}

export function LoggedOutMenu(props: LoggedOutMenuProps): React.ReactElement {
  return (
    <Menu className="loggedout-menu">
      <IAMButton login={true} ref={props.myRefLoggedout[0]} suffix="loggedout-menu" />
      <IAMButton login={false} ref={props.myRefLoggedout[1]} suffix="loggedout-menu" />
    </Menu>
  );
}
