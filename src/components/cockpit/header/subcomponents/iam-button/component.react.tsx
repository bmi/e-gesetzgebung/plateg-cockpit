// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ExternalLinkOutlined } from '@plateg/theme';
import { useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';

interface IAMButtonProps {
  login: boolean;
  ref?: React.MutableRefObject<HTMLAnchorElement | null>;
  suffix: string;
}

export function IAMButton(props: IAMButtonProps): React.ReactElement {
  const { t } = useTranslation();
  const title = props.login ? t('cockpit.login.loginButton') : t('cockpit.login.registerButton');
  const className = props.login ? 'login-button' : 'register-button';
  const type = props.login ? 'primary' : 'text';
  const { keycloak } = useKeycloak();
  return (
    <Button
      id={`iam-btn-loggedin-${(!props.login).toString()}-${props.suffix}`}
      className={className}
      type={type}
      ref={props.ref}
      onClick={() => (props.login ? void keycloak.login() : void keycloak.register())}
    >
      {title}
      <ExternalLinkOutlined />
    </Button>
  );
}
