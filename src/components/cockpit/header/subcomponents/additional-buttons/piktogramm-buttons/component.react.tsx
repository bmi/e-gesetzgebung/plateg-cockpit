// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './piktogramm-buttons.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { BarrierefreiheitOutlined, LeichteSpracheOutlined, TooltipComponent } from '@plateg/theme';

export function PiktogrammButtons(props: { currentUrl: string }): React.ReactElement {
  const { t } = useTranslation();

  return (
    <>
      <div className="vertical-bar" />
      <div className="piktogramm-buttons">
        <TooltipComponent
          placement="bottom"
          title={t('cockpit.login.barrierefreiheitBtn')}
          trigger="hover focus"
          overlayClassName="main-nav-tooltip"
        >
          <a
            id="barrierefreiheit-link"
            type="text"
            href="#/barrierefreiheit"
            className={props.currentUrl === 'barrierefreiheit' ? 'active' : ''}
          >
            <BarrierefreiheitOutlined label={t('cockpit.login.barrierefreiheitBtn')} />
          </a>
        </TooltipComponent>
        <TooltipComponent
          placement="bottom"
          title={t('cockpit.login.leichteSpracheBtn')}
          trigger="hover focus"
          overlayClassName="main-nav-tooltip"
        >
          <a
            id="leichte-sprache-link"
            type="text"
            href="#/leichteSprache"
            className={props.currentUrl === 'leichteSprache' ? 'active' : ''}
          >
            <LeichteSpracheOutlined label={t('cockpit.login.leichteSpracheBtn')} />
          </a>
        </TooltipComponent>
      </div>
    </>
  );
}
