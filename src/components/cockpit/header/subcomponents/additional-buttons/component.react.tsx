// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { createRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';

import { PiktogrammButtons } from './piktogramm-buttons/component.react';

interface IAMButtonProps {
  setIsFeedbacklVisible: (value: boolean) => void;
  setIsNewseletterModalVisible: (value: boolean) => void;
  setIsSearchModalVisible: (value: boolean) => void;
  isFeedbackModalVisible: boolean;
  isSearchModalVisible: boolean;
  modalNewseletterRef: React.RefObject<HTMLAnchorElement>;
  btnNewsletterRef: React.RefObject<HTMLAnchorElement>;
  fullVersion?: boolean;
  signedIn?: boolean;
  customClassName?: string;
  setIsMenuClosed?: (value: boolean) => void;
  menuCollapseBtnRef?: React.RefObject<HTMLButtonElement>;
  lastMenuBtnRef?: React.RefObject<HTMLButtonElement>;
}

export function AdditionalButtons(props: IAMButtonProps): React.ReactElement {
  const history = useHistory();
  const { t } = useTranslation();
  const routeMatcherVorbereitung = useRouteMatch<{ pageName: string }>('/:pageName');
  const [currentUrl, setCurrentUrl] = useState<string>('');
  const btnFeedbackRef = createRef<HTMLAnchorElement>();
  const btnSearchRef = createRef<HTMLAnchorElement>();

  const feedbackButtonCallback = () => {
    props.setIsNewseletterModalVisible(true);
    setTimeout(() => props.modalNewseletterRef.current?.focus(), 200);
  };

  useEffect(() => {
    if (!props.isFeedbackModalVisible && !props.fullVersion) {
      btnFeedbackRef.current?.focus();
    }
  }, [props.isFeedbackModalVisible]);

  useEffect(() => {
    if (!props.isSearchModalVisible) {
      btnSearchRef.current?.focus();
    }
  }, [props.isSearchModalVisible]);

  useEffect(() => {
    const pageName = routeMatcherVorbereitung?.params?.pageName as string;
    setCurrentUrl(pageName);
  }, [routeMatcherVorbereitung]);

  const isLinkActive = (linkUrl: string, activeUrl: string) => {
    return linkUrl === activeUrl;
  };

  const lastButtonTabNavigation = (e: React.KeyboardEvent<HTMLElement>) => {
    if (!e.shiftKey && e.code === 'Tab') {
      props.menuCollapseBtnRef?.current?.focus();
      e.preventDefault();
    }
  };

  return (
    <>
      <div className={props.customClassName ?? 'additional-buttons'}>
        <>
          {!props.fullVersion && (
            <Button
              id="ueber-diese-seite-btn"
              type="text"
              onClick={() => history.push('/vorstellung')}
              className={isLinkActive('vorstellung', currentUrl) ? 'active' : ''}
            >
              {t('cockpit.login.ueberDieseSeiteButton')}
            </Button>
          )}
          {props.fullVersion && props.customClassName && (
            <Button
              id="releaseNotes-btn"
              type="text"
              onClick={() => {
                props.setIsMenuClosed?.(false);
                history.push('/releaseNotes');
              }}
              className={isLinkActive('releaseNotes', currentUrl) ? 'active' : ''}
            >
              {t('cockpit.login.releaseNotes')}
            </Button>
          )}
          {((props.fullVersion && props.customClassName) || !props.fullVersion) && (
            <Button
              id="ueber-die-massnahme-btn"
              type="text"
              onClick={() => {
                props.setIsMenuClosed?.(false);
                history.push('/ueberDasProjekt');
              }}
              className={isLinkActive('ueberDasProjekt', currentUrl) ? 'active' : ''}
            >
              {t('cockpit.login.ueberDasProjektButton')}
            </Button>
          )}
        </>
        {((props.customClassName && props.fullVersion) || !props.fullVersion) && (
          <Button
            id="contact-btn"
            type="text"
            onClick={() => {
              props.setIsMenuClosed?.(false);
              props.setIsFeedbacklVisible(true);
            }}
            ref={btnFeedbackRef}
          >
            {t('cockpit.login.contactButton')}
          </Button>
        )}
        {props.fullVersion && props.customClassName && (
          <Button
            id="faq-btn"
            type="text"
            onClick={() => {
              props.setIsMenuClosed?.(false);
              history.push('/faq');
            }}
            className={isLinkActive('faq', currentUrl) ? 'active' : ''}
            onKeyDown={(e) => (!props.signedIn ? lastButtonTabNavigation(e) : {})}
            ref={!props.signedIn ? props.lastMenuBtnRef : null}
          >
            {t('cockpit.login.faqButton')}
          </Button>
        )}
        {props.fullVersion && props.customClassName && props.signedIn && (
          <Button
            id="barrierefreiheit-btn"
            type="text"
            onClick={() => {
              props.setIsMenuClosed?.(false);
              history.push('/barrierefreiheit');
            }}
            className={isLinkActive('barrierefreiheit', currentUrl) ? 'active' : ''}
            onKeyDown={(e) => (props.signedIn ? lastButtonTabNavigation(e) : {})}
            ref={props.signedIn ? props.lastMenuBtnRef : null}
          >
            {t('cockpit.login.barrierefreiheitBtn')}
          </Button>
        )}
      </div>
      {(!props.fullVersion || !props.signedIn) && !props.customClassName && (
        <PiktogrammButtons currentUrl={currentUrl} />
      )}
      {props.fullVersion && <div className="vertical-bar" />}
    </>
  );
}
