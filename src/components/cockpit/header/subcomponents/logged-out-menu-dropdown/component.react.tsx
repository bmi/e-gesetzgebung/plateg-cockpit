// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';

import { UserOutlined } from '@plateg/theme';

import { LoggedOutMenu } from '../logged-out-menu/component.react';
interface LoggedOutMenuDropdownProps {
  myRefLoggedout: React.MutableRefObject<HTMLAnchorElement | null>[];
  isLoggedoutMenuVisible: boolean;
  setIsLoggedoutMenuVisible: (value: boolean) => void;
  hilfenRef: React.RefObject<HTMLAnchorElement>;
}

export function LoggedOutMenuDropdown(props: LoggedOutMenuDropdownProps): React.ReactElement {
  return (
    <>
      <Button
        id="user-button"
        ref={props.hilfenRef}
        className={props.isLoggedoutMenuVisible ? 'user-button-active' : 'user-button'}
        shape="circle"
        aria-label="Nutzermenü - unangemeldet"
        aria-expanded={props.isLoggedoutMenuVisible}
        onClick={() => {
          props.setIsLoggedoutMenuVisible(!props.isLoggedoutMenuVisible);
        }}
      >
        <UserOutlined />
      </Button>
      {props.isLoggedoutMenuVisible && (
        <div>
          <LoggedOutMenu myRefLoggedout={props.myRefLoggedout} />
        </div>
      )}
    </>
  );
}
