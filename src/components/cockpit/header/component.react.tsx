// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../general/header/header.less';

import React, { useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';

import { UserEntityWithStellvertreterDTO } from '@plateg/rest-api';
import { FocusController, GlobalDI } from '@plateg/theme';
import { useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { useAppSelector } from '@plateg/theme/src/components/store';

import { HeaderGeneral } from '../../general/header/component.react';
import { RoutingItem } from '../component.react';
import { IAMButton } from './subcomponents/iam-button/component.react';
import { LoggedInMenuDropdown } from './subcomponents/logged-in-menu-dropdown/component.react';
import { LoggedOutMenuDropdown } from './subcomponents/logged-out-menu-dropdown/component.react';

interface HeaderProps {
  routingItems: RoutingItem[];
}

export function Header(props: HeaderProps): React.ReactElement {
  const [userInfo, setUserInfo] = useState<UserEntityWithStellvertreterDTO>();
  const [signedIn, setSignedIn] = useState<boolean | undefined>(undefined);
  const [isMenuVisible, setIsMenuVisible] = useState<boolean>(false);
  const [isLoggedoutMenuVisible, setIsLoggedoutMenuVisible] = useState(false);
  const loggedInContainerRef = useRef<HTMLDivElement>(null);
  const params = useParams<{ bereich: string }>();
  const selectedItemId = props.routingItems.filter((item) => item.path.endsWith(params.bereich)).shift()?.id;
  const appStore = useAppSelector((state) => state.user);
  const { keycloak } = useKeycloak();
  useEffect(() => {
    if (appStore.user) {
      setUserInfo(appStore.user.dto);
    } else {
      setUserInfo(undefined);
    }
  }, [appStore.user]);
  useEffect(() => {
    setSignedIn(keycloak.authenticated);
  }, [keycloak.authenticated]);

  const hilfenRef = useRef<HTMLAnchorElement>(null);
  const userMenuRef: React.MutableRefObject<HTMLAnchorElement | null>[] = [
    useRef<HTMLAnchorElement>(null),
    useRef<HTMLAnchorElement>(null),
    useRef<HTMLAnchorElement>(null),
  ];
  const myRefLoggedout: React.MutableRefObject<HTMLAnchorElement | null>[] = [
    useRef<HTMLAnchorElement>(null),
    useRef<HTMLAnchorElement>(null),
  ];

  GlobalDI.getOrRegister(
    'cockpitFocusControllerLoggedout',
    () => new FocusController(setIsLoggedoutMenuVisible, myRefLoggedout, hilfenRef),
  );

  const handleClickOutside = (event: MouseEvent) => {
    if (loggedInContainerRef.current && !loggedInContainerRef.current?.contains(event.target as Node)) {
      setIsMenuVisible(false);
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent) => {
    if (event.key === 'Escape') {
      setIsMenuVisible(false);
      hilfenRef.current?.focus();
    }
  };

  useEffect(() => {
    if (isMenuVisible) {
      userMenuRef[0].current?.focus();
      window.addEventListener('wheel', () => setIsMenuVisible(false));
      document.addEventListener('click', (event: MouseEvent) => handleClickOutside(event), true);
    }
    if (isLoggedoutMenuVisible) {
      window.addEventListener('wheel', () => setIsLoggedoutMenuVisible(false));
    }
  }, [isMenuVisible, isLoggedoutMenuVisible]);

  return (
    <HeaderGeneral
      fullVersion={true}
      signedIn={signedIn}
      stellvertretung={userInfo?.stellvertreter ? userInfo?.name : undefined}
      routingItems={props.routingItems}
      selectedItemId={selectedItemId}
    >
      <>
        {signedIn !== undefined && (
          <>
            {userInfo?.name === undefined || !signedIn ? (
              <div className="iam-btns" onKeyDown={(event) => handleKeyDown(event)}>
                <LoggedOutMenuDropdown
                  isLoggedoutMenuVisible={isLoggedoutMenuVisible}
                  setIsLoggedoutMenuVisible={setIsLoggedoutMenuVisible}
                  hilfenRef={hilfenRef}
                  myRefLoggedout={myRefLoggedout}
                />
                <IAMButton login={true} suffix="login-div" />
                <IAMButton login={false} suffix="login-div" />
              </div>
            ) : (
              // onKeyPress cannot be used here, because it doesn't recognize arrow key up and down
              <div ref={loggedInContainerRef} onKeyDown={(event) => handleKeyDown(event)}>
                <LoggedInMenuDropdown
                  hilfenRef={hilfenRef}
                  userMenuRef={userMenuRef}
                  isMenuVisible={isMenuVisible}
                  setIsMenuVisible={setIsMenuVisible}
                  userInfo={userInfo}
                />
              </div>
            )}
          </>
        )}
      </>
    </HeaderGeneral>
  );
}
