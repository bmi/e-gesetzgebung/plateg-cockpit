// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { GlobalDI } from '@plateg/theme';

import { HeaderComponentController } from './controller';
const ctrl = GlobalDI.getOrRegister('cockpitHeaderComponentController', () => new HeaderComponentController());
const names: string[] = [
  'Max Mustermann',
  'Lisa Musterrfrau',
  'Max Peter Mustermann',
  'Jennifer Marta Sophia von Eckstein',
];
const invalidNames: string[] = ['Max', 'Tina', 'Peter', 'Anna'];

describe('test creation of initials from full name', () => {
  it('positive - all names should be computed to two letter initials', () => {
    names.forEach((name) => {
      expect(ctrl.computeInitials(name).length).to.eql(2);
    });
  });
  it('negative - all names should not be computed to two letter initials', () => {
    invalidNames.forEach((name) => {
      expect(ctrl.computeInitials(name).length).to.eql(0);
    });
  });
  it('positive - undefined name is okay', () => {
    expect(ctrl.computeInitials().length).to.eql(0);
  });
  it('positive - correct initals', () => {
    expect(ctrl.computeInitials(names[0])).to.eql('MM');
    expect(ctrl.computeInitials(names[1])).to.eql('LM');
    expect(ctrl.computeInitials(names[2])).to.eql('MM');
    expect(ctrl.computeInitials(names[3])).to.eql('JE');
  });
});
