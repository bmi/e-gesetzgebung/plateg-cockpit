// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './more-items.less';

import { Button, Popover } from 'antd';
import React, { useEffect, useRef, useState } from 'react';

interface MoreItemsProps {
  items: React.ReactElement[];
}
export function MoreItems(props: MoreItemsProps): React.ReactElement {
  const [isOpened, setIsOpened] = useState(false);
  const selected = isOpened ? ' selected' : '';
  const moreItemsOverlayTriggerRef = useRef<HTMLButtonElement>(null);
  const moreItemsOverlayRef = useRef<HTMLDivElement>(null);
  const [maxHeight, setMaxHeight] = useState<number>();

  const handleBlur = () => {
    requestAnimationFrame(() => {
      if (!moreItemsOverlayRef.current?.contains(document.activeElement)) {
        setIsOpened(false);
      }
    });
  };

  const handleClickOutside = (event: MouseEvent) => {
    if (
      !moreItemsOverlayRef.current?.contains(event.target as Node) &&
      !moreItemsOverlayTriggerRef.current?.contains(event.target as Node)
    ) {
      setIsOpened(false);
      document.removeEventListener('click', handleClickOutside, true);
    }
  };

  const handleMouseEnter = () => {
    document.addEventListener('click', handleClickOutside, true);
  };

  useEffect(() => {
    function updateSize() {
      setMaxHeight(window.innerHeight - 64);
    }
    window.addEventListener('resize', updateSize);
    updateSize();

    return () => {
      window.removeEventListener('resize', updateSize);
    };
  }, []);

  return (
    <Popover
      overlayClassName="main-nav-tooltip more-items-overlay"
      open={isOpened}
      placement="right"
      content={
        <div style={{ maxHeight: maxHeight }} ref={moreItemsOverlayRef} className="menu-more-items" onBlur={handleBlur}>
          {props.items}
        </div>
      }
      getPopupContainer={(triggerNode) => triggerNode.parentNode as HTMLElement}
    >
      <Button
        ref={moreItemsOverlayTriggerRef}
        type="link"
        id={`menu-more-items-btn`}
        className={`cockpit-menu-tile ${selected} more-items-trigger`}
        aria-label="Weitere Menüeinträge"
        onBlur={handleBlur}
        onFocus={() => setIsOpened(true)}
        onMouseEnter={() => {
          setIsOpened(true);
          handleMouseEnter();
        }}
      >
        ...
      </Button>
    </Popover>
  );
}
