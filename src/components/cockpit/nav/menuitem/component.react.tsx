// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';
import { useTranslation } from 'react-i18next';

import { RoutingItem } from '../../component.react';

type Props = {
  item: RoutingItem;
  isSelectedItem: boolean;
  nowrap?: boolean;
};

export function MenuItemButton(props: Props): React.ReactElement {
  const { t } = useTranslation();
  const { item } = props;
  const label = t(`${item.menuItem}`);
  const selected = props.isSelectedItem ? ' selected' : '';
  const ariaLabel = selected ? `${label} (aktiv)` : label;
  const whiteSpace = props.nowrap ? 'nowrap' : '';

  return (
    <span
      className={`menu-item-${item.moduleName} ${item.id > 100 && 'remove-gap'} item-holder ${whiteSpace}`}
      aria-label={ariaLabel}
    >
      <span className="cockpit-header-logo">{item.icon}</span>
      <span className="text">{t(`${item.menuItem || ''}`)}</span>
    </span>
  );
}
