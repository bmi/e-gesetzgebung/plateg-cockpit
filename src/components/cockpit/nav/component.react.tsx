// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './nav.less';

import { Drawer, Menu, MenuRef } from 'antd';
import React from 'react';
import { useHistory } from 'react-router-dom';

import { RoutingItem } from '../component.react';
import { MenuItemButton } from './menuitem/component.react';

interface Props extends IAMButtonProps {
  items: RoutingItem[];
  selectedItemId?: number;
  isCollapsed: boolean;
  handleOnClose: (value: boolean) => void;
}

interface IAMButtonProps {
  setIsFeedbacklVisible: (value: boolean) => void;
  setIsNewseletterModalVisible: (value: boolean) => void;
  setIsSearchModalVisible: (value: boolean) => void;
  isFeedbackModalVisible: boolean;
  isSearchModalVisible: boolean;
  modalNewseletterRef: React.RefObject<HTMLAnchorElement>;
  btnNewsletterRef: React.RefObject<HTMLAnchorElement>;
  fullVersion?: boolean;
  signedIn?: boolean;
  mainNavRef: React.RefObject<MenuRef>;
  menuCollapseBtnRef: React.RefObject<HTMLButtonElement>;
  lastMenuBtnRef: React.RefObject<HTMLButtonElement>;
}

export function MenuItemBar(props: Props): React.ReactElement {
  const history = useHistory();
  const selectedItems: string[] = [];

  const items = props.items
    .filter((item) => item.label) // show only items that have a label in the menu
    .filter((item) => (props.fullVersion ? item.id : item.id < 100))
    .map((item) => {
      const isSelectedItem = item.id === props.selectedItemId;
      if (isSelectedItem) {
        selectedItems.push(item.path);
      }
      return {
        key: item.path,
        label: <MenuItemButton key={item.id} isSelectedItem={isSelectedItem} item={item} nowrap={item.nowrap} />,
        className: `${item.id === 101 && `divider-push-up`} menu-item-${!props.fullVersion ? `light-` : ``}${item.moduleName}`,
        disabled: item.disabled,
      };
    });

  return (
    <Drawer
      id="menu-item-bar-sider"
      open={props.isCollapsed}
      width={390}
      className="cockpit-sider"
      onClose={props.handleOnClose}
      placement="left"
      closeIcon={false}
      keyboard={true}
      maskClosable={true}
      size="default"
      autoFocus={false}
    >
      <Menu
        items={items}
        className="cockpit-sticky-below-header"
        onClick={(key) => {
          history.push(`${key.key}`);
          props.handleOnClose(true);
        }}
        onKeyDown={(e) => {
          if ((e.shiftKey || !props.fullVersion) && e.code === 'Tab') {
            props.menuCollapseBtnRef?.current?.focus();
            e.preventDefault();
          }
        }}
        defaultSelectedKeys={selectedItems}
        selectedKeys={selectedItems}
        aria-label="Hauptmenü"
        ref={props.mainNavRef}
      />
    </Drawer>
  );
}
