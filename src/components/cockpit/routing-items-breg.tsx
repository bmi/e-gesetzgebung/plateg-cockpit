// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { lazy } from 'react';

import { AdministrationApp } from '@plateg/administration';
import { RolleGlobalType } from '@plateg/rest-api';
import {
  AdministrationMenuIcon,
  EditorMenuIcon,
  GesetzesfolgenabschaetzungMenuIcon,
  GGPMenuIcon,
  HRAMenuIcon,
  PKPMenuIcon,
  RVMenuIcon,
  StartseiteMenuIcon,
  VorbereitungMenuIcon,
  ZeitMenuIcon,
} from '@plateg/theme';

import { routingItemsGeneral } from '../general/routing-items-general';
import { RoutingItem } from './component.react';
import { routingItemsCommon } from './routing-items-common';
import { Welcome } from './welcome/component.react';

const AppRv = lazy(() =>
  import('@plateg/rv/').then((module) => {
    return { default: module.AppRv };
  }),
);
const Editor = lazy(() =>
  import('@plateg/editor').then((module) => {
    return { default: module.Editor };
  }),
);
const EVorApp = lazy(() =>
  import('@plateg/evor').then((module) => {
    return { default: module.EVorApp };
  }),
);
const AppEgfa = lazy(() =>
  import('@plateg/egfa').then((module) => {
    return { default: module.AppEgfa };
  }),
);
const AppEnorm = lazy(() =>
  import('@plateg/enorm').then((module) => {
    return { default: module.AppEnorm };
  }),
);
const AppZeit = lazy(() =>
  import('@plateg/period-calculator').then((module) => {
    return { default: module.AppZeit };
  }),
);
const LoginComponent = lazy(() =>
  import('./login-component/component.react').then((module) => {
    return { default: module.LoginComponent };
  }),
);
const PKPComponent = lazy(() =>
  import('./PKPComponent/component.react').then((module) => {
    return { default: module.PKPComponent };
  }),
);
const GGPApp = lazy(() =>
  import('@plateg/ggp').then((module) => {
    return { default: module.GGPApp };
  }),
);
export const routingItemsBRegSpecific: RoutingItem[] = [
  {
    id: 1,
    path: '/cockpit',
    label: 'cockpit.menuItems.cockpit.label' /* 'cockpit' */,
    hint: 'cockpit.header.logo.title' /* 'cockpit' */,
    menuItem: 'cockpit.menuItems.cockpit.menuItem',
    component: <Welcome />,
    moduleName: 'cockpit',
    fullscreen: true,
    icon: <StartseiteMenuIcon />,
  },
  {
    id: 5,
    path: '/regelungsvorhaben',
    label: 'cockpit.menuItems.rv.label' /* 'RV' */,
    hint: 'cockpit.menuItems.rv.hint' /* 'Regelungsvorhaben' */,
    menuItem: 'cockpit.menuItems.rv.menuItem',
    component: (
      <LoginComponent>
        <AppRv />
      </LoginComponent>
    ),
    moduleName: 'rv',
    fullscreen: true,
    icon: <RVMenuIcon />,
  },
  {
    id: 6,
    path: '/editor',
    label: 'cockpit.menuItems.lea.label' /* 'LEA' */,
    hint: 'cockpit.menuItems.lea.hint' /* 'LEA' */,
    menuItem: 'cockpit.menuItems.lea.menuItem',
    component: (
      <LoginComponent>
        <Editor />
      </LoginComponent>
    ),
    moduleName: 'lea',
    nowrap: true,
    fullscreen: true,
    icon: <EditorMenuIcon />,
  },
  {
    id: 7,
    label: 'cockpit.menuItems.zeit.label' /* 'ZEIT' */,
    hint: 'cockpit.menuItems.zeit.hint' /* 'Zeitplanung' */,
    menuItem: 'cockpit.menuItems.zeit.menuItem',
    path: '/zeit',
    component: (
      <LoginComponent>
        <AppZeit />
      </LoginComponent>
    ),
    moduleName: 'zeit',
    fullscreen: true,
    icon: <ZeitMenuIcon />,
  },
  {
    id: 9,
    label: 'cockpit.menuItems.enorm.label' /* 'Abstimmung' */,
    hint: 'cockpit.menuItems.enorm.hint' /* 'Abstimmung von Regelungsentwürfen' */,
    menuItem: 'cockpit.menuItems.enorm.menuItem',
    path: '/hra',
    component: (
      <LoginComponent>
        <AppEnorm />
      </LoginComponent>
    ),
    moduleName: 'hra',
    fullscreen: true,
    icon: <HRAMenuIcon />,
  },
  {
    id: 4,
    label: 'cockpit.menuItems.evor.label' /* 'Vorbereitung' */,
    hint: 'cockpit.menuItems.evor.hint' /* 'Vorbereitung von Regelungsentwürfen' */,
    menuItem: 'cockpit.menuItems.evor.menuItem',
    path: '/evor',
    component: <EVorApp />,
    moduleName: 'evor',
    fullscreen: true,
    icon: <VorbereitungMenuIcon />,
  },
  {
    id: 8,
    label: 'cockpit.menuItems.egfa.label' /* 'Gesetzesfolgenabschätzung' */,
    hint: 'cockpit.menuItems.egfa.hint' /* 'Gesetzesfolgenabschätzung' */,
    menuItem: 'cockpit.menuItems.egfa.menuItem',
    path: '/egfa',
    component: (
      <LoginComponent>
        <AppEgfa />
      </LoginComponent>
    ),
    moduleName: 'egfa',
    fullscreen: true,
    icon: <GesetzesfolgenabschaetzungMenuIcon />,
  },

  {
    id: 10,
    label: 'cockpit.menuItems.ggp.label' /* 'Gesetzgebungsportal' */,
    hint: 'cockpit.menuItems.ggp.hint' /* 'Gesetzgebungsportal' */,
    menuItem: 'cockpit.menuItems.ggp.menuItem',
    path: '/gesetzgebungsportal',
    component: <GGPApp />,
    moduleName: 'ggp',
    fullscreen: true,
    icon: <GGPMenuIcon />,
    disabled: false,
  },
  {
    id: 24,
    label: 'cockpit.menuItems.pkp.label' /* 'Planungs- und Kabinettsmanagement-Programm (PKP)' */,
    hint: 'cockpit.menuItems.pkp.hint' /* 'Planungs- und Kabinettsmanagement-Programm (PKP)' */,
    menuItem: 'cockpit.menuItems.pkp.menuItem',
    path: '/pkp',
    component: <PKPComponent />,
    moduleName: 'pkp',
    icon: <PKPMenuIcon />,
  },
];

const routingItemFachadmin = {
  id: 25,
  label: 'cockpit.menuItems.admin.label' /* 'Administration' */,
  hint: 'cockpit.menuItems.admin.hint' /* 'Administration' */,
  menuItem: 'cockpit.menuItems.admin.menuItem',
  path: '/fachadministration',
  component: (
    <LoginComponent>
      <AdministrationApp />
    </LoginComponent>
  ),
  moduleName: 'fachadmin',
  fullscreen: true,
  icon: <AdministrationMenuIcon />,
};

export const routingItemsBReg: RoutingItem[] = [
  ...routingItemsBRegSpecific,
  ...routingItemsCommon,
  ...routingItemsGeneral,
];

export const getBRegRoutes = (role: RolleGlobalType | undefined) => {
  if (role === RolleGlobalType.FachadministratorBundesregierung) {
    return [...routingItemsBRegSpecific, routingItemFachadmin, ...routingItemsCommon, ...routingItemsGeneral];
  }
  return routingItemsBReg;
};
