// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './functions-preview.less';

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

import { FunctionPreviewComponent } from '../../../cockpit/welcome/functions-preview/function-preview/component.react';

export function FunctionsPreviewComponent(): React.ReactElement {
  const { Title } = Typography;

  const { t } = useTranslation();
  return (
    <section>
      <div className="holder">
        <div className="functions-preview-component" style={{ marginBottom: '192px' }}>
          <strong className="sub-title">{t('cockpit.welcomeLight.functionsPreview.subTitle')}</strong>
          <Title level={2}> {t('cockpit.welcomeLight.functionsPreview.title')}</Title>
          <FunctionPreviewComponent
            textIsOnTheRight={true}
            text={{
              heading: t('cockpit.welcomeLight.functionsPreview.enap.heading'),
              content: (
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.welcomeLight.functionsPreview.enap.content', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            }}
            image={
              <Image
                loading="lazy"
                preview={false}
                src={(require(`../../../../media/welcome-page/Plattform_light-eNAP.svg`) as ImageModule).default}
                alt={t('cockpit.welcomeLight.functionsPreview.enap.imgAlt')}
              />
            }
            customLink={{
              name: t('cockpit.welcomeLight.functionsPreview.anwendungLink'),
              link: '#/egfa',
            }}
          />
          <FunctionPreviewComponent
            textIsOnTheRight={false}
            text={{
              heading: t('cockpit.welcomeLight.functionsPreview.evor.heading'),
              content: (
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.welcomeLight.functionsPreview.evor.content', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            }}
            image={
              <Image
                loading="lazy"
                preview={false}
                src={(require(`../../../../media/welcome-page/Plattform_light-eVoR.svg`) as ImageModule).default}
                alt={t('cockpit.welcomeLight.functionsPreview.evor.imgAlt')}
              />
            }
            customLink={{
              name: t('cockpit.welcomeLight.functionsPreview.anwendungLink'),
              link: '#/evor',
            }}
          />
          <FunctionPreviewComponent
            textIsOnTheRight={true}
            text={{
              heading: t('cockpit.welcomeLight.functionsPreview.evir.heading'),
              content: (
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.welcomeLight.functionsPreview.evir.content', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            }}
            image={
              <Image
                loading="lazy"
                preview={false}
                src={(require(`../../../../media/welcome-page/Plattform_light-eViR.svg`) as ImageModule).default}
                alt={t('cockpit.welcomeLight.functionsPreview.evir.imgAlt')}
              />
            }
            customLink={{
              name: t('cockpit.welcomeLight.functionsPreview.anwendungLink'),
              link: '#/evir',
            }}
          />
          <FunctionPreviewComponent
            textIsOnTheRight={false}
            text={{
              heading: t('cockpit.welcomeLight.functionsPreview.bib.heading'),
              content: (
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.welcomeLight.functionsPreview.bib.content', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            }}
            image={
              <Image
                loading="lazy"
                preview={false}
                src={(require(`../../../../media/welcome-page/Plattform_light-BIB.svg`) as ImageModule).default}
                alt={t('cockpit.welcomeLight.functionsPreview.bib.imgAlt')}
              />
            }
            customLink={{
              name: t('cockpit.welcomeLight.functionsPreview.anwendungLink'),
              link: '#/hilfen',
            }}
          />
          <FunctionPreviewComponent
            textIsOnTheRight={true}
            text={{
              heading: t('cockpit.welcomeLight.functionsPreview.zeit.heading'),
              content: (
                <p
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.welcomeLight.functionsPreview.zeit.content', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
              ),
            }}
            image={
              <Image
                loading="lazy"
                preview={false}
                src={(require(`../../../../media/welcome-page/Plattform_light-zeit.svg`) as ImageModule).default}
                alt={t('cockpit.welcomeLight.functionsPreview.zeit.imgAlt')}
              />
            }
            customLink={{
              name: t('cockpit.welcomeLight.functionsPreview.anwendungLink'),
              link: '#/zeit',
            }}
          />
        </div>
      </div>
    </section>
  );
}
