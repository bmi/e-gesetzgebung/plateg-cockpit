// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './ueber-uns.less';

import { Image, Typography } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

import { FunctionPreviewComponent } from '../../../cockpit/welcome/functions-preview/function-preview/component.react';

export function UeberUnsComponent(): React.ReactElement {
  const { Title } = Typography;

  const { t } = useTranslation();
  return (
    <section style={{ background: '#fff' }}>
      <div className="holder">
        <div className="ueber-uns-component" style={{ marginBottom: '192px' }}>
          <strong className="sub-title">{t('cockpit.welcomeLight.ueberUns.subTitle')}</strong>
          <Title level={2}>
            <span
              dangerouslySetInnerHTML={{
                __html: t('cockpit.welcomeLight.ueberUns.title'),
              }}
            />
          </Title>
          <FunctionPreviewComponent
            textIsOnTheRight={true}
            text={{
              content: <p>{t('cockpit.welcomeLight.ueberUns.content')}</p>,
            }}
            image={
              <Image
                loading="lazy"
                preview={false}
                src={(require(`../../../../media/welcome-page/Grafik-Startseite2.svg`) as ImageModule).default}
                alt={t('cockpit.welcomeLight.ueberUns.imgAlt')}
              />
            }
            customLink={{
              name: t('cockpit.welcomeLight.ueberUns.linkName'),
              link: '#/ueberDasProjekt',
              asLink: true,
            }}
          />
        </div>
      </div>
    </section>
  );
}
