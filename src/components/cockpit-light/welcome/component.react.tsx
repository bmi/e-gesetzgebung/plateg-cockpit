// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../../cockpit/welcome/welcome.less';
import './welcome-light.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule } from '@plateg/theme';

import { HeadComponent } from '../../cockpit/welcome/head/component.react';
import { FunctionsPreviewComponent } from './functions-preview/component.react';
import { UeberUnsComponent } from './ueber-uns/component.react';

export function Welcome(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <div className="welcome-page welcome-light">
      <HeadComponent
        title={{
          title: t('cockpit.welcomeLight.intro.title'),
          subtitle: t('cockpit.welcomeLight.intro.subTitle'),
        }}
        img={{
          img: require(`../../../media/welcome-page/Plattform_light-Headergrafik.svg`) as ImageModule,
          alt: t('cockpit.welcomeLight.intro.alt'),
        }}
      />
      <FunctionsPreviewComponent />
      <UeberUnsComponent />
    </div>
  );
}
