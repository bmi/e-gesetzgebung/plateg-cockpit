// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './ueber-die-massnahme.less';

import { Image, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { StandardContentCol } from '@plateg/theme';

import RechtsskreislaufImage from '../../../media/about-project/Rechtsetzungskreislauf.svg';
import StartSeiteImage from '../../../media/about-project/Startseite.svg';
import { StaticPageWrapper } from '../../general/static-page-wrapper/component.react';

export function UeberDieMassnahmeLight(): React.ReactElement {
  const { t } = useTranslation();

  const parTransKey = ['par2', 'par3', 'par4', 'par5', 'par6', 'par7', 'par8'];

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <div className="ueber-die-massnahme-light">
            <Title level={1}>{t('cockpit.ueberDieMassnahme.title')}</Title>
            <section>
              <Title level={2}>{t('cockpit.ueberDieMassnahme.content.section1.title')}</Title>
              <p>{t('cockpit.ueberDieMassnahme.content.section1.paragraph1')}</p>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('cockpit.ueberDieMassnahme.content.section1.paragraph2'),
                }}
              />
              <ul>
                {(t('cockpit.ueberDieMassnahme.content.section1.vorteile', { returnObjects: true }) as []).map(
                  (vorteil: string, index) => {
                    return <li key={index}>{vorteil}</li>;
                  },
                )}
              </ul>
            </section>
            <section>
              <Title style={{ marginTop: 40 }} level={2}>
                {t('cockpit.ueberDieMassnahme.content.section2.title')}
              </Title>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('cockpit.ueberDieMassnahme.content.section2.paragraph1'),
                }}
              />
              <ul>
                {(t('cockpit.ueberDieMassnahme.content.section2.ziele', { returnObjects: true }) as []).map(
                  (vorteil: string, index) => {
                    return <li key={index}>{vorteil}</li>;
                  },
                )}
              </ul>
              <Title style={{ marginTop: 40 }} level={2}>
                {t('cockpit.ueberDieMassnahme.content.section2.titleImage')}
              </Title>
              <Image
                loading="lazy"
                preview={false}
                src={RechtsskreislaufImage}
                alt={t('cockpit.ueberDieMassnahme.content.section2.imgAlt')}
              />
              <span className="image-title">
                <em>{t('cockpit.ueberDieMassnahme.content.section2.imgTitle')}</em>
              </span>
              {parTransKey.map((item) => (
                <p key={crypto.randomUUID()}>{t(`cockpit.ueberDieMassnahme.content.section2.${item}`)}</p>
              ))}
            </section>
            <section>
              <Title style={{ marginTop: 40 }} level={2}>
                {t('cockpit.ueberDieMassnahme.content.section3.title')}
              </Title>
              <p>{t('cockpit.ueberDieMassnahme.content.section3.paragraph1')}</p>
              <p>{t('cockpit.ueberDieMassnahme.content.section3.paragraph2')}</p>
              <p>{t('cockpit.ueberDieMassnahme.content.section3.paragraph3')}</p>
              <Link className="legal-doc-link" to={`/ueberDasProjekt/legaldoc`}>
                {t('cockpit.ueberDieMassnahme.content.section3.linkText')}
              </Link>
            </section>

            <section>
              <Title level={2}>{t('cockpit.ueberDieMassnahme.content.section6.title')}</Title>
              <Image
                loading="lazy"
                preview={false}
                src={StartSeiteImage}
                alt={t('cockpit.ueberDieMassnahme.content.section6.imgAlt')}
              />
              <p>
                <em>{t('cockpit.ueberDieMassnahme.content.section6.imgTitle')}</em>
              </p>
              <p>{t('cockpit.ueberDieMassnahme.content.section6.paragraph1')}</p>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('cockpit.ueberDieMassnahme.content.section6.paragraph2'),
                }}
              />
            </section>
          </div>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
