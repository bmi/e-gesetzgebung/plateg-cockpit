// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../ueber-die-massnahme/ueber-die-massnahme.less';

import { Image, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { ImageModule, StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../general/static-page-wrapper/component.react';

export function LegalDocLight(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <div className="legal-doc-light">
            <Title level={1}>{t('cockpit.legalDoc.title')}</Title>
            <section>
              <p>{t('cockpit.legalDoc.section1.paragraph1')}</p>
              <Image
                loading="lazy"
                preview={false}
                src={
                  (require(`../../../media/welcome-page/Plattform-Light-04_Verhaeltnis-LegalDocML.svg`) as ImageModule)
                    .default
                }
              />
              <span className="image-title">
                <em>{t('cockpit.legalDoc.section1.imgTitle1')}</em>
              </span>
              <p>{t('cockpit.legalDoc.section1.paragraph2')}</p>
              <p>{t('cockpit.legalDoc.section1.paragraph3')}</p>
              <p>{t('cockpit.legalDoc.section1.paragraph4')}</p>
              <Image
                loading="lazy"
                preview={false}
                src={
                  (
                    require(
                      `../../../media/welcome-page/Plattform-Light-05_Auszeichnung-Inhaltselemente.svg`,
                    ) as ImageModule
                  ).default
                }
              />
              <span className="image-title">
                <em>{t('cockpit.legalDoc.section1.imgTitle2')}</em>
              </span>
              <p>{t('cockpit.legalDoc.section1.paragraph5')}</p>
              <p>{t('cockpit.legalDoc.section1.paragraph6')}</p>
              <p>{t('cockpit.legalDoc.section2.paragraph1')}</p>
            </section>

            <section>
              <Title level={2}>{t('cockpit.legalDoc.section3.title')}</Title>
              {(t('cockpit.legalDoc.section3.steckbrief', { returnObjects: true }) as []).map(
                (item: { title: string; content: string; image: string }) => {
                  return (
                    <>
                      <Image
                        rootClassName="image-steckbrief"
                        loading="lazy"
                        preview={false}
                        src={
                          (require(`../../../media/welcome-page/LegalDoc-Icon-${item.image}.svg`) as ImageModule)
                            .default
                        }
                      />
                      <Title level={3}>{item.title}</Title>
                      <p
                        dangerouslySetInnerHTML={{
                          __html: item.content,
                        }}
                      />
                    </>
                  );
                },
              )}
              <p
                dangerouslySetInnerHTML={{
                  __html: t('cockpit.legalDoc.section3.paragraph1'),
                }}
              />
            </section>
          </div>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
