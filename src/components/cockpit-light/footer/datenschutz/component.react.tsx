// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next/';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../../general/static-page-wrapper/component.react';

export function DatenschutzLight(): React.ReactElement {
  const { t } = useTranslation();

  const generateListEntry = (index: number) => {
    return (
      <li>
        <Title level={4}>{t(`cockpit.footerLight.datenschutz.listTitle6${index}`)}</Title>
        <p className="ant-typography p-no-style">{t(`cockpit.footerLight.datenschutz.listContent6${index}`)}</p>
      </li>
    );
  };

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footerLight.datenschutz.title')}</Title>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle1')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf11')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf12')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf13')}</p>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle11')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf111')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footerLight.datenschutz.block111', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf112')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footerLight.datenschutz.block112', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle12')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf121')}</p>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle13')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf131')}</p>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle2')}</Title>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle21')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf21')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footerLight.datenschutz.list21', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle22')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf22')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footerLight.datenschutz.list22', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf23')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf24')}</p>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle23')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf25')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf26')}</p>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle3')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf31')}</p>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle31')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footerLight.datenschutz.paragraf311', {
                interpolation: { escapeValue: false },
                eGesetzSupportEmail: t('cockpit.eGesetzSupportEmail'),
              }),
            }}
          ></p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf312')}</p>
          <Title level={3}>{t('cockpit.footerLight.datenschutz.subsubtitle32')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf321')}</p>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle4')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf41')}</p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf42')}</p>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle5')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf51')}</p>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle6')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf61')}</p>
          <ul>{[1, 2, 3, 4, 5, 6, 7].map(generateListEntry)}</ul>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle7')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footerLight.datenschutz.paragraf62', {
                interpolation: { escapeValue: false },
              }),
            }}
          ></p>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf63')}</p>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle8')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf71')}</p>
          <Title level={2}>{t('cockpit.footerLight.datenschutz.subtitle9')}</Title>
          <p className="ant-typography p-no-style">{t('cockpit.footerLight.datenschutz.paragraf81')}</p>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
