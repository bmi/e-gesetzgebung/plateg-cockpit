// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { LeichteSpracheWerkzeugeGeneral } from '../../../../general/footer/leichte-sprache/werkzeuge/component.react';

export function LeichteSpracheWerkzeugeLight(): React.ReactElement {
  return <LeichteSpracheWerkzeugeGeneral light />;
}
