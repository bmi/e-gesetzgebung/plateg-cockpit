// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { AppLightEgfa } from '@plateg/egfa';
import { EVorApp } from '@plateg/evor';
import { AppLightZeit } from '@plateg/period-calculator/src/components/zeit-light';
import { EnapMenuIcon, StartseiteMenuIcon, VorbereitungMenuIcon, ZeitMenuIcon } from '@plateg/theme';

import { RoutingItem } from '../cockpit/component.react';
import { VersionenComponent } from '../cockpit/footer/versionen/component.react';
import { BarrierefreiheitErklaerungLight } from './footer/barrierefreiheit/erklaerung/component.react';
import { DatenschutzLight } from './footer/datenschutz/component.react';
import { LeichteSpracheNavLight } from './footer/leichte-sprache/navigationshilfe/component.react';
import { LeichteSpracheWerkzeugeLight } from './footer/leichte-sprache/werkzeuge/component.react';
import { VorstellungPlattFormLight } from './vorstellung/component.react';
import { Welcome } from './welcome/component.react';

export const routingItemsLight: RoutingItem[] = [
  {
    id: 1,
    path: '/cockpit',
    label: 'cockpit.menuItems.cockpit.label' /* 'cockpit' */,
    hint: 'cockpit.header.logo.title' /* 'cockpit' */,
    menuItem: 'cockpit.menuItems.cockpit.menuItem',
    component: <Welcome />,
    moduleName: 'cockpit',
    fullscreen: true,
    icon: <StartseiteMenuIcon />,
  },
  {
    id: 5,
    label: 'cockpit.menuItems.evor.label' /* 'eVoR' */,
    hint: 'cockpit.menuItems.evor.hint' /* 'Vorbereitung von Regelungsentwürfen' */,
    path: '/evor',
    menuItem: 'cockpit.menuItems.evor.menuItem',
    component: <EVorApp />,
    moduleName: 'evor',
    fullscreen: true,
    icon: <VorbereitungMenuIcon />,
  },
  {
    id: 62,
    label: 'cockpit.menuItems.zeit.label' /* 'Zeitplanung' */,
    hint: 'cockpit.menuItems.zeit.hint' /* 'Zeitplanung' */,
    path: '/zeit',
    menuItem: 'cockpit.menuItems.zeit.menuItem',
    component: <AppLightZeit />,
    moduleName: 'zeit',
    fullscreen: true,
    icon: <ZeitMenuIcon />,
  },
  {
    id: 6,
    label: 'cockpit.menuItems.enap.label' /* 'enap' */,
    hint: 'cockpit.menuItems.enap.hint' /* 'Nachhaltigkeitsprüfung' */,
    path: '/egfa',
    menuItem: 'cockpit.menuItems.enap.menuItem',
    component: <AppLightEgfa />,
    moduleName: 'egfa',
    fullscreen: true,
    icon: <EnapMenuIcon />,
  },

  {
    id: 12,
    path: '/barrierefreiheit/erklaerung',
    component: <BarrierefreiheitErklaerungLight />,
    fullscreen: true,
  },
  {
    id: 13,
    path: '/leichteSprache/navigationshilfe',
    component: <LeichteSpracheNavLight />,
    fullscreen: true,
  },
  {
    id: 20,
    path: '/leichteSprache/werkzeuge',
    component: <LeichteSpracheWerkzeugeLight />,
    fullscreen: true,
  },
  {
    id: 59,
    path: '/datenschutz',
    component: <DatenschutzLight />,
    fullscreen: true,
  },
  { id: 20, path: '/vorstellung', component: <VorstellungPlattFormLight />, fullscreen: true },
  { id: 63, path: '/versionen', component: <VersionenComponent light />, fullscreen: true },

  {
    id: 11,
    path: '/cockpit',
    component: <Welcome />,
    fullscreen: true,
  },
];
