// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { CockpitLayout } from '../general/cockpit-layout/component.react';
import { Footer } from '../general/footer/component.react';
import { HeaderGeneral } from '../general/header/component.react';
import { routingItemsGeneral } from '../general/routing-items-general';
import { routingItemsLight } from './routing-items-light';

export interface RoutingItem {
  id: number;
  label?: string;
  hint?: string;
  path: string;
  component: React.ReactElement;
  moduleName?: string;
  fullscreen?: boolean;
  nowrap?: boolean;
}
const mainTitle = 'Plattform - E-Gesetzgebung';
const routingItems: RoutingItem[] = [...routingItemsLight, ...routingItemsGeneral];

export function CockpitLight(): React.ReactElement {
  return (
    <CockpitLayout
      footer={<Footer />}
      header={<HeaderGeneral routingItems={routingItems} fullVersion={false} />}
      mainTitle={mainTitle}
      routingItems={routingItems}
    />
  );
}
