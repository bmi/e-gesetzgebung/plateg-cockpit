// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './vorstellung.less';

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../general/static-page-wrapper/component.react';

export function VorstellungPlattFormLight(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <div className="vorstellung-light">
            <Title level={1}>{t('cockpit.vorstellungLight.title')}</Title>
            <p
              className="vorstellung-notice"
              dangerouslySetInnerHTML={{
                __html: t('cockpit.vorstellungLight.notice', {
                  interpolation: { escapeValue: false },
                }),
              }}
            />
            <Title level={2}>{t('cockpit.vorstellungLight.content.section1.title')}</Title>
            <p
              dangerouslySetInnerHTML={{
                __html: t('cockpit.vorstellungLight.content.section1.paragraph1'),
              }}
            />
            <p
              dangerouslySetInnerHTML={{
                __html: t('cockpit.vorstellungLight.content.section1.paragraph2'),
              }}
            />
            <p
              dangerouslySetInnerHTML={{
                __html: t('cockpit.vorstellungLight.content.section1.paragraph3'),
              }}
            />
            <p
              dangerouslySetInnerHTML={{
                __html: t('cockpit.vorstellungLight.content.section1.paragraph4'),
              }}
            />
            <ul>
              {(t('cockpit.vorstellungLight.content.section1.ziele', { returnObjects: true }) as []).map(
                (ziel: string) => {
                  return (
                    <li
                      dangerouslySetInnerHTML={{
                        __html: ziel,
                      }}
                    />
                  );
                },
              )}
            </ul>
            <p
              className="vorstellung-contact-paragraph"
              dangerouslySetInnerHTML={{
                __html: t('cockpit.vorstellungLight.content.section1.contact', {
                  interpolation: { escapeValue: false },
                  eGesetzSupportEmail: t('cockpit.eGesetzSupportEmail'),
                }),
              }}
            />
          </div>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
