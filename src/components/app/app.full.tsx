// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18n from 'i18next';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Subscription } from 'rxjs';
import { AjaxError } from 'rxjs/ajax';

import { ErrorController, GlobalDI, UserController } from '@plateg/theme';
import { useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { useAppDispatch } from '@plateg/theme/src/components/store';
import { clearUser, setUser } from '@plateg/theme/src/components/store/slices/userSlice';

import { CockpitFull } from '../cockpit/component.react';
import { AppComponent } from './component.react';

export function AppFull(): React.ReactElement {
  const controller = GlobalDI.getOrRegister<UserController>('userControllerTheme', () => new UserController());
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const history = useHistory();
  const dispatch = useAppDispatch();
  const { keycloak } = useKeycloak();
  useEffect(() => {
    let sub: Subscription;
    if (keycloak.authenticated) {
      const stellvertretungActiveId = localStorage.getItem('stellvertretungActiveId') || undefined;
      sub = controller.getUser(stellvertretungActiveId).subscribe({
        next: (data) => {
          if (data.base.id != null) {
            dispatch(setUser(data));
          }
        },
        error: (error: AjaxError) => {
          if (error.status == 400 && error.response.status == 1300) {
            errorCtrl.displayErrorMsg(
              error,
              'Ihnen wurde die Stellvertretung entzogen. Sie können deshalb nicht mehr als Stellvertretung arbeiten.',
            );
            localStorage.removeItem('stellvertretungActiveId');
            history.push('/cockpit');
            setTimeout(() => {
              window.location.reload();
            }, 2000);
          }
          dispatch(clearUser());
        },
      });
    }
    return () => {
      dispatch(clearUser());
      sub?.unsubscribe?.();
    };
  }, [keycloak.authenticated]);

  return (
    <>
      <AppComponent>
        <CockpitFull />
      </AppComponent>
    </>
  );
}
