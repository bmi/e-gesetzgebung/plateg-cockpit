// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { CockpitLight } from '../cockpit-light/component.react';
import { AppComponent } from './component.react';
export function AppLight(): React.ReactElement {
  return (
    <>
      <AppComponent>
        <CockpitLight />
      </AppComponent>
    </>
  );
}
