// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import i18next from 'i18next';
import React, { useEffect, useState } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { useAppSelector } from '@plateg/theme/src/components/store';

import loadMessages from '../../shares/i18n';

export function AppComponent(props: { children: React.ReactElement }): React.ReactElement {
  const [hasLoadedMessages, setHasLoadedMessages] = useState<boolean>(false);
  const appStore = {
    setting: useAppSelector((state) => state.setting),
    neuigkeiten: useAppSelector((state) => state.neuigkeiten),
    user: useAppSelector((state) => state.user),
  };
  let ressortLanguageFile: string;

  switch (appStore.user?.user?.dto.ressort?.kurzbezeichnung) {
    case 'BT': {
      ressortLanguageFile = 'bt';
      break;
    }
    case 'BR': {
      ressortLanguageFile = 'br';
      break;
    }
    default: {
      ressortLanguageFile = 'de';
      break;
    }
  }

  useEffect(() => {
    if (ressortLanguageFile === 'bt' || ressortLanguageFile === 'br') {
      setHasLoadedMessages(false);
    }
    loadMessages(ressortLanguageFile === 'bt' || ressortLanguageFile === 'br' ? ressortLanguageFile : undefined)
      .then(() => {
        i18next
          .changeLanguage(i18next.language)
          .then(() => {
            setHasLoadedMessages(true);
          })
          .catch(() => {
            setHasLoadedMessages(false);
          });
      })
      .catch(() => {
        setHasLoadedMessages(false);
      });
  }, [ressortLanguageFile]);

  return (
    <>
      {hasLoadedMessages && (
        <Switch>
          <Route exact path="/">
            <Route render={() => <Redirect to="/cockpit" />} />
          </Route>
          <Route path="/:bereich">{props.children}</Route>
        </Switch>
      )}
    </>
  );
}
