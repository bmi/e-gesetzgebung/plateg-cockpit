// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './header.less';

import { Button, Layout, MenuRef } from 'antd';
import React, { useEffect, useRef, useState } from 'react';

import { CloseOutlined, MenuOutlined } from '@plateg/theme';

import { AdditionalButtons } from '../../cockpit/header/subcomponents/additional-buttons/component.react';
import { MenuItemBar } from '../../cockpit/nav/component.react';
import { RoutingItem } from '../cockpit-layout/component.react';
import { CockpitTooltip } from './subcomponents/cockpit-tooltip/component.react';
import { FeedbackModal } from './subcomponents/feedback-modal/component.react';
import { LogoutTimerComponent } from './subcomponents/logout-timer/component.react';
import { NeuigkeitenInfo } from './subcomponents/neuigkeiten-info/component.react';
import { NewsletterModal } from './subcomponents/newsletter-modal/component.react';
import { SearchModal } from './subcomponents/search-modal/component.react';
import { StellvertretungInfo } from './subcomponents/stellvertretung-info/component.react';

interface Props {
  selectedItemId?: number;
  fullVersion: boolean;
  signedIn?: boolean;
  children?: React.ReactElement;
  stellvertretung?: string;
  routingItems: RoutingItem[];
}

export function HeaderGeneral(props: Props): React.ReactElement {
  const [isFeedbackModalVisible, setIsFeedbackModalVisible] = useState<boolean>(false);
  const [isNewseletterModalVisible, setIsNewseletterModalVisible] = useState<boolean>(false);
  const [isCollapsed, setIsCollapsed] = useState(false);
  const [isSearchModalVisible, setIsSearchModalVisible] = useState<boolean>(false);
  const mainNavRef = useRef<MenuRef>(null);
  const modalNewseletterRef = useRef<HTMLAnchorElement>(null);
  const btnNewsletterRef = useRef<HTMLAnchorElement>(null);
  const headerLayout = props.stellvertretung ? 'stellvertretung' : 'standard';
  const menuCollapseBtnRef = useRef<HTMLButtonElement>(null);
  const lastMenuBtnRef = useRef<HTMLButtonElement>(null);
  useEffect(() => {
    if (!isFeedbackModalVisible) {
      menuCollapseBtnRef.current?.focus();
    }
  }, [isFeedbackModalVisible]);
  return (
    <Layout.Header className={`cockpit-header-${headerLayout}`}>
      <Button
        id="menu-item-bar-collapse-btn"
        type="primary"
        aria-expanded={isCollapsed}
        onClick={() => setIsCollapsed(!isCollapsed)}
        aria-haspopup={true}
        {...{ 'aria-label': isCollapsed ? 'Menü schließen' : 'Menü öffnen' }}
        ref={menuCollapseBtnRef}
        onKeyDown={(e) => {
          if (e.code === 'Tab' && isCollapsed) {
            if (e.shiftKey && props.fullVersion) {
              setTimeout(() => {
                lastMenuBtnRef.current?.focus();
              }, 1);
              e.preventDefault();
            } else {
              setTimeout(() => {
                mainNavRef.current?.focus();
              }, 1);
              e.preventDefault();
            }
          }
        }}
      >
        {isCollapsed ? <CloseOutlined /> : <MenuOutlined />}
      </Button>
      <MenuItemBar
        selectedItemId={props.selectedItemId}
        items={props.routingItems}
        isCollapsed={isCollapsed}
        handleOnClose={() => setIsCollapsed(!isCollapsed)}
        isSearchModalVisible={isSearchModalVisible}
        isFeedbackModalVisible={isFeedbackModalVisible}
        setIsFeedbacklVisible={setIsFeedbackModalVisible}
        setIsNewseletterModalVisible={setIsNewseletterModalVisible}
        setIsSearchModalVisible={setIsSearchModalVisible}
        modalNewseletterRef={modalNewseletterRef}
        fullVersion={props.fullVersion}
        signedIn={props.signedIn}
        btnNewsletterRef={btnNewsletterRef}
        mainNavRef={mainNavRef}
        menuCollapseBtnRef={menuCollapseBtnRef}
        lastMenuBtnRef={lastMenuBtnRef}
      />
      <CockpitTooltip selectedItemId={props.selectedItemId} fullVersion={props.fullVersion} />
      <FeedbackModal isModalVisible={isFeedbackModalVisible} setIsModalVisible={setIsFeedbackModalVisible} />
      <NewsletterModal
        isModalVisible={isNewseletterModalVisible}
        setIsModalVisible={setIsNewseletterModalVisible}
        modalRef={modalNewseletterRef}
        btnNewsletterRef={btnNewsletterRef}
      />
      <SearchModal isModalVisible={isSearchModalVisible} setIsModalVisible={setIsSearchModalVisible} />

      <div className="login-div">
        <AdditionalButtons
          isSearchModalVisible={isSearchModalVisible}
          isFeedbackModalVisible={isFeedbackModalVisible}
          setIsFeedbacklVisible={setIsFeedbackModalVisible}
          setIsNewseletterModalVisible={setIsNewseletterModalVisible}
          setIsSearchModalVisible={setIsSearchModalVisible}
          modalNewseletterRef={modalNewseletterRef}
          fullVersion={props.fullVersion}
          signedIn={props.signedIn}
          btnNewsletterRef={btnNewsletterRef}
        />
        {props.signedIn ? (
          <>
            <LogoutTimerComponent />
            <NeuigkeitenInfo />
          </>
        ) : null}
        <StellvertretungInfo stellvertretung={props.stellvertretung} />
        {props.children}
      </div>
    </Layout.Header>
  );
}
