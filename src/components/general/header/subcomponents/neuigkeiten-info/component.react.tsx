// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './neuigkeiten-info.less';

import React from 'react';
import { useTranslation } from 'react-i18next';

import { BellIcon } from '@plateg/theme/src/components/icons/BellIcon';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import { NeuigkeitenState, setNeuigkeiten } from '@plateg/theme/src/components/store/slices/neuigkeitenSlice';

import { NeuigkeitenDrawer } from '../../../../cockpit/dashboard/neuigkeiten-drawer/component.react';

export function NeuigkeitenInfo(): React.ReactElement {
  const dispatch = useAppDispatch();
  const appStore = useAppSelector((state) => state.neuigkeiten);
  const { t } = useTranslation();

  return (
    <NeuigkeitenDrawer
      id="head-neuigkeiten-drawer"
      buttonAriaLabel={t(`cockpit.header.ungeleseneNeuigkeiten`, {
        ungeleseneNeuigkeiten: appStore.ungeleseneNeuigkeiten,
      })}
      buttonText={
        <>
          <span>
            <BellIcon />
          </span>

          {appStore.ungeleseneNeuigkeiten > 0 && <span>{appStore.ungeleseneNeuigkeiten}</span>}
        </>
      }
      buttonId="head-neuigkeiten-drawer-btn"
      setNeuigkeitenData={(data: NeuigkeitenState) => dispatch(setNeuigkeiten(data))}
      getContainer=".ant-layout-has-sider"
    />
  );
}
