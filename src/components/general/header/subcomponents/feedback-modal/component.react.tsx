// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { EmailOutlined, ModalWrapper } from '@plateg/theme';
import './contact-modal.less';

interface FeedbackModalProps {
  isModalVisible: boolean;
  setIsModalVisible: (value: boolean) => void;
}

export function FeedbackModal(props: FeedbackModalProps): React.ReactElement {
  const { t } = useTranslation();
  const modalRef = useRef<HTMLAnchorElement>(null);
  const onCancelHandle = () => {
    props.setIsModalVisible(false);
  };
  let timer: NodeJS.Timeout;
  useEffect(() => {
    if (props.isModalVisible) {
      timer = setTimeout(() => {
        modalRef.current?.focus();
      }, 200);
    }
    return () => clearTimeout(timer);
  }, [props.isModalVisible]);

  return (
    <ModalWrapper
      className="contact-modal"
      title={<h3>{t('cockpit.login.contactButton')}</h3>}
      open={props.isModalVisible}
      onCancel={onCancelHandle}
      footer={[
        <Button id="contact-ok-button" key="contact-ok-button" type="primary" onClick={onCancelHandle}>
          Ok
        </Button>,
      ]}
    >
      <p
        dangerouslySetInnerHTML={{
          __html: t('cockpit.contactModal.text', {
            interpolation: { escapeValue: false },
          }),
        }}
      ></p>
      <p>
        <EmailOutlined />{' '}
        <a
          id="feedback-modal-mailto-link"
          ref={modalRef}
          href="mailto:support.egesetzgebung@bmi.bund.de?subject=Feedback%20zur%20E-Gesetzgebung"
        >
          {t('cockpit.contactModal.mail')}
        </a>
      </p>
    </ModalWrapper>
  );
}
