// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { FC, useEffect, useRef, useState } from 'react';
import { Button, Input, InputRef, Typography } from 'antd';
import { useTranslation } from 'react-i18next';

const { Search } = Input;
const { Text } = Typography;

type Props = {
  isModalVisible: boolean;
  searchValueParent: string;
  handleSearch: (value: string) => void;
};

export const SearchComponent: FC<Props> = (props): React.ReactElement => {
  const { isModalVisible, searchValueParent, handleSearch } = props;

  const searchRef = useRef<InputRef>(null);
  const { t } = useTranslation();
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    setSearchValue(searchValueParent);
  }, [searchValueParent]);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (isModalVisible) {
        searchRef?.current?.focus();
      }
    }, 200);
    return () => clearTimeout(timer);
  }, [isModalVisible]);

  const handleResetSearchWord = () => {
    setSearchValue('');
    searchRef?.current?.focus();
  };

  return (
    <Search
      role={'searchbox'}
      onChange={(e) => setSearchValue(e.target.value)}
      value={searchValue}
      ref={searchRef}
      className="search-input-container"
      onSearch={handleSearch}
      enterButton={t('cockpit.searchModal.searchInput.submitBtn')}
      size="large"
      placeholder={t('cockpit.searchModal.searchInput.inputPlaceholder')}
      suffix={
        searchValue.trim() ? (
          <Button className="suffix-reset-search_world" type="text" onClick={handleResetSearchWord}>
            <Text>{t('cockpit.searchModal.searchInput.resetSearchWord')}</Text>
          </Button>
        ) : (
          <></>
        )
      }
    />
  );
};
