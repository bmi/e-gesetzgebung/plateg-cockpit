// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './search-modal.less';

import { Image, Select, Tabs, Typography } from 'antd';
import React, { FC, useEffect, useLayoutEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
  DoSucheRequest,
  SucheAnwendungType,
  SucheArbeitshilfeType,
  SucheControllerApi,
  SucheResponseDTO,
  SuchergebnisAnwendungDTO,
  SucheSortierungType,
} from '@plateg/rest-api';
import { GlobalDI, ModalWrapper } from '@plateg/theme';

import arrowLeftIcon from '../../../../../media/search/arrowLeftIcon.svg';
import arrowRightIcon from '../../../../../media/search/arrowRightIcon.svg';
import noResultsGraphic from '../../../../../media/search/noResultsGraphic.svg';
import searchStartGraphic from '../../../../../media/search/searchStartGraphic.svg';
import { SearchComponent } from './search-component/component.react';
import { SearchResultItem } from './search-result-item/component.react';

import type { Subscription } from 'rxjs';

const { Title, Text } = Typography;
const { TabPane } = Tabs;

interface Props {
  isModalVisible: boolean;
  setIsModalVisible: (value: boolean) => void;
}

type Sub = Omit<Subscription, '_parentage'>;

export const SearchModal: FC<Props> = (props) => {
  const searchCtrl = GlobalDI.getOrRegister('sucheController', () => new SucheControllerApi());
  const { isModalVisible, setIsModalVisible } = props;
  let sub: Sub;

  const { t } = useTranslation();
  const [activeSearch, setActiveSearch] = useState(false);
  const [searchWord, setSearchWord] = useState<string | undefined>(undefined);
  const [searchResults, setSearchResults] = useState<SucheResponseDTO | undefined>();
  const [searchValueChild, setSearchValueChild] = useState('');
  const [sortMode, setSortMode] = useState(SucheSortierungType.Alphabetical);
  const [pageCounterState, setPageCounterState] = useState(1);
  const [isModalVisibleChild, setIsModalVisibleChild] = useState(isModalVisible);
  const [filterBibValue, setFilterBibValue] = useState(SucheArbeitshilfeType.Bib);
  const [applicationType, setApplicationType] = useState(SucheAnwendungType.Alle);

  const pageCounterRef = useRef<number>();
  const applicationTypeRef = useRef<SucheAnwendungType>();

  useEffect(() => {
    setIsModalVisibleChild(isModalVisible);
  }, [isModalVisible]);

  useEffect(() => {
    if (searchWord) {
      fetchData(searchWord, sortMode, 1, SucheAnwendungType.Alle);
      applicationTypeRef.current = SucheAnwendungType.Alle;
      setApplicationType(SucheAnwendungType.Alle);
      pageCounterRef.current = 1;
      setPageCounterState(1);
    }
  }, [searchWord, sortMode]);

  useEffect(() => {
    if (searchWord) {
      fetchData(searchWord, sortMode, 1, SucheAnwendungType.Alle);

      pageCounterRef.current = 1;
      setPageCounterState(1);
    }
  }, [filterBibValue]);

  useEffect(() => {
    if (searchWord && applicationTypeRef.current !== applicationType) {
      fetchData(searchWord, sortMode, 1);
      pageCounterRef.current = 1;
      setPageCounterState(1);
      applicationTypeRef.current = applicationType;
    }
  }, [applicationType]);

  useEffect(() => {
    if (searchWord && pageCounterRef.current !== pageCounterState) {
      fetchData(searchWord, sortMode, pageCounterState);
      pageCounterRef.current = pageCounterState;
    }
  }, [pageCounterState]);

  const sortOptions = [
    { value: SucheSortierungType.Alphabetical, label: t('cockpit.searchModal.selectSort.alphabetical') },
    { value: SucheSortierungType.Rank, label: t('cockpit.searchModal.selectSort.rank') },
  ];

  const filterOptionsBib = [
    { value: SucheArbeitshilfeType.Bib, label: t('cockpit.searchModal.selectBibFilter.bib') },
    { value: SucheArbeitshilfeType.Evir, label: t('cockpit.searchModal.selectBibFilter.evir') },
  ];

  const searchActiveClass = activeSearch ? 'active-search' : 'initial-loading';

  useLayoutEffect(() => {
    return () => {
      sub?.unsubscribe();
    };
  }, []);

  const fetchData = (
    value: string,
    sort = SucheSortierungType.Alphabetical,
    seitennummer = 1,
    anwendungType = applicationType,
    arbeitshilfeType = filterBibValue,
  ) => {
    const suchObj: DoSucheRequest = {
      suchString: value.split(' '),
      seitennummer,
      sortierung: sort,
      sucheFilterDTO: { anwendungType, arbeitshilfeType },
    };

    const getResultsToUpdate = (oldResults: SucheResponseDTO, newResults: SucheResponseDTO) => {
      switch (newResults.filter.anwendungType) {
        case SucheAnwendungType.Arbeitshilfen:
          oldResults.suchergebnisArbeitshilfen = newResults.suchergebnisArbeitshilfen;
          break;
        case SucheAnwendungType.DokumenteUndDateien:
          oldResults.suchergebnisDokumenteUndDateien = newResults.suchergebnisDokumenteUndDateien;
          break;
        case SucheAnwendungType.HaeufigeFragen:
          oldResults.suchergebnisHaeufigeFragen = newResults.suchergebnisHaeufigeFragen;
          break;
      }
    };

    sub = searchCtrl.doSuche(suchObj).subscribe({
      next: (data) => {
        if (data.filter.anwendungType !== SucheAnwendungType.Alle || data.suchergebnisAlle.anzahlSuchergebnisse === 0) {
          if (searchResults) {
            const copyResults = { ...searchResults };
            getResultsToUpdate(copyResults, data);
            setSearchResults(copyResults);
          }
        } else {
          setSearchResults(data);
        }
      },
    });
  };

  const handleSearch = (value: string) => {
    const valueTrimmed = value.trim();
    if (valueTrimmed.length > 0) {
      if (!activeSearch) {
        setActiveSearch(true);
      }
      setSearchWord(valueTrimmed);
      setSearchValueChild(valueTrimmed);
    }
  };

  const displayPageButtons = (pageNumber: number) => {
    if (searchResults) {
      const buttonsArray = [];
      for (let i = 0; i < pageNumber; i++) {
        const pageNumberLoc = i + 1;
        const classNameBtn = `btn-page_number ${pageNumberLoc === pageCounterState ? 'selected' : ''}`;
        buttonsArray.push(
          <div
            tabIndex={0}
            className={classNameBtn}
            onClick={() => {
              setPageCounterState(pageNumberLoc);
            }}
            key={i}
          >
            <Text>{pageNumberLoc}</Text>
          </div>,
        );
      }
      return buttonsArray;
    }
    return <></>;
  };

  const displayFilterAppBib = (
    <div className="div-filter_bib">
      <label htmlFor="filter-bib" className="label-select">
        {t('cockpit.searchModal.selectBibFilter.label')}
      </label>
      <br />
      <Select
        value={filterBibValue}
        onChange={(filterVal) => setFilterBibValue(filterVal)}
        options={filterOptionsBib}
        id="filter-bib"
      ></Select>
    </div>
  );

  const displayUpToDataNote = (
    <div className="div-upto_date_note">
      <Text className="title"> {t('cockpit.searchModal.upToDateNote.title')}</Text>
      <span
        dangerouslySetInnerHTML={{
          __html: t('cockpit.searchModal.upToDateNote.text', {
            emailLink: `<a href="mailto:support.egesetzgebung@bmi.bund.de?subject=Feedback%20zur%20Aktualität%20bei%20Arbeitshilfen">
              per E-Mail</a>`,
          }),
        }}
      ></span>
    </div>
  );

  const displayPagination = (pageNumber: number, pageCounter: number) => {
    return (
      <div className="page_number-content">
        <Image
          loading="lazy"
          tabIndex={0}
          onClick={() => {
            if (pageCounter > 1) {
              setPageCounterState((pageNr) => pageNr - 1);
            }
          }}
          className="image-arrow"
          preview={false}
          src={arrowLeftIcon}
        />
        {displayPageButtons(pageNumber)}
        <Image
          loading="lazy"
          tabIndex={0}
          className="image-arrow"
          onClick={() => {
            if (pageCounter < pageNumber) {
              setPageCounterState((pageNr) => pageNr + 1);
            }
          }}
          preview={false}
          src={arrowRightIcon}
        />
      </div>
    );
  };

  const displayTabPaneItem = (tab: string, key?: SucheAnwendungType, data?: SuchergebnisAnwendungDTO) => {
    if (data && key) {
      return (
        <TabPane tab={`${tab} (${data.anzahlSuchergebnisse})`} key={key}>
          <div className="search-tab-content">
            {data.anwendungType === SucheAnwendungType.Arbeitshilfen && displayUpToDataNote}
            <div className="header-content">
              <Text className="label-result">
                {t('cockpit.searchModal.results', { resNumber: data.suchergebnis.length })}
              </Text>
              <div className="div-select-holder">
                {data.anwendungType === SucheAnwendungType.Arbeitshilfen && displayFilterAppBib}
                <div>
                  <label htmlFor="select-sort" className="label-select">
                    {t('cockpit.searchModal.selectSort.label')}
                  </label>
                  <br />
                  <Select
                    value={sortMode}
                    id="select-sort"
                    options={sortOptions}
                    onChange={(sort) => setSortMode(sort)}
                  />
                </div>
              </div>
            </div>
            <div className="body-content">
              {!data.anzahlSuchergebnisse && searchWord && (
                <div className="content-no_result">
                  <div className="image">
                    <Image loading="lazy" preview={false} src={noResultsGraphic} />
                  </div>
                  <Text className="text-1">{t('cockpit.searchModal.noResult.text1', { searchWord })}</Text>
                  <br />
                  <Text className="text-2">{t('cockpit.searchModal.noResult.text2')}</Text>
                </div>
              )}
            </div>
            {data.suchergebnis &&
              data.suchergebnis.map((itemData) => {
                return (
                  <SearchResultItem
                    applicationType={applicationType}
                    key={`${itemData.id}-
              ${'itemData.anwendungType'}
              -${itemData.arbeitshilfeType || ''}-item`}
                    data={itemData}
                  />
                );
              })}
          </div>
          {data.anzahlSuchergebnisse ? displayPagination(data.seitenanzahl, data.seitennummer) : <></>}
        </TabPane>
      );
    } else {
      return <></>;
    }
  };

  const activeSearchDisplay = (
    <>
      <h3 className="results-title">{t('cockpit.searchModal.searchResults')}</h3>
      <Tabs
        onChange={(el) => setApplicationType(el as SucheAnwendungType)}
        role={'tab'}
        className="search-tabs-main"
        size="small"
        defaultActiveKey="1"
      >
        {displayTabPaneItem(
          t('cockpit.searchModal.tabLabels.all'),
          searchResults?.suchergebnisAlle.anwendungType,
          searchResults?.suchergebnisAlle,
        )}
        {displayTabPaneItem(
          t('cockpit.searchModal.tabLabels.workingAids'),
          searchResults?.suchergebnisArbeitshilfen.anwendungType,
          searchResults?.suchergebnisArbeitshilfen,
        )}
        {displayTabPaneItem(
          t('cockpit.searchModal.tabLabels.faq'),
          searchResults?.suchergebnisHaeufigeFragen.anwendungType,
          searchResults?.suchergebnisHaeufigeFragen,
        )}
        {displayTabPaneItem(
          t('cockpit.searchModal.tabLabels.docsAndData'),
          searchResults?.suchergebnisDokumenteUndDateien.anwendungType,
          searchResults?.suchergebnisDokumenteUndDateien,
        )}
      </Tabs>
    </>
  );

  const handleModalClose = () => {
    setIsModalVisible(false);
    setActiveSearch(false);
    setSearchValueChild('');
    setSearchWord('');
    setPageCounterState(1);
    setSortMode(SucheSortierungType.Alphabetical);
  };

  return (
    <ModalWrapper
      className="search-modal"
      styles={{ body: { minHeight: '100vh' } }}
      onCancel={handleModalClose}
      title={false}
      open={isModalVisible}
      footer={null}
    >
      <div className={`search-modal-content_${searchActiveClass}`}>
        <div className="inner-content">
          <Title className="search-title">{t('cockpit.searchModal.title')}</Title>
          <div className="info-text">
            <Text>{t('cockpit.searchModal.text1')}</Text>
            <br />
            <Text>{t('cockpit.searchModal.text2')}</Text>
          </div>
          <SearchComponent
            handleSearch={handleSearch}
            isModalVisible={isModalVisibleChild}
            searchValueParent={searchValueChild}
          />
          {!activeSearch && (
            <div className="image">
              <Image loading="lazy" preview={false} src={searchStartGraphic} />
            </div>
          )}
          {activeSearch && activeSearchDisplay}
        </div>
      </div>
    </ModalWrapper>
  );
};
