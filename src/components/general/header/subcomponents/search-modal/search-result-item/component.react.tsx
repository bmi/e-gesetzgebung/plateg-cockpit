// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../search-modal.less';

import { Typography } from 'antd';
import { format } from 'date-fns';
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';

import { SucheAnwendungType, SucheArbeitshilfeType, SuchergebnisDTO } from '@plateg/rest-api';

const { Text } = Typography;

interface Props {
  data: SuchergebnisDTO;
  applicationType: SucheAnwendungType;
}

export const SearchResultItem: FC<Props> = (props) => {
  const { t } = useTranslation();
  const { title, description, rank, arbeitshilfeType, url, publishers, publicationDate } = props.data;
  const maxDescriptionLength = 270;

  const getDescriptionCutWordEnd = () => {
    if (description.length > maxDescriptionLength) {
      for (let i = maxDescriptionLength; i > 0; i--) {
        if (description[i] === ' ') {
          return `${description.substring(0, i)}...`;
        }
      }
    }
    return description;
  };
  const getAnwendungsType = () => {
    if (arbeitshilfeType) {
      if (arbeitshilfeType === SucheArbeitshilfeType.Evir) {
        return t('cockpit.searchModal.selectBibFilter.evir');
      }
      return arbeitshilfeType;
    }
    switch (props.applicationType) {
      case SucheAnwendungType.HaeufigeFragen:
        return t('cockpit.searchModal.tabLabels.faq');
      case SucheAnwendungType.DokumenteUndDateien:
        return t('cockpit.searchModal.tabLabels.docsAndData');
      default:
        return;
    }
  };
  const displayTitle =
    arbeitshilfeType === SucheArbeitshilfeType.Bib ? (
      <a href={url} target="_blank" className="title link">
        {title}
      </a>
    ) : (
      <Text className="title">{title}</Text>
    );

  const displayFooter =
    arbeitshilfeType === SucheArbeitshilfeType.Bib && publishers && publicationDate
      ? `Herausgegeben von: ${publishers[0]} · Aktualisierung 
      ${format(new Date(publicationDate), 'MM.dd.yyyy')}`
      : `${rank} ${t('cockpit.searchModal.searchResultItem.referencesFound')}`;

  return (
    <div className="search-result-item">
      <div className="breadcrumb">{getAnwendungsType()}</div>
      {displayTitle}
      <Text className="description">{getDescriptionCutWordEnd()}</Text>
      <Text className="breadcrumb">{displayFooter}</Text>
    </div>
  );
};
