// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { EmailOutlined, ModalWrapper } from '@plateg/theme';

interface NewsletterModalProps {
  isModalVisible: boolean;
  setIsModalVisible: (value: boolean) => void;
  modalRef: React.RefObject<HTMLAnchorElement>;
  btnNewsletterRef: React.RefObject<HTMLAnchorElement>;
}

export function NewsletterModal(props: NewsletterModalProps): React.ReactElement {
  const { t } = useTranslation();
  const onCancelHandle = () => {
    props.setIsModalVisible(false);
    setTimeout(() => props.btnNewsletterRef.current?.focus(), 200);
  };
  return (
    <ModalWrapper
      title={<h3>{t('cockpit.newsletterModal.title')}</h3>}
      open={props.isModalVisible}
      onCancel={onCancelHandle}
      footer={[
        <Button id="newsletter-ok-button" key="newsletter-ok-button" type="primary" onClick={onCancelHandle}>
          Ok
        </Button>,
      ]}
    >
      <p>{t('cockpit.newsletterModal.text')}</p>
      <p>
        <EmailOutlined />
        &nbsp;
        <a
          id="newsletter-modal-mailto-link"
          ref={props.modalRef}
          href={`mailto:support.egesetzgebung@bmi.bund.de?subject=${t('cockpit.newsletterModal.subject')}`}
        >
          {t('cockpit.newsletterModal.mail')}
        </a>
      </p>
    </ModalWrapper>
  );
}
