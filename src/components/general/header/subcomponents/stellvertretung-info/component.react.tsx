// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React from 'react';

import { ExclamationMarkRed } from '@plateg/theme';
interface StellvertretungInfoProps {
  stellvertretung?: string;
}

export function StellvertretungInfo(props: StellvertretungInfoProps): React.ReactElement {
  return props.stellvertretung ? (
    <span className="additional-buttons stellvertretung-title">
      <ExclamationMarkRed style={{}} /> {`Stellvertretung für ${props.stellvertretung}`}
    </span>
  ) : (
    <></>
  );
}
