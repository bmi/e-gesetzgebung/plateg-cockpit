// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './logout-timer.less';

import { Button, Menu } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Subscription } from 'rxjs';

import { UserControllerApi } from '@plateg/rest-api';
import { displayMessage, FocusController, GlobalDI, ModalWrapper } from '@plateg/theme';
import { logoutAction, useKeycloak } from '@plateg/theme/src/components/auth/KeycloakAuth';
import { DirectionDownOutlined } from '@plateg/theme/src/components/icons/DirectionDownOutlined';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import { clearKcToken } from '@plateg/theme/src/components/store/slices/kcTokenSlice';
import { clearUser, setUser } from '@plateg/theme/src/components/store/slices/userSlice';
import { LastRestApiCallController } from '@plateg/theme/src/controllers/lastRestApiCallController';

import { LoginTimeoutController, LOGOUT_LAST_MINUTES } from './controller';

export function LogoutTimerComponent(): React.ReactElement {
  const { t } = useTranslation();
  const [isMenuVisible, setIsMenuVisible] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [hasModalShown, setHasModalShown] = useState(false);
  const [timeLeftLabel, setTimeLeftLabel] = useState('');
  const [timeLeft, setTimeLeft] = useState<number>();
  const [lastActivityTime, setLastActivityTime] = useState<string>();
  const logoutTimerRef = useRef<HTMLAnchorElement>(null);
  const linkResetTimerRef: React.MutableRefObject<HTMLAnchorElement | null>[] = [useRef<HTMLAnchorElement>(null)];
  const logoutTimerWrapperRef = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const { keycloak } = useKeycloak();
  const focusCtrl = GlobalDI.getOrRegister(
    'logoutTimerFocusController',
    () => new FocusController(setIsMenuVisible, linkResetTimerRef, logoutTimerRef),
  );
  const lastRestApiCallController = GlobalDI.getOrRegister(
    'lastRestApiCallController',
    () => new LastRestApiCallController(),
  );
  const loginTimeoutCtrl = GlobalDI.getOrRegister('loginTimeoutController', () => new LoginTimeoutController());
  const userController = GlobalDI.getOrRegister<UserControllerApi>('userController', () => new UserControllerApi());
  let timeInterval: NodeJS.Timer;
  let getUserSubs: Subscription;
  let localstorageChecker: NodeJS.Timer;
  const appStore = {
    appSetting: useAppSelector((state) => state.appSetting),
  };
  let idleTime = 0;
  if (appStore.appSetting.appSetting?.sessionIdle) {
    idleTime = appStore.appSetting.appSetting.sessionIdle * 60 * 1000;
  }
  useEffect(() => {
    // Set initial last activity time
    loginTimeoutCtrl.saveLastActivityTimeToLocalStorage(new Date().toISOString());
    // Listen to local store changes for anoter tabs
    window.addEventListener('storage', (e) => {
      if (e.key == 'logoutTimer') {
        const localstorageLastTimeActivity = loginTimeoutCtrl.getLastActivityTimeFromLocalStorage();

        setLastActivityTime(localstorageLastTimeActivity);
        if (keycloak.authenticated && !localstorageLastTimeActivity) {
          dispatch(clearUser());
          clearInterval(timeInterval);
          keycloak
            .logout()
            .then(() => {
              dispatch(clearKcToken());
              localStorage.removeItem('logoutTimer');
              localStorage.removeItem('stellvertretungActiveId');
            })
            .catch((error) => {
              console.log(error);
            });
        }
      }
    });
    // Subscribe to last user activity for current tab
    const lastRestApiCallSub = lastRestApiCallController.subscribeLastRestApiCall().subscribe((time) => {
      loginTimeoutCtrl.saveLastActivityTimeToLocalStorage(time);
      setLastActivityTime(time);
    });
    return () => {
      lastRestApiCallSub.unsubscribe();
      if (getUserSubs) {
        getUserSubs.unsubscribe();
      }
      localStorage.removeItem('logoutTimer');
      localStorage.removeItem('stellvertretungActiveId');
      clearInterval(localstorageChecker);
    };
  }, []);

  useEffect(() => {
    setIsModalVisible(false);
    setHasModalShown(false);
    if (lastActivityTime && idleTime) {
      // Start timer to counting time what is left to automatic logout
      timeInterval = setInterval(() => {
        setTimeLeft(loginTimeoutCtrl.getLeftTime(lastActivityTime, idleTime));
      }, 1000);
    }

    return () => {
      clearInterval(timeInterval);
    };
  }, [lastActivityTime]);

  useEffect(() => {
    // Show modal with hinweis what user will be automatically logged out
    if (timeLeft && timeLeft < LOGOUT_LAST_MINUTES && !hasModalShown) {
      setIsModalVisible(true);
      setHasModalShown(true);
    }
    // Update label with correct time
    setTimeLeftLabel(loginTimeoutCtrl.formatTime(timeLeft));
    // Do logout on last second to prevent logout on inital page loading
    // timeLeft === 0 happens only if PC fell asleep and after waking up timer has already ended -> bug with empty timer is appears
    if ((timeLeft && timeLeft > 0 && timeLeft < 1000) || timeLeft === 0) {
      loginTimeoutCtrl.setStatusLogoutByTimeout(true);
      dispatch(clearUser());
      clearInterval(timeInterval);
      keycloak
        .logout()
        .then(() => {
          dispatch(clearKcToken());
          localStorage.removeItem('logoutTimer');
          localStorage.removeItem('stellvertretungActiveId');
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [timeLeft]);

  // This section is about web-accessibility and correct handling focus in dropdown
  useEffect(() => {
    if (isMenuVisible) {
      window.addEventListener('wheel', () => setIsMenuVisible(false));
      document.addEventListener('click', (event: MouseEvent) => handleClickOutside(event), true);
    }
  }, [isMenuVisible]);
  const handleClickOutside = (event: MouseEvent) => {
    if (logoutTimerWrapperRef.current && !logoutTimerWrapperRef.current?.contains(event.target as Node)) {
      setIsMenuVisible(false);
    }
  };
  const handleKeyDown = (visible: boolean, event: React.KeyboardEvent) => {
    if (visible) {
      focusCtrl.setFocus(event);
    }
  };

  // Reset time for idle. Here is used call to BE just for simulating user activity
  const resetTimerIdle = () => {
    setHasModalShown(false);
    if (getUserSubs) {
      getUserSubs.unsubscribe();
    }
    getUserSubs = userController.getUser().subscribe({
      next: (data) => {
        displayMessage(t('cockpit.logoutTimer.resetSuccessMsg'), 'success');
        dispatch(setUser(data));
      },
      error: () => {
        dispatch(clearUser());
      },
    });
  };

  return (
    <div
      className="logout-timer-wrapper"
      id="timertext"
      onKeyDown={(event) => handleKeyDown(isMenuVisible, event)}
      ref={logoutTimerWrapperRef}
      aria-label={t('cockpit.logoutTimer.mainLabel', { timeLeftLabel: timeLeftLabel })}
    >
      <Button
        ref={logoutTimerRef}
        className={`logout-timer-button ${isMenuVisible ? 'active' : ''}`}
        aria-label="LogoutTimer"
        aria-expanded={isMenuVisible}
        aria-describedby="timertext"
        aria-haspopup={true}
        onClick={() => {
          setIsMenuVisible(!isMenuVisible);
        }}
      >
        <div className="logout-timer">{t('cockpit.logoutTimer.mainLabel', { timeLeftLabel: timeLeftLabel })}</div>
        <DirectionDownOutlined />
      </Button>
      <div className="logout-timer-menu" hidden={!isMenuVisible}>
        <Menu
          items={[
            {
              label: (
                <Button id="logout-timer-default-btn" type="text" ref={linkResetTimerRef[0]} onClick={resetTimerIdle}>
                  {t('cockpit.logoutTimer.btnReset')}
                </Button>
              ),

              key: 'menu-item-1',
            },
          ]}
          className="user-menu"
          mode="vertical"
          onClick={() => {
            setIsMenuVisible(false);
          }}
        />
      </div>
      <ModalWrapper
        open={isModalVisible}
        onCancel={() => setIsModalVisible(false)}
        maskClosable={false}
        footer={[
          <Button
            id="timer-default-button"
            key="timer-default-button"
            type="primary"
            onClick={() => {
              resetTimerIdle();
              setIsModalVisible(false);
            }}
          >
            {t('cockpit.logoutTimer.modal.btnReset')}
          </Button>,
          <Button
            id="timer-logout-button"
            key="timer-logout-button"
            type="default"
            onClick={() => {
              dispatch(clearUser());
              logoutAction(keycloak);
            }}
          >
            {t('cockpit.logoutTimer.modal.btnLogout')}
          </Button>,
        ]}
      >
        <p>
          <strong>{t('cockpit.logoutTimer.modal.title', { timeLeftLabel: timeLeftLabel })}</strong> <br />
          {t('cockpit.logoutTimer.modal.text')}
        </p>
      </ModalWrapper>
    </div>
  );
}
