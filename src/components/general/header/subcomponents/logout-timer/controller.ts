// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { differenceInMilliseconds, format } from 'date-fns';
import { Observable, Subject } from 'rxjs';

export const LOGOUT_LAST_MINUTES = 5 * 60 * 1000; // 5 minutes
export class LoginTimeoutController {
  // Calculate time what is left till automatic logout by timeout
  public getLeftTime = (loggenInTime: string, idleTime: number) => {
    const nowISO = new Date().toISOString();
    const timeLeft = idleTime - differenceInMilliseconds(new Date(nowISO), new Date(loggenInTime));
    return timeLeft > 0 ? timeLeft : 0;
  };

  public formatTime = (time?: number) => {
    if (!time) {
      return '';
    }
    // If time is more than 5 minutes show only minutes
    // If less - show also seconds
    const timeFormat = time >= LOGOUT_LAST_MINUTES ? 'm' : 'm:ss';
    return format(time, timeFormat);
  };

  // Emmiting status if user was logged out by timeout
  private subjectLogout = new Subject();
  private statusLogoutByTimeout = false;
  public setStatusLogoutByTimeout(status: boolean): void {
    this.statusLogoutByTimeout = status;
    this.subjectLogout.next(this.statusLogoutByTimeout);
  }
  public subscribeStatusLogoutByTimeout(): Observable<boolean> {
    return this.subjectLogout.asObservable() as Observable<boolean>;
  }

  public saveLastActivityTimeToLocalStorage = (value: string): void => {
    global.window.localStorage.setItem('logoutTimer', value);
  };

  public getLastActivityTimeFromLocalStorage(): string {
    return global.window.localStorage.getItem('logoutTimer') || '';
  }
}
