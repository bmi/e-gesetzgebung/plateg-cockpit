// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { expect } from 'chai';

import { LoginTimeoutController } from './controller';

const loginTimeoutController = new LoginTimeoutController();

describe('Test for LoginTimeoutController', () => {
  it('Emmiting status if user was logged out by timeout', () => {
    let status = false;
    loginTimeoutController.setStatusLogoutByTimeout(true);
    expect(() =>
      loginTimeoutController.subscribeStatusLogoutByTimeout().subscribe({
        next: (data) => {
          status = data;
        },
        complete: () => {
          expect(status).to.eql(true);
        },
      }),
    ).to.not.throw();
  });
  it('Format Time without params', () => {
    const time = loginTimeoutController.formatTime();
    expect(time).to.eql('');
  });
  it('Format Time more 5 minutes', () => {
    const time = loginTimeoutController.formatTime(6 * 60 * 1000);
    expect(time).to.eql('6');
  });
  it('Format Time less 5 minutes', () => {
    const time = loginTimeoutController.formatTime(4 * 60 * 1000);
    expect(time).to.eql('4:00');
  });

  it('getLeftTime if already past', () => {
    const now6MinAgo = new Date(Date.now() - 6 * 60 * 1000).toISOString();
    const time = loginTimeoutController.getLeftTime(now6MinAgo, 5 * 60 * 1000);
    expect(time).to.eql(0);
  });

  it('Set last activity time', () => {
    loginTimeoutController.saveLastActivityTimeToLocalStorage('time');
    const draftCollectionString: string = global.window.localStorage.getItem('logoutTimer') || '';
    expect(draftCollectionString).to.eql('time');
  });

  it('Get last activity time', () => {
    loginTimeoutController.saveLastActivityTimeToLocalStorage('New time');
    const newTime: string = loginTimeoutController.getLastActivityTimeFromLocalStorage();
    expect(newTime).to.eql('New time');
  });

  it('Get last activity time as empty string', () => {
    global.window.localStorage.clear();
    const newTime: string = loginTimeoutController.getLastActivityTimeFromLocalStorage();
    expect(newTime).to.eql('');
  });
});
