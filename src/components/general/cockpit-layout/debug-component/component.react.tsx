// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Button, Col, Divider, Form, Input, Row } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import React from 'react';
import { AjaxError } from 'rxjs/ajax';
import { Observable } from 'rxjs/internal/Observable';

import { DataControllerApi, DummyDataOverwriteDTO } from '@plateg/rest-api';
import {
  Constants,
  displayMessage,
  ErrorController,
  GeneralFormWrapper,
  GlobalDI,
  LoadingStatusController,
} from '@plateg/theme';
import { useAppSelector } from '@plateg/theme/src/components/store';

interface DeleteDummyDataForm {
  id: string;
}

export function DebugComponent(): React.ReactElement {
  const dataControllerApi = GlobalDI.getOrRegister('dataControllerApi', () => new DataControllerApi());
  const loadingStatusController = GlobalDI.get<LoadingStatusController>('loadingStatusController');
  const errorCtrl = GlobalDI.getOrRegister('errorController', () => new ErrorController());
  const appStore = { user: useAppSelector((state) => state.user) };
  const [generateForm] = Form.useForm();
  const [deleteForm] = Form.useForm();
  const [loading, setLoading] = React.useState(false);

  const getApiCallFunction = (
    kurzbezeichnung: string | undefined,
    params?: DummyDataOverwriteDTO,
  ): Observable<boolean> => {
    switch (kurzbezeichnung) {
      case 'BR':
        return dataControllerApi.generateBundesratData();
      case 'BT':
        return dataControllerApi.generateBundestagData({ dummyDataOverwriteDTO: params });
      default:
        throw new Error('Invalid kurzbezeichnung');
    }
  };

  const generateDummyDataBtnClick = (params?: DummyDataOverwriteDTO | undefined): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      loadingStatusController.setLoadingStatus(true);
      try {
        const kurzbezeichnung = appStore.user.user?.dto.ressort?.kurzbezeichnung ?? '';
        const apiCallFunction = getApiCallFunction(kurzbezeichnung, params);
        apiCallFunction.subscribe({
          next: () => {
            loadingStatusController.setLoadingStatus(false);
            displayMessage('Dummy Data has been generated successfully', 'success');
            resolve(); // Resolve the Promise when the function is completed successfully
          },
          error: (error: AjaxError) => {
            loadingStatusController.setLoadingStatus(false);
            errorCtrl.displayErrorMsg(error, 'Error on generating Dummy Data');
            console.log(error);
            reject(error); // Reject the Promise if there is an error
          },
        });
      } catch (error) {
        displayMessage('Error on generating Dummy Data', 'error');
        console.log(error);
        reject(error); // Reject the Promise if there is an error
      } finally {
        loadingStatusController.setLoadingStatus(false);
      }
    });
  };

  const deleteDummyDataBtnClick = (values: DeleteDummyDataForm) => {
    loadingStatusController.setLoadingStatus(true);
    try {
      dataControllerApi.deleteRegelungsvorhaben1({ id: values.id }).subscribe({
        next: () => {
          loadingStatusController.setLoadingStatus(false);
          displayMessage(`Dummy Data ${values.id} has been deleted successfully`, 'success');
        },
        error: (error: AjaxError) => {
          loadingStatusController.setLoadingStatus(false);
          errorCtrl.displayErrorMsg(error, 'Error on deleting Dummy Data');
          console.log(error);
        },
      });
    } catch (error) {
      displayMessage('Error on deleting Dummy Data', 'error');
      console.log(error);
    } finally {
      loadingStatusController.setLoadingStatus(false);
    }
  };

  return (
    <>
      {appStore.user.user?.dto.ressort?.kurzbezeichnung === 'BT' ? (
        <>
          <Row>
            <Col xs={{ span: 7, offset: 1 }}>
              <GeneralFormWrapper
                onFinishFailed={(errorInfo) => {
                  (generateForm.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function })?.focus();
                }}
                form={generateForm}
                id={`dummy-data-generate`}
                name="dummy-data-generate"
                className="dummy-data-generate-form"
                layout="vertical"
                onFinish={(values: DummyDataOverwriteDTO) => {
                  setLoading(true);
                  generateDummyDataBtnClick(values)
                    .then(() => {
                      setLoading(false);
                    })
                    .catch(() => {
                      setLoading(false);
                    });
                }}
              >
                <Form.Item
                  name="titel"
                  label={<span>Kurzbezeichung/Titel</span>}
                  rules={[
                    {
                      max: Constants.TEXT_BOX_LENGTH,
                      message: `Die maximal zulässige Anzahl von ${Constants.TEXT_BOX_LENGTH} Zeichen wurde überschritten.`,
                    },
                  ]}
                >
                  <Input type="text" showCount />
                </Form.Item>
                <Form.Item
                  name="langtitel"
                  label={<span>Bezeichnung/Langtitel</span>}
                  rules={[
                    {
                      max: 5000,
                      message: `Die maximal zulässige Anzahl von 5000 Zeichen wurde überschritten.`,
                    },
                  ]}
                >
                  <TextArea showCount />
                </Form.Item>
                <Button type="primary" htmlType="submit" disabled={loading}>
                  Generate Dummy data
                </Button>
              </GeneralFormWrapper>
              <Divider />
              <GeneralFormWrapper
                onFinishFailed={(errorInfo) => {
                  (deleteForm.getFieldInstance(errorInfo.errorFields[0].name) as { focus: Function })?.focus();
                }}
                form={deleteForm}
                id={`dummy-data-delete`}
                name="dummy-data-delete"
                className="dummy-data-delete-form"
                layout="vertical"
                onFinish={(values) => {
                  deleteDummyDataBtnClick(values);
                }}
              >
                <Form.Item
                  name="id"
                  label={<span>ID</span>}
                  rules={[
                    {
                      max: Constants.TEXT_BOX_LENGTH,
                      message: `Die maximal zulässige Anzahl von ${Constants.TEXT_BOX_LENGTH} Zeichen wurde überschritten.`,
                    },
                  ]}
                >
                  <Input type="text" placeholder="Regelungsvorhaben ID" />
                </Form.Item>
                <Button type="primary" htmlType="submit">
                  Delete Dummy data
                </Button>
              </GeneralFormWrapper>
            </Col>
          </Row>
        </>
      ) : (
        <Button
          type="primary"
          onClick={() => {
            setLoading(true);
            generateDummyDataBtnClick()
              .then(() => {
                setLoading(false);
              })
              .catch((error) => {
                setLoading(false);
              });
          }}
          disabled={loading}
        >
          Generate Dummy data
        </Button>
      )}
    </>
  );
}
