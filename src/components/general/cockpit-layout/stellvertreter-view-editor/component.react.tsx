// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
export function StellvertreterViewEditorComponent(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <>
      <Title level={1}>{t('cockpit.stellvertretung.editorTitle')}</Title>
      <p>{t('cockpit.stellvertretung.editorContent')}</p>
    </>
  );
}
