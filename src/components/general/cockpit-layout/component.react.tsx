// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './cockpit.less';

import { Layout } from 'antd';
import React, { Suspense, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, Switch, useHistory, useLocation, useParams } from 'react-router-dom';
import { forkJoin, Subscription } from 'rxjs';

import { GlobalDI } from '@plateg/theme';
import { useAppDispatch, useAppSelector } from '@plateg/theme/src/components/store';
import { setNeuigkeiten } from '@plateg/theme/src/components/store/slices/neuigkeitenSlice';
import { setSetting } from '@plateg/theme/src/components/store/slices/settingSlice';

import { DashboardController } from '../../cockpit/dashboard/controller.react';
import { GeneralErrorPage } from '../../cockpit/general-error-page/component.react';
import { Page } from '../../cockpit/page/component.react';
import { SettingsController } from '../../cockpit/settings-component/controller';
import { DebugComponent } from './debug-component/component.react';

export interface RoutingItem {
  id: number;
  label?: string;
  hint?: string;
  path: string;
  component: React.ReactElement;
  moduleName?: string;
  fullscreen?: boolean;
  nowrap?: boolean;
}

interface CockpitLayoutProps {
  routingItems: RoutingItem[];
  footer: React.ReactElement;
  header: React.ReactElement;
  mainTitle: string;
}
export function CockpitLayout(props: CockpitLayoutProps): React.ReactElement {
  const { t } = useTranslation();
  const [isStellvertreter, setIsStellvertreter] = useState<boolean>(false);
  const dashboardController = GlobalDI.getOrRegister('dashboardController', () => new DashboardController());
  const settingsController = GlobalDI.getOrRegister('settingsController', () => new SettingsController());
  const params = useParams<{ bereich: string }>();
  const selectedItemId = props.routingItems.filter((item) => item.path.endsWith(params.bereich)).shift()?.id;
  const dispatch = useAppDispatch();
  const appStore = {
    user: useAppSelector((state) => state.user),
    neuigkeiten: useAppSelector((state) => state.neuigkeiten),
    appSetting: useAppSelector((state) => state.appSetting),
  };
  useEffect(() => {
    document.getElementById('logo-link')?.focus();
    document.getElementById('logo-link')?.blur();
  }, []);

  useEffect(() => {
    let sub: Subscription;
    if (appStore.user.user?.base.id != null) {
      setIsStellvertreter(appStore.user.user.dto.stellvertreter || false);
      sub = forkJoin({
        neuigkeiten: dashboardController.getNeuigkeitenCall(0),
        setting: settingsController.getSettingsCall(),
      }).subscribe({
        next: (data) => {
          data.neuigkeiten.dtos.sort((neuigkeit1, neuigkeit2) =>
            dashboardController.dateSort(neuigkeit1.erstelltAm, neuigkeit2.erstelltAm),
          );
          if (appStore.user.user && !appStore.neuigkeiten.neuigkeitenData.length) {
            dispatch(
              setNeuigkeiten({
                neuigkeitenData: data.neuigkeiten.dtos,
                neuigkeitenHomeData: data.neuigkeiten.dtos,
                neuigkeitenInsgesamt: data.neuigkeiten.neuigkeitenInsgesamt,
                ungeleseneNeuigkeiten: appStore.user.user.dto.ungeleseneNeuigkeiten,
              }),
            );
          }
          dispatch(setSetting(data.setting));
        },
      });
    }
    return () => {
      sub?.unsubscribe();
    };
  }, [appStore.user]);
  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    if (location.pathname.includes('&state=') || location.pathname.includes('&error=')) {
      history.replace(location.pathname.split('&')[0]);
    }
  }, [location.pathname]);

  return (
    <Layout
      id="cockpit-layout"
      style={{ minHeight: '100vh' }}
      className={`${isStellvertreter ? 'stellvertreter-active' : ''} `}
    >
      <div className="overflow-class">{React.cloneElement(props.header, { selectedItemId: selectedItemId })}</div>

      <Layout style={{ marginTop: 64 }} className="ant-layout-has-sider">
        <Layout className="ant-layout-content">
          <Suspense>
            <Switch>
              {appStore.appSetting.appSetting?.createDummyData &&
                (appStore.user.user?.dto.ressort?.kurzbezeichnung == 'BT' ||
                  appStore.user.user?.dto.ressort?.kurzbezeichnung == 'BR') && (
                  <Route path={'/cockpit/debug'}>
                    <div id="main-area" tabIndex={0} className={`main-area-holder content-area`}>
                      <Page title={'Debug'}>
                        <DebugComponent />
                      </Page>
                    </div>
                  </Route>
                )}
              {props.routingItems.map((item) => {
                if (isStellvertreter && item.path === '/einstellungen') {
                  item.path = '/cockpit';
                }
                const isFullscreen = item.fullscreen ? 'fullscreen' : '';
                const contentAreaName = item.moduleName ? `content-area-${item.moduleName}` : '';
                const pageTitle = item.label ? `${t(item.label)} - ${props.mainTitle}` : props.mainTitle;
                return (
                  <Route key={item.id} path={item.path}>
                    <div
                      id="main-area"
                      tabIndex={0}
                      className={`main-area-holder content-area ${contentAreaName} ${isFullscreen}`}
                    >
                      <Page title={pageTitle}>{item.component}</Page>
                    </div>
                  </Route>
                );
              })}
              <Route exact path="/fehlerseite">
                <div id="main-area" className={'main-area-holder content-area'}>
                  <GeneralErrorPage />
                </div>
              </Route>
              <Route render={() => <Redirect to="/cockpit" />} />
            </Switch>
            {params.bereich !== 'editor' && props.footer}
          </Suspense>
        </Layout>
      </Layout>
    </Layout>
  );
}
