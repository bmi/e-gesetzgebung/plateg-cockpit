// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Layout } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useRouteMatch } from 'react-router-dom';

import { BreadcrumbComponent, GlobalDI, HeaderComponent, HeaderController } from '@plateg/theme';

const { Header, Content } = Layout;

export function StaticPageWrapper(props: { children?: React.ReactElement }): React.ReactElement {
  const { t } = useTranslation();
  const headerController = GlobalDI.getOrRegister('headerController', () => new HeaderController());
  const routeMatcherVorbereitung = useRouteMatch<{ pageName: string; secondLevel?: string }>(
    '/:pageName/:secondLevel?',
  );

  useEffect(() => {
    const pageName = routeMatcherVorbereitung?.params?.pageName as string;
    const secondLevel = routeMatcherVorbereitung?.params?.secondLevel as string;
    const breadcrumbItems = [];
    const lastItemKey = secondLevel && secondLevel !== ':sectionRelease' ? secondLevel : pageName;

    if (secondLevel && secondLevel !== ':sectionRelease') {
      breadcrumbItems.push(
        <Link id={`static-page-${pageName}`} key={`static-page-${pageName}`} to={`/${pageName}`}>
          {t(`cockpit.staticPages.${pageName}`)}
        </Link>,
      );
    }
    breadcrumbItems.push(<span key={`static-page-${lastItemKey}`}>{t(`cockpit.staticPages.${lastItemKey}`)}</span>);

    headerController.setHeaderProps({
      headerLeft: [<BreadcrumbComponent key="breadcrumb" items={breadcrumbItems} />],
      headerCenter: [],
      headerRight: [],
      headerLast: [],
    });
  }, [routeMatcherVorbereitung]);
  return (
    <Layout style={{ height: '100%', display: 'flex' }} className="site-layout-background">
      <Header className="header-component site-layout-background">
        <HeaderComponent ctrl={headerController} />
      </Header>
      <Content style={{ padding: '24px 0' }} className="main-content-area site-layout-background">
        {props.children}
      </Content>
    </Layout>
  );
}
