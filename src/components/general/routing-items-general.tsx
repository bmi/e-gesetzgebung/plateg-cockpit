// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import React, { lazy } from 'react';

import { ArbeitshilfenbibliothekMenuIcon, VerfahrenassistentMenuIcon } from '@plateg/theme';

import { RoutingItem } from '../cockpit/component.react';

const EVIRApp = lazy(() =>
  import('@plateg/evir').then((module) => {
    return { default: module.EVIRApp };
  }),
);
const WorkingAidsApp = lazy(() =>
  import('@plateg/working-aids').then((module) => {
    return { default: module.WorkingAidsApp };
  }),
);
const Impressum = lazy(() =>
  import('../cockpit/footer/impressum/component.react').then((module) => {
    return { default: module.Impressum };
  }),
);
const Barrierefreiheit = lazy(() =>
  import('./footer/barrierefreiheit/component.react').then((module) => {
    return { default: module.Barrierefreiheit };
  }),
);

const BarrierefreiheitHandbuch = lazy(() =>
  import('./footer/barrierefreiheit/handbuch/component.react').then((module) => {
    return { default: module.BarrierefreiheitHandbuch };
  }),
);
const LeichteSprache = lazy(() =>
  import('./footer/leichte-sprache/component.react').then((module) => {
    return { default: module.LeichteSprache };
  }),
);
const UeberDieMassnahme = lazy(() =>
  import('../cockpit-light/ueber-die-massnahme/component.react').then((module) => {
    return { default: module.UeberDieMassnahmeLight };
  }),
);
const LegalDocLight = lazy(() =>
  import('../cockpit-light/legal-doc/component.react').then((module) => {
    return { default: module.LegalDocLight };
  }),
);
export const routingItemsGeneral: RoutingItem[] = [
  {
    id: 2,
    label: 'cockpit.menuItems.evir.label' /* 'eViR' */,
    menuItem: 'cockpit.menuItems.evir.menuItem',
    hint: 'cockpit.menuItems.evir.hint' /* 'Verfahrensassistent im Rechtsetzungsprozess' */,
    path: '/evir',
    component: <EVIRApp />,
    moduleName: 'evir',
    fullscreen: true,
    icon: <VerfahrenassistentMenuIcon />,
  },
  {
    id: 3,
    label: 'cockpit.menuItems.hilfen.label' /* 'Hilfen' */,
    menuItem: 'cockpit.menuItems.hilfen.menuItem',
    hint: 'cockpit.menuItems.hilfen.hint' /* 'Arbeitshilfenbibliothek' */,
    path: '/hilfen',
    component: <WorkingAidsApp />,
    moduleName: 'hilfe',
    fullscreen: true,
    icon: <ArbeitshilfenbibliothekMenuIcon />,
  },
  {
    id: 11,
    path: '/impressum',
    component: <Impressum />,
    fullscreen: true,
  },
  {
    id: 21,
    path: '/leichteSprache',
    component: <LeichteSprache />,
    fullscreen: true,
  },
  {
    id: 22,
    path: '/barrierefreiheit/handbuch',
    component: <BarrierefreiheitHandbuch />,
    fullscreen: true,
  },
  {
    // 'Über das Projekt'
    id: 104,
    path: '/ueberDasProjekt',
    label: 'cockpit.login.ueberDasProjektButton',
    menuItem: 'cockpit.login.ueberDasProjektButton',
    component: <UeberDieMassnahme />,
    fullscreen: true,
  },
  {
    // 'Barrierefreiheit in der E‑Gesetzgebung'
    id: 101,
    path: '/barrierefreiheit',
    label: 'cockpit.login.barrierefreiheitBtn',
    menuItem: 'cockpit.login.barrierefreiheitBtn',
    hint: 'cockpit.login.barrierefreiheitBtn',
    component: <Barrierefreiheit />,
    fullscreen: true,
  },
  { id: 60, path: '/ueberDasProjekt/legaldoc', component: <LegalDocLight />, fullscreen: true },
];
