// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './leichte-sprache.less';

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../static-page-wrapper/component.react';

export function LeichteSprache(): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();

  const textSections = ['section1', 'section2'];

  const displayText = (textArr: string[]) =>
    textArr.map((item) => {
      return (
        <div key={item}>
          {<Title level={2}>{t(`cockpit.footer.leichteSprache.${item}.title`)}</Title>}
          <p
            className="leichte-sprache-par-text"
            dangerouslySetInnerHTML={{ __html: t(`cockpit.footer.leichteSprache.${item}.text`) }}
          ></p>
        </div>
      );
    });

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.leichteSprache.title')}</Title>
          <div className="leichte-sprache-text-section">{displayText(textSections)}</div>
          <hr />
          <div key={'section3'}>
            {<Title level={2}>{t(`cockpit.footer.leichteSprache.section3.title`)}</Title>}
            <p
              className="leichte-sprache-par-text"
              dangerouslySetInnerHTML={{
                __html: t(`cockpit.footer.leichteSprache.section3.text`, {
                  urlNavigationshilfe: history.createHref({ pathname: '/leichteSprache/navigationshilfe' }),
                }),
              }}
            />
          </div>
          <hr />
          <div key={'section4'}>
            {<Title level={2}>{t(`cockpit.footer.leichteSprache.section4.title`)}</Title>}
            <p
              className="leichte-sprache-par-text"
              dangerouslySetInnerHTML={{
                __html: t(`cockpit.footer.leichteSprache.section4.text`, {
                  urlWerkzeuge: history.createHref({ pathname: '/leichteSprache/werkzeuge' }),
                }),
              }}
            />
          </div>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
