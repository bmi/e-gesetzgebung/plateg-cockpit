// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../leichte-sprache.less';

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { UserControllerApi } from '@plateg/rest-api';
import { GlobalDI, StandardContentCol } from '@plateg/theme';

import { LoginController } from '../../../../cockpit/login-component/controller';
import { StaticPageWrapper } from '../../../static-page-wrapper/component.react';

export function LeichteSpracheNavGeneral(props: Readonly<{ light: boolean }>): React.ReactElement {
  const { t } = useTranslation();
  const history = useHistory();
  const loginCtrl = GlobalDI.getOrRegister('cockpitLoginController', () => new LoginController());
  const userController: UserControllerApi = GlobalDI.get('userController');

  const textSections = [
    'startseite',
    'leichteSprache',
    'barrierefrei',
    'registrieren',
    'anmelden',
    'zeit',
    'neuigkeiten',
    'profil',
    'hauptmenue',
    'massnahme',
    'kontakt',
    'faq',
    'arbeit',
    'fusszeile',
  ];
  const textSectionsLight = [
    'startseite',
    'leichteSprache',
    'barrierefrei',
    'hauptmenue',
    'seite',
    'massnahmeLight',
    'newsletter',
    'kontakt',
    'arbeit',
    'fusszeileLight',
  ];

  const displayText = (textArr: string[]) =>
    textArr.map((item) => {
      return (
        <div key={item}>
          {<Title level={2}>{t(`cockpit.footer.leichteSprache.navigationshilfe.${item}.title`)}</Title>}
          <p
            className="leichte-sprache-par-text"
            dangerouslySetInnerHTML={{
              __html: t(`cockpit.footer.leichteSprache.navigationshilfe.${item}.text`, {
                eGesetzSupportEmail: t('cockpit.eGesetzSupportEmail'),
                urlFaq: history.createHref({ pathname: '/faq' }),
                urlLeichteSprache: history.createHref({ pathname: '/leichteSprache' }),
                urlLogin: loginCtrl.getLoginUrl(),
                urlRegister: `${userController.configuration.basePath}/user/register`,
                urlUeberDieMassnahme: history.createHref({ pathname: '/ueberDasProjekt' }),
                urlUeberDieSeite: history.createHref({ pathname: '/vorstellung' }),
                urlWerkzeuge: history.createHref({ pathname: '/leichteSprache/werkzeuge' }),
              }),
            }}
          />
        </div>
      );
    });

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.leichteSprache.navigationshilfe.title')}</Title>
          <div className="leichte-sprache-text-section">
            <p
              className="leichte-sprache-par-text"
              dangerouslySetInnerHTML={{
                __html: t(`cockpit.footer.leichteSprache.navigationshilfe.text`),
              }}
            />
            {!props.light ? displayText(textSections) : displayText(textSectionsLight)}
          </div>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
