// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../leichte-sprache.less';

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../../static-page-wrapper/component.react';

export function LeichteSpracheWerkzeugeGeneral(props: Readonly<{ light: boolean }>): React.ReactElement {
  const { t } = useTranslation();

  const sectionNamesApp = ['rv', 'editor', 'zeit', 'hra', 'evor', 'egfa', 'bib', 'pkp', 'evir'];
  const sectionNamesLight = ['evor', 'zeit', 'enap', 'evir', 'bib'];
  const sectionNames = props.light ? sectionNamesLight : sectionNamesApp;

  const displayRows = sectionNames.map((item) => {
    return (
      <tr key={item}>
        <td>{t(`cockpit.footer.leichteSprache.table.${item}.nameWerkzeug`)}</td>
        <td
          className="leichte-sprache-table-erklaerung"
          dangerouslySetInnerHTML={{ __html: t(`cockpit.footer.leichteSprache.table.${item}.erklaerung`) }}
        />
      </tr>
    );
  });

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.leichteSprache.table.title')}</Title>
          <table className="leichte-sprache-table">
            <thead>
              <tr>
                <th dangerouslySetInnerHTML={{ __html: t(`cockpit.footer.leichteSprache.table.header.name`) }}></th>
                <th>{t(`cockpit.footer.leichteSprache.table.header.descr`)}</th>
              </tr>
            </thead>
            <tbody>{displayRows}</tbody>
          </table>
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
