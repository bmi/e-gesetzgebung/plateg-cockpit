// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './footer.less';

import { Button, Divider, Image, Layout } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useRouteMatch } from 'react-router-dom';

import { EmailOutlined, ImageModule } from '@plateg/theme';

interface FooterProps {
  isFull?: boolean;
}

export function Footer(props: FooterProps): React.ReactElement {
  const { t } = useTranslation();
  const routeMatcherVorbereitung = useRouteMatch<{ pageName: string }>('/*');
  const [wrapperWidth, setWrapperWidth] = useState<number | '100%'>('100%');
  const [holderWidth, setHolderWidth] = useState<number | '100%'>('100%');
  const [scrollToTop, setScrollToTop] = useState(false);

  const className = 'top-of-page-button';
  const type = 'text';

  const scrollBackToTop = () => {
    setScrollToTop(true);
  };

  useEffect(() => {
    if (scrollToTop) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      setScrollToTop(false);
    }
  }, [scrollToTop]);

  useEffect(() => {
    const updateFooterWidth = () => {
      setWrapperWidth(document.body.scrollWidth);
      setHolderWidth(document.body.clientWidth);
    };
    window.addEventListener('resize', updateFooterWidth);
    window.addEventListener('scroll', updateFooterWidth);
    return () => {
      window.removeEventListener('resize', updateFooterWidth);
      window.removeEventListener('scroll', updateFooterWidth);
    };
  }, []);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setWrapperWidth('100%');
      setHolderWidth('100%');
    }, 100);
    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [routeMatcherVorbereitung?.url]);
  return (
    <div style={{ width: wrapperWidth }}>
      <div className="footer-holder" style={{ width: holderWidth }}>
        <Layout.Footer className="cockpit-footer" id="cockpitFooter">
          <div className="button-container">
            <Button id="2" className={className} type={type} onClick={scrollBackToTop}>
              {t('cockpit.footer.backToTop')}
            </Button>
          </div>
          <Divider />
          <div className="feedback-component">
            <div className="info-wrapper">
              <div className="text-holder">
                <strong className="title">{t('cockpit.welcome.feedback.title')}</strong>
                <p
                  className="ant-typography p-no-style"
                  dangerouslySetInnerHTML={{
                    __html: t('cockpit.welcome.feedback.text', {
                      interpolation: { escapeValue: false },
                    }),
                  }}
                />
                <a
                  id="feedback-link"
                  href="mailto:support.egesetzgebung@bmi.bund.de?subject=Feedback%20zur%20E-Gesetzgebung"
                >
                  <EmailOutlined />
                  <span
                    dangerouslySetInnerHTML={{
                      __html: t('cockpit.welcome.feedback.link', {
                        interpolation: { escapeValue: false },
                      }),
                    }}
                  />
                </a>
              </div>
              <div className="big-img">
                <Image
                  loading="lazy"
                  height={92}
                  preview={false}
                  alt="BMI NMO Doppellogo"
                  src={(require(`../../../media/welcome-page/BMI-NMO_Doppellogo.svg`) as ImageModule).default}
                />
              </div>
            </div>
          </div>
          <div className="bottom-wrapper">
            <p>{t('cockpit.footer.copyright')}</p>
            <div className="link-list">
              <ul>
                {props.isFull && (
                  <li>
                    <Link id="faq-link" to="/faq">
                      {t('cockpit.footer.faq.shortTitle')}
                    </Link>
                  </li>
                )}
                <li>
                  <Link id="impressum-link" to="/impressum">
                    {t('cockpit.footer.impressum.title')}
                  </Link>
                </li>
                <li>
                  <Link id="barrierefreiheit-link" to="/barrierefreiheit/erklaerung">
                    {t('cockpit.footer.barrierefreiheit.erklaerung.title')}
                  </Link>
                </li>
                <li>
                  <Link id="leichte-sprache-link" to="/leichteSprache">
                    {t('cockpit.footer.leichteSprache.title')}
                  </Link>
                </li>
                <li>
                  <Link id="datenschutz-link" to="/datenschutz">
                    {t('cockpit.footer.datenschutz.title')}
                  </Link>
                </li>
                {props.isFull && (
                  <li>
                    <Link id="vs-nfd-link" to="/vsnfd">
                      {t('cockpit.footer.vsNfd.title')}
                    </Link>
                  </li>
                )}
                {props.isFull && (
                  <li>
                    <Link id="realease-notes-link" to="/releaseNotes">
                      {t('cockpit.footer.releaseNotes.title')}
                    </Link>
                  </li>
                )}
                <li className="gray-footer-element">
                  <Link id="versionen" to="/versionen">
                    {t('cockpit.footer.versionen.title')}
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </Layout.Footer>
      </div>
    </div>
  );
}
