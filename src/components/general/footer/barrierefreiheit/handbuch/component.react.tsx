// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './handbuch.less';

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../../static-page-wrapper/component.react';

export function BarrierefreiheitHandbuch(): React.ReactElement {
  const { t } = useTranslation();

  const createTable = (table: string) => {
    const headers = t('cockpit.footer.barrierefreiheit.handbuch.tableHeader').split(';');
    return (
      <table className="handbuch-table" key={table}>
        <thead>
          <tr>
            {headers.map((headerItem: string, index) => {
              return (
                <th key={headerItem} style={index === 0 ? { width: '30%' } : {}}>
                  {headerItem}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          {(t(`cockpit.footer.barrierefreiheit.handbuch.${table}`, { returnObjects: true }) as []).map(
            (rowItem: string) => {
              const row = rowItem.split(';');
              return (
                <tr key={rowItem}>
                  {row.map((cellItem: string) => {
                    return (
                      <td
                        key={cellItem}
                        dangerouslySetInnerHTML={{
                          __html: cellItem,
                        }}
                      />
                    );
                  })}
                </tr>
              );
            },
          )}
        </tbody>
      </table>
    );
  };

  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.barrierefreiheit.handbuch.title')}</Title>
          <p>{t('cockpit.footer.barrierefreiheit.handbuch.description')}</p>
          <Title level={2}>{t('cockpit.footer.barrierefreiheit.handbuch.subtitle1')}</Title>
          <p>{t('cockpit.footer.barrierefreiheit.handbuch.section1')}</p>
          {createTable('table1')}
          <u>{t('cockpit.footer.barrierefreiheit.handbuch.table2title')}</u>
          {createTable('table2')}
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
