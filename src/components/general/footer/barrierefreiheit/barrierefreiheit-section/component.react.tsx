// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import '../barrierefreiheit.less';

import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import { EmailOutlined } from '@plateg/theme';
import { RightDirectionArrow } from '@plateg/theme/src/components/icons';

interface BarrierefreiheitSectionProps {
  section: number;
  type: 'link' | 'email';
  url: string;
}

export function BarrierefreiheitSection(props: BarrierefreiheitSectionProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <div key={`section${props.section}`}>
      <Title level={2}>{t(`cockpit.footer.barrierefreiheit.section${props.section}.title`)}</Title>
      <p
        dangerouslySetInnerHTML={{
          __html: t(`cockpit.footer.barrierefreiheit.section${props.section}.text`),
        }}
      />
      {props.type === 'link' ? (
        <Link className="barrierefreiheit-link icon-right" to={props.url}>
          {t(`cockpit.footer.barrierefreiheit.section${props.section}.link`)}
          <RightDirectionArrow />
        </Link>
      ) : (
        <a className="barrierefreiheit-link icon-left" href={`mailto:${props.url}`}>
          <EmailOutlined />
          {t(`cockpit.footer.barrierefreiheit.section${props.section}.link`)}
        </a>
      )}
    </div>
  );
}
