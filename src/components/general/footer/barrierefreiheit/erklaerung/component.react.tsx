// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../../static-page-wrapper/component.react';

export function BarrierefreiheitErklaerung(props: Readonly<{ light: boolean }>): React.ReactElement {
  const { t } = useTranslation();
  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.barrierefreiheit.erklaerung.title')}</Title>
          <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element1')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.barrierefreiheit.erklaerung.element1a', {
                interpolation: { escapeValue: false },
              }),
            }}
          />
          <Title level={2}>{t('cockpit.footer.barrierefreiheit.erklaerung.element2')}</Title>
          {props.light ? (
            <p>
              {`${t('cockpit.footer.barrierefreiheit.erklaerung.element2aLight')} ${t(
                'cockpit.footer.barrierefreiheit.erklaerung.element2c',
              )}`}
            </p>
          ) : (
            <>
              <p
                dangerouslySetInnerHTML={{
                  __html: `${t('cockpit.footer.barrierefreiheit.erklaerung.element2a')} ${t(
                    'cockpit.footer.barrierefreiheit.erklaerung.element2b',
                    {
                      interpolation: { escapeValue: false },
                      pkpBarrierefreiheit: t('cockpit.footer.barrierefreiheit.erklaerung.pkpBarrierefreiheit'),
                    },
                  )}`,
                }}
              />
              <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element2c')}</p>
            </>
          )}
          <Title level={2}>{t('cockpit.footer.barrierefreiheit.erklaerung.element3')}</Title>
          {props.light ? (
            <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element3aLight')}</p>
          ) : (
            <>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('cockpit.footer.barrierefreiheit.erklaerung.element3a', {
                    interpolation: { escapeValue: false },
                  }),
                }}
              />
              <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element3b')}</p>
              <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element3c')}</p>
            </>
          )}
          <Title level={2}>{t('cockpit.footer.barrierefreiheit.erklaerung.element4')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.barrierefreiheit.erklaerung.element4a', {
                interpolation: { escapeValue: false },
                creationDate: '05.01.2024',
                validationDate: props.light ? '26.08.2024' : '19.09.2024',
              }),
            }}
          />
          <Title level={2}>{t('cockpit.footer.barrierefreiheit.erklaerung.element5')}</Title>
          {t('cockpit.footer.barrierefreiheit.erklaerung.element5a')}
          <Title level={3}>{t('cockpit.footer.barrierefreiheit.erklaerung.element5b')}</Title>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.barrierefreiheit.erklaerung.element5c', {
                interpolation: { escapeValue: false },
                eGesetzSupportEmail: t('cockpit.eGesetzSupportEmail'),
              }),
            }}
          />
          <Title level={2}>{t('cockpit.footer.barrierefreiheit.erklaerung.element6')}</Title>
          <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element6a')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.barrierefreiheit.erklaerung.element6b', {
                interpolation: { escapeValue: false },
              }),
            }}
          />
          <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element6c')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.barrierefreiheit.erklaerung.element6d', {
                interpolation: { escapeValue: false },
              }),
            }}
          />
          <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element6e')}</p>
          <p>{t('cockpit.footer.barrierefreiheit.erklaerung.element6f')}</p>
          <p
            dangerouslySetInnerHTML={{
              __html: t('cockpit.footer.barrierefreiheit.erklaerung.element6g', {
                interpolation: { escapeValue: false },
              }),
            }}
          />
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
