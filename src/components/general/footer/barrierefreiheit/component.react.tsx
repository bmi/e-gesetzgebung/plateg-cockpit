// Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit
//
// SPDX-License-Identifier: MPL-2.0

import './barrierefreiheit.less';

import { Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { StandardContentCol } from '@plateg/theme';

import { StaticPageWrapper } from '../../static-page-wrapper/component.react';
import { BarrierefreiheitSection } from './barrierefreiheit-section/component.react';

export function Barrierefreiheit(): React.ReactElement {
  const { t } = useTranslation();
  return (
    <StaticPageWrapper>
      <Row>
        <StandardContentCol>
          <Title level={1}>{t('cockpit.footer.barrierefreiheit.title')}</Title>
          <BarrierefreiheitSection section={1} type="link" url="/barrierefreiheit/erklaerung" />
          <hr />
          <BarrierefreiheitSection section={2} type="link" url="/barrierefreiheit/handbuch" />
          <hr />
          <BarrierefreiheitSection section={3} type="link" url="/leichteSprache" />
          <hr />
          <BarrierefreiheitSection section={4} type="email" url={t('cockpit.eGesetzSupportEmail')} />
        </StandardContentCol>
      </Row>
    </StaticPageWrapper>
  );
}
